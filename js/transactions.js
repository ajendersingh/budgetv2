
$(function() {
        var box = $('header');
        var button = $('.menu-button');
        button.on('click', function(){
          box.toggleClass('active');
        });
      });
$(document).ready(function(){
	
	$("#show_nav").click(function(){
				$("#side_nav_ul").show();
				$("#show_nav").hide();
				$("#hide_nav").show();
			})
			$("#hide_nav").click(function(){
				$("#side_nav_ul").hide();
				$("#show_nav").show();
				$("#hide_nav").hide();
			})
			
			
	   $("#is_recurring_transaction").click(function(){
		  
		   
		   if($(this).is(":checked"))
		   {
			   $("#reccuring_parent").show();
			   $('#recurring_period').val('');
		   }
		   else{
			   $("#reccuring_parent").hide();
			   $("#recurring_option").hide();
			   $("#repeat_month_day_div").hide();
			   
			   $(".bimonth_div").hide();
		   }
		   })
		   
		    $('#recurring_period').change(function(){
				var sid = $(this).val();
				$(".bimonth_div").hide(); 
				$("#repeat_month_day_div").hide();
				if(sid >1 && sid !=7){
					$.ajax({
					   type:"get",
					   url:site_url_js_path+"repeat_option/"+sid,  //Please see the note at the end of the post**
					   success:function(res)
					   {       
							if(res)
							{
								var optn = $("#recurring_select").attr("data_select");
								$("#recurring_select").empty();
								$("#recurring_select").append('<option value="">'+optn+'</option>');
								$.each(res,function(key,value){
									$("#recurring_select").append('<option value="'+key+'">'+value+'</option>');
								});
								 
								
								$("#recurring_option").show();
							}
					   }

					});
				}
				else if(sid == 7)
				{
					 
					$(".bimonth_div").show();
					$("#recurring_option").hide();
					$("#recurring_select").empty();
				}
				else{
					$("#recurring_option").hide();
					$("#recurring_select").empty();
				}
			});
			
			$("#trans_type").change(function(){
				var sid = $(this).val();
				
				if(sid == 'EXPENCES')
				{
					
					$("#destination_account_div").hide();
					$("#source_account_div").show();
				}
				if(sid == 'REVENUE')
				{
					$("#destination_account_div").show();
					$("#source_account_div").hide();
				}
				if(sid == 'TRANSFER')
				{
					$("#destination_account_div").show();
					$("#source_account_div").show();
				}
			})
			
			$("#trans_form").submit(function(){
				var trans_type = $("#trans_type").val(); 
				var error = true;
				if(trans_type == 'EXPENCES')
				{
					
					 
					if($("#source_account").val() =='')
					{
						$("#source_account").addClass("error");
						error = false;
						
					}
				}
				if(trans_type == 'REVENUE')
				{
					if($("#destination_account").val() =='')
					{
						$("#destination_account").addClass("error");
						error = false;
						
					}
					 
				}
				if(trans_type == 'TRANSFER')
				{ 
					if($("#destination_account").val() =='' && $("#source_account").val())
					{
						$("#destination_account").addClass("error");
						$("#source_account").addClass("error");
						error = false;
					}
					
				}
				
				if($("#is_recurring_transaction").is(":checked") )
				{
					 if($("#recurring_period").val() =='')
					 {
						$("#recurring_period").addClass("error"); 
						error = false;
						
					 }
					 /******** 7 is bimonthly ******/
					 if( $("#recurring_period").val() > 1  && $("#recurring_period").val() != 7)
					 {
					 
						 if($("#recurring_select").val() == '')
						 {
							$("#recurring_select").addClass("error"); 
							error = false;
						 }
						 else{
							 if($("#recurring_select").val() == 10 && $("#repeat_month_day").val() == '')
								 {
									$("#repeat_month_day").addClass("error"); 
									error = false;
								 }
						 }
						  
					 }
					 
					 if($("#recurring_period").val() == 7)
					 {
						 if($("#first_payment_date").val() == '')
						 {
							$("#first_payment_date").addClass("error"); 
							error = false;
						 }
						 if($("#second_payment_date").val() == '')
						 {
							$("#second_payment_date").addClass("error"); 
							error = false;
						 }
						 
						 
						 
					 }
				}
				return error;
			})
			$('#recurring_select').change(function(){
				var sid = $(this).val();
				 
				if(parseFloat(sid) == 10){
					 
					$("#repeat_month_day").empty();
					$("#repeat_month_day").show();
					var optn = $("#recurring_select").attr("data_select");
					$("#repeat_month_day").append('<option>'+optn+'</option>');
							
					 for(var i=1; i<=28; i++)
					 {
						 $("#repeat_month_day").append('<option value="'+parseFloat(i)+'">'+parseFloat(i)+'</option>');
						if(i == 28)
						{
							$("#repeat_month_day_div").show();
						}
					 }
				
				}
				else{
					$("#repeat_month_day_div").hide();
					$("#repeat_month_day").empty();
				}
			});
		    
		    
	});