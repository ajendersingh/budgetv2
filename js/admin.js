 

$(document).ready(function(){
						   
 
    $('#frm_ebbok').ajaxForm({
							 
      beforeSend:function(){
        $('#success').empty();
      },
      uploadProgress:function(event, position, total, percentComplete)
      {
        $('.progress-bar').text(percentComplete + '%');
        $('.progress-bar').css('width', percentComplete + '%');
      },
      success:function(data)
      {
        if(data.errors)
        {
          $('.progress-bar').text('0%');
          $('.progress-bar').css('width', '0%');
          $('#success').html('<span class="text-danger"><b>'+data.errors+'</b></span>');
        }
        if(data.success)
        {
          $('.progress-bar').text('Uploaded');
          $('.progress-bar').css('width', '100%');
          $('#success').html('<span class="text-success"><b>'+data.success+'</b></span><br /><br />');
          $('#success').append(data.image);
		  var exploded = data.file_name_uploaded.split('.');
		  if(exploded[1]=='jpg' || exploded[1]=='jpeg' || exploded[1]=='png')
		  {
 		  $("#image_file_name_hidden").val(data.file_name_uploaded);
		  }
		  else
		  {
		 $("#pdf_file_name_hidden").val(data.file_name_uploaded);  
		  }
        }
      }
    });
	 
	
 	$('#btn1_upload_img').click(function(e){	
									 
	$('#frm_ebbok').attr('action', ''+site_url_js_path+'admin/file-upload/storecoverimagepdf');	
	$('#frm_ebbok').attr('id', 'frm_submit_all');	
	
	$( "#frm_submit_all" ).submit();
	
	$('#frm_submit_all').attr('id', 'frm_ebbok');	
	$('#frm_ebbok').attr('action', ''+site_url_js_path+'admin/file-upload/upload');	
	 


	});
	
	
	$('#btn2_upload_img').click(function(e){	
									 
	$('#frm_ebbok').attr('action', ''+site_url_js_path+'admin/file-upload/storecoverimagepdf');	
	$('#frm_ebbok').attr('id', 'frm_submit_all');	
	
	$( "#frm_submit_all" ).submit();
	
	$('#frm_submit_all').attr('id', 'frm_ebbok');	
	$('#frm_ebbok').attr('action', ''+site_url_js_path+'admin/file-upload/upload');		
	 


	});
	
	
	
	$('#btn1_upload').click(function(e){		
 	var cats_selected = []; 
	
	$("input:checkbox[name=cat_id]:checked").each(function() { 
	cats_selected.push($(this).val()); 
	});
	
	var tags_selected = [];
	$("input:checkbox[name=tag_id]:checked").each(function() {          
	tags_selected.push($(this).val());
	});
	
	if($('#featured').prop("checked") == true)
	{
	var featured=1;
	}
	else
	{
	var featured=0;	
	}

    e.preventDefault(); // avoid to execute the actual submit of the form.
	
	var book_name_en=$('#book_name_en').val();
	var book_name_fr=$('#book_name_fr').val();
	var book_name_nl=$('#book_name_nl').val();
	var book_name_dk=$('#book_name_dk').val();
	var book_name_at=$('#book_name_at').val();
	var book_name_de=$('#book_name_de').val();
	var book_name_se=$('#book_name_se').val();
	var book_name_no=$('#book_name_no').val();
	var book_name_it=$('#book_name_it').val();
	
	
	var book_description_en=$('#book_description_en').val();
	var book_description_fr=$('#book_description_fr').val();
	var book_description_nl=$('#book_description_nl').val();
	var book_description_dk=$('#book_description_dk').val();
	var book_description_at=$('#book_description_at').val();
	var book_description_de=$('#book_description_de').val();
	var book_description_se=$('#book_description_se').val();
	var book_description_no=$('#book_description_no').val();
	var book_description_it=$('#book_description_it').val();
	
	
	var update_id=$('#update_id').val();
	var cat_id = cats_selected.join(",");
 	
	var tag_id = tags_selected.join(",");
	
 	var pdf_file_name_hidden=$('#pdf_file_name_hidden').val();
	var image_file_name_hidden=$('#image_file_name_hidden').val();
	
	var url = ''+site_url_js_path+'admin/file-upload/store';
 
    $.ajax({
           type: "POST",
           url: url,
           data: ({book_name_en: book_name_en,book_name_fr:book_name_fr,book_name_nl:book_name_nl,book_name_dk: book_name_dk,book_name_at:book_name_at,book_name_de:book_name_de,book_name_se: book_name_se,book_name_no:book_name_no,book_name_it:book_name_it,book_description_en:book_description_en,book_description_fr:book_description_fr,book_description_nl:book_description_nl,book_description_dk:book_description_dk,book_description_at:book_description_at,book_description_de:book_description_de,book_description_se:book_description_se,book_description_no:book_description_no,book_description_it:book_description_it, cat_id: cat_id,featured:featured, tag_id: tag_id, pdf_file_name_hidden: pdf_file_name_hidden,image_file_name_hidden:image_file_name_hidden,"_token": $('#token').val()}), // serializes the form's elements.
           success: function(data) 
           {
			
			
 			   location.href=''+site_url_js_path+'admin/ebooks';
            }
         });


	});
	
	
	
	
	$('#btn1_upload_edit').click(function(e){	
										  
	$("#process_image").css("display", "block");									  
									 
	var cats_selected = []; 
	
	$("input:checkbox[name=cat_id]:checked").each(function() { 
	cats_selected.push($(this).val()); 
	});
	
	var tags_selected = [];
	$("input:checkbox[name=tag_id]:checked").each(function() {                   
	tags_selected.push($(this).val());
	});
	
	if($('#featured').prop("checked") == true)
	{
	var featured=1;
	}
	else
	{
	var featured=0;	
	}
    e.preventDefault(); // avoid to execute the actual submit of the form.
	
	var book_name_en=$('#book_name_en').val();
	var book_name_fr=$('#book_name_fr').val();
	var book_name_nl=$('#book_name_nl').val();
	var book_name_dk=$('#book_name_dk').val();
	var book_name_at=$('#book_name_at').val();
	var book_name_de=$('#book_name_de').val();
	var book_name_se=$('#book_name_se').val();
	var book_name_no=$('#book_name_no').val();
	var book_name_it=$('#book_name_it').val();
	
	
	var book_description_en=$('#book_description_en').val();
	var book_description_fr=$('#book_description_fr').val();
	var book_description_nl=$('#book_description_nl').val();
	var book_description_dk=$('#book_description_dk').val();
	var book_description_at=$('#book_description_at').val();
	var book_description_de=$('#book_description_de').val();
	var book_description_se=$('#book_description_se').val();
	var book_description_no=$('#book_description_no').val();
	var book_description_it=$('#book_description_it').val();
	
	var update_id=$('#update_id').val();
	var cat_id = cats_selected.join(",");
	var tag_id = tags_selected.join(",");
	var pdf_file_name_hidden=$('#pdf_file_name_hidden').val();
	var image_file_name_hidden=$('#image_file_name_hidden').val();
	
    var url = ''+site_url_js_path+'admin/file-upload/update';
 
    $.ajax({
           type: "POST",
           url: url,
           data: ({ update_id:update_id,book_name_en: book_name_en,book_name_fr:book_name_fr,book_name_nl:book_name_nl,book_name_dk: book_name_dk,book_name_at:book_name_at,book_name_de:book_name_de,book_name_se: book_name_se,book_name_no:book_name_no,book_name_it:book_name_it,book_description_en:book_description_en,book_description_fr:book_description_fr,book_description_nl:book_description_nl,book_description_dk:book_description_dk,book_description_at:book_description_at,book_description_de:book_description_de,book_description_se:book_description_se,book_description_no:book_description_no,book_description_it:book_description_it, cat_id: cat_id,featured:featured, tag_id: tag_id, pdf_file_name_hidden: pdf_file_name_hidden,image_file_name_hidden:image_file_name_hidden,"_token": $('#token').val()}), // serializes the form's elements.
           success: function(data) 
           { 
			   $("#process_image").css("display", "none"); 
 			   location.href=''+site_url_js_path+'admin/ebooks';
           }
         });


	});

});