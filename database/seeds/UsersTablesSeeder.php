<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\User;
class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
		
		'name'				=> 'vivek',
		'email'				=> 'vivek@famcominc.com',
		'password'			=> Hash::make('vivek123'),
		'remember_token'	=> Str::random(40),
		]
		);
    }
}
