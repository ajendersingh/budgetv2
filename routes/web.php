<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* set language for site starts here   */
Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale); 
    return redirect()->back();
});

/* set language for site ends here   */

Route::post('ckeditor/image_upload', 'CKEditorController@upload')->name('upload');

Route::get('/', 'MainController@index');

Route::get('admin/file-upload', 'EbookController@create');
Route::post('admin/file-upload/upload', 'EbookController@upload')->name('upload');
Route::post('admin/file-upload/store', 'EbookController@store');
Route::post('admin/file-upload/storecoverimagepdf', 'EbookController@storecoverimagepdf');
Route::post('admin/file-upload/update', 'EbookController@update'); 

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');


Route::get('about', 'CmsController@about');
Route::get('careers', 'CmsController@careers');
Route::get('testimonials', 'CmsController@testimonials');
Route::get('terms', 'CmsController@terms');
Route::get('privacy-policy', 'CmsController@privacy');
Route::get('help', 'CmsController@help');
Route::get('benefits', 'CmsController@benefits');
Route::get('refund-policy', 'CmsController@refund_policy');
Route::get('thank-you', 'CmsController@thank_you');


Route::get('/contact', 'ContactController@index');
Route::post('/contact', 'ContactController@store');


Route::get('/subscribe', 'SubscribeController@index')->name('subscribe');
Route::get('/subscribe-membership','SubscribeController@buyMembership');
Route::get('/subscribe-membership2','SubscribeController@buyMembership2');
Route::post('/subscribe-membership','SubscribeController@buyMembershipPost')->name('subscribe-membership');

  
Route::get('/', function () {
    return view('home');
});
Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
    Route::get('/home', 'AdminController@index')->name('admin.home');
	
	Route::resource('/users','UserController'); 
	Route::resource('/categories','CategoryController'); 
	Route::resource('/faqs','FaqController'); 
	Route::resource('/features','FeaturesController'); 
	Route::resource('/testimonials','TestimonialController');
	Route::resource('/tags','TagController');  
	Route::resource('/contents','ContentController'); 
	Route::resource('/seos','SeoController');  
	
	Route::resource('/subscribers','UserController'); 
	Route::get('/active/subscribers', 'UserController@showsubscribers_active');
	Route::get('/inactive/subscribers', 'UserController@showsubscribers_inactive');
	Route::get('subscriber_remove/{subscriber_id}','UserController@subscriber_remove');
	Route::get('inactive_subscriber_view/{subscriber_id}','UserController@inactive_subscriber_view');
	Route::get('active_subscriber_view/{subscriber_id}','UserController@active_subscriber_view');
	Route::any('/active/subscribers/change_password','UserController@change_password');
	Route::resource('/ebooks','EbookController'); 

});

Route::get('/logout',[
        'uses' => 'MainController@logout', 
        'as'   => 'logout'
        ]);
Route::get('/cancel-account', 'MainController@cancel_account');
Route::post('/cancel-account','MainController@store')->name('cancel-account');

/*Route::get('/pricing', 'MainController@pricing');
Route::post('/pricing', 'MainController@pricing_post');*/



/*Route::get('/pricing-step2', 'MainController@index')->name('pricing_step2');
Route::get('/pricing-step2','MainController@pricing_step2');
Route::post('/pricing-step2','MainController@pricing_post_step2')->name('pricing_step2');
*/
Route::get('pricing','MainController@pricing_step1');
Route::get('/pricing-step2/{plan_id}','MainController@pricing_step2');
Route::post('/pricing-step2', 'MainController@pricing_post')->name('pricing_step2');
Route::get('/', ['as'=>'home', 'uses'=>'MainController@home']);


Route::get('/view-profile','MainController@view_profile');
Route::post('/view-profile', 'MainController@update_profile')->name('update_profile');
Route::get('books','CatController@showallcategory');
Route::get('bookcategory/{category_id}','CatController@show');
Route::get('ebookview/{book_id}','CatController@ebookview');
Route::get('ebookpreview/{book_id}','PreviewController@ebookpreview');

Route::get('ebookpreview2','PreviewController@ebookpreview2');

/*********** USER Routes ************/


Route::get('dashboard','DashboardController@index');

/*********** Accounts route **********/
Route::get('accounts','AccountsController@index');
Route::get('accounts/add','AccountsController@add');
Route::post('accounts/save','AccountsController@save');
Route::get('delete_account/{id}','AccountsController@delete_account');
Route::get('show_account/{id}','AccountsController@show');
Route::get('show_account/{id}/{all}','AccountsController@show');

Route::resource('accounts','AccountsController');

/*********** Budgets route **********/

Route::get('budgets/add','BudgetsController@add');
Route::post('budgets/save','BudgetsController@save');
Route::get('budgets/show/{id}','BudgetsController@show');
Route::get('budgets/show/{id}/{all}','BudgetsController@show');

Route::get('delete_budget/{id}','BudgetsController@delete_budget');
Route::resource('budgets','BudgetsController');

/*********** Categories route **********/
Route::get('categories/add','CategoriesController@add');
Route::post('categories/save','CategoriesController@save');
Route::get('categories/show/{id}','CategoriesController@show');
Route::get('categories/show/{id}/{all}','CategoriesController@show');

 

Route::get('delete_category/{id}','CategoriesController@delete_category');
Route::resource('categories','CategoriesController');

/*********** Liabilities route **********/
Route::get('liabilities','LiabilitiesController@index');
Route::get('liabilities/add','LiabilitiesController@add');
Route::post('liabilities/save','LiabilitiesController@save');
Route::get('liabilities/show/{id}','LiabilitiesController@show');
Route::get('liabilities/show/{id}/{all}','LiabilitiesController@show');

 
Route::resource('liabilities','LiabilitiesController');
Route::get('remove_liability/{id}','LiabilitiesController@remove_liability');

/*********** Transactions route **********/
Route::get('transactions','TransactionsController@index');
Route::post('transactions','TransactionsController@index');
Route::get('transations/add','TransactionsController@add');
Route::get('repeat_option/{id}','TransactionsController@repeat_option');

Route::get('transations/show/{id}','TransactionsController@show');

Route::get('repeat_month_day/{id}','TransactionsController@repeat_month_day');
Route::get('clone_transaction/{id}','TransactionsController@clone_transaction');
Route::get('stop_repeated_transections/{id}','TransactionsController@stop_repeated_transections');
Route::get('remove_transaction/{id}','TransactionsController@remove_transaction');
Route::post('transactions/save','TransactionsController@save');
Route::post('transactions/search','TransactionsController@search');
 
Route::resource('transactions','TransactionsController');
//Route::resource('/categories','CategoryController'); 
 
Auth::routes();

 

/******************************/
Route::get('budget_cron','CronController@manage_budgets');
Route::get('create_transection','CronController@create_transection');
 

/* language translator manager url rewriting starts here */
Route::get('/admin/translations/view/{groupKey?}',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@getView']);
Route::get('/admin/translations/{groupKey?}',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@getIndex']);
Route::post('/admin/translations/add/{groupKey}',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postAdd']);
Route::post('/admin/translations/edit/{groupKey}',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postEdit']); 
Route::post('/admin/translations/delete/{groupKey}/{translationKey}',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postDelete']);
Route::post('/admin/translations/import',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postImport']);
Route::post('/admin/translations/find',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postFind']);
Route::post('/admin/translations/locales/add',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postAddLocale']);
Route::post('/admin/translations/locales/remove',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postRemoveLocale']);
Route::post('/admin/translations/publish/{groupKey}',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postPublish']);
Route::post('/admin/translations/translate-missing',['middleware' => ['auth:admin'],'uses' =>'\Barryvdh\TranslationManager\Controller@postTranslateMissing']);
Route::post('/admin/translations/groups/add','\Barryvdh\TranslationManager\Controller@postAddGroup');

/* language translator manager url rewriting ends here */
