<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CmsController extends Controller

{

 	 
	 
	 
	 public function __construct()
     {
		 
     }
	 
	 

   
	 
	 public function about()

    {
	 	 $dataCms = DB::select('select * from contents where id=1');
		 $seoData = DB::select('select * from seos where id=6'); 
		  
         return view('cms',['dataCms'=>$dataCms[0],'seoData'=>$seoData[0]]);
     }
	 
	 public function careers() 

    {
	 	 $dataCms = DB::select('select * from contents where id=2');
		 
		  
         return view('cms',['dataCms'=>$dataCms[0]]);
     }
	 
	 public function terms()

    {
	
	 	 $dataCms = DB::select('select * from contents where id=3');
		 $seoData = DB::select('select * from seos where id=8');
		  
         return view('cms',['dataCms'=>$dataCms[0],'seoData'=>$seoData[0]]);
     }	 
	 
	 public function testimonials()

    {
	
	 	 $dataCms = DB::select('select * from contents where id=3');
		 $seoData = DB::select('select * from seos where id=8');
		 $testimonials = DB::select('select * from testimonials order by id asc'); 
         return view('testimonials',['dataCms'=>$dataCms[0],'seoData'=>$seoData[0], 'testimonials'=>$testimonials]);
     }
	 
	  public function privacy()

    {
	 	 $dataCms = DB::select('select * from contents where id=4');
		 $seoData = DB::select('select * from seos where id=7'); 
		  
         return view('cms',['dataCms'=>$dataCms[0],'seoData'=>$seoData[0]]);
     }
	 
	 
	 public function help()

    {
	 	 $dataCms = DB::select('select * from contents where id=55');
		 
		  
         return view('cms',['dataCms'=>$dataCms[0]]);
     }
	 
	 public function benefits()

    {
	 	 $dataCms = DB::select('select * from contents where id=6');
		 
		 $y='en'; 
		 $seoData = DB::select('select * from seos where id=2');
         return view('benefits',['dataCms'=>$dataCms[0],'y'=>$y,'seoData'=>$seoData[0]]);
     }
	 
	 public function cancel_account()

    {
	 	 $dataCms = DB::select('select * from contents where id=10');
		 
		  
         return view('cms',['dataCms'=>$dataCms[0]]);
     }
	 
	 public function blog()

    {
	 	 $dataCms = DB::select('select * from contents where id=6');
		 
		  
         return view('cms',['dataCms'=>$dataCms[0]]);
     }
	 
	 public function contact()

    {
	 	 $dataCms = DB::select('select * from cms_pages where id=3');
		 
		  
         return view('cms',['dataCms'=>$dataCms[0]]);
     }
	 
	 public function refund_policy()

    {
	 	 $dataCms = DB::select('select * from contents where id=8');
		 $seoData = DB::select('select * from seos where id=9');
		  
         return view('cms',['dataCms'=>$dataCms[0],'seoData'=>$seoData[0]]);
     }
	 
	  public function thank_you()

    {
	 	 $dataCms = DB::select('select * from contents where id=29');
		 
		  
         return view('cms',['dataCms'=>$dataCms[0]]);
     }
	 
	 
	 
	
	 
	
	 
	 
	 
	 



}