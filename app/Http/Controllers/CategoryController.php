<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Category; 


  

class CategoryController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "Category";
        $this->middleware('auth:admin'); 
    } 

    public function index()
 
    {
	 
	 	 
	 
        $categories = Category::orderBy('created_on', 'desc')->paginate(10);
 
        return view('categories.index',compact('categories'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		 
        return view('categories.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $request->validate([

            'name_en' => 'required',

            'name_fr' => 'required',
			
			'name_nl' => 'required',
			
			'name_dk' => 'required',
			
			'name_at' => 'required',
			
			'name_de' => 'required',
			
			'name_se' => 'required',
			
			'name_no' => 'required',
			
			'name_it' => 'required',
					

        ]);
		
		$new_name='';
		$image = $request->file('image');
		if($image!="")
		{
     	$new_name = rand() . '.' . $image->getClientOriginalExtension();
     	$image->move(public_path('catimages'), $new_name);
		}

 		$created_on=date("Y-m-d H:i:s");
	 	 
		
        Category::create(array_merge($request->all(),['image' => $new_name,'created_on' => $created_on]));

   		 

        return redirect()->route('categories.index')

                        ->with('success','Category created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Category $category)

    {

        return view('categories.show',compact('category'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Category $category)

    {

        return view('categories.edit',compact('category'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Category $category)

    {

        $request->validate([

            'name_en' => 'required',

            'name_fr' => 'required',
			
			'name_nl' => 'required',
			
			'name_dk' => 'required',
			
			'name_at' => 'required',
			
			'name_de' => 'required',
			
			'name_se' => 'required',
			
			'name_no' => 'required',
			
			'name_it' => 'required',
					

        ]);
		
		
		
		$image = $request->file('image');
		if($image!="")
		{

     	$new_name = rand() . '.' . $image->getClientOriginalExtension();
     	$image->move(public_path('catimages'), $new_name);
        $category->update(array_merge($request->all(),['image' => $new_name]));
		}
		else
		{
		 $category->update($request->all());
		}
		
		 
  

        return redirect()->route('categories.index')

                        ->with('success','Category updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Category $category)

    {

        $category->delete();

  

        return redirect()->route('categories.index')

                        ->with('success','Category deleted successfully');

    }

}