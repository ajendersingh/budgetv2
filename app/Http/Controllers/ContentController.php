<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Content; 


  

class ContentController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		 $this->middleware('auth:admin'); 
    } 

    public function index()

    {
	 
	 	 
	 
        $contents = Content::orderBy('title_en', 'asc')->paginate(10);
		
         return view('contents.index',compact('contents'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('contents.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $request->validate([

            'title_en' => 'required',

            'title_fr' => 'required',
			
			'title_nl' => 'required',
			
			'title_dk' => 'required',
			
			'title_at' => 'required',
			
			'title_de' => 'required',
			
			'title_se' => 'required',
			
			'title_no' => 'required',
			
			'title_it' => 'required',
					

        ]);

 		$created_on=date("Y-m-d H:i:s");
		
		
        Content::create(array_merge($request->all(),['created_on' => $created_on]));

   		 

        return redirect()->route('contents.index')

                        ->with('success','Content created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Content $content)

    {

        return view('contents.show',compact('content'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Content $content)

    {

        return view('contents.edit',compact('content'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Content $content)

    {

        $request->validate([

            'title_en' => 'required',

            'title_fr' => 'required',
			
			'title_nl' => 'required',
			
			'title_dk' => 'required',
			
			'title_at' => 'required',
			
			'title_de' => 'required',
			
			'title_se' => 'required',
			
			'title_no' => 'required',
			
			'title_it' => 'required',
					

        ]);

  		

        $content->update($request->all());

  

        return redirect()->route('contents.index')

                        ->with('success','Content updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.
 
     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Content $content)

    {

        $content->delete();

  

        return redirect()->route('contents.index')

                        ->with('success','Content deleted successfully');

    }

}