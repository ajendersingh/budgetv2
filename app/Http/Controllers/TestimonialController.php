<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Testimonial; 


  

class TestimonialController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "Category";
        $this->middleware('auth:admin'); 
    } 

    public function index()
 
    {
	 
	 	 
	 
        $testimonials = Testimonial::orderBy('created_on', 'desc')->paginate(10);
 
        return view('testimonials.index',compact('testimonials'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		 
        return view('testimonials.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $request->validate([

            'testimonial_client_en' => 'required',

            'testimonial_location_en' => 'required',
			
			'description_en' => 'required',
			
			'description_fr' => 'required',
			
			'description_nl' => 'required',
			
			'description_dk' => 'required',
			
			'description_at' => 'required',
			
			'description_de' => 'required',
			
			'description_se' => 'required',
			
			'description_no' => 'required',
			
			'description_it' => 'required',
					

        ]);
		
		 

 		$created_on 		= date("Y-m-d H:i:s");
		$image 				= $request->file('image');
		
		if ($image !== null) 
		{
			$input['imagename'] = time().'.'.$image->getClientOriginalExtension();
			$destinationPath 	= public_path('/images');
			$image->move($destinationPath, $input['imagename']);
			$thumbnail = $input['imagename'];
			
			//$data_arr['thumbnail'] = $thumbnail;
			 
			//$testimonial->update($request->all(),array('thumbnail' => $thumbnail));
		} 
		else
		{
			$thumbnail = '';
			//$data_arr['thumbnail'] = '';
		}
		
		
		
		
        Testimonial::create(array_merge($request->all(),['created_on' => $created_on, 'thumbnail' => $thumbnail]));

   		 

        return redirect()->route('testimonials.index')

                        ->with('success','Testimonial created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Category $category)

    {

        return view('testimonials.show',compact('category'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Testimonial $testimonial)

    {

        return view('testimonials.edit',compact('testimonial'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Testimonial $testimonial)

    {

        $request->validate([

            'testimonial_client_en' => 'required',

            'testimonial_location_en' => 'required',
			
			'description_en' => 'required',
			
			'description_fr' => 'required',
			
			'description_nl' => 'required',
			
			'description_dk' => 'required',
			
			'description_at' => 'required',
			
			'description_de' => 'required',
			
			'description_se' => 'required',
			
			'description_no' => 'required',
			
			'description_it' => 'required',
					

        ]);
		
	$postdata = $_POST;
		$data_arr = array ('testimonial_client_en' => $postdata['testimonial_client_en'],

            'testimonial_location_en' => $postdata['testimonial_location_en'],
			
			'description_en' => $postdata['description_en'],
			
			'description_fr' => $postdata['description_fr'],
			
			'description_nl' => $postdata['description_nl'],
			
			'description_dk' => $postdata['description_dk'],
			
			'description_at' => $postdata['description_at'],
			
			'description_de' => $postdata['description_de'],
			
			'description_se' => $postdata['description_se'],
			
			'description_no' => $postdata['description_no'],
			
			'description_it' => $postdata['description_it']);
			
		//$created_on 		= date("Y-m-d H:i:s");
		$image 				= $request->file('image');
		 
		
		if ($image !== null) 
		{
			$input['imagename'] 	= time().'.'.$image->getClientOriginalExtension();
			$destinationPath 		= public_path('/images');
			$image->move($destinationPath, $input['imagename']);
			$thumbnail 				= $input['imagename'];
			$data_arr['thumbnail'] 	= $thumbnail;
			 
			//$testimonial->update($request->all(),array('thumbnail' => $thumbnail));
		} 
		else
		{
			//$testimonial->update($request->all());
			$data_arr['thumbnail'] = '';
		}
		
		
		$data_id = 	$testimonial->id;
		DB::table('testimonials')->where('id', $data_id)->update($data_arr);
 
        return redirect()->route('testimonials.index')

                        ->with('success','Testimonial updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Testimonial $testimonial)

    {

        $testimonial->delete();

  

        return redirect()->route('testimonials.index')

                        ->with('success','Testimonial deleted successfully');

    }

}