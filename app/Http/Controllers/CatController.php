<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Category; 


  

class CatController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 
	public function __construct() 
	{
	$this->middleware('auth');
	}
	 

    public function show($Category_id)

    {
		return redirect()->action('DashboardController@index');
		/*
		$seoData = DB::select('select * from seos where id=13');
		$book_exist=0; 
		if(is_numeric($Category_id))
		{
		 
		
			$categorydata_all =  DB::select('select * from category  ');
 			
			$categorydata = Category::find($Category_id);
			if(!empty($categorydata)) 
			{
			
				$ebooks_category = DB::select('select * from ebook_category  where  category_id='.$Category_id.'');
  				if(count($ebooks_category)>0)
				{
					$data_val=array(); 
					$total_books_cat='';
					foreach ($ebooks_category as $val)
					{
					
					$data_val[]=$val->book_id;
					
					}
					$totalBooks=0;
					$ebooks=array();
					if(count($data_val)>0)
					{
					$total_books_cat=implode(",",$data_val);
					
					$ebooks = DB::select('select * from ebook  where id in ('.$total_books_cat.')');
					$totalBooks=count($ebooks);
					$book_exist=count($ebooks);
					}
	 
 				return view('ebooks.frontcategory',['categorydata'=>$categorydata,'ebooks'=>$ebooks,'totalBooks'=>$totalBooks,'total_books_cat'=>$total_books_cat,'book_exist'=>$book_exist,'categorydata_all'=>$categorydata_all,'Category_id'=>$Category_id,'seoData'=>$seoData[0]]);
			
				}
				else
				{
				$ebooks=array();
 			return view('ebooks.frontcategory',['ebooks'=>$ebooks,'totalBooks'=>0,'categorydata'=>$categorydata,'book_exist'=>$book_exist,'categorydata_all'=>$categorydata_all,'Category_id'=>$Category_id,'seoData'=>$seoData[0]]);
				}
			}
			else
			{
			 
			$ebooks=array();
 			return view('ebooks.frontcategory',['ebooks'=>$ebooks,'totalBooks'=>0,'categorydata'=>$categorydata,'book_exist'=>$book_exist,'categorydata_all'=>$categorydata_all,'Category_id'=>$Category_id,'seoData'=>$seoData[0]]);
			}
		}
		else
		{
		 
		return view('ebooks.frontcategory',['book_exist'=>$book_exist,'seoData'=>$seoData[0]]);
		}
  */
    }
	
	public function ebookview($book_id)

    {
	 	$book_exist=0; 
		$similar_book_count=0;
		$similar_book_name_all=array();
 		$categorydata = Category::find($book_id);
		 
		if(is_numeric($book_id))
		{
		$seoData = DB::select('select * from seos where id=14');
		$ebooks = DB::select('select * from ebook  where ebook.id='.$book_id.'');
		
			if(count($ebooks)>0)
			{
				$similar_book_cat = DB::select("select category_id from ebook_category where book_id='".$book_id."'");
				$similar_book_cat_array=array();
				foreach ($similar_book_cat as $val)
				{
				$similar_book_cat_array[]=$val->category_id;
				}
				$all_sim_cat=implode(",",$similar_book_cat_array);
 			 	$ebooks_sim = DB::select("select distinct book_id from ebook_category   where category_id in (".$all_sim_cat.") and book_id !=".$book_id."");
				
				$book_arr=array();
				foreach ($ebooks_sim as $val)
				{
				$book_arr[]=$val->book_id;
				}
				 
					if(count($book_arr)>0)
					{
					$books_all_id=implode(",",$book_arr); 
					$similar_book_name_all = DB::select("select * from ebook where id in (".$books_all_id.")");
					$similar_book_count=count($similar_book_name_all);
					
					}
				 
				$book_exist=1;
				$swf_file_name=explode(".",$ebooks[0]->pdf_file);
				$swf_file_name=$swf_file_name[0];
				return view('ebooks.ebookview',['ebooks'=>$ebooks[0],'swf_file_name'=>$swf_file_name,'book_exist'=>$book_exist,'similar_book_name_all'=>$similar_book_name_all,'similar_book_count'=>$similar_book_count,'seoData'=>$seoData[0]]);
			}
			else
			{
			$book_exist=0;
			return view('ebooks.ebookview',['book_exist'=>$book_exist,'seoData'=>$seoData[0]]);
			}
		}
		else
		{
		return view('ebooks.ebookview',['book_exist'=>$book_exist]);
		}
		
  
    }
	
	
	
	public function showallcategory()

    {
 		
		$categories_exist=0;
		$showallcategory = DB::select('select * from category order by name_en asc');
 		if(count($showallcategory)>0)
		{
		return view('ebooks.showallcategory',['showallcategory'=>$showallcategory,'categories_exist'=>$categories_exist]);
		}
		
		
  
    }

   

    

}