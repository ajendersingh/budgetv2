<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Category; 
use App\Mail\WelcomeMail;

  

class CronController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 
	public function __construct() 
	{
	//$this->middleware('auth');
	}
	 

 
	public function manage_budgets()
	{
			$start_date 			= strtotime("1-".date("m-Y")." 00:00:00");
			$end_date 				= strtotime(getLastDateOfMonth()."-".date("m-Y")." 23:59:59");
			$first_day_last_month 	= strtotime(date('Y-m-d', strtotime('first day of last month'))." 00:00:00");
			$last_day_last_month 	= strtotime(date('Y-m-d', strtotime('last day of last month'))." 23:59:59");
			$date 					= date("Y-m-d");
			$yesterday 				= strtotime(date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $date) ) ))." 23:59:59");
			//$budgets 				= $this->db->select("*")->where("is_active", 1)->where('end_date', $yesterday)->get('tbl_budgets')->result();
			$budgets 				= DB::select("select * from tbl_budgets where is_active=1 and end_date = '".$yesterday."'");  
			if(isset($budgets[0]))
			{
				foreach($budgets as $budget)
				{
					$budget_id 				= $budget->id;
					$name 					= $budget->name;
					$user_id 				= $budget->user_id;
					$parent_id 				= $budget->parent_id;
					$start_date 			= $budget->start_date;
					$end_date 				= $budget->end_date;
					$auto_budget 			= $budget->budget_type;
					$budget_amount 			= $budget->budget_amount;
					$budget_period 			= $budget->budget_period;
					$repeated_amount 		= $budget->repeated_amount;
					
				 
					
					//auto_budget == 2 //Set Fixed Amount every period
					//auto_budget == 3 //Add an amount every period
					if($auto_budget > 0)
					{
						if($auto_budget == 2)
						{
							$budget_amount = $budget_amount;
						}
						if($auto_budget == 3)
						{
							$budget_amount = $budget_amount+$repeated_amount;
						}
					 
						$data_arr['name']  			= $name;
						$data_arr['user_id']  		= $user_id;
						$data_arr['parent_id']  	= $parent_id>0?$parent_id:$budget_id;
						$data_arr['budget_amount']  = $budget_amount;
						$data_arr['repeated_amount']  = $repeated_amount;
						$data_arr['created_on']  	= time();
						if( $budget_period == 1)
						{
							$data_arr['start_date'] = strtotime(date("d-m-Y")." 00:00:00");
							$data_arr['end_date'] 	= strtotime(date("d-m-Y")." 23:59:59");
						}
						if( $budget_period == 2)
						{
							$data_arr['start_date'] = strtotime(date("Y-m-d", strtotime('monday this week'))." 00:00:00");
							$data_arr['end_date'] 	= strtotime(date("Y-m-d", strtotime('sunday this week'))." 23:59:59");
						}
						if( $budget_period == 3)
						{
							$data_arr['start_date'] = $start_date;
							$data_arr['end_date'] 	= $end_date;
						}
						
						if( $budget_period == 4)
						{
							$quarter = get_dates_of_quarter();
							$data_arr['start_date'] = strtotime($quarter['start']['date']);
							$data_arr['end_date'] 	= strtotime($quarter['end']['date']);
						}
						
						if( $budget_period == 5)
						{
							$hdate = get_dates_of_halfy();
							$data_arr['start_date'] = strtotime($hdate['start_date']);
							$data_arr['end_date'] 	= strtotime($hdate['end_date']);
						}
						if( $budget_period == 6)
						{
							 
							$data_arr['start_date'] = strtotime("01-01-".date("Y")." 00:00:00");
							$data_arr['end_date'] 	= strtotime("31-12-".date("Y")." 23:59:59");
						}
						
						if( $budget_period == 7)
						{
							 
							//$data_arr['start_date'] = strtotime("01-01-".date("Y")." 00:00:00");
							//$data_arr['end_date'] 	= strtotime("31-12-".date("Y")." 23:59:59");
						}
						
						
						//$ex_bdg = $this->db->select("id")->where("start_date", $data_arr['start_date'])->where("end_date", $data_arr['end_date'])->where("parent_id", $parent_id)->get('tbl_budgets')->row();
						
						$ex_bdg = DB::table('tbl_budgets')
						->where("start_date", $data_arr['start_date'])
						->where("end_date", $data_arr['end_date'])
						->where("parent_id", $parent_id)
						->where('id', $budget_id )
						->first();
					
					
						if(!isset($ex_bdg->id) )
						{
						//$this->db->insert('tbl_budgets', $data_arr);
						DB::table('tbl_budgets')->insert($data_arr);
						}
					
						
					}
				//$this->db->where('id', $budget_id)->update('tbl_budgets', array("is_active"=>0) );
				
				DB::table('tbl_budgets')
				->where('id', $budget_id)
				->update($data_arr);
			
			
				}
			}
			
	}
	public function set_budget_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		if($trans->transaction_type == 'EXPENCES' && $trans->budget >0)
		{
			
			$budget = DB::table('tbl_budgets')->where('id', $trans->budget)->first();
			$spent 	= $budget->amount_spent+$trans_amount;
			if($budget->amount_spent == 0 && $budget->amount_left == 0)
			{
				$left 	= $budget->budget_amount-$trans_amount;
			}
			else{
				$left 	= $budget->amount_left-$trans_amount;
			}
			$row_arr = array('amount_spent'=>$spent, 'amount_left'=>$left);
			DB::table('tbl_budgets')
			->where('id', $budget->id)
			->update($row_arr);
		}
	}
	
	public function set_account_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		 
		if($trans->transaction_type == 'EXPENCES')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('balance'=> ($account->balance-$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
			
		}
		
		if($trans->transaction_type == 'REVENUE')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			$row_arr = array('balance'=> ($account->balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
		}	
		
		if($trans->transaction_type == 'TRANSFER')
		{
			$saccount = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('balance'=> ($saccount->balance-$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $saccount->id)
			->update($row_arr);
			$daccount = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			$amount  = ($account->balance+$trans_amount);
			$row_arr = array('balance'=> $amount );
			DB::table('tbl_accounts')
			->where('id', $daccount->id)
			->update($row_arr);
			
		}
	}

	
	public function createClone($tans)
	{
		$data_arr = array(
						'title' 					=> isset($tans->title )?$tans->title:'',
						'base_id' 					=> isset($tans->base_id ) && $tans->base_id>0 ?$tans->base_id:$tans->id,
						'transaction_type' 			=> isset($tans->transaction_type)?$tans->transaction_type:'',
						//'is_recurring_transaction' 	=> isset($tans->is_recurring_transaction)?$tans->is_recurring_transaction:'',
						'recurring_period' 			=> isset($tans->recurring_period)?$tans->recurring_period:'',
						'user_id' 					=> $tans->user_id,
						'source_account' 			=> isset($tans->source_account)?$tans->source_account:'',
						'destination_account' 		=> isset($tans->destination_account)?$tans->destination_account:'',
						'trans_date' 				=> strtotime( date("d-m-Y") ),
					
						'liability_id' 				=> isset($tans->liability_id)?$tans->liability_id:'',
						'income_id' 				=> isset($tans->income_id)?$tans->income_id:'',
						'trans_amount' 				=> isset($tans->trans_amount)?$tans->trans_amount:'',
						
						'first_payment_date' 		=> isset($tans->first_payment_date)?$tans->first_payment_date:'',
						'second_payment_date' 		=> isset($tans->second_payment_date)?$tans->second_payment_date:'',
				
				
						'budget' 					=> isset($tans->budget)?$tans->budget:'',
						'category' 					=> isset($tans->category)?$tans->category:'',
						'tag' 						=> isset($tans->tag)?$tans->tag:'',
						'description' 				=> isset($tans->description)?$tans->description:'' );
				 
				 
					$data_arr['created_by'] = $tans->user_id;
					$data_arr['created_on'] = time();
							 	
					DB::table('tbl_transections')->insert($data_arr);			
					$id = DB::getPdo()->lastInsertId();
					$trans = DB::table('tbl_transections')->where('id', $id)->first(); 
					self::set_budget_amount($id, $trans);
					self::set_account_amount($id, $trans);
			
			//$trans_id	=	$this->transections->add($data_arr);
					//echo $this->db->last_query();
					//set_budget($trans_id);
					//set_account_amount($trans_id);
	}
	
	public function repeat_option($id)
	{
		//$data = $this->db->select('id, name')->where('id', $id)->get('tbl_period_options')->row();
		$data = DB::table('tbl_period_options')->where('id', $id)->first();
		return $data;
	}
	
	
		/************* Create Repeated Transaction 
	

	***************/
	public function create_transection()
	{
		//$options = $this->db->select('id,name')->where('is_deleted', 0)->order_by('id', 'asc')->get('tbl_periods')->result();
		$options   = DB::select("select id,name from tbl_periods where is_deleted=0 order by id asc");  
		if(isset($options[0]))
		{
			foreach($options as $period)
			{
				/*$this->db->select('*')
				->where('is_recurring_transaction', 1)
				->where("recurring_period", $period->id)
				->where("stop_recurring", 0)
				->where("base_id", 0)
				
				->order_by("id", 'desc');
				$transections = $this->db->get('tbl_transections')->result();*/
				$transections = DB::select("select * from tbl_transections where base_id=0 and stop_recurring=0 and recurring_period=".$period->id." and is_recurring_transaction=1");
				
				//echo $this->db->last_query(); die();
				if( isset($transections[0]) )
				{
					foreach($transections as $trans)
					{
						if( strtolower($period->id) == 1) 
						{
							self::createClone($trans);
						} 
						
						if( strtolower($period->id) == 2) 
						{
							$repeat_options = self::repeat_option($trans->repeat_on_every);
							if(isset($repeat_options->id) && $repeat_options->id >0) 
							{
								if(strtolower($repeat_options->name)  == strtolower(date("l")))
								{
									self::createClone($trans);
								}
							}
						}						
						
						if( strtolower($period->id) == 3) 
						{
							$repeat_options = self::repeat_option($trans->repeat_on_every);
							
							if(isset($repeat_options->id) && $repeat_options->id >0) 
							{
								if(strtolower($repeat_options->name)  == strtolower('Last day') )
								{
									if(date("d") == getLastDateOfMonth() )
									self::createClone($trans);
								}
								if(strtolower($repeat_options->name)  == strtolower('1st day') )
								{
									if(date("d") == 1 )
									self::createClone($trans);
								}
								
								if(strtolower($repeat_options->name)  == strtolower('Other day before 28th
') )
								{
									if(date("d") == $trans->repeat_month_day )
									self::createClone($trans);
								}
							}
						}
						
						/*********** Quarterly **********/
						
						if( strtolower($period->id) == 4) 
						{
							$repeat_options = self::repeat_option($trans->repeat_on_every);
							$quarter        = get_dates_of_quarter();
							
							if(isset($repeat_options->id) && $repeat_options->id >0) 
							{
								if(strtolower($repeat_options->name)  == strtolower('Last day') )
								{
									if( strtotime(date("Y-m-d").'23:59:59.000000' ) == strtotime($quarter['end']['date']) )
									self::createClone($trans);
								}
								if(strtolower($repeat_options->name)  == strtolower('1st day') )
								{
									if( strtotime(date("Y-m-d").'00:00:00.000000' ) == strtotime($quarter['start']['date']) )
									self::createClone($trans);
								}
 							}
						}
						
						
						/*******************/
						
						
						/********** Half yearly repeat functioanlity ***********/
						
						if( strtolower($period->id) == 5 ) 
						{
							$repeat_options = self::repeat_option($trans->repeat_on_every);
							$quarter        = get_dates_of_quarter();
							
							if(isset($repeat_options->id) && $repeat_options->id >0) 
							{
								if(strtolower($repeat_options->name)  == strtolower('Last day') )
								{
									if( strtotime(date("Y-m-d") ) == strtotime(date("Y")."-06-30" ) || strtotime(date("Y-m-d") ) == strtotime(date("Y")."-12-30" ))
									self::createClone($trans);
								}
								if(strtolower($repeat_options->name)  == strtolower('1st day') )
								{
									if( strtotime(date("Y-m-d") ) == strtotime(date("Y")."-07-01" ) || strtotime(date("Y-m-d") ) == strtotime(date("Y")."-01-01" ))
									self::createClone($trans);
								}
 							}
						}
						
						
						/*******************/
						
						
						
						/********** Yearly repeat functionlaity ***********/
						
						if( strtolower($period->id) == 6 ) 
						{
							$repeat_options = self::repeat_option($trans->repeat_on_every);
							$quarter        = get_dates_of_quarter();
							
							if(isset($repeat_options->id) && $repeat_options->id >0) 
							{
								if(strtolower($repeat_options->name)  == strtolower('Last day') )
								{
									if( strtotime(date("Y-m-d") ) == strtotime(date("Y")."-12-30" ))
									self::createClone($trans);
								}
								if(strtolower($repeat_options->name)  == strtolower('1st day') )
								{
									if( strtotime(date("Y-m-d") ) == strtotime(date("Y")."-01-01" ))
									self::createClone($trans);
								}
 							}
						}
						
						
						/*******************/
						
						
						
						/********** Yearly repeat functionlaity ***********/
						
						if( strtolower($period->id) == 7 ) 
						{
							
							 
							$week_no = weekOfMonth();
							if($week_no == 2 || 4)
							{}
								$repeat_options = self::repeat_option($trans->repeat_on_every);
								if(isset($repeat_options->id) && $repeat_options->id >0) 
								{}
									
									if(strtolower($trans->first_payment_date)  == strtolower(date("d")) || strtolower($trans->second_payment_date)  == strtolower(date("d")))
									{
										self::createClone($trans);
									}
								 
								
							
							 
						}
						/*******************/
					}
				}
				
				
			}
		}
		
	}
	/************* EOF Repeated Transaction ***************/
	
	

   

}