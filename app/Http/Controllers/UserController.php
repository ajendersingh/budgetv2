<?php

  

namespace App\Http\Controllers;

  

use App\User; 

use Auth;

use DB; 

use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

  

class UserController extends Controller

{
 
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	  
	public function __construct()
    {
	
		 
		  
         $this->middleware('auth:admin'); 
    }
	
    public function index()

    {
	 
	 	 
		if(Auth::guard('admin')->check())
		{  
	 	
		$users = \DB::table('users')
    ->select(
        'users.id',
        'name',
		'email',
		'created_at'
    )
    ->whereNotExists( function ($query){
        $query->select(DB::raw(1))
        ->from('user_membership')
        ->whereRaw('users.id = user_membership.user_id');
    })->orderBy('users.id', 'desc')->paginate(10);
	
	 
	
	 
 		 
 
        return view('users.index',compact('users'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);
		}
		else
		{
		return redirect('admin/login');
		}

    }
	
	
	public function subscriber_remove($subscriber_id)
	{
	
	$unsubscribe_date=date("Y-m-d H:i:s");
	DB::table('user_membership')
			->where('id', $subscriber_id)
			->update([
				'status' => 'inactive',
				'unsubscribe_date' => $unsubscribe_date
			
			]
			);
	return redirect()->action('UserController@showsubscribers_active')->with('success', 'User  unsubscribed successfully!!!');
 	}
	
	
	public function inactive_subscriber_view($subscriber_id)
	{
	
	 $subscribers = DB::select("select * from users left join user_membership on  user_membership.user_id=users.id   where user_membership.id=".$subscriber_id." order by users.id desc");
	 $countrydata = DB::select('select * from apps_countries ');
	 return view('users.viewinactive',['user'=>$subscribers[0],'countrydata'=>$countrydata]);
	//return redirect()->action('UserController@showsubscribers_active')->with('success', 'User  unsubscribed successfully!!!');
 	}
	
	public function active_subscriber_view($subscriber_id) 
	{
	
	 $subscribers = DB::select("select * from users left join user_membership on  user_membership.user_id=users.id   where user_membership.id=".$subscriber_id." order by users.id desc");
 	 $countrydata = DB::select('select * from apps_countries ');
	 return view('users.viewactive',['user'=>$subscribers[0],'countrydata'=>$countrydata,'subscriber_id'=>$subscriber_id]);
	//return redirect()->action('UserController@showsubscribers_active')->with('success', 'User  unsubscribed successfully!!!');
 	}
	
	public function showsubscribers_active()

    { 
	 
	 	 
		if(Auth::guard('admin')->check())
		{  
	 	
		 
		$subscribers = DB::table('users')
                ->join('user_membership', 'user_id', '=', 'users.id')
				->where('user_membership.status', '=', 'active')
                ->select('users.*', 'user_membership.*')
                ->orderby('users.id', 'desc')
                ->paginate(10);
		 
		 
 
        return view('users.activesubscribers',compact('subscribers'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);
		}
		else
		{
		return redirect('admin/login');
		}

    }
	
	
	public function showsubscribers_inactive()

    {
	 
	 	 
		if(Auth::guard('admin')->check())
		{  
	 	
		 
		$subscribers = DB::table('users')
                ->join('user_membership', 'user_id', '=', 'users.id')
				->where('user_membership.status', '=', 'inactive')
                ->select('users.*', 'user_membership.*')
				->orderby('users.id', 'desc')
                ->paginate(10);
		 
		 
		 
 
        return view('users.inactivesubscribers',compact('subscribers'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);
		}
		else
		{
		return redirect('admin/login');
		}

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('users.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */
	public function change_password()
	{
	 
		if($_POST['password']!="")
		{
		$trackings = DB::select("select * from user_membership where id=".$_POST['subscriber_id']."");
		$user_id=$trackings[0]->user_id;
				DB::table('users')
				->where('id', $user_id) 
				->update([
					'password' => Hash::make($_POST['password'])
				
				]
				);
		return redirect()->action('UserController@active_subscriber_view', ['subscriber_id' => $_POST['subscriber_id']])->with('success', 'Password changes successfully!!!');	
		}
		else
		{
		return redirect()->action('UserController@active_subscriber_view', ['subscriber_id' => $_POST['subscriber_id']]);	
		}	
	}
    public function store(Request $request)

    {

        $request->validate([

            'name' => 'required',

            'email' => 'required',
			
			'password' => 'required',
			

        ]);

 		$created_on=date("Y-m-d H:i:s");
		
		 User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        

   

        return redirect()->route('users.index')

                        ->with('success','Users created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(User $user)

    {

        return view('users.show',compact('user'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(User $user)

    {
		
		$countrydata = DB::select('select * from apps_countries '); 	
        return view('users.edit',['user'=>$user,'countrydata'=>$countrydata]);

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, User $user)

    {

        $request->validate([

            'name' => 'required',

            'email' => 'required',
			

        ]);

  		

 		$user->update(array_merge($request->all(),['password' => Hash::make($_POST['password'])]));

  

        return redirect()->route('users.index')

                        ->with('success','User updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(User $user)

    {

        $user->delete();

  

        return redirect()->route('users.index')

                        ->with('success','User deleted successfully');

    }

}