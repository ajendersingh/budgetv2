<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Arr;

use Auth; 


use App\Category; 


use DB; 
 
class PreviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   
	public function __construct()
    {
        //$this->middleware('auth');
    }

	public function index()
    {
        if (Auth::user()) 
		{ 
		return view('userdashboard');
		}
		else
		{
		return view('home'); 
		}
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ebookpreview($book_id)

    {
	
	 
	 	
 		$categorydata = Category::find($book_id);
		$seoData = DB::select('select * from seos where id=15');
		$ebooks = DB::select('select * from ebook  where ebook.id='.$book_id.'');
		$swf_file_name=explode(".",$ebooks[0]->pdf_file);
		$swf_file_name=$swf_file_name[0];
		return view('ebooks.ebookpreview',['ebooks'=>$ebooks[0],'swf_file_name'=>$swf_file_name,'seoData'=>$seoData[0]]);
  
    }	
	
	
	public function ebookpreview2()

    {
	
	 
	 	 
 		return view('ebooks.ebookpreview2');
  
    }	
	 
	
	
	
	
}
