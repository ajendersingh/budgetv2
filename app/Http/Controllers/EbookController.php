<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Ebook; 

use Validator;


  

class EbookController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "ebook";
        $this->middleware('auth:admin'); 
    } 
	
	function index()
    {
	
	$ebooks = DB::table('ebook')
                ->paginate(10);
				
		 
    // $ebooks = Ebook::orderBy('created_on', 'desc')->paginate(10);
		return view('ebooks.index',compact('ebooks'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);
          
    }
	
	
	
	
	 
   

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
 		$dataCms = DB::select('select * from category order by name_en asc');
		
		$dataCms2 = DB::select('select * from tags order by name_en asc');
  		return view('ebooks.create',['dataCms'=>$dataCms,'dataCms2'=>$dataCms2]);

    }
	
	
	function upload(Request $request)
    {
     $rules = array(
      'file'  => 'required|mimetypes:application/pdf|max:10240'
     );

     $error = Validator::make($request->all(), $rules);

     if($error->fails())
     {
      return response()->json(['errors' => $error->errors()->all()]);
     }

     $image = $request->file('file');
		
     $new_name = rand() . '.' . $image->getClientOriginalExtension();
     $image->move(public_path('pdffiles'), $new_name);

     $output = array(
         'success' => 'Pdf uploaded successfully',
         'image'  => '<img src="'.url('/').'/img/pdf.png" class="img-thumbnail" style="width: 70px he;width: 70px;height: 60px;" />',
		 'file_name_uploaded'  => ''.$new_name.''
        );

        return response()->json($output);
    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
		
		 
		
        $request->validate([

            'book_name_en' => 'required',
			'book_description_en' => 'required',
			'book_name_fr' => 'required',
			'book_description_fr' => 'required',
			'book_name_nl' => 'required',
			'book_description_nl' => 'required',
			'book_name_dk' => 'required',
			'book_description_dk' => 'required',
			'book_name_at' => 'required',
			'book_description_at' => 'required',
			'book_name_de' => 'required',
			'book_description_de' => 'required',
			'book_name_se' => 'required',
			'book_description_se' => 'required',
			'book_name_no' => 'required',
			'book_description_no' => 'required',
			'book_name_it' => 'required',
			'book_description_it' => 'required',


        ]);
		
		
 		 $created_on=date("Y-m-d H:i:s");
		 $values = array('book_name_en' => $_POST['book_name_en'],
		 				'book_description_en' => $_POST['book_description_en'],
						'book_name_fr' => $_POST['book_name_fr'],
						'book_description_fr' => $_POST['book_description_fr'],
						'book_name_nl' => $_POST['book_name_nl'],
						'book_description_nl' => $_POST['book_description_nl'],
						'book_name_dk' => $_POST['book_name_dk'],
						'book_description_dk' => $_POST['book_description_dk'],
						'book_name_at' => $_POST['book_name_at'],
						'book_description_at' => $_POST['book_description_at'],
						'book_name_de' => $_POST['book_name_de'],
						'book_description_de' => $_POST['book_description_de'],
						'book_name_se' => $_POST['book_name_se'],
						'book_description_se' => $_POST['book_description_se'],
						'book_name_no' => $_POST['book_name_no'],
						'book_description_no' => $_POST['book_description_no'],
						'book_name_it' => $_POST['book_name_it'],
						'book_description_it' => $_POST['book_description_it'],
						'featured' => $_POST['featured'],
						'pdf_file' => $_POST['pdf_file_name_hidden'],
						'cover_image' => $_POST['image_file_name_hidden'],
						'created_on' => $created_on);
		
		$file_name_orig=explode(".",$_POST['pdf_file_name_hidden']);
		$file_name_orig_to_convert=$file_name_orig[0];
		$inserted_id=DB::table('ebook')->insertGetId($values);
		 
		
		if(trim($_POST['tag_id'])!="")
		{
		 
			if(strpos($_POST['tag_id'], ',') !== false) 
			{
			$data2=explode(",",$_POST['tag_id']);
				foreach ($data2 as $key => $val)
				{
				$values2 = array('book_id' => $inserted_id,'tag_id' => $val,'created_on' => $created_on);
				$inserted_id_tag=DB::table('book_tags')->insertGetId($values2);
				}
			 
			}
			else
			{
				$values2 = array('book_id' => $inserted_id,'tag_id' =>$_POST['tag_id'],'created_on' => $created_on);
				$inserted_id_tag=DB::table('book_tags')->insertGetId($values2);
			}
		}
		
		
		
		if(trim($_POST['cat_id'])!="")
		{
		 
			if(strpos($_POST['cat_id'], ',') !== false) 
			{
			$data2=explode(",",$_POST['cat_id']);
				foreach ($data2 as $key => $val)
				{
				$values2 = array('book_id' => $inserted_id,'category_id' => $val,'created_on' => $created_on);
				$inserted_id_tag=DB::table('ebook_category')->insertGetId($values2);
				}
			 
			}
			else
			{
				$values2 = array('book_id' => $inserted_id,'category_id' =>$_POST['tag_id'],'created_on' => $created_on);
				$inserted_id_tag=DB::table('ebook_category')->insertGetId($values2);
			}
		}
		
		/* pdf to swf  */
		$output=shell_exec('pdf2swf   /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.pdf -o  /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.swf');
		
		
		
		/* pdf to js */
		$output2=shell_exec('pdf2json /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.pdf -enc UTF-8 -compress /data/ebook-repo/design1/public/pdfjson/'.$file_name_orig_to_convert.'.js');
		sleep(3);
		
		$strJsonFileContents = file_get_contents('/data/ebook-repo/design1/public/pdfjson/'.$file_name_orig_to_convert.'.js');
		
		$json = json_decode($strJsonFileContents);
		$total_pages=$json[0]->pages;
		
		
		DB::table('ebook')
        ->where('id', $inserted_id)
        ->update(		['total_pages' => $total_pages]);
		
		/* pdf to jpg */
		
		shell_exec('convert -density 150 /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.pdf -scene 1 /data/ebook-repo/design1/public/pdfhtml5images/'.$file_name_orig_to_convert.'.jpg');
		
		echo "success";
   		 

         

    }
	
	
	public function storecoverimagepdf(Request $request)

    {
	
		$image = $request->file('image_file');
		$img_ext=$image->getClientOriginalExtension();
		$img_ext=strtolower($img_ext);
     	$new_name = rand() . '.' . $img_ext;
     	$image->move(public_path('pdfimages'), $new_name); 
		
		
         $output = array(
         'success' => 'Image uploaded successfully',
         'image'  => '<img src="'.url('/').'/public/pdfimages/'.$new_name.'" class="img-thumbnail" style="width: 70px he;width: 70px;height: 60px;" />',
		 'file_name_uploaded'  => ''.$new_name.''
        );

        return response()->json($output);
   		 

         

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Ebook $ebook)

    {

        return view('ebooks.show',compact('ebook'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {
			 
 		//echo $ebook;	
		
		$ebooks = DB::select('select * from ebook  where ebook.id='.$id.'');
		$dataCms = DB::select('select * from category order by name_en asc');
 		$dataCms2 = DB::select('select * from tags order by name_en asc');

		$dataCms3 = DB::select("select * from book_tags where book_id='".$id."'");
		$dataCms4 = DB::select("select * from ebook_category where book_id='".$id."'");
		 
        return view('ebooks.edit',['dataCms'=>$dataCms,'dataCms2'=>$dataCms2,'ebooks'=>$ebooks[0],
		'dataCms3'=>$dataCms3,'dataCms4'=>$dataCms4,'update_id'=>$id]);
		

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Ebook $ebook) 

    {
		
		
		
		/*DB::table('ebook')->where('id', $_POST['update_id'])->update([
        [
            'book_name'      		=> $_POST['book_name'],
            'book_description'      => $_POST['book_description'],
            'cat_id'      			=> $_POST['cat_id'],
            'pdf_file'      		=> $_POST['pdf_file']
        ]
    	]);*/
		
		
		 
		DB::table('ebook')
        ->where('id', $_POST['update_id'])
        ->update(		['book_name_en' => $_POST['book_name_en'],
		 				'book_description_en' => $_POST['book_description_en'],
						'book_name_fr' => $_POST['book_name_fr'],
						'book_description_fr' => $_POST['book_description_fr'],
						'book_name_nl' => $_POST['book_name_nl'],
						'book_description_nl' => $_POST['book_description_nl'],
						'book_name_dk' => $_POST['book_name_dk'],
						'book_description_dk' => $_POST['book_description_dk'],
						'book_name_at' => $_POST['book_name_at'],
						'book_description_at' => $_POST['book_description_at'],
						'book_name_de' => $_POST['book_name_de'],
						'book_description_de' => $_POST['book_description_de'],
						'book_name_se' => $_POST['book_name_se'],
						'book_description_se' => $_POST['book_description_se'],
						'book_name_no' => $_POST['book_name_no'],
						'book_description_no' => $_POST['book_description_no'],
						'book_name_it' => $_POST['book_name_it'],
						'book_description_it' => $_POST['book_description_it'],
						'featured' => $_POST['featured'],
						'pdf_file' => $_POST['pdf_file_name_hidden'],
						'cover_image' => $_POST['image_file_name_hidden']]);
		
		
		
		$file_name_orig=explode(".",$_POST['pdf_file_name_hidden']);
		$file_name_orig_to_convert=$file_name_orig[0];
		
 		
 		$created_on=date("Y-m-d H:i:s");
 		 
		DB::table('book_tags')->where('book_id', '=', $_POST['update_id'])->delete();
		if(trim($_POST['tag_id'])!="")
		{
		 
			if(strpos($_POST['tag_id'], ',') !== false) 
			{
			$data2=explode(",",$_POST['tag_id']);
				foreach ($data2 as $key => $val)
				{
				$values2 = array('book_id' => $_POST['update_id'],'tag_id' => $val,'created_on' => $created_on);
				$inserted_id_tag=DB::table('book_tags')->insertGetId($values2);
				}
			  
			}
			else
			{
				$values2 = array('book_id' => $_POST['update_id'],'tag_id' =>$_POST['tag_id'],'created_on' => $created_on);
				$inserted_id_tag=DB::table('book_tags')->insertGetId($values2);
			}
		}
		
		
		DB::table('ebook_category')->where('book_id', '=', $_POST['update_id'])->delete();
		if(trim($_POST['cat_id'])!="")
		{
		 
			if(strpos($_POST['cat_id'], ',') !== false) 
			{
			$data2=explode(",",$_POST['cat_id']);
				foreach ($data2 as $key => $val)
				{
				$values2 = array('book_id' => $_POST['update_id'],'category_id' => $val,'created_on' => $created_on);
				$inserted_id_tag=DB::table('ebook_category')->insertGetId($values2);
				}
			 
			}
			else
			{
				$values2 = array('book_id' => $_POST['update_id'],'category_id' =>$_POST['cat_id'],'created_on' => $created_on);
				$inserted_id_tag=DB::table('ebook_category')->insertGetId($values2);
			}
		}
		 
 		 /* pdf to swf  required */
		$output=shell_exec('pdf2swf   /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.pdf -o  /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.swf');
		
		
		/* pdf to js  required */
		$output2=shell_exec('pdf2json /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.pdf -enc UTF-8 -compress /data/ebook-repo/design1/public/pdfjson/'.$file_name_orig_to_convert.'.js');
		sleep(3);
		
		$strJsonFileContents = file_get_contents('/data/ebook-repo/design1/public/pdfjson/'.$file_name_orig_to_convert.'.js');
		
		$json = json_decode($strJsonFileContents);
		$total_pages=$json[0]->pages;
		
		
		DB::table('ebook')
        ->where('id', $_POST['update_id'])
        ->update(		['total_pages' => $total_pages]);
		
		/* pdf to jpg  required */
		shell_exec('convert -density 150 /data/ebook-repo/design1/public/pdffiles/'.$file_name_orig_to_convert.'.pdf -scene 1 /data/ebook-repo/design1/public/pdfhtml5images/'.$file_name_orig_to_convert.'.jpg');
		echo "success"; 
 		
   		 

         

    }

  

    /**

     * Remove the specified resource from storage.
 
     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Ebook $ebook)

    {

        $ebook->delete();

  

        return redirect()->route('ebooks.index')

                        ->with('success','Ebook deleted successfully');

    }

}