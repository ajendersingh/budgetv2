<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Tag; 


  

class TagController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "Category";
        $this->middleware('auth:admin'); 
    } 

    public function index()

    {
	 
	 	 
	 
        $tags = Tag::orderBy('created_on', 'desc')->paginate(10);
 
        return view('tags.index',compact('tags'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('tags.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $request->validate([

            'name_en' => 'required',

            'name_fr' => 'required',
			
			'name_nt' => 'required',
			
			'name_dm' => 'required',
			
			'name_at' => 'required',
			
			'name_ge' => 'required',
			
			'name_sw' => 'required',
			
			'name_no' => 'required',
					

        ]);

 		$created_on=date("Y-m-d H:i:s");
		
		
        Tag::create(array_merge($request->all(),['created_on' => $created_on]));

   		 

        return redirect()->route('tags.index')

                        ->with('success','Tag created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Tag $tag)

    {

        return view('tags.show',compact('tag'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Tag $tag)

    {

        return view('tags.edit',compact('tag'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Tag $tag)

    {

        $request->validate([

            'name_en' => 'required',

            'name_fr' => 'required',
			
			'name_nt' => 'required',
			
			'name_dm' => 'required',
			
			'name_at' => 'required',
			
			'name_ge' => 'required',
			
			'name_sw' => 'required',
			
			'name_no' => 'required',
					

        ]);

  		

        $tag->update($request->all());

  

        return redirect()->route('tags.index')

                        ->with('success','Tag updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.
 
     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Tag $tag)

    {

        $tag->delete();

  

        return redirect()->route('tags.index')

                        ->with('success','Tag deleted successfully');

    }

}