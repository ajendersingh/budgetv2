<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Faq; 


  

class FaqController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "Category";
        $this->middleware('auth:admin'); 
    } 

    public function index()
 
    {
	 
	 	 
	 
        $faqs = Faq::orderBy('created_on', 'desc')->paginate(10);
 
        return view('faqs.index',compact('faqs'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		 
        return view('faqs.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $request->validate([

            'name_en' => 'required',

            'name_fr' => 'required',
			
			'name_nl' => 'required',
			
			'name_dk' => 'required',
			
			'name_at' => 'required',
			
			'name_de' => 'required',
			
			'name_se' => 'required',
			
			'name_no' => 'required',
			
			'name_it' => 'required',
					

        ]);
		
		 
 		$created_on=date("Y-m-d H:i:s");
	 	 
		
        Faq::create(array_merge($request->all(),['created_on' => $created_on]));

   		 

        return redirect()->route('faqs.index')

                        ->with('success','Faq created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Faq $faq)

    {

        return view('faqs.show',compact('faq'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Faq $faq)

    {

        return view('faqs.edit',compact('faq'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Faq $faq)

    {

        $request->validate([

            'name_en' => 'required',

            'name_fr' => 'required',
			
			'name_nl' => 'required',
			
			'name_dk' => 'required',
			
			'name_at' => 'required',
			
			'name_de' => 'required',
			
			'name_se' => 'required',
			
			'name_no' => 'required',
			
			'name_it' => 'required',
					

        ]);
		
		
		
		
		 $faq->update($request->all());

		
		 
  

        return redirect()->route('faqs.index')

                        ->with('success','Faq updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Faq $faq)

    {

        $faq->delete();

  

        return redirect()->route('faqs.index')

                        ->with('success','Faq deleted successfully');

    }

}