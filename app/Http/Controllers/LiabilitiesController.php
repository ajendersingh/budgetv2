<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Liability; 
use DB; 
use App\Category; 
 


class LiabilitiesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
	 
    public function index(\Illuminate\Http\Request $request, $page = 1)
    {
		$user 				= auth()->user();
		$user_id			= $user->id;
		$items_per_page 	= 15; 
		
		$search_arr			= array(
		'search_text'=>$request->get('search_text'), 
		'trans_type'=>$request->get('trans_type'),  
		'start_date'=>$request->get('start_date'),  
		'end_date'=>$request->get('end_date'));
		
		
		
		
		$liabilities = DB::table('tbl_liability as l')
        ->leftJoin('tbl_accounts as ac','l.linked_account', '=', 'ac.id' )
        ->leftJoin('tbl_liability_types as lt','l.type', '=', 'lt.id' )
       
        ->where('l.user_id', $user_id)
        ->where('l.is_deleted', 0)
        ->orderBy('l.id', 'desc')
        ->select('l.*', 'ac.title as account_name',  'lt.name as type_name')
        ->paginate($items_per_page);
		 
		 $trans_types = DB::table('tbl_transacton_types')->get();
		 if(isset($liabilities[0]))
		 {
			 $i = 0;
			 foreach($liabilities as $liability)
			 {
				 $liabilities[$i]->start_date 			= date("F d, Y", $liabilities[$i]->start_date);
				 $liabilities[$i]->end_date 			= date("F d, Y", $liabilities[$i]->end_date);
				 $liabilities[$i]->amount_of_debt 		= formated_amount1($liabilities[$i]->amount_of_debt);
				 $liabilities[$i]->liability_amount 	= formated_amount1($liabilities[$i]->liability_amount);
				$i++; 
			 }
		 }
		$data = ['active_class'=>'liabilities', 'title'=>"Liabilities","liabilities"=>$liabilities];
		return view('users.liabilities.index', $data);
    }
	
    public function destroy(Category $category)
    {

        $category->delete();
		return redirect()->route('categories.index')
		->with('success','Category deleted successfully');

    }
	
	public function edit($id)
    {
  
		
		
		$user 			= auth()->user();
		$user_id		= $user->id;   
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_liability_types')->get();
		$liability 		= DB::table('tbl_liability')->where('id', $id)->first();
		$p_options      = array();
		$month_days		= array();
		
		 
		$liability->start_date = date("m/d/Y", $liability->start_date);
		$liability->end_date = date("m/d/Y", $liability->end_date);
		$liability->first_go_on = date("m/d/Y", $liability->first_go_on);
		
		if($liability->recurring_period > 1)
		{
			$p_options = DB::table("tbl_period_options")
                ->where("period_id",$liability->recurring_period)->get(); 
		}
		 
		 
		if($liability->repeat_on_every >= 1)
		{
			
				
				if($liability->repeat_on_every == 10)
				{
					for($i=1; $i<=28; $i++)
					{
					$month_days[$i-1] = array('id'=>$i, "name"=>$i);
					}
					
					$month_days = json_decode (json_encode ($month_days), FALSE);
					
					//$month_days = (object)$month_days;
					//$month_days = json_encode($month_days);
				}
				else{
				$month_days = DB::table("tbl_period_options")
                ->where("period_id",$liability->repeat_on_every)->get();	
				}
		}
		
		$first_pay_days = array();
		for($i=1; $i<=15; $i++)
		{
			$first_pay_days[$i-1] = array('id'=>$i, "name"=>$i);
		}
		
		$first_pay_days = json_decode (json_encode ($first_pay_days), FALSE);
					
		$sec_pay_days = array();
		for($i=1; $i<=16; $i++)
		{
			$sec_pay_days[$i-1] = array('id'=>$i+15, "name"=>$i+15);
		}
		
		$sec_pay_days = json_decode (json_encode ($sec_pay_days), FALSE);		
		
		$data_array 	= array('first_pay_days'=>$first_pay_days,'sec_pay_days'=>$sec_pay_days, 'month_days'=>$month_days, 'p_options'=>$p_options, 'liability'=>$liability, 'active_class'=>'liabilities','types'=>$types, 'budgets'=>$budgets, 'categories'=>$categories, 'accounts'=>$accounts,'periods'=>$periods,"errors"=>array() );
		 
		return view('users.liabilities.edit', $data_array );

    }	
	
	public function add()
    {
		 
		$user 			= auth()->user();
		$user_id		= $user->id;   
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_liability_types')->get();
		$first_pay_days = array();
		for($i=1; $i<=15; $i++)
		{
			$first_pay_days[$i-1] = array('id'=>$i, "name"=>$i);
		}
		
		$first_pay_days = json_decode (json_encode ($first_pay_days), FALSE);
					
		$sec_pay_days = array();
		for($i=1; $i<=16; $i++)
		{
			$sec_pay_days[$i-1] = array('id'=>$i+15, "name"=>$i+15);
		}
		
		$sec_pay_days = json_decode (json_encode ($sec_pay_days), FALSE);
		
		$data_array 	= array('first_pay_days'=>$first_pay_days,'sec_pay_days'=>$sec_pay_days, 'active_class'=>'liabilities','types'=>$types, 'budgets'=>$budgets, 'categories'=>$categories, 'accounts'=>$accounts,'periods'=>$periods,"errors"=>array() );
 
        return view('users.liabilities.add', $data_array );

    }
	
	public function repeat_option($id)
	{
		$options = DB::table("tbl_period_options")
                ->where("period_id",$id)
                ->pluck("name","id");
		return response()->json($options);
	}	
	
	public function repeat_month_day($id)
	{
		$options = DB::table("tbl_period_options")
                ->where("period_id",$id)
                ->pluck("name","id");
		return response()->json($options);
	}
	
	 public function set_account_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		if($trans->transaction_type == 'EXPENCES')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$balance = ($account->opening_balance-$trans_amount);
			$row_arr = array('opening_balance'=> $balance, 'balance'=> $balance );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
		}
		
	 }
	 
	public function save_transaction($trans_id)
	{
 		$lib = DB::table('tbl_liability')->where('id', $trans_id)->first();
		$user = auth()->user();
			$user_id	= $user->id;
		 $data_arr = array(
		'title' 					=> $lib->title,
		'liability_id' 				=> $lib->id,
		'transaction_type' 			=> 'EXPENCES',
		'is_recurring_transaction' 	=> $lib->is_recurring_transaction,
		'recurring_period' 			=> $lib->recurring_period,
		'repeat_on_every' 			=> $lib->repeat_on_every,
		'repeat_month_day' 			=> $lib->repeat_month_day,
		'user_id' 					=> $user_id,
		'source_account' 			=> $lib->linked_account,
		'trans_date' 				=> $lib->created_on,
		'trans_amount' 				=> $lib->amount_of_debt,
		
		'first_payment_date' 		=> $lib->first_payment_date,
		'second_payment_date' 		=> $lib->second_payment_date,
				
		'budget' 					=> '',
		'category' 					=> '',
		'tag' 						=> '',
		'description' 				=> $lib->description );
				
				
			 
			
		$msg = '';
		$data_arr['created_by'] = $user_id;
		$data_arr['created_on'] = time();
		DB::table('tbl_transections')->insert($data_arr);
		$id = DB::getPdo()->lastInsertId();
		$trans 			= DB::table('tbl_transections')->where('id', $id)->first(); 
		self::set_account_amount($id, $trans);
 		
 	}
	
	public function save(Request $request)
	{
			$this->validate($request, [
			 'title' => 'required',
			 //'name' => 'required|unique:tbl_budgets|max:255',
			 'type' => 'required', 
			 'liability_amount' => 'required', 
			 'amount_of_debt' => 'required', 
			 'start_date' => 'required', 
			 'end_date' => 'required', 
			 'linked_account' => 'required', 
			]);  
		
		
		
		   
			$user = auth()->user();
			$user_id	= $user->id; 
			 
			$request->session()->put('dataSession', $_POST);
			$dataSession = session()->get('dataSession');
			$trans_id = isset( $dataSession['id'] )&&$dataSession['id']>0?$dataSession['id']:'' ;
			$dataCms = DB::select("select * from tbl_liability where id ='".$dataSession['id']."'");
			 
			 //$period = $dataSession['budget_period'];
			  		
			 $data_arr = array(
				'title' 					=> isset($dataSession['title'] )?$dataSession['title']:'',
				'type' 						=> isset($dataSession['type'])?$dataSession['type']:'',
				'is_recurring_transaction' 	=> isset($dataSession['is_recurring_transaction'])?$dataSession['is_recurring_transaction']:'',
				'recurring_period' 			=> isset($dataSession['recurring_period'])?$dataSession['recurring_period']:'',
				'repeat_on_every' 			=> isset($dataSession['repeat_on_every'])?$dataSession['repeat_on_every']:'',
				'repeat_month_day' 			=> isset($dataSession['repeat_month_day'])?$dataSession['repeat_month_day']:'',
				'user_id' 					=> $user_id,
				'linked_account' 			=> isset($dataSession['linked_account'])?$dataSession['linked_account']:'',
				'start_date' 				=> isset($dataSession['start_date'])?strtotime($dataSession['start_date']):'',
				'end_date' 					=> isset($dataSession['end_date'])?strtotime($dataSession['end_date']):'',
				'first_payment_date' 		=> isset($dataSession['first_payment_date'])?$dataSession['first_payment_date']:'',
				'second_payment_date' 		=> isset($dataSession['second_payment_date'])?$dataSession['second_payment_date']:'',
				'first_go_on' 				=> isset($dataSession['first_go_on'])?strtotime($dataSession['first_go_on']):'',
				'amount_of_debt' 			=> isset($dataSession['amount_of_debt'] )?$dataSession['amount_of_debt']:'',
				'liability_amount' 			=> isset($dataSession['liability_amount'])?$dataSession['liability_amount']:'',
				'description' 				=> isset($dataSession['description'])?$dataSession['description']:'' );
				
				
			 
			
		$msg = '';		
		if(!empty($dataCms[0])) 
		{
			$data_arr['modefied_by'] = $user_id;
			$data_arr['modefied_on'] = time();
			
			DB::table('tbl_liability')
			->where('id', $trans_id)
			->update($data_arr);
			$msg = __('liabilities.liability_updated');
		}
		else
		{
			$data_arr['created_by'] = $user_id;
			$data_arr['created_on'] = time();
			DB::table('tbl_liability')->insert($data_arr);
			$id = DB::getPdo()->lastInsertId();
			  
			//self::set_budget_amount($id, $trans);
			self::save_transaction($id );
			
			$msg = __('liabilities.liability_added');
		}
		return redirect('liabilities')->with('success',$msg);
		
    }
	
	public function unset_account_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		 
		if($trans->transaction_type == 'EXPENCES')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('opening_balance'=> ($account->opening_balance+$trans_amount), 'balance'=> ($account->opening_balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
			
			
		 
			
		}
		
		if($trans->transaction_type == 'REVENUE')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			$row_arr = array('opening_balance'=> ($account->opening_balance-$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
		}	
		
		if($trans->transaction_type == 'TRANSFER')
		{
			$saccount = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('opening_balance'=> ($saccount->opening_balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $saccount->id)
			->update($row_arr);
			
			$daccount = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			
			$row_arr = array('opening_balance'=> ($daccount->opening_balance-$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $daccount->id)
			->update($row_arr);
			
		}
	}
	
	public function unset_budget_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		if($trans->transaction_type == 'EXPENCES' && $trans->budget >0)
		{
			
			$budget = DB::table('tbl_budgets')->where('id', $trans->budget)->first();
			$spent 	= $budget->amount_spent-$trans_amount;
			if($budget->amount_spent == 0 && $budget->amount_left == 0)
			{
				
			}
			else{
				$left 	= $budget->amount_left+$trans_amount;
			}
			$row_arr = array('amount_spent'=>$spent, 'amount_left'=>$left);
			DB::table('tbl_budgets')
			->where('id', $budget->id)
			->update($row_arr);
		}
		
		 
	}
	public function remove_transaction($id)
	{
		 
		$user = auth()->user();
			$user_id	= $user->id; 
			
		$trans 			= DB::table('tbl_transections')->where('id', $id)->first();
		self::unset_account_amount($id, $trans);
		self::unset_budget_amount($id, $trans);
		
		$row_arr = array('is_deleted'=> 0 );
			DB::table('tbl_transections')
			->where('id', $id)
			->update($row_arr);
			
		$msg = __('liabilities.liability_deleted');
		 
		return redirect('liabilities')->with('success',$msg);
	}	
	
	
	public function remove_liability($id)
	{
		 
		$user = auth()->user();
			$user_id	= $user->id; 
			
		$trans 			= DB::table('tbl_liability')->where('id', $id)->first();
		//self::unset_account_amount($id, $trans);
		//self::unset_budget_amount($id, $trans);
		
		$row_arr = array('is_deleted'=> 1 );
			DB::table('tbl_liability')
			->where('id', $id)
			->update($row_arr);
			
		$msg = __('liabilities.liability_deleted');
		 
		return redirect('liabilities')->with('success',$msg);
	}
	
	public function clone_transaction($id)
	{
		 
		
		$trans 			= DB::table('tbl_transections')->where('id', $id)->first();
	 
	 
	 
		$user = auth()->user();
		$user_id	= $user->id; 
		$data_arr = array(
				'title' 					=> $trans->title,
				'transaction_type' 			=> $trans->transaction_type,
				'is_recurring_transaction' 	=> $trans->is_recurring_transaction,
				'recurring_period' 			=> $trans->recurring_period,
				'repeat_on_every' 			=> $trans->repeat_on_every,
				'repeat_month_day' 			=> $trans->repeat_month_day,
				'user_id' 					=> $user_id,
				'source_account' 			=> $trans->source_account,
				'destination_account' 		=> $trans->destination_account,
				'trans_date' 				=> strtotime(date("m/d/Y")),
				'trans_amount' 				=> $trans->trans_amount,
				'budget' 					=> $trans->budget,
				'category' 					=> $trans->category,
				'tag' 						=> $trans->tag,
				'description' 				=> $trans->description );
				
			 
		 
			$data_arr['created_by'] = $user_id;
			$data_arr['created_on'] = time();
			DB::table('tbl_transections')->insert($data_arr);
			$id = DB::getPdo()->lastInsertId();
			 
			self::set_budget_amount($id, $trans);
			self::set_account_amount($id, $trans);
			
			$msg = __('transactions.trans_added ');
		  
		return redirect('transactions')->with('success',$msg); 
	}

	public function show($lib_id, $all='')
    {
		$items_per_page = 25;
		 
		$liability 		= DB::select('select * from tbl_liability where is_deleted=0 and id='.$lib_id.' order by title asc limit 0,1');
		$account_types 	= DB::select('select id, name from tbl_account_types where is_deleted=0 order by name asc');
		$account_roles 	= DB::select('select id,name from tbl_account_roles where is_deleted=0 order by name asc');
		//$transactions  	= DB::select('select * from tbl_transections where ( source_account='.$account.' or destination_account='.$account.') and  is_deleted=0 ');
		if($all == '')
		{
			$search_s_date = strtotime(date("Y-m")."-01 00:00:00"); 
			$search_e_date = strtotime(date("Y-m")."-".getLastDateOfMonth()." 23:59:59");
			$start_date = "1 ".__('common.'.date("F")).", ".date("Y"); 
			$end_date	= getLastDateOfMonth()." ".__('common.'.date("F")).", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
			
			 $diff 				= $search_e_date-$search_s_date; 
			
			 $diff_days 		= abs(round($diff / 86400));			
			
			
		}
		else
		{
			$search_s_date = strtotime(date("Y")."-01-01 00:00:00"); 
			$search_e_date = strtotime(date("Y")."-12-31 23:59:59");
			$start_date 		= "1 ".__('common.January').", ".date("Y"); 
			$end_date			= "31 ".__('common.December').", ".date("Y");
			$month_date			= $start_date." and ".$end_date;
			$diff 				=$search_e_date-$search_s_date; 
			$diff_days 			= abs(round($diff / 86400));
		}
		 
	 
		$transactions = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        
		/*
        ->where(function($q) use ($account) { 
		$q->where('sac.id',$account )
       ->orWhere('dac.id',$account);
		})*/
	
        ->where('tbl_transections.liability_id',$lib_id)
        ->where('tbl_transections.trans_date', ">=", $search_s_date)
        ->where('tbl_transections.trans_date', "<=", $search_e_date)
		
        ->where('tbl_transections.is_deleted', 0)
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name')
        ->paginate($items_per_page);
		
		
		
		$liability  		= isset($liability[0])?$liability[0]:array();
		
		$labels 			= array();
		$stats				= array();
		$chart_data			= array();
		
		if( isset($transactions[0]) )
		{
			$i = 0;
			foreach($transactions as $trans)
			{
				$transactions[$i]->trans_date = date("d", $transactions[$i]->trans_date).__('common.'.date("F", $transactions[$i]->trans_date)).", ".date("Y", $transactions[$i]->trans_date);
				$transactions[$i]->trans_amount = formated_amount1($transactions[$i]->trans_amount);
				$i++;
			}
			  
			$start_ldate = date("d-m-Y", $search_s_date);
			for($d=0; $d < $diff_days; $d++ )
			{
				
				if($d >0)
				{
					$start_ldate = date('d-m-Y', strtotime($start_ldate . ' +1 day'));
				}
				
			 
				$start_label = strtotime($start_ldate." 00:00:00");
				$end_label 	 = strtotime($start_ldate." 23:59:59");
				  	
					/*
					$chart_amt 		= DB::select('select source_account, destination_account, trans_amount from 
					tbl_transections where 
					trans_date >= '.$start_label.' and trans_date <= '.$end_label.' and 
					is_deleted=0 and ( source_account='.$account_id.' or  destination_account='.$account_id.' )');
					
					 
 						
					if(isset($chart_amt[0]))
					{
						foreach($chart_amt as $amt)
						{
							$stats['label'][$d] 	= date("F d, Y", $start_label) ;
							$stats['color'][$d] 	= "#3e95cd";
							if( isset($amt->source_account) && $amt->source_account >0 )
							{
								$stats['data'][$d] 		= isset($amt->trans_amount)&&$amt->trans_amount > 0?"-".$amt->trans_amount:0;
							}
							else if(isset($amt->destination_account) && $amt->destination_account >0 )
							{
								$stats['data'][$d] 		= isset($amt->trans_amount)&&$amt->trans_amount > 0?$amt->trans_amount:0;	
							}
							else
								$stats['data'][$d] = 0;
							

 							
						}
					}	
				else{}*/
					
					$stats['label'][$d] 	= date("F d, Y", $start_label) ;
								$stats['data'][$d] 		= 0;
								//$stats['color'][$d+1] 	= "#3e95cd";
				 					
						       
							 
				 
				 
			}
		}
	 
 
		
		 $chart_data  		= json_encode($stats);
		
		$day_transactions = array();
		 
		  $data_array = array( 
		  'start_date'=>$start_date, 
		  'all'=>$all, 
		  'chart_data'=>$chart_data, 
		  'end_date'=>$end_date, 
		  'month_date'=>$month_date,
		  'active_class'=>'liabilities', 
		  'transactions'=>$transactions,
		  'currency'=>__('common.currency_symbol'),
		  'chart_title'=>__('account_detail.chart_title')." ".$month_date,		  
		  'title'=>"Liability",
		  'liability'=>$liability, 
		  'account_types'=>$account_types,
		  'account_roles'=>$account_roles,
		  "errors"=>array() 
		  );
		 // print_r($data_array);die();
        return view('users.liabilities.show', $data_array );

    }


}
