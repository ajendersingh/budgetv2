<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

class FileUploadController extends Controller
{
    function index()
    {
     return view('file_upload');
    }

    function upload(Request $request)
    {
     $rules = array(
      'file'  => 'required|mimetypes:application/pdf|max:10240'
     );

     $error = Validator::make($request->all(), $rules);

     if($error->fails())
     {
      return response()->json(['errors' => $error->errors()->all()]);
     }

     $image = $request->file('file');
		
     $new_name = rand() . '.' . $image->getClientOriginalExtension();
     $image->move(public_path('pdffiles'), $new_name);

     $output = array(
         'success' => 'Pdf uploaded successfully',
         'image'  => '<img src="'.url('/').'/img/pdf.png" class="img-thumbnail" style="width: 70px he;width: 70px;height: 60px;" />',
		 'file_name_uploaded'  => ''.$new_name.''
        );

        return response()->json($output);
    }
	
	function store(Request $request)
	{
		echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		
		die();
		
	}
}

?>
