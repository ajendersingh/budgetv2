<?php

namespace App\Http\Controllers;
 
use App\User;
use Illuminate\Http\Request;
use Redirect,Response;
Use DB;
use Carbon\Carbon;
 
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(\Illuminate\Http\Request $request)
    {
		
		$user = auth()->user();
		$user_id	= $user->id;
		
		 
		$dates = array('start_date'=>$request->get('start_date'), 'end_date'=>$request->get('end_date'));
		
		
		$date               = setSearchDate($dates);
		$start_date 		= $date['start_date']; 	 
		$end_date 			= $date['end_date']; 
		
		$s_time         = strtotime($start_date);
		$e_time         = strtotime($end_date);
		if($request->get('start_date') != '')
		{
			$search_s_date = strtotime(date("d-m-Y", $s_time)." 00:00:00"); 
			$start_date = date("d", $s_time).__('common.'.date("F", $s_time)).", ".date("Y", $s_time);
		}
		else
		{
			$search_s_date = strtotime("1-".date("m-Y", $s_time)." 00:00:00"); 
			$start_date = "1 ".__('common.'.date("F", $s_time)).", ".date("Y", $s_time);
		}
		
		if($request->get('end_date') != '')
		{
			$search_e_date = strtotime(date("d-m-Y", $e_time)." 23:59:59"); 
			$end_date	= date("d", $e_time)." ".__('common.'.date("F", $e_time)).", ".date("Y", $e_time);
		}
		else
		{
			$search_e_date = strtotime(getLastDateOfMonth()."-".date("m-Y", $e_time)." 23:59:59"); 
			$end_date	= getLastDateOfMonth()." ".__('common.'.date("F", $e_time)).", ".date("Y", $e_time);
		}
 		 
		
		$month_date	= $start_date." and ".$end_date;
 	
	
		$accounts 			= self::get_all_accounts($user_id);
		$labels 			= array();
		$stats				= array();
		
		//echo strtotime($start_date)." ".strtotime($end_date)." ".$user_id;
		
		
		if( isset($accounts[0]) )
		{
			$i = 0;
			foreach($accounts as $account)
			{
				$stats['label'][$i] 	= $account->title;
				$balance 				= self::getBalance($user_id, $search_s_date, $search_e_date, $account->id);
				$stats['data'][$i] 		= isset($balance)&&$balance > 0?$balance:0;
				//$stats['data'][$i] 		= __('accounts.currency')." ".$stats['data'][$i];
				$stats['color'][$i] 	= "#3e95cd";
				$i++;
				
			}
		}
	 
 	$income				= self::get_expences_revenue($user_id, $search_s_date, $search_e_date, 'REVENUE');
	$expences			= self::get_expences_revenue($user_id, $search_s_date, $search_e_date, 'EXPENCES');
	$budgets			= self::get_budgets($user_id, $search_s_date, $search_e_date);
	$transactions       = self::get_front_transections($user_id, $search_s_date, $search_e_date); 
	$listaccounts 		=  DB::select('select id,title,balance from tbl_accounts where is_deleted=0 and user_id='.$user_id.' order by title asc   ');
	$available_balance	= 0;
	if( isset($listaccounts[0]) )
		{
			$i = 0;
			foreach($listaccounts as $listaccount)
			{
				$available_balance = formated_amount1($available_balance+$listaccount->balance);
				$listaccounts[$i]->balance = formated_amount1($listaccount->balance);
				$i++;
				
			}
		}
		
		
	$provisonal_bal 	= ($income-$budgets);
	$left_to_spent 	    = ($budgets-$expences);
	//echo '<pre>';
	//print_r($transactions); die();
 
    $chart_data  		= json_encode($stats);
    
    $account_info  		= $transactions['account_info'];
	$transactions  		= $transactions['tansections'];
	$net_deposit 		= 0;
	$total_budget 		= 0;
	$provisonal_amount 	= 0;
	$total_expences 	= 0;
	$remaining_balance 	= 0;
	
	$accounts_list      = DB::select('select id from tbl_accounts where is_deleted=0 and user_id='.$user_id);
	$budgets_list       = DB::select('select id from tbl_budgets where is_deleted=0 and user_id='.$user_id);
	$cat_list       	= DB::select('select cat_id from tbl_categories where cat_is_deleted=0 and cat_user_id='.$user_id);
	$liability_list     = DB::select('select id from tbl_liability where is_deleted=0 and user_id='.$user_id);
	$show_add_account   = true;
	if( isset($accounts_list[0]) )
	{
		$show_add_account   = false;
	}
	
	$show_add_budget   = true;
	if( isset($budgets_list[0]) )
	{
		$show_add_budget   = false;
	}	
	
	$show_add_cat   = true;
	if( isset($cat_list[0]) )
	{
		$show_add_cat   = false;
	}	
	
	$show_add_lib   = true;
	if( isset($liability_list[0]) )
	{
		$show_add_lib   = false;
	}
		 $data = [
		 'show_add_account'=>$show_add_account, 
		 'show_add_budget'=>$show_add_budget, 
		 'show_add_cat'=>$show_add_cat, 
		 'show_add_lib'=>$show_add_lib, 
		 'active_class'=>'dashboard', 
		 'listaccounts'=>$listaccounts, 
		 'available_balance'=>$available_balance, 
		 'transactions'=>$transactions, 
		 'account_info'=>$account_info, 
		 'month_date'=>$month_date, 
		 'income'=>formated_amount1($income), 
		 'bar_chart_title'=>__('user_dashboard.withdrwn_amount')." ".$month_date,
		 'currency'=>__('common.currency_symbol'),
		 'provisonal_bal'=>formated_amount1($provisonal_bal), 
		 'expences'=>formated_amount1($expences), 
		 'budgets'=>formated_amount1($budgets), 
		 'left_to_spent'=>formated_amount1($left_to_spent), 
		 'title'=>"Transactions", 
		 'chart_data'=>$chart_data
		 ];
         return view('users.dashboard', $data);
    }
	public function get_all_accounts($user_id)
    {
        return DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc' );
    }
	public function get_expences_revenue($user_id, $start_date, $end_date, $transe_type)
	{
		$trans_amount = DB::table('tbl_transections')
		->where('transaction_type',$transe_type )
	  ->where('user_id',  $user_id)
	  ->where('trans_date','>=',  $start_date)
	  ->where('trans_date', '<=', $end_date)
	  ->where('is_deleted',0)->sum('trans_amount');
	   
	  //echo $this->db->last_query();
	  return isset($trans_amount) && $trans_amount >0?$trans_amount:0;
	}
	
	
	public function getBalance($user_id, $start_date, $end_date, $account_id='')
	  {
		 
		 return  DB::table('tbl_transections')
		  ->where('trans_date', '>=',  $start_date)
		  ->where('trans_date', '<=', $end_date)
		  ->where('user_id', $user_id)
		  ->where('source_account', $account_id)
		  ->where('is_deleted',0)->sum('trans_amount');
		   
	  } 
  //1590969600
  //1591042800
  
	 public function get_budgets($user_id, $start_date, $end_date)
	  {
		 $budgets 	=  DB::select('select b.budget_amount from tbl_budgets as b 
		inner join tbl_periods as p on p.id = b.budget_period where b.is_deleted=0 and b.user_id='.$user_id.' order by b.name asc');
		$total_budget = 0;
		if(isset($budgets[0]))
		{
			$i = 0;
			foreach($budgets as $bdg)
			{ 
				 
				$total_budget 				= $total_budget+$bdg->budget_amount;
				 
				
				$i++;
			}
		}
		return $total_budget;
		/*return DB::table('tbl_budgets')
		->where('user_id', $user_id)
		->where('is_active', 1)
		->where('is_deleted', 0)->sum("budget_amount");*/
	  }
	public function get_account_transections($user_id, $account_id, $start_date, $end_date)
	{
		$transactions = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        ->where('tbl_transections.user_id',  $user_id)
        ->where('tbl_transections.is_deleted', 0)
		/*
		->where(function($q) use ($account_id) { 
		$q->where('sac.id',$account_id )
       ->orWhere('dac.id',$account_id);
		})*/
		->where('tbl_transections.trans_date', ">=", $start_date)
        ->where('tbl_transections.trans_date', "<=", $end_date)
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name')
       ->paginate(30);
		$i=0;
		 
		if( isset($transactions[0]) )
		{
			$i = 0;
			foreach($transactions as $trans)
			{
				$transactions[$i]->trans_date = date("d", $transactions[$i]->trans_date).__('common.'.date("F", $transactions[$i]->trans_date)).", ".date("Y", $transactions[$i]->trans_date);
				$transactions[$i]->trans_amount = formated_amount1($transactions[$i]->trans_amount);
				$i++;
			}
		}
		
		 return $transactions ;
	 
	} 
	public function commonAccount($user_id, $start_date, $end_date)
	{
	
		$account =  DB::select('select source_account, count(source_account) c from tbl_transections where user_id='.$user_id.' and trans_date>='.$start_date.' and trans_date<='.$end_date.' and source_account>0 group by source_account order by c desc limit 1');
		
		
		return isset($account[0]->source_account)&&$account[0]->source_account>0?$account[0]->source_account:0;
	}
	 
	public function get_front_transections($user_id, $start_date, $end_date)
	{
		
		
		$common_account = self::commonAccount($user_id, $start_date, $end_date);
		if($common_account > 0)
		{
			 
			$account_id 		= $common_account;
			 
			$account_info 		= DB::table('tbl_accounts')->where('id', $account_id)->first(); 
			$account_name 		= $account_info->title;
			$tansections = self::get_account_transections($user_id, $account_id,$start_date, $end_date);
		}
		else
		{
			$tansections = array();
			$account_info = array();
		}
		 
		return array( 'tansections'=>$tansections, 'account_info'=>$account_info );
	}

	
	public function logout(Request $request)
	{
	
	echo "sdfsdfsd";
	die();
	Auth::guard('admin')->logout();
	
	$request->session()->flush();
	
	$request->session()->regenerate();
	
	return redirect()->guest(route( 'admin.login' ));
	}
}
