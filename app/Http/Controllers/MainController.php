<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\Store;

use Illuminate\Support\Facades\Mail;
 
use Illuminate\Support\Facades\Hash;


use Illuminate\Support\Facades\Input;
use App\User;
use Auth;
use Redirect; 

use DB;
use Validator;

use App\Mail\WelcomeMail;
 


use Session;



class MainController extends Controller
{
    //
	
	 
	function index()
	{
	
	
		/*if (Auth::user()) 
		{ 
		return view('userdashboard');
		}
		else
		{
		return view('home');
		}*/
		
		//return view('books');
		return redirect()->action('CatController@show', ['category_id' => 1]);
	}
	
  	function home()
	{
	
 		$testimonials = DB::select('select * from testimonials ');  
		$benefits = DB::select('select * from contents where id=6'); 
		$seoData = DB::select('select * from seos where id=1');
		$features = DB::select('select * from features where is_deleted=0 order by id asc limit 0,3');
		$faqData = DB::select("select * from faq where faq_type='faq'");
 		$faqDataTech = DB::select("select * from faq where faq_type='technical_problems'");
		return view('home',['features'=>$features, 'testimonials'=>$testimonials,'benefits'=>$benefits[0],'seoData'=>$seoData[0],'faqData'=>$faqData,'faqDataTech'=>$faqDataTech]);
		 
	}
	
	function checklogin(Request $request)
	{
		$request->validate([

            'email' => 'required|email',
            'password' => 'required',

        ]);
		$user_data=array(
			'email' => $request->post('email'),
            'password' => $request->post('password'),
		);
		if(Auth::attempt($user_data))
		{
		return redirect('budgets');
		}
		else
		{
		return back()->with('error','Invalid login details');
		}
	}
	
	
	function successlogin()
	{
	
		if(Auth::guard('admin')->check())
		{
		return view('admin');
		} 
		else
		{
		return redirect('login');
		}
	}
	
	public function cancel_account()
	{
         
		$dataCms = DB::select('select * from contents where id=9'); 
		
		$dataCms2 = DB::select('select * from contents where id=10'); 
		$seoData = DB::select('select * from seos where id=5'); 
		$faqData = DB::select("select * from faq where faq_type='faq'");
 		$faqDataTech = DB::select("select * from faq where faq_type='technical_problems'");
 		return view('cancel',['dataCms'=>$dataCms[0],'dataCms2'=>$dataCms2[0],'seoData'=>$seoData[0],'faqData'=>$faqData,'faqDataTech'=>$faqDataTech]);
		 
    }
	
	 
	public function pricing()
	{
         
		 
		if (Auth::user()) 
		{ 
 		 return redirect()->action('DashboardController@index');
		}
		else
		{
		$dataCms = DB::select('select * from contents where id=24');  
		$countrydata = DB::select('select * from apps_countries '); 
		$seoData = DB::select('select * from seos where id=3');
		return view('pricing',['dataCms'=>$dataCms[0],'countrydata'=>$countrydata,'seoData'=>$seoData[0]]);
		} 
		
		 
    }
	
	
	public function pricing_step1()
	{
         
		 
		 if (Auth::user()) 
		{ 
 		 return redirect()->action('DashboardController@index' );
		}
		else
		{ 
		$dataCms = DB::select('select * from contents where id=52');  
		$seoData = DB::select('select * from seos where id=16');
 		return view('pricing_step1',['seoData'=>$seoData[0],'dataCms'=>$dataCms[0]]);
	}
		
		
		 
    }
	
	public function pricing_step2($plan_id)
	{
         
		if (Auth::user()) 
		{ 
 		 return redirect()->action('DashboardController@index');
		}
		else
		{ 
			Session::put('userPricingPlan', $plan_id);
			$dataCms = DB::select('select * from contents where id=24');  
			$countrydata = DB::select('select * from apps_countries '); 
			$seoData = DB::select('select * from seos where id=3');
			return view('pricing',['dataCms'=>$dataCms[0],'countrydata'=>$countrydata,'seoData'=>$seoData[0],'plan_id'=>$plan_id]);
		}
		
		
		 
    }
	
	public function pricing_post(Request $request)
	{
	
	 
         
		 $this->validate(
			$request,
			[
			'first_name' => 'required',
			'last_name' => 'required',
            'address' => 'required',
			'city' => 'required',
			'country_code' => 'required',
			'zip_code' => 'required',
			'email' => 'required|email|unique:users|max:255',
			'phone' => 'required',
			'terms_check' => 'required'
        	],
			[
			'first_name.required' => __('pricing_validation_messages.first_name'),
			'last_name.required' => __('pricing_validation_messages.last_name'),
            'address.required' => __('pricing_validation_messages.address'),
			'city.required' => __('pricing_validation_messages.city'),
			'country_code.required' => __('pricing_validation_messages.country_code'),
			'zip_code.required' => __('pricing_validation_messages.zip_code'),
			'email.required' => __('pricing_validation_messages.email'),
			'phone.required' => __('pricing_validation_messages.phone'),
			'terms_check.required' => __('pricing_validation_messages.terms_check')
        	]);
			
  		
		$request->session()->put('userPricingSession', $_POST);
		
		$userPricingSession = session()->get('userPricingSession');
		$dataCms = DB::select("select * from users where email='".$userPricingSession['email']."'");
		
		if(!empty($dataCms[0])) 
		{
			$password_user=$this->random_strings(8);
			$user_existing_data=$dataCms[0];
			$existing_user_id=$user_existing_data->id;
		
			/* if user already exist update user password, profile,send email, auto login and redirect to next step*/
			DB::table('users')
			->where('id', $existing_user_id)
			->update([
				'name' => $userPricingSession['first_name']." ".$userPricingSession['last_name'],
				'first_name' => $userPricingSession['first_name'],
				'last_name' => $userPricingSession['last_name'],
				'address' => $userPricingSession['address'],
				'pass' => $password_user,
				'zip_code' => $userPricingSession['zip_code'],
				'country_id' => $userPricingSession['country_code'],
				'city' => $userPricingSession['city'],
				'email' => $userPricingSession['email'],
				'phone' => $userPricingSession['phone'],
				'password' => Hash::make($password_user)
			
			]
			);
			
			
			
			 
   		}
		else
		{
		 
			/*create random password for user */	
			$password_user=$this->random_strings(8);
		
		 
			/*register user */	
			
			$user =User::create([
				'name' => $userPricingSession['first_name']." ".$userPricingSession['last_name'],
				'first_name' => $userPricingSession['first_name'],
				'last_name' => $userPricingSession['last_name'],
				'address' => $userPricingSession['address'],
				'zip_code' => $userPricingSession['zip_code'],
				'country_id' => $userPricingSession['country_code'],
				'city' => $userPricingSession['city'],
				'email' => $userPricingSession['email'],
				'phone' => $userPricingSession['phone'],
				'password' => Hash::make($password_user)
			]);
		
		
		
    	}
		
 		return redirect('subscribe-membership');
		
		
    }
	 
	
	public function store(Request $request){ 
 
        // validate fields
		
		$this->validate(
			$request,
			[
			'email' => 'required',
			'order_id' => 'required'
        	],
			[
			'email.required' => __('cancel_page.email_validation'),
			'order_id.required' => __('cancel_page.order_id_validation')
        	]);
		  
		  /*echo "<pre>";
		  print_r($_POST);
		  echo "</pre>";
		  die();*/
		/* proceed here with cancellation process*/
		 
         
 
    }
	
	
	public function view_profile()
	{
       if(isset(Auth::user()->id) ) 
	   {		   
		$user_id = Auth::user()->id;
		$orderDataUser=array();
		$membershipDataUser=array();
		 
		$userData = DB::select('select * from users  where id='.$user_id.''); 
		$orderData = DB::select('select * from order_data  where user_id='.$user_id.' order by id desc limit 1'); 
		
		if(!empty($orderData))
		{
		$orderDataUser=$orderData[0];
		$membershipData = DB::select('select * from user_membership  where user_id='.$user_id.' order by id desc limit 1'); 
		
		$membershipDataUser=$membershipData[0];
		
		 
		}
		 
		 
		$dataCms = DB::select('select * from contents where id=24');  
		$countrydata = DB::select('select * from apps_countries '); 
		$seoData = DB::select('select * from seos where id=3');
		return view('viewprofile',['dataCms'=>$dataCms[0],'countrydata'=>$countrydata,'seoData'=>$seoData[0],'userData'=>$userData[0],'orderDataUser'=>$orderDataUser,'membershipDataUser'=>$membershipDataUser]); 
		 
	   }
	   else{
		   
	   }
		
		 
    } 
	
	public function update_profile()
	{
         
		$user_id = Auth::user()->id;
		DB::table('users')
			->where('id', $user_id)
			->update([
				'name' => $_POST['first_name']." ".$_POST['last_name'],
				'first_name' => $_POST['first_name'],
				'last_name' => $_POST['last_name'],
				'address' => $_POST['address'],
				'zip_code' => $_POST['zip_code'],
				'country_id' => $_POST['country_code'],
				'city' => $_POST['city'],
				'phone' => $_POST['phone']
			
			]
			);
		
		return redirect('view-profile')->with('success',__('update_profile.profile_updated'));
		 
		
		
		 
    } 
	
	function logout()
	{
 	Session::flush ();
	Auth::logout();
	return redirect('login');
	}
	
	public function random_strings($length_of_string) 
	{ 
        
    return substr(sha1(time()), 0, $length_of_string); 
	} 
}
