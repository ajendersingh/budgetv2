<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User; 
use DB; 
use App\Category; 
use Validator;
use Illuminate\Support\Facades\Hash;
 
use Session;
class AccountsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = auth()->user();
		$user_id	= $user->id;
		$accounts =  DB::select('select * from tbl_accounts where is_deleted=0 and user_id='.$user_id.' order by title asc   ');
		
		
		$search_s_date = strtotime("1-".date("m-Y")." 00:00:00"); 
		$search_e_date = strtotime(getLastDateOfMonth()."-".date("m-Y")." 23:59:59");
		
		$total_spent = 0;
		$total_left = 0;
		$total_budget = 0;
		$total_balance = 0;
		 
		if(isset($accounts[0]))
		{
			$i = 0;
			foreach($accounts as $act)
			{
				$bdg_id 					= $act->id;
				/*
				$spent  					= self::budget_expences($bdg_id, $search_s_date, $search_e_date);
				$budgets[$i]->spent  		= $spent; 
				$budgets[$i]->left  		= $act->budget_amount-$spent;
				
				$total_balance 				= $total_balance+$act->budget_amount;
				$total_spent 				= $total_spent+$spent;
			 */
			 $accounts[$i]->balance   = formated_amount1($accounts[$i]->balance);
				 
				$i++;
			}
		}
		
		//$pct_spent = round(($total_spent/$total_balance)*100);
		 
		
		$start_date = "1 ".__('common.'.date("F")).", ".date("Y"); 
		$end_date	= getLastDateOfMonth()." ".__('common.'.date("F")).", ".date("Y");
		$month_date	= $start_date." and ".$end_date;
		
		
		$data = [
		'start_date'=>$start_date, 
		'end_date'=>$end_date, 
		'month_date'=>$month_date,
		'total_spent'=>$total_spent,
		'total_left'=>$total_left,
		
		'active_class'=>'accounts', 
		'title'=>"Accounts",
		"accounts"=>$accounts
		];
		return view('users.accounts.accounts', $data);
    }
	
    public function destroy($account_id)
    {
		$user = auth()->user();
		$user_id	= $user->id;
	
		DB::table('tbl_accounts')
			->where('id', $account_id)
			->update([
			'is_deleted'=>1,
			'is_modefied_on'=>time(),
			'modefied_by'=>$user_id,
			]);
			
       return redirect()->route('accounts.index')->with('success','Account deleted successfully');

    }    
	
	public function delete_account($account_id)
    {
		$user = auth()->user();
		$user_id	= $user->id;
		DB::table('tbl_accounts')
			->where('id', $account_id)
			->update([
			'is_deleted'=>1,
			'is_modefied_on'=>time(),
			'modefied_by'=>$user_id,
			]);
			
       return redirect()->route('accounts.index')->with('success','Account deleted successfully');

    }
	
	
	
	public function show($account, $all='')
    {
		$items_per_page = 25;
		$user = auth()->user();
		$user_id	= $user->id;
		$account_id 	= $account;
		$accounts 		= DB::select('select * from tbl_accounts where is_deleted=0 and id='.$account.' and user_id='.$user_id.' order by title asc limit 0,1');
		$account_types 	= DB::select('select id, name from tbl_account_types where is_deleted=0 order by name asc');
		$account_roles 	= DB::select('select id,name from tbl_account_roles where is_deleted=0 order by name asc');
		//$transactions  	= DB::select('select * from tbl_transections where ( source_account='.$account.' or destination_account='.$account.') and  is_deleted=0 ');
		if($all == '')
		{
			$search_s_date = strtotime(date("Y-m")."-01 00:00:00"); 
			$search_e_date = strtotime(date("Y-m")."-".getLastDateOfMonth()."23:59:59");
			$start_date = "1 ".__('common.'.date("F")).", ".date("Y"); 
			$end_date	= getLastDateOfMonth()." ".__('common.'.date("F")).", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
			
			 $diff 				= $search_e_date-$search_s_date; 
			
			 $diff_days 		= abs(round($diff / 86400));			
			
			
		}
		else
		{
			//$search_s_date = strtotime("01-01".date("Y")." 00:00:00"); 
			$search_s_date = strtotime(date("Y")."-01-01 00:00:00"); 
			//$search_e_date = strtotime("31-12-".date("Y")." 23:59:59");
			$search_e_date = strtotime(date("Y")."-12-31 23:59:59");
			$start_date = "1 ".__('common.January').", ".date("Y"); 
			$end_date	= "31 ".__('common.December').", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
			$diff 				=$search_e_date-$search_s_date; 
			$diff_days 		= abs(round($diff / 86400));
		}
		 
	 
		$transactions = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        
		
        ->where(function($q) use ($account) { 
		$q->where('sac.id',$account )
       ->orWhere('dac.id',$account);
		})
	
        ->where('tbl_transections.trans_date', ">=", $search_s_date)
        ->where('tbl_transections.trans_date', "<=", $search_e_date)
		
        ->where('tbl_transections.is_deleted', 0)
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name')
        ->paginate($items_per_page);
		
		
		
		$account  		= isset($accounts[0])?$accounts[0]:array();
		if(!isset($account->id))
		{
			$msg = __('accounts.account_not_exist');
			return redirect()->route('accounts.index')->with('success',$msg);
		}
		$labels 			= array();
		$stats				= array();
		$chart_data			= array();
		
		if( isset($transactions[0]) )
		{
			$i = 0;
			foreach($transactions as $trans)
			{
				$transactions[$i]->trans_date = date("d", $transactions[$i]->trans_date).__('common.'.date("F", $transactions[$i]->trans_date)).", ".date("Y", $transactions[$i]->trans_date);
				$transactions[$i]->trans_amount = formated_amount1($trans->trans_amount);
				$i++;
			}
			  
			//echo $diff_days;
			$start_ldate = date("d-m-Y", $search_s_date);
			 
			for($d=0; $d < $diff_days; $d++ )
			{
				
				if($d >0)
				{
					$start_ldate = date('d-m-Y', strtotime($start_ldate . ' +1 day'));
				}
				
			 
				$start_label = strtotime($start_ldate." 00:00:00");
				$end_label 	 = strtotime($start_ldate." 23:59:59");
				  	
					
					$chart_amt 		= DB::select('select source_account, destination_account, sum(trans_amount) as trans_amount from 
					tbl_transections where 
					trans_date >= '.$start_label.' and trans_date <= '.$end_label.' and 
					is_deleted=0 and ( source_account='.$account_id.' or  destination_account='.$account_id.' )');
					
					 
 						
					if(isset($chart_amt[0]))
					{
						foreach($chart_amt as $amt)
						{
							$stats['label'][$d] 	= date("F d, Y", $start_label) ;
							$stats['color'][$d] 	= "#3e95cd";
							if( isset($amt->source_account) && $amt->source_account >0 )
							{
								$stats['data'][$d] 		= isset($amt->trans_amount)&&$amt->trans_amount > 0?"-".$amt->trans_amount:0;
							}
							else if(isset($amt->destination_account) && $amt->destination_account >0 )
							{
								$stats['data'][$d] 		= isset($amt->trans_amount)&&$amt->trans_amount > 0?$amt->trans_amount:0;	
							}
							else
								$stats['data'][$d] = 0;
							

 							
						}
					}	
				else{
					
					$stats['label'][$d] 	= date("F d, Y", $start_label) ;
					$stats['data'][$d] 		= 0;
								//$stats['color'][$d+1] 	= "#3e95cd";
				}					
						       
							 
				 
				 
			}
		}
		else{
			
			$start_ldate = date("d-m-Y", $search_s_date);
			for($d=0; $d < $diff_days; $d++ )
			{
				
				if($d >0)
				{
					$start_ldate = date('d-m-Y', strtotime($start_ldate . ' +1 day'));
				}
					$stats['label'][$d] 	= date("F d, Y", strtotime($start_ldate)) ;
					$stats['data'][$d] 		= 0;
			}
		}
	 
 
		
		 $chart_data  		= json_encode($stats);
		
		$day_transactions = array();
		 
		  $data_array = array( 
		  'start_date'=>$start_date, 
		  'all'=>$all, 
		  'chart_data'=>$chart_data, 
		  'end_date'=>$end_date, 
		  'month_date'=>$month_date,
		  'active_class'=>'accounts', 
		  'transactions'=>$transactions,
		  'currency'=>__('common.currency_symbol'),
		  'chart_title'=>__('account_detail.chart_title')." ".$month_date,		  
		  'title'=>"Accounts",
		  'account'=>$account, 
		  'account_types'=>$account_types,
		  'account_roles'=>$account_roles,
		  "errors"=>array() 
		  );
		 // print_r($data_array);die();
        return view('users.accounts.show', $data_array );

    }
	
	
	public function edit($account)
    {
 
		$accounts =  DB::select('select * from tbl_accounts where is_deleted=0 and id='.$account.' order by title asc limit 0,1');
		//$accounts = DB::table('tbl_accounts')->where('id' ,$account)->value('*');
		
		$account_types = DB::select('select id, name from tbl_account_types where is_deleted=0 order by name asc');
		$account_roles = DB::select('select id,name from tbl_account_roles where is_deleted=0 order by name asc');
 
		 $account  = isset($accounts[0])?$accounts[0]:array();
		  //echo '<pre>';
		  //print_r($account_types);die();
		  $data_array = array( 'active_class'=>'accounts', 'title'=>"Accounts",'account'=>$account, 'account_types'=>$account_types,'account_roles'=>$account_roles,"errors"=>array() );
		   
		  
		 // print_r($data_array);die();
        return view('users.accounts.edit', $data_array );

    }	
	
	public function add()
    {
		
		 
		$account_types = DB::select('select id, name from tbl_account_types where is_deleted=0 order by name asc');
		$account_roles = DB::select('select id,name from tbl_account_roles where is_deleted=0 order by name asc');
 
		 $account  = isset($accounts[0])?$accounts[0]:array();
		  //echo '<pre>';
		  //print_r($account_types);die();
		  $data_array = array( 'active_class'=>'accounts', 'title'=>"Accounts", 'account_types'=>$account_types,'account_roles'=>$account_roles,"errors"=>array() );
		   
		  
		 // print_r($data_array);die();
        return view('users.accounts.add', $data_array );

    }
	
	public function update (Request $request)
	{
		 $this->validate(
			$request,
			[
			'title' => 'required',
			'account_type' => 'required' 
			],
			[
			'title.required' => __('Accounts.account_name_required'),
			'account_type.required' => __('Accounts.account_type_required') 
        	]);
			
			 
			$request->session()->put('userAccountSession', $_POST);
			$userAccountSession = session()->get('userAccountSession');
			$account_id = $userAccountSession['id'];
			$dataCms = DB::select("select * from tbl_accounts where id ='".$userAccountSession['id']."'");
			 
		if(!empty($dataCms[0])) 
		{
			DB::table('tbl_accounts')
			->where('id', $account_id)
			->update([
				'title' => $userAccountSession['title'],
				'account_type' => $userAccountSession['account_type'],
				'account_role' => $userAccountSession['account_role'],
				'opening_balance' => $userAccountSession['opening_balance'],
				'description' => $userAccountSession['description'] ] );
		}
		else
		{
				DB::table('tbl_accounts')->insert([
				'title' => $userAccountSession['title'],
				'account_type' => $userAccountSession['account_type'],
				'account_role' => $userAccountSession['account_role'],
				'opening_balance' => $userAccountSession['opening_balance'],
				'description' => $userAccountSession['description'] ]);
		}
		$msg = __('accounts.account_updated');
		return redirect()->route('accounts.index')->with('success',$msg);
		
    }	
	
	public function save (Request $request)
	{
 
	 
			  $this->validate(request(), [
					'title' => 'required',
					'account_type' => 'required' 
				]);
		
		
			$user = auth()->user();
			$user_id	= $user->id; 
			 
			$request->session()->put('userAccountSession', $_POST);
			$userAccountSession = session()->get('userAccountSession');
			$account_id = isset( $userAccountSession['id'] )&&$userAccountSession['id']>0?$userAccountSession['id']:'' ;
			$dataCms = DB::select("select * from tbl_accounts where id ='".$userAccountSession['id']."'");
			 
		if(!empty($dataCms[0])) 
		{
						
			DB::table('tbl_accounts')
			->where('id', $account_id)
			->update([
				'title' => $userAccountSession['title'],
				'account_type' => $userAccountSession['account_type'],
				'account_role' => $userAccountSession['account_role'],
				'opening_balance' => $userAccountSession['opening_balance'],
				'balance' => $userAccountSession['balance'],
				'description' => $userAccountSession['description'] ] );
		}
		else
		{
				DB::table('tbl_accounts')->insert([
				'title' => $userAccountSession['title'],
				'account_type' => $userAccountSession['account_type'],
				'user_id' => $user_id,
				'account_role' => $userAccountSession['account_role'],
				'opening_balance' => $userAccountSession['opening_balance'],
				'balance' => $userAccountSession['opening_balance'],
				'description' => $userAccountSession['description'] ]);
				$acc_id = DB::getPdo()->lastInsertId();
				if($userAccountSession['opening_balance'] > 0)
				{
					$data_arr = array(
					'title' 					=> "Opening balance",
					'liability_id' 				=> '',
					'transaction_type' 			=> 'REVENUE',
					'is_recurring_transaction' 	=> '',
					'recurring_period' 			=> '',
					'repeat_on_every' 			=> '',
					'repeat_month_day' 			=> '',
					'user_id' 					=> $user_id,
					'source_account' 			=> '',
					'destination_account' 		=> $acc_id,
					'trans_date' 				=> time(),
					'trans_amount' 				=> $userAccountSession['opening_balance'],
					
					'first_payment_date' 		=> '',
					'second_payment_date' 		=> '',
							
					'budget' 					=> '',
					'category' 					=> '',
					'tag' 						=> '',
					'description' 				=> "Opening balance" );
					$msg = '';
					$data_arr['created_by'] = $user_id;
					$data_arr['created_on'] = time();
					DB::table('tbl_transections')->insert($data_arr);
				}
		}
		$msg = __('accounts.account_added');
		return redirect()->route('accounts.index')->with('success',$msg);
		
    }
}
