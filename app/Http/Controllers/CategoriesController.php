<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User; 
use DB; 
use App\Category; 
 


class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user 		= auth()->user();
		$user_id	= $user->id;
		
		$budgets 	=  DB::select('select c.* from tbl_categories as c 
		where c.cat_is_deleted=0 and c.cat_user_id='.$user_id.' order by c.cat_name asc');
		 
		$data = ['active_class'=>'categories', 'title'=>"Categories","categories"=>$budgets];
		return view('users.categories.index', $data);
    }
	
    public function destroy(Category $category)
    {

        $category->delete();

  

        return redirect()->route('categories.index')

                        ->with('success','Category deleted successfully');

    }
	public function delete_category($id)
    {
		$user = auth()->user();
		$user_id	= $user->id;
		DB::table('tbl_categories')
			->where('cat_id', $id)
			->update([
			'cat_is_deleted'=>1,
			'cat_modified_on'=>time(),
			'cat_modified_by'=>$user_id,
			]);
			
       return redirect()->route('categories.index')->with('success','Category deleted successfully');

    }
	public function edit($id)
    {
 
		
		$cat 			= DB::table('tbl_categories')->where('cat_id', $id)->first();
		
		$data_array = array( 'cat'=>$cat,'active_class'=>'categories', "errors"=>array() );
 
        return view('users.categories.edit', $data_array );
		

    }	
	
	public function add()
    {
		$data_array = array( 'active_class'=>'categories', "errors"=>array() );
 
        return view('users.categories.add', $data_array );

    }
	
	 public function save(Request $request)
	{
	 
		  
			$this->validate($request, [
					'cat_name' => 'required'] );  
		   
			$user = auth()->user();
			$user_id	= $user->id; 
			 
			$request->session()->put('catSession', $_POST);
			$catSession = session()->get('catSession');
			$budget_id = isset( $catSession['id'] )&&$catSession['id']>0?$catSession['id']:'' ;
			$dataCms = DB::select("select * from tbl_categories where cat_id ='".$catSession['id']."'");
			  
			 $data_arr = array('cat_name' => $catSession['cat_name'],
				'cat_description' => $catSession['cat_description'],
				'cat_user_id' => $user_id);
				
		$msg = '';		
		if(!empty($dataCms[0])) 
		{
			$data_arr['cat_modified_by'] = $user_id;
			$data_arr['cat_modified_on'] = time();
			
			DB::table('tbl_categories')
			->where('cat_id', $budget_id)
			->update($data_arr);
			$msg = __('categories.cat_updated');
		}
		else
		{
			$data_arr['cat_created_by'] = $user_id;
			$data_arr['cat_created_on'] = time();
			DB::table('tbl_categories')->insert($data_arr);
			$msg = __('categories.cat_added');
		}
		return redirect('categories')->with('success',$msg);
		
    }
	
	
	
		
	public function show($category, $all='')
    {
		$items_per_page = 25;
		$cat		= DB::select('select * from tbl_categories where cat_is_deleted=0 and cat_id='.$category.' order by cat_name asc limit 0,1');
		
		//$transactions  	= DB::select('select * from tbl_transections where ( source_account='.$account.' or destination_account='.$account.') and  is_deleted=0 ');
		if($all == '')
		{
			$search_s_date = strtotime(date("Y-m")."-01 00:00:00"); 
			$search_e_date = strtotime(date("Y-m")."-".getLastDateOfMonth()." 23:59:59");
			$start_date = "1 ".__('common.'.date("F")).", ".date("Y"); 
			$end_date	= getLastDateOfMonth()." ".__('common.'.date("F")).", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
			
			
			
			
		}
		else
		{
			$search_s_date = strtotime(date("Y")."-01-01 00:00:00"); 
			$search_e_date = strtotime(date("Y")."-12-31 23:59:59");
			$start_date = "1 ".__('common.January').", ".date("Y"); 
			$end_date	= "31 ".__('common.December').", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
		}
		 
	 
		$transactions = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        
		 
	
        ->where('tbl_transections.category', $category)
        ->where('tbl_transections.trans_date', ">=", $search_s_date)
        ->where('tbl_transections.trans_date', "<=", $search_e_date)
		
        ->where('tbl_transections.is_deleted', 0)
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name')
        ->paginate($items_per_page);
		
		
		
		$account  		= isset($accounts[0])?$accounts[0]:array();
		if( isset($transactions[0]) )
		{
			$i = 0;
			foreach($transactions as $trans)
			{
				$transactions[$i]->trans_date = date("d", $transactions[$i]->trans_date).__('common.'.date("F", $transactions[$i]->trans_date)).", ".date("Y", $transactions[$i]->trans_date);
				$transactions[$i]->trans_amount = formated_amount1($transactions[$i]->trans_amount);
				$i++;
			}
		}
		$day_transactions = array();
		 
		  $data_array = array( 
		  'start_date'=>$start_date, 
		  'all'=>$all, 
		  'chart_data'=>array(), 
		  'end_date'=>$end_date, 
		  'month_date'=>$month_date,
		  'active_class'=>'accounts', 
		  'transactions'=>$transactions, 
		  'title'=>"Accounts",
		  'category'=>$cat[0],
		  "errors"=>array() 
		  );
		 // print_r($data_array);die();
        return view('users.categories.show', $data_array );

    }
	
	
	
	
}
