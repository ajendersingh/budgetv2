<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User; 
use DB; 
use App\Category; 
 


class BudgetsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user 		= auth()->user();
		$user_id	= $user->id;
		$budgets 	=  DB::select('select b.*,p.name as budget_period_name from tbl_budgets as b 
		inner join tbl_periods as p on p.id = b.budget_period where b.is_deleted=0 and b.user_id='.$user_id.' order by b.name asc');
		
		$search_s_date = strtotime("1-".date("m-Y")." 00:00:00"); 
		$search_e_date = strtotime(getLastDateOfMonth()."-".date("m-Y")." 23:59:59");
		
		$total_spent = 0;
		$total_budget = 0;
		 
		if(isset($budgets[0]))
		{
			$i = 0;
			foreach($budgets as $bdg)
			{
				$bdg_id 					= $bdg->id;
				$spent  					= self::budget_expences($bdg_id, $search_s_date, $search_e_date);
				$budgets[$i]->amount_spent  = formated_amount1($spent); 
				$budgets[$i]->amount_left  	= formated_amount1($bdg->budget_amount-$spent);
				 
				$total_budget 				= $total_budget+$bdg->budget_amount;
				$total_spent 				= $total_spent+$spent;
				
				$i++;
			}
		}
		if($total_spent >0)
		{
		$pct_spent = round(($total_spent/$total_budget)*100);
		}
		else
		$pct_spent = 0; 
		
		$start_date = "1 ".__('common.'.date("F")).", ".date("Y"); 
		$end_date	= getLastDateOfMonth()." ".__('common.'.date("F")).", ".date("Y");
		$month_date	= $start_date." and ".$end_date;
		 
		
		$data = [
		'total_budget'=>$total_budget, 
		'total_spent'=>$total_spent, 
		'pct_spent'=>$pct_spent, 
		'active_class'=>'budgets', 
		'title'=>"Budgets",
		"budgets"=>$budgets, 
		'month_date'=>$month_date
		];
		 
		
		return view('users.budgets.budget', $data);
    }
	
	public function delete_budget($id)
    {
		$user = auth()->user();
		$user_id	= $user->id;
		DB::table('tbl_budgets')
			->where('id', $id)
			->update([
			'is_deleted'=>1,
			'modefied_on'=>time(),
			'modefied_by'=>$user_id,
			]);
			
       return redirect()->route('budgets.index')->with('success','Budget deleted successfully');

    }
	
	
	public function budget_expences($bdg_id, $start_date, $end_date)
	{
		$trans = DB::table('tbl_transections')
		->where("trans_date", '>=',  $start_date)
		->where("trans_date", "<=", $end_date)
		->where("budget", $bdg_id)->sum('trans_amount');
 
		 
		if(isset($trans) && $trans>0)
		{
			return $trans;
		}
		else
			return 0;
	}
	
    public function destroy(Category $category)
    {

        $category->delete();

  

        return redirect()->route('categories.index')

                        ->with('success','Category deleted successfully');

    }
	
	public function edit($id)
    {
 
		  
		$periods = DB::select('select id, name from tbl_periods where is_deleted=0 and id !=7 order by name asc');
		$budget_types = DB::select('select id, name from tbl_budget_types where is_deleted=0 order by name asc');
		
		$budget =  DB::select('select * from tbl_budgets where is_deleted=0 and id='.$id.' order by name asc limit 0,1');
		$budget  = isset($budget[0])?$budget[0]:array();
		$data_array = array( 'active_class'=>'budgets','budget'=>$budget, 'budget_types'=>$budget_types,'periods'=>$periods,"errors"=>array() );
		
		 
        return view('users.budgets.edit', $data_array );

    }	
	
	public function add()
    {
		$qurter 		= get_dates_of_quarter();
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 and id !=7 order by name asc');
		$budget_types 	= DB::select('select id, name from tbl_budget_types where is_deleted=0 order by name asc');
		$data_array 	= array( 'active_class'=>'budgets', 'budget_types'=>$budget_types,'periods'=>$periods,"errors"=>array() );
 
        return view('users.budgets.add', $data_array );

    }
	
	 public function save(Request $request)
	{
	 
		  
			$this->validate($request, [
					'name' => 'required',
					//'name' => 'required|unique:tbl_budgets|max:255',
					'budget_type' => 'required', 
					'budget_amount' => 'required', 
					'budget_period' => 'required', 
				] );  
		
		
		
		   
			$user = auth()->user();
			$user_id	= $user->id; 
			 
			$request->session()->put('userAccountSession', $_POST);
			$userAccountSession = session()->get('userAccountSession');
			$budget_id = isset( $userAccountSession['id'] )&&$userAccountSession['id']>0?$userAccountSession['id']:'' ;
			$dataCms = DB::select("select * from tbl_budgets where id ='".$userAccountSession['id']."'");
			 //
			 $period 			= $userAccountSession['budget_period'];
			 $data_arr 			= array('name' => $userAccountSession['name'],
				'budget_type' => $userAccountSession['budget_type'],
				'repeated_amount' =>($userAccountSession['budget_type'] == 3)?$userAccountSession['budget_amount']:0,
			 
				'budget_amount' => $userAccountSession['budget_amount'],
				'budget_period' => $userAccountSession['budget_period'],
				'budget_description' => $userAccountSession['budget_description'],
				'user_id' => $user_id);
			$date               = setSearchDate(array());
		$start_date 		= $date['start_date']; 	 
		$end_date 			= $date['end_date']; 

		
			if( $period == 1)
			{
				$data_arr['start_date'] = strtotime(date("d-m-Y")." 00:00:00");
				$data_arr['end_date'] 	= strtotime(date("d-m-Y")." 23:59:59");
			}
			if( $period == 2)
			{
				$data_arr['start_date'] = strtotime(date("Y-m-d", strtotime('monday this week'))." 00:00:00");
				$data_arr['end_date'] 	= strtotime(date("Y-m-d", strtotime('sunday this week'))." 23:59:59");
			}
			if($period == 3)
			{
				$data_arr['start_date'] = $start_date;
				$data_arr['end_date'] 	= $end_date;
			}
			
			if( $period == 4)
			{
				$quarter = get_dates_of_quarter();
			 
				$data_arr['start_date'] = strtotime($quarter['start']['date']);
				$data_arr['end_date'] 	= strtotime($quarter['end']['date']);
			}
			
			if( $period == 5)
			{
				$hdate = get_dates_of_halfy();
				$data_arr['start_date'] = strtotime($hdate['start_date']);
				$data_arr['end_date'] 	= strtotime($hdate['end_date']);
			}
			if( $period == 6)
			{
				 
				$data_arr['start_date'] = strtotime("01-01-".date("Y")." 00:00:00");
				$data_arr['end_date'] 	= strtotime("31-12-".date("Y")." 23:59:59");
			}
			
		$msg = '';		
		if(!empty($dataCms[0])) 
		{
			$data_arr['modefied_by'] = $user_id;
			$data_arr['modefied_on'] = time();
			
			DB::table('tbl_budgets')
			->where('id', $budget_id)
			->update($data_arr);
			$msg = __('budgets.budget_updated');
		}
		else
		{
			$data_arr['created_by'] = $user_id;
			$data_arr['created_on'] = time();
			DB::table('tbl_budgets')->insert($data_arr);
			$msg = __('budgets.budget_added');
		}
		return redirect('budgets')->with('success',$msg);
		
    }
	
	
		public function show($budget_id, $all='')
    {
		$items_per_page = 25;
		$budget		= DB::select('select * from tbl_budgets where is_deleted=0 and id='.$budget_id.' order by name asc limit 0,1');
		
		//$transactions  	= DB::select('select * from tbl_transections where ( source_account='.$account.' or destination_account='.$account.') and  is_deleted=0 ');
		if($all == '')
		{
			$search_s_date = strtotime("1-".date("m-Y")." 00:00:00"); 
			$search_e_date = strtotime(getLastDateOfMonth()."-".date("m-Y")." 23:59:59");
			$start_date = "1 ".__('common.'.date("F")).", ".date("Y"); 
			$end_date	= getLastDateOfMonth()." ".__('common.'.date("F")).", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
			
			
			
			
		}
		else
		{
			 
			$search_s_date = strtotime(date("Y")."-01-01 00:00:00"); 
		 
			$search_e_date = strtotime(date("Y")."-12-31 23:59:59");
			
			
			$start_date = "1 ".__('common.January').", ".date("Y"); 
			$end_date	= "31 ".__('common.December').", ".date("Y");
			$month_date	= $start_date." and ".$end_date;
		}
		 
	 
		$transactions = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        
		 
	
        ->where('tbl_transections.budget', $budget_id)
        ->where('tbl_transections.trans_date', ">=", $search_s_date)
        ->where('tbl_transections.trans_date', "<=", $search_e_date)
		
        ->where('tbl_transections.is_deleted', 0)
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name')
        ->paginate($items_per_page);
		
		
		
		$account  		= isset($accounts[0])?$accounts[0]:array();
		if( isset($transactions[0]) )
		{
			$i = 0;
			foreach($transactions as $trans)
			{
				$transactions[$i]->trans_date = date("d", $transactions[$i]->trans_date).__('common.'.date("F", $transactions[$i]->trans_date)).", ".date("Y", $transactions[$i]->trans_date);
				$transactions[$i]->trans_amount = formated_amount1($transactions[$i]->trans_amount);
				$i++;
			}
		}
		$day_transactions = array();
		 
		  $data_array = array( 
		  'start_date'=>$start_date, 
		  'all'=>$all, 
		  'chart_data'=>array(), 
		  'end_date'=>$end_date, 
		  'month_date'=>$month_date,
		  'active_class'=>'accounts', 
		  'transactions'=>$transactions, 
		  'title'=>"Accounts",
		  'budget'=>$budget[0],
		  "errors"=>array() 
		  );
		 // print_r($data_array);die();
        return view('users.budgets.show', $data_array );

    }
	
	
}
