<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\Store;

use Auth;
use App\User; 
use DB; 
use Session;

use App\Mail\WelcomeMail;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Hash;

class SubscribeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       
    }
	
	public function buyMembership()
	{
		
		if (Auth::user()) 
		{ 
 		 return redirect()->action('CatController@show', ['category_id' => 1]);
		}
		else
		{
		$plan_id=Session::get('userPricingPlan');
		$seoData = DB::select('select * from seos where id=10');
		$dataCmsPlan1 = DB::select('select * from contents where id =25'); 
		$dataCmsPlan2 = DB::select('select * from contents where id =53'); 
		
		$dataCms2 = DB::select('select * from contents where id =26'); 
		$dataCms3 = DB::select('select * from contents where id =27'); 
		return view('buymembership',['dataCmsPlan1'=>$dataCmsPlan1[0],'dataCmsPlan2'=>$dataCmsPlan2[0],'dataCms2'=>$dataCms2[0],'dataCms3'=>$dataCms3[0],'seoData'=>$seoData[0],'plan_id'=>$plan_id]);
		}
		
		 
    }
	
	public function buyMembership2()
	{
	
		 
		return view('buymembership2');
		
		 
    }
	
	public function buyMembershipPost(Request $request)
	{ 
	
	 
		/*validate form fields */	
		$this->validate(
			$request,
			[
            'cc-cardnumber' => 'required',
            'cc-expires-month' => 'required',
			'cc-expires-year' => 'required',
			'cc-cvv2' => 'required',
			'terms' => 'required'
        	],
			[
            'cc-cardnumber.required' => __('buy_membership_validations.cardnumber'),
            'cc-cvv2.required' => __('buy_membership_validations.cvv2'),
			'terms.required' => __('buy_membership_validations.terms')
        	]);
	
		$userPricingSession = session()->get('userPricingSession');
		
		$dataCms = DB::select("select * from users where email='".$userPricingSession['email']."'");
		
		
 		if(!empty($dataCms[0])) 
		{
			$password_user=$this->random_strings(8);
			$user_existing_data=$dataCms[0];
			$existing_user_id=$user_existing_data->id;
		
			/* if user already exist update user password, profile,send email, auto login and redirect to next step*/
			DB::table('users')
			->where('id', $existing_user_id)
			->update([
				'name' => $userPricingSession['first_name']." ".$userPricingSession['last_name'],
				'first_name' => $userPricingSession['first_name'],
				'last_name' => $userPricingSession['last_name'],
				'address' => $userPricingSession['address'],
				'zip_code' => $userPricingSession['zip_code'],
				'country_id' => $userPricingSession['country_code'],
				'city' => $userPricingSession['city'],
				'email' => $userPricingSession['email'],
				'phone' => $userPricingSession['phone'],
				'password' => Hash::make($password_user)
			
			]
			);
			
 			$user_data=[
				'name' => $userPricingSession['first_name']." ".$userPricingSession['last_name'],
				'email' => $userPricingSession['email'],
				'password' => $password_user,
			];
			
		 
			Mail::to($userPricingSession['email'])->send(new WelcomeMail($user_data));
			
			$get_user_id=explode("_",$dataCms[0]->id);
			$get_user_id=$get_user_id[0];
 			
			 
   		}
		 
		
	
	 	$user = User::where('id',$get_user_id)->first();
		
		Auth::login($user);
		
		$user = auth()->user();
		
		$Transaction_id=$this->random_strings(12);	
		$transaction_date=date("Y-m-d H:i:s");
		
		
		/*insert order table data */
		$last_four = substr($_POST['cc-cardnumber'], -4);
	 	if($request['product_id']==1)
		{
		$values = array('user_id' => $get_user_id,'product_id' => $request['product_id'],'amount' => '49.00','transaction_id' => $Transaction_id,'last_four' => $last_four,'order_date' => $transaction_date);
		}
		else
		{
		$values = array('user_id' => $get_user_id,'product_id' => $request['product_id'],'amount' => '1.95','transaction_id' => $Transaction_id,'last_four' => $last_four,'order_date' => $transaction_date);
		}
		 
		
		$inserted_id=DB::table('order_data')->insertGetId($values);
		
		
		/*insert membership table data */
		$date = date("Y-m-d H:i:s");// current date
		
		if($request['product_id']==1)
		{
		$add_days = 30;
		}
		else
		{
		$add_days = 3;
		}
		
		$membership_expire_date = date('Y-m-d H:i:s',strtotime($date) + (24*3600*$add_days));
		
		$values_mmebership = array('user_id' => $user->id,'order_id' => $inserted_id,'membership_type' => $request['product_id'],'status' => 'active','membership_purchase_date' => $transaction_date,'membership_expire_date' => $membership_expire_date);
		 
		$inserted_id=DB::table('user_membership')->insertGetId($values_mmebership); 
		 
		$request->session()->forget('userPricingSession');
		 
 		return redirect()->action('CatController@show', ['category_id' => 1]);
		 
		
		 
 	}
	
	public function random_strings($length_of_string) 
	{ 
        
    return substr(sha1(time()), 0, $length_of_string); 
	} 
}
