<?php

  

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\User; 
use DB; 
use App\Seo; 


  

class SeoController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "Category";
        $this->middleware('auth:admin'); 
    } 

    public function index()

    {
	 
	 	 
	 
        $seos = Seo::orderBy('page_name', 'asc')->paginate(10);
 
        return view('seos.index',compact('seos'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        return view('seos.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

         

 		$created_on=date("Y-m-d H:i:s");
		
		
        Seo::create(array_merge($request->all(),['created_on' => $created_on]));

   		 

        return redirect()->route('seos.index')

                        ->with('success','Seo created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Seo $seo)

    {

        return view('seos.show',compact('seo'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Seo $seo)

    {

        return view('seos.edit',compact('seo'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Seo $seo)

    {

        

  		

        $seo->update($request->all()); 

  

        return redirect()->route('seos.index')

                        ->with('success','Seos updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.
 
     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Seo $seo)

    {

        $seo->delete();

  

        return redirect()->route('seos.index')

                        ->with('success','Seos deleted successfully');

    }

}