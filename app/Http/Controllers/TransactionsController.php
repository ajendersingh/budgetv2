<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User; 
use DB; 
use App\Category; 
 


class TransactionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
	 
    public function index(\Illuminate\Http\Request $request, $page = 1)
    {
		$user 				= auth()->user();
		$user_id			= $user->id;
		$items_per_page 	= 15; 
		
		$s_text 			= $request->input('search_text'); 
		$s_trans_type 		= $request->input('transaction_type');  
		$s_start_date 		= $request->input('start_date');  
		$s_end_date			= $request->input('end_date');
		$s_account			= $request->input('account');
		$s_category			= $request->input('category');
		$s_budget			= $request->input('budget');
		  
		 
		 
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_transacton_types')->get(); 
		 
		 
	$show_add_account   = true;
	if( isset($accounts[0]) )
	{
		$show_add_account   = false;
	}
	
	$show_add_budget   = true;
	if( isset($budgets[0]) )
	{
		$show_add_budget   = false;
	}	
	
	$show_add_cat   = true;
	if( isset($categories[0]) )
	{
		$show_add_cat   = false;
	}	
	 
	
		  
		$query = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        ->where('tbl_transections.user_id', $user_id)
        ->where('tbl_transections.is_deleted', 0)
		
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name');
        
		if($s_text !='')
		{
			$query->where('tbl_transections.title', 'like', '%' . $s_text . '%');
		}
		if($s_start_date !='' && $s_end_date == '')
		{
			$query->where('tbl_transections.trans_date', '>=', strtotime($s_start_date));
		}
		if($s_start_date =='' && $s_end_date != '')
		{
			$query->where('tbl_transections.trans_date', '<=', strtotime($s_end_date));
		}
		if($s_start_date !='' && $s_end_date != '')
		{
			$query->where('tbl_transections.trans_date', '>=', strtotime($s_start_date));
			$query->where('tbl_transections.trans_date', '<=', strtotime($s_end_date));
		}
		if($s_category !='' && $s_category>0)
		{
			$query->where('tbl_transections.category', $s_category);
		}
		if($s_budget !='' && $s_budget>0)
		{
			$query->where('tbl_transections.budget', $s_budget);
		}
		if($s_trans_type !='')
		{
			$query->where('tbl_transections.transaction_type', $s_trans_type);
		}
		if($s_account !='' && $s_account>0)
		{
			 
			$query->where(function($q) use ($s_account) { 
					$q->where('sac.id',$s_account )
				   ->orWhere('dac.id',$s_account);
					});
		
		}
		 
		$transactions = $query->paginate($items_per_page);
	
		
		 
		 $trans_types = DB::table('tbl_transacton_types')->get();
		 if(isset($transactions[0]))
		 {
			 $i = 0;
			 foreach($transactions as $trans)
			 {
				 $transactions[$i]->trans_date = date("F d, Y", $transactions[$i]->trans_date);
				 $transactions[$i]->trans_amount = formated_amount1($transactions[$i]->trans_amount);
				$i++; 
			 }
		 }
		$data = [
		'show_add_account'		=> $show_add_account, 
		'show_add_budget'		=> $show_add_budget, 
		'show_add_cat'			=> $show_add_cat,
		'active_class'			=> 'transactions', 
		'title'					=> "Transactions",
		"transactions"			=> $transactions, 
		'trans_types'			=> $trans_types, 
		's_text'				=> $s_text,
		's_trans_type'			=> $s_trans_type,
		's_start_date'			=> $s_start_date,
		's_end_date'			=> $s_end_date,
		's_account'				=> $s_account,
		's_category'			=> $s_category,
		's_budget'				=> $s_budget,
		'types'					=> $types, 
		'budgets'				=> $budgets, 
		'categories'			=> $categories, 
		'accounts'				=> $accounts,
		'periods'				=> $periods,
		
		];
		
		return view('users.transections.index', $data);
    }
 
	public function store()
	{
		
	}
	public function search(Request $request, $page = 1)
    {
		$user 				= auth()->user();
		$user_id			= $user->id;
		$items_per_page 	= 15; 
		
		$s_text 			= $request->input('search_text'); 
		$s_trans_type 		= $request->input('transaction_type');  
		$s_start_date 		= $request->input('start_date');  
		$s_end_date			= $request->input('end_date');
		$s_account			= $request->input('account');
		$s_category			= $request->input('category');
		$s_budget			= $request->input('budget');
		  
		
		 
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_transacton_types')->get(); 
		 
		  
		$transactions = DB::table('tbl_transections')
        ->leftJoin('tbl_accounts as sac','tbl_transections.source_account', '=', 'sac.id' )
        ->leftJoin('tbl_accounts as dac', 'tbl_transections.destination_account', '=', 'dac.id' )
        ->leftJoin('tbl_categories as cat', 'tbl_transections.category', '=','cat.cat_id')
        ->leftJoin('tbl_budgets as b', 'tbl_transections.budget', '=','b.id')
        ->where('tbl_transections.user_id', $user_id)
        ->where('tbl_transections.is_deleted', 0)
        ->orderBy('tbl_transections.id', 'desc')
        ->select('tbl_transections.*', 'sac.title as trans_account', 'dac.title as trans_destini', 'cat.cat_name as trans_cat', 'b.name as budget_name')
        ->paginate($items_per_page);
		 
		 $trans_types = DB::table('tbl_transacton_types')->get();
		 if(isset($transactions[0]))
		 {
			 $i = 0;
			 foreach($transactions as $trans)
			 {
				 $transactions[$i]->trans_date = date("F d, Y", $transactions[$i]->trans_date);
				$i++; 
			 }
		 }
		 
		 $s_text 			= $request->input('search_text'); 
		$s_trans_type 		= $request->input('transaction_type');  
		$s_start_date 		= $request->input('start_date');  
		$s_end_date			= $request->input('end_date');
		$s_account			= $request->input('account');
		$s_category			= $request->input('category');
		$s_budget			= $request->input('budget');
		
		
		$data = [
		
		'active_class'			=> 'transactions', 
		'title'					=> "Transactions",
		"transactions"			=> $transactions, 
		'trans_types'			=> $trans_types, 
		's_text'				=> $s_text,
		's_trans_type'			=> $s_trans_type,
		's_start_date'			=> $s_start_date,
		's_end_date'			=> $s_end_date,
		's_account'				=> $s_account,
		's_category'			=> $s_category,
		's_budget'				=> $s_budget,
		'types'					=> $types, 
		'budgets'				=> $budgets, 
		'categories'			=> $categories, 
		'accounts'				=> $accounts,
		'periods'				=> $periods,
		
		];
		
		return view('users.transections.index', $data);
    }
	
    public function destroy(Category $category)
    {

        $category->delete();
		return redirect()->route('categories.index')
		->with('success','Category deleted successfully');

    }
	public function show($id)
	{
		$user = auth()->user();
		$user_id	= $user->id;   
		 
		 
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		 
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_transacton_types')->get();
		$trans 			= DB::table('tbl_transections')->where('id', $id)->get();
		$trans          = isset($trans[0])?$trans[0]:array();
		$trans->trans_date = date("m/d/Y", $trans->trans_date);
		$p_options      = array();
		$month_days		= array();
		if($trans->recurring_period > 1)
		{
			//$p_options = DB::table("tbl_period_options")->where("period_id",$trans->recurring_period)->get(); 
				
				$p_options 		= DB::select('select id, name from tbl_period_options where period_id='.$trans->recurring_period);
				$p_options 		= $p_options; 
				
		}
		$first_pay_days = array();
		for($i=1; $i<=15; $i++)
		{
			$first_pay_days[$i-1] = array('id'=>$i, "name"=>$i);
		}
		
		$first_pay_days = json_decode (json_encode ($first_pay_days), FALSE);
					
		$sec_pay_days = array();
		for($i=1; $i<=16; $i++)
		{
			$sec_pay_days[$i-1] = array('id'=>$i+15, "name"=>$i+15);
		}
		
		$sec_pay_days = json_decode (json_encode ($sec_pay_days), FALSE);
		
		if($trans->repeat_on_every >= 1)
		{
			
				
				if($trans->repeat_on_every == 10)
				{
					for($i=1; $i<=28; $i++)
					{
					$month_days[$i-1] = array('id'=>$i, "name"=>$i);
					}
					
					$month_days = json_decode (json_encode ($month_days), FALSE);
					
					//$month_days = (object)$month_days;
					//$month_days = json_encode($month_days);
				}
				else{
					$month_days = DB::table("tbl_period_options")
                ->where("period_id",$trans->repeat_on_every)->get();
				}
		} 
		 
		$data_array 	= array('sec_pay_days'=>$sec_pay_days, 'first_pay_days'=>$first_pay_days, 'month_days'=>$month_days, 'p_options'=>$p_options, 'trans'=>$trans, 'active_class'=>'transactions','types'=>$types, 'budgets'=>$budgets, 'categories'=>$categories, 'accounts'=>$accounts,'periods'=>$periods,"errors"=>array() );
 
        return view('users.transections.show', $data_array );
	}
	public function edit($id)
    {
 
		  
		$user = auth()->user();
		$user_id	= $user->id;   
		 
		 
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		 
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_transacton_types')->get();
		$trans 			= DB::table('tbl_transections')->where('id', $id)->get();
		$trans          = isset($trans[0])?$trans[0]:array();
		$trans->trans_date = date("m/d/Y", $trans->trans_date);
		$p_options      = array();
		$month_days		= array();
		if($trans->recurring_period > 1)
		{
			//$p_options = DB::table("tbl_period_options")->where("period_id",$trans->recurring_period)->get(); 
				
				$p_options 		= DB::select('select id, name from tbl_period_options where period_id='.$trans->recurring_period);
				$p_options 		= $p_options; 
				
		}
		$first_pay_days = array();
		for($i=1; $i<=15; $i++)
		{
			$first_pay_days[$i-1] = array('id'=>$i, "name"=>$i);
		}
		
		$first_pay_days = json_decode (json_encode ($first_pay_days), FALSE);
					
		$sec_pay_days = array();
		for($i=1; $i<=16; $i++)
		{
			$sec_pay_days[$i-1] = array('id'=>$i+15, "name"=>$i+15);
		}
		
		$sec_pay_days = json_decode (json_encode ($sec_pay_days), FALSE);
		
		if($trans->repeat_on_every >= 1)
		{
			
				
				if($trans->repeat_on_every == 10)
				{
					for($i=1; $i<=28; $i++)
					{
					$month_days[$i-1] = array('id'=>$i, "name"=>$i);
					}
					
					$month_days = json_decode (json_encode ($month_days), FALSE);
					
					//$month_days = (object)$month_days;
					//$month_days = json_encode($month_days);
				}
				else{
					$month_days = DB::table("tbl_period_options")
                ->where("period_id",$trans->repeat_on_every)->get();
				}
		} 
		 
		$data_array 	= array('sec_pay_days'=>$sec_pay_days, 'first_pay_days'=>$first_pay_days, 'month_days'=>$month_days, 'p_options'=>$p_options, 'trans'=>$trans, 'active_class'=>'transactions','types'=>$types, 'budgets'=>$budgets, 'categories'=>$categories, 'accounts'=>$accounts,'periods'=>$periods,"errors"=>array() );
 
        return view('users.transections.edit', $data_array );

    }	
	
	public function add()
    {
		 
		$user = auth()->user();
		$user_id	= $user->id;   
		 
		 
		$periods 		= DB::select('select id, name from tbl_periods where is_deleted=0 order by id asc');
		$accounts 		= DB::select('select id, title from tbl_accounts where user_id='.$user_id.' and is_deleted=0 order by title asc');
		$categories 	= DB::select('select cat_id, cat_name from tbl_categories where cat_user_id='.$user_id.' and cat_is_deleted=0 order by cat_name asc');
		 
		$budgets 		= DB::select('select id, name from tbl_budgets where user_id='.$user_id.' and is_deleted=0 order by name asc');
		$types 			= DB::table('tbl_transacton_types')->get();
		
		$first_pay_days = array();
		for($i=1; $i<=15; $i++)
		{
			$first_pay_days[$i-1] = array('id'=>$i, "name"=>$i);
		}
		
		$first_pay_days = json_decode (json_encode ($first_pay_days), FALSE);
					
		$sec_pay_days = array();
		for($i=1; $i<=16; $i++)
		{
			$sec_pay_days[$i-1] = array('id'=>$i+15, "name"=>$i+15);
		}
		
		$sec_pay_days = json_decode (json_encode ($sec_pay_days), FALSE);

		
		$data_array 	= array('sec_pay_days'=>$sec_pay_days, 'first_pay_days'=>$first_pay_days, 'active_class'=>'transactions','types'=>$types, 'budgets'=>$budgets, 'categories'=>$categories, 'accounts'=>$accounts,'periods'=>$periods,"errors"=>array() );
 
        return view('users.transections.add', $data_array );

    }
	
	public function repeat_option($id)
	{
		$options = DB::table("tbl_period_options")
                ->where("period_id",$id)
                ->pluck("name","id");
				 $i=0;
				  
					foreach($options as $key => $opt)
					{
						
						$options[$key]  = __('common.'.$opt);
						$i++;
					}
				 
		return response()->json($options);
	}	
	
	public function repeat_month_day($id)
	{
		$options = DB::table("tbl_period_options")
                ->where("period_id",$id)
                ->pluck("name","id");
				 
					$i=0;
					foreach($options as $key => $opt)
					{
						
						$options[$key]  = __('common.'.$opt);
						$i++;
					}
				 
		return response()->json($options);
	}
	
	public function set_budget_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		if($trans->transaction_type == 'EXPENCES' && $trans->budget >0)
		{
			
			$budget = DB::table('tbl_budgets')->where('id', $trans->budget)->first();
			$spent 	= $budget->amount_spent+$trans_amount;
			if($budget->amount_spent == 0 && $budget->amount_left == 0)
			{
				$left 	= $budget->budget_amount-$trans_amount;
			}
			else{
				$left 	= $budget->amount_left-$trans_amount;
			}
			$row_arr = array('amount_spent'=>$spent, 'amount_left'=>$left);
			DB::table('tbl_budgets')
			->where('id', $budget->id)
			->update($row_arr);
		}
	}
	
	public function set_account_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		 
		if($trans->transaction_type == 'EXPENCES')
		{
			
			$account = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('balance'=> ($account->balance-$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
			
		}
		
		if($trans->transaction_type == 'REVENUE')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			$row_arr = array('balance'=> ($account->balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
		}	
		
		if($trans->transaction_type == 'TRANSFER')
		{
			$saccount = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('balance'=> ($saccount->balance-$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $saccount->id)
			->update($row_arr);
			$daccount = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			
			$row_arr = array('balance'=> ($daccount->balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $daccount->id)
			->update($row_arr);
			
		}
	}

	 public function save(Request $request)
	{
	 
		 
			$this->validate($request, [
					'title' => 'required',
					//'name' => 'required|unique:tbl_budgets|max:255',
					'transaction_type' => 'required', 
					'trans_amount' => 'required', 
					'trans_date' => 'required', 
				] );  
		
		
		
		   
			$user = auth()->user();
			$user_id	= $user->id; 
			 
			$request->session()->put('dataSession', $_POST);
			$dataSession = session()->get('dataSession');
			$trans_id = isset( $dataSession['id'] )&&$dataSession['id']>0?$dataSession['id']:'' ;
			$dataCms = DB::select("select * from tbl_transections where id ='".$dataSession['id']."'");
			 
			 //$period = $dataSession['budget_period'];
			  
			 
			 $data_arr = array(
				'title' 					=> isset($dataSession['title'] )?$dataSession['title']:'',
				'transaction_type' 			=> isset($dataSession['transaction_type'])?$dataSession['transaction_type']:'',
				'is_recurring_transaction' 	=> isset($dataSession['is_recurring_transaction'])?$dataSession['is_recurring_transaction']:'',
				'recurring_period' 			=> isset($dataSession['recurring_period'])?$dataSession['recurring_period']:'',
				'repeat_on_every' 			=> isset($dataSession['repeat_on_every'])?$dataSession['repeat_on_every']:'',
				'repeat_month_day' 			=> isset($dataSession['repeat_month_day'])?$dataSession['repeat_month_day']:'',
				'user_id' 					=> $user_id,
				'source_account' 			=> isset($dataSession['source_account'])?$dataSession['source_account']:'',
				'destination_account' 		=> isset($dataSession['destination_account'])?$dataSession['destination_account']:'',
				'trans_date' 				=> isset($dataSession['trans_date'])?strtotime($dataSession['trans_date']):'',
				'trans_amount' 				=> isset($dataSession['trans_amount'])?$dataSession['trans_amount']:'',
				'first_payment_date' 		=> isset($dataSession['first_payment_date'])?$dataSession['first_payment_date']:'',
				'second_payment_date' 		=> isset($dataSession['second_payment_date'])?$dataSession['second_payment_date']:'',
				'budget' 					=> isset($dataSession['budget'] )?$dataSession['budget']:'',
				'category' 					=> isset($dataSession['category'])?$dataSession['category']:'',
				'tag' 						=> isset($dataSession['tag'])?$dataSession['tag']:'',
				'description' 				=> isset($dataSession['description'])?$dataSession['description']:'' );
				
				
			 
			
		$msg = '';		
		if(!empty($dataCms[0])) 
		{
						$data_arr['modefied_by'] = $user_id;
			$data_arr['modefied_on'] = time();
			
			$trans 				= DB::table('tbl_transections')->where('id', $trans_id)->first();
			$previous_amount 	=  $trans->trans_amount;
			$current_amount 	=  $dataSession['trans_amount'];
			
			if($previous_amount != $current_amount)
			{}
				self::unset_account_amount($trans_id, $trans);
				self::unset_budget_amount($trans_id, $trans);
			
			 
			DB::table('tbl_transections')
			->where('id', $trans_id)
			->update($data_arr);
			
			$transn = DB::table('tbl_transections')->where('id', $trans_id)->first(); 
			self::set_budget_amount($trans_id, $transn);
			self::set_account_amount($trans_id, $transn);
			
			
			$msg = __('transactions.trans_updated');
		}
		else
		{
			$data_arr['created_by'] = $user_id;
			$data_arr['created_on'] = time();
			DB::table('tbl_transections')->insert($data_arr);
			$id = DB::getPdo()->lastInsertId();
			$trans = DB::table('tbl_transections')->where('id', $id)->first(); 
			self::set_budget_amount($id, $trans);
			self::set_account_amount($id, $trans);
			
			$msg = __('transactions.trans_added');
		}
		return redirect('transactions')->with('success',$msg);
		
    }
	
	public function unset_account_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		 
		if($trans->transaction_type == 'EXPENCES')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('balance'=> ($account->balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
			
			
		 
			
		}
		
		if($trans->transaction_type == 'REVENUE')
		{
			$account = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			$amount  = ($account->balance-$trans_amount)>-1?($account->balance-$trans_amount):0;
			$row_arr = array('balance'=> $amount );
			DB::table('tbl_accounts')
			->where('id', $account->id)
			->update($row_arr);
		}	
		
		if($trans->transaction_type == 'TRANSFER')
		{
			$saccount = DB::table('tbl_accounts')->where('id', $trans->source_account)->first();
			$row_arr = array('balance'=> ($saccount->balance+$trans_amount) );
			DB::table('tbl_accounts')
			->where('id', $saccount->id)
			->update($row_arr);
			
			$daccount = DB::table('tbl_accounts')->where('id', $trans->destination_account)->first();
			$amount  = ($daccount->balance-$trans_amount)>-1?($daccount->balance-$trans_amount):0;
			$row_arr = array('balance'=> $amount );
			DB::table('tbl_accounts')
			->where('id', $daccount->id)
			->update($row_arr);
			
		}
	}
	
	public function unset_budget_amount($trans_id, $trans)
	{
		$trans_amount = $trans->trans_amount;
		if($trans->transaction_type == 'EXPENCES' && $trans->budget >0)
		{
			
			$budget = DB::table('tbl_budgets')->where('id', $trans->budget)->first();
			$spent 	= $budget->amount_spent-$trans_amount;
			if($budget->amount_spent == 0 && $budget->amount_left == 0)
			{
				
			}
			else{
				$left 	= $budget->amount_left+$trans_amount;
			}
			$row_arr = array('amount_spent'=>$spent, 'amount_left'=>$left);
			DB::table('tbl_budgets')
			->where('id', $budget->id)
			->update($row_arr);
		}
		
		 
	}
	public function remove_transaction($id)
	{
		 
		$user = auth()->user();
			$user_id	= $user->id; 
			
		$trans 			= DB::table('tbl_transections')->where('id', $id)->first();
		//self::unset_account_amount($id, $trans);
		//self::unset_budget_amount($id, $trans);
		
		$row_arr = array('is_deleted'=> 1 );
			DB::table('tbl_transections')
			->where('id', $id)
			->update($row_arr);
		self::unset_account_amount($id, $trans);
		self::unset_budget_amount($id, $trans);	
		$msg = __('transactions.trans_deleted');
		return redirect('transactions')->with('success',$msg);
	}
	
	public function clone_transaction($id)
	{
		 
		
		$trans 			= DB::table('tbl_transections')->where('id', $id)->first();
	 
	 
	 
		$user = auth()->user();
		$user_id	= $user->id; 
		$data_arr = array(
				'title' 					=> $trans->title,
				'base_id' 					=> $trans->base_id,
				'transaction_type' 			=> $trans->transaction_type,
				'is_recurring_transaction' 	=> $trans->is_recurring_transaction,
				'recurring_period' 			=> $trans->recurring_period,
				'repeat_on_every' 			=> $trans->repeat_on_every,
				'repeat_month_day' 			=> $trans->repeat_month_day,
				'user_id' 					=> $user_id,
				'liability_id' 				=> isset($tans->liability_id)?$tans->liability_id:'',
				'income_id' 				=> isset($tans->income_id)?$tans->income_id:'',
				'source_account' 			=> $trans->source_account,
				'destination_account' 		=> $trans->destination_account,
				'trans_date' 				=> strtotime(date("m/d/Y")),
				'trans_amount' 				=> $trans->trans_amount,
				
				'first_payment_date' 				=> $trans->first_payment_date,
				'second_payment_date' 				=> $trans->second_payment_date,
				
				'budget' 					=> $trans->budget,
				'category' 					=> $trans->category,
				'tag' 						=> $trans->tag,
				'description' 				=> $trans->description );
				
			 
		 
			$data_arr['created_by'] = $user_id;
			$data_arr['created_on'] = time();
			DB::table('tbl_transections')->insert($data_arr);
			$id = DB::getPdo()->lastInsertId();
			 
			self::set_budget_amount($id, $trans);
			self::set_account_amount($id, $trans);
			
			
			$msg = __('transactions.trans_added');
		  
		return redirect('transactions')->with('success',$msg); 
	}
	
	public function stop_repeated_transections($id)
	{
		$trans 		= DB::table('tbl_transections')->where('id', $id)->first();
		$user 		= auth()->user();
		$user_id	= $user->id; 
		$base_id 	= isset($trans->base_id ) && $trans->base_id>0 ?$trans->base_id:$trans->id;
		
		$row_arr = array('stop_recurring'=> 1 );
			DB::table('tbl_transections')
			->where('id', $id)
			->update($row_arr);
			
		$row_arr = array('stop_recurring'=> 1 );
			DB::table('tbl_transections')
			->where('base_id', $id)
			->update($row_arr);	
			
		$msg = __('transactions.recuring_trans_stoped');
		return redirect('transactions')->with('success',$msg);
		
	}
	
}
