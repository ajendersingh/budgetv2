<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\User; 
use DB; 
use App\Features; 
class FeaturesController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */
	 private $apiKey; 
	 
	 public function __construct()
    {
		$this->apiKey = "Category";
        $this->middleware('auth:admin'); 
    } 

    public function index()
 
    {
	 
	 	 
	 
        $features = Features::orderBy('created_on', 'desc')->paginate(10);
  
        return view('features.index',compact('features'))
			->with(['MainTitle'=>$this->apiKey])
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

   

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
		 
        return view('features.create');

    }

  

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $request->validate([
            'name_en' => 'required',
			'name_fr' => 'required',
			'name_nl' => 'required',
			'name_dk' => 'required',
			'name_at' => 'required',
			'name_de' => 'required',
			'name_se' => 'required',
			'name_no' => 'required',
			'name_it' => 'required',
		/*	'title_en' => 'required',
			'title_fr' => 'required',
			'title_nl' => 'required',
			'title_dk' => 'required',
			'title_at' => 'required',
			'title_de' => 'required',
			'title_se' => 'required',
			'title_no' => 'required',
			'title_it' => 'required',*/
			 
        ]);
		
		 
 		$created_on=date("Y-m-d H:i:s");
	 	 
		
        Features::create(array_merge($request->all(),['created_on' => $created_on]));

   		 

        return redirect()->route('features.index')

                        ->with('success','Feature created successfully.');

    }

   

    /**

     * Display the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function show(Features $faq)

    {

        return view('features.show',compact('features'));

    }

   

    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function edit(Features $feature)

    {

        return view('features.edit',compact('feature'));

    }

  

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Features $feature)

    {

        $request->validate([

            'name_en' => 'required',
			'name_fr' => 'required',
			'name_nl' => 'required',
			'name_dk' => 'required',
			'name_at' => 'required',
			'name_de' => 'required',
			'name_se' => 'required',
			'name_no' => 'required',
			'name_it' => 'required',
			
			/*'title_en' => 'required',
			'title_fr' => 'required',
			'title_nl' => 'required',
			'title_dk' => 'required',
			'title_at' => 'required',
			'title_de' => 'required',
			'title_se' => 'required',
			'title_no' => 'required',
			'title_it' => 'required',*/
        ]);
		
		
		
		
		 $feature->update($request->all());

		
		 
  

        return redirect()->route('features.index')

                        ->with('success','Feature updated successfully');

    }

  

    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Product  $product

     * @return \Illuminate\Http\Response

     */

    public function destroy(Features $feature)

    {

        $feature->delete();

  

        return redirect()->route('features.index')

                        ->with('success','feature deleted successfully');

    }

}