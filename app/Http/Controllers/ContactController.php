<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;

use DB;

use App\Rules\NoHtml;

use Illuminate\Support\Facades\Mail;

use App\Mail\ContactFormMessage;

use App\Rules\GoogleRecaptcha;
 
class ContactController extends Controller{
     
    public function index(){
	
		$dataCms = DB::select('select * from contents where id=3'); 
        return view('contact',['dataCms'=>$dataCms[0]]);
    }
	
	public function store(Request $request){
 
        // validate fields
        $this->validate($request, [
            'name' => ['required', 'string', new NoHtml],
            'email' => ['required', 'email', new NoHtml],
            'subject' => ['required', 'string', new NoHtml],
            'message' => ['required', 'string', new NoHtml],
			'g-recaptcha-response' => ['required', new GoogleRecaptcha]
        ],
		[
            'name.required' => __('contact_us.name_validation'),
            'email.required' => __('contact_us.email_validation'),
			'subject.required' => __('contact_us.subject_validation'),
			'message.required' => __('contact_us.message_validation'),
			'g-recaptcha-response.required' => __('contact_us.captcha_validation')
        	]);
 
 
         // redirect to contact form with message
        session()->flash('success', __('contact_us.success_msg')); 
 		Mail::send( new ContactFormMessage() );
        return redirect()->back();
 
    }
 
}