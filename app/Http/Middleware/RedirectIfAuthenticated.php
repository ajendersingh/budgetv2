<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
	public function handle($request, Closure $next, $guard = null)
	{
        switch ($guard) {
            case 'admin' :
                if (Auth::guard($guard)->check()) {
                    return redirect()->route('admin.home');
                } 
                break;
            default:
                if (Auth::guard($guard)->check()) {
                    //return redirect()->route('books');
					//return redirect()->action('DashboardController@index', ['category_id' => 1]);
					return redirect()->action('DashboardController@index');
                }
                break;
        }
     return $next($request);
	}
	
}
