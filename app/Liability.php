<?php

  

namespace App;

  

use Illuminate\Database\Eloquent\Model;

   

class Liability extends Model

{
  
	protected $table = 'tbl_liability';
    protected $fillable = [

        'title',  
		'user_id', 
		'linked_account', 
		'start_date ', 
		'end_date', 
		'type', 
		'is_recurring_transaction', 
		'liability_amount', 
		'recurring_period', 
		'repeat_on_every', 
		'repeat_month_day', 
		'amount_of_debt', 
		'description',
		'created_on', 
		'created_by',
		'modefied_on',
		'modefied_by', 
		'is_active',
		'is_deleted' 

    ];
	public $timestamps = false;
	  
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


    public function setCreatedAt($value)

    {

      return NULL;

    }
}