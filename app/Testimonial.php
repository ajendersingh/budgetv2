<?php

  

namespace App;

  
use Illuminate\Database\Eloquent\Model;

   

class Testimonial extends Model

{

	protected $table = 'testimonials';
    public $fillable = [

        'testimonial_client_en',
		'testimonial_location_en',
		'thumbnail',
		'description_en','description_fr','description_nl','description_dk','description_at',
		'description_de','description_se','description_no','description_it','created_on'

    ];
	public $timestamps = false;
	 
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


   
}