<?php

  

namespace App;

  

use Illuminate\Database\Eloquent\Model;

   

class Ebook extends Model

{

	protected $table = 'ebook';
    protected $fillable = [

        'book_name_en',  'book_description_en', 'book_name_fr', ' book_description_fr ', 
		'book_name_nl', 'book_description_nl', 'book_name_dk', 'book_description_dk', 
		'book_name_at', 'book_description_at', 'book_name_de', 'book_description_de', 
		'book_name_se', 'book_description_se', 'book_name_no','book_description_no',
		'book_name_it', 'book_description_it',
		'pdf_file','created_on' 

    ];
	public $timestamps = false;
	  
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


    public function setCreatedAt($value)

    {

      return NULL;

    }
}