<?php

  

namespace App;

  
use Illuminate\Database\Eloquent\Model;

   

class Features extends Model

{

	protected $table = 'features';
    public $fillable = [
		//'title_en', 'title_fr','title_nl','title_dk','title_at','title_de','title_se','title_no','title_it',
        'name_en', 'name_fr','name_nl','name_dk','name_at','name_de','name_se','name_no','name_it',
		
		'description_en','description_fr','description_nl','description_dk','description_at','description_de',
		'description_se','description_no','description_it','created_on'

    ];
	public $timestamps = false;
	 
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


   
}