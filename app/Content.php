<?php

  

namespace App;

  

use Illuminate\Database\Eloquent\Model;

   

class Content extends Model

{

	protected $table = 'contents';
    public $fillable = [

        'title_en', 'title_fr','title_nl','title_dk','title_at','title_de','title_se','title_no','title_it',
		'description_en','description_fr','description_nl','description_dk','description_at','description_de',
		'description_se','description_no','description_it','is_language','language_url','created_on'

    ];
	public $timestamps = false;
	  
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


    public function setCreatedAt($value)

    {

      return NULL;

    }
}