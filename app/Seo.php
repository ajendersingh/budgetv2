<?php

  

namespace App;

  

use Illuminate\Database\Eloquent\Model;

   

class Seo extends Model

{

	protected $table = 'seos';
    public $fillable = [

        'page_name', 
		'page_title_en','meta_title_en','meta_description_en',
		'page_title_fr','meta_title_fr','meta_description_fr',
		'page_title_nl','meta_title_nl','meta_description_nl',
		'page_title_dk','meta_title_dk','meta_description_dk',
		'page_title_at','meta_title_at','meta_description_at',
		'page_title_de','meta_title_de','meta_description_de',
		'page_title_se','meta_title_se','meta_description_se',
		'page_title_no','meta_title_no','meta_description_no',
		'page_title_it','meta_title_it','meta_description_it',
		'created_on'

    ];
	public $timestamps = false;
	  
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


    public function setCreatedAt($value)

    {

      return NULL;

    }
}