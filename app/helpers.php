<?php

 
  function imploadValue($types){
    $strTypes = implode(",", $types);
    return $strTypes;
  }

  function explodeValue($types){
    $strTypes = explode(",", $types);
    return $strTypes;
  }

	function dateDiffInDays($date1, $date2)  
	{ 
		// Calulating the difference in timestamps 
		$diff = strtotime($date2) - strtotime($date1); 
		  
		// 1 day = 24 hours 
		// 24 * 60 * 60 = 86400 seconds 
		return abs(round($diff / 86400)); 
	} 
  function random_code(){

    return rand(1111, 9999);
  }
	function weekOfMonth($date='') {
		if($date == '')
		{
			$date = date("Y-m-d");
		}
		//Get the first day of the month.
		$firstOfMonth = strtotime(date("Y-m-01", $date));
		//Apply above formula.
		return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;
	}
  function remove_special_char($text) {

        $t = $text;

        $specChars = array(
            ' ' => '-',    '!' => '',    '"' => '',
            '#' => '',    '$' => '',    '%' => '',
            '&amp;' => '',    '\'' => '',   '(' => '',
            ')' => '',    '*' => '',    '+' => '',
            ',' => '',    '₹' => '',    '.' => '',
            '/-' => '',    ':' => '',    ';' => '',
            '<' => '',    '=' => '',    '>' => '',
            '?' => '',    '@' => '',    '[' => '',
            '\\' => '',   ']' => '',    '^' => '',
            '_' => '',    '`' => '',    '{' => '',
            '|' => '',    '}' => '',    '~' => '',
            '-----' => '-',    '----' => '-',    '---' => '-',
            '/' => '',    '--' => '-',   '/_' => '-',   
            
        );

        foreach ($specChars as $k => $v) {
            $t = str_replace($k, $v, $t);
        }

        return $t;
  }
  
  
  
if(!function_exists('dateFormat'))
{
    function dateFormat($format='d-m-Y', $givenDate=null)
    {
        return date($format, strtotime($givenDate));
    }
}
 if(!function_exists('timeToDate'))
{
    function timeToDate($format='d-m-Y', $givenDate=null)
    {
        return date($format, $givenDate);
    }
} 


 if(!function_exists('formated_amount1'))
{
    function formated_amount1($price)
    {
		 
        return number_format(floor($price*100)/100,2, '.', ''); 
        //return number_format($price); 
    }
} 
 if(!function_exists('currnecy_symbol'))
{
    function currnecy_symbol()
    {
		$ci =& get_instance();
		$ci->load->database();
		$session = $ci->session->userdata('site_currency');
		$symbol = '£';
		if(isset($session['symbol']))
		{
				$symbol = $session['symbol'];
		}
			return $symbol;
    }
} 


if(!function_exists('currnecy_code'))
{
    function currnecy_code()
    {
		$ci =& get_instance();
		$ci->load->database();
		$session = $ci->session->userdata('site_currency');
		$symbol = 'GBP';
		if(isset($session['currnecy_code']))
		{
				$symbol = $session['currnecy_code'];
		}
			return $symbol;
    }
} 



 if(!function_exists('formated_amount'))
{
    function formated_amount($price)
    {
		 
		$ci =& get_instance();
		$ci->load->database();
		$session = $ci->session->userdata('site_currency');
		$symbol = '£';
		if(isset($session['symbol']))
		{
				$symbol = $session['symbol'];
		}
		
        return $symbol.''.number_format(floor($price*100)/100,2, '.', ''); 
    }
} 
function get_number_of_days_in_month($month, $year) {
    // Using first day of the month, it doesn't really matter
    $date = $year."-".$month."-1";
    return date("t", strtotime($date));
}
function getLastDateOfMonth()
    {
        $date = date('Y').'-'.date('m');  //make date of month 
        return date('t', strtotime($date)); 
    }

function get_dates_of_halfy()
{
	$m = date("m");
	if($m >6)
	{
		$start_date = "01-07-".date("Y")." 00:00:00";
		$end_date 	= "31-12-".date("Y")." 23:59:59";
	}
	else{
		$start_date = "01-01-".date("Y")." 00:00:00";
		$end_date 	= "01-06-".date("Y")." 23:59:59";
	}
	
	return array("start_date"=>$start_date, "end_date"=>$end_date);
}

function get_dates_of_quarter($quarter = 'current', $year = null, $format = "d-m-Y")
{
    if ( !is_int($year) ) {        
       $year = (new DateTime)->format('Y');
    }
    $current_quarter = ceil((new DateTime)->format('n') / 3);
    switch (  strtolower($quarter) ) {
    case 'this':
    case 'current':
       $quarter = ceil((new DateTime)->format('n') / 3);
       break;

    case 'previous':
       $year = (new DateTime)->format('Y');
       if ($current_quarter == 1) {
          $quarter = 4;
          $year--;
        } else {
          $quarter =  $current_quarter - 1;
        }
        break;

    case 'first':
        $quarter = 1;
        break;

    case 'last':
        $quarter = 4;
        break;

    default:
        $quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
        break;
    }
    if ( $quarter === 'this' ) {
        $quarter = ceil((new DateTime)->format('n') / 3);
    }
    $start = new DateTime($year.'-'.(3*$quarter-2).'-1 00:00:00');
    $end = new DateTime($year.'-'.(3*$quarter).'-'.($quarter == 1 || $quarter == 4 ? 31 : 30) .' 23:59:59');



    return array(
        'start' => array('date'=>$format ? $start->format($format) : $start),
        'end' =>array('date'=> $format ? $end->format($format) : $end),
    );
}


function set_budget_amount($trans_id, $data_array)
{
	$trans_amount = $data_array['trans_amount'];
	if($data_array['transaction_type'] == 'EXPENCES')
	{
		
	}
}

function query_helperr()
{
	
	 
	$query = DB::table('tbl_accounts')
                ->select('*');
				echo '<pre>';
				print_r($query);die();
}
function set_account_amount($trans_id, $data_array)
{
	$trans_amount = $data_array['trans_amount'];
	if($data_array['transaction_type'] == 'EXPENCES')
	{
		$query = DB::table('tbl_cms_menus')
                ->select('*')
                ->where('is_active', '=', 'Y')
                ->where('is_delete', '=', 'N')
                ->where('parent_id', '=', $parent_id);
    ($sort_order != "")? $query->orderBy($sort_order, "ASC") : "";
	
    $resultData = $query->get()->toArray();
	}
	
	if($data_array['transaction_type'] == 'REVENUE')
	{
		
	}	
	
	if($data_array['transaction_type'] == 'TRANSFER')
	{
		
	}
}

if(!function_exists("setSearchDate"))
{
	function setSearchDate($data)
	{
		
		$date = array();
		if(isset($data['start_date']) && $data['start_date'] !='')
		{
			$start_date = date("d-m-Y", strtotime($data['start_date']))." 00:00:00";
		}
		else
		{
			$start_date 		= "1-".date("m-Y")." 00:00:00";
			
			//$start_date 		= date('d-m-Y', strtotime('-30 days'))." 00:00:00";
			
			
		}
		
		
		if( isset($data['end_date']) && $data['end_date'] !=''  )
		{
			$end_date = date("d-m-Y", strtotime($data['end_date']))." 23:59:59";
		}
		else
		{
			$end_date 			= getLastDateOfMonth()."-".date("m-Y")." 23:59:59";
			
			 
		}
		
		return array("start_date"=>$start_date, "end_date"=>$end_date, "days"=>30);
	}
}

