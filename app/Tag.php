<?php

  

namespace App;

  

use Illuminate\Database\Eloquent\Model;

   

class Tag extends Model

{

	protected $table = 'tags';
    public $fillable = [

        'name_en', 'name_fr','name_nt','name_dm','name_at','name_ge','name_sw','name_no',
		'description_en','description_fr','description_nt','description_dm','description_at',
		'description_ge','description_sw','description_no','created_on'

    ];
	public $timestamps = false;
	  
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


    public function setCreatedAt($value)

    {

      return NULL;

    }
}