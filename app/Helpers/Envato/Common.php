<?php
//app/Helpers/Envato/Common.php
namespace App\Helpers\Envato;
 
use Illuminate\Support\Facades\DB;
 
class Common {
    /**
     * @param int $user_id User-id
     * 
     * @return string
     */
    public static function get_username($user_id) {
        $user = DB::table('users')->where('userid', $user_id)->first();
         
        return (isset($user->username) ? $user->username : '');
    }
	
	public static function dateFormat($format='d-m-Y', $givenDate=null)
    {
        return date($format, strtotime($givenDate));
    }
	public static function timeToDate($format='d-m-Y', $givenDate=null)
    {
        return date($format, $givenDate);
    }
	public static function formated_amount1($price)
    {
		 
        return number_format(floor($price*100)/100,2, '.', ''); 
    }
	public static function get_number_of_days_in_month($month, $year) {
    // Using first day of the month, it doesn't really matter
    $date = $year."-".$month."-1";
    return date("t", strtotime($date));
	}

	public static function getLastDateOfMonth()
    {
        $date = date('Y').'-'.date('m');  //make date of month 
        return date('t', strtotime($date)); 
    }
	
	public static function get_dates_of_halfy()
	{
		$m = date("m");
		if($m >6)
		{
			$start_date = "01-07-".date("Y")." 00:00:00";
			$end_date 	= "31-12-".date("Y")." 23:59:59";
		}
		else{
			$start_date = "01-01-".date("Y")." 00:00:00";
			$end_date 	= "01-06-".date("Y")." 23:59:59";
		}
		
		return array("start_date"=>$start_date, "end_date"=>$end_date);
	}



	public static function get_dates_of_quarter($quarter = 'current', $year = null, $format = null)
	{
		if ( !is_int($year) ) {        
		   $year = (new DateTime)->format('Y');
		}
		$current_quarter = ceil((new DateTime)->format('n') / 3);
		switch (  strtolower($quarter) ) {
		case 'this':
		case 'current':
		   $quarter = ceil((new DateTime)->format('n') / 3);
		   break;

		case 'previous':
		   $year = (new DateTime)->format('Y');
		   if ($current_quarter == 1) {
			  $quarter = 4;
			  $year--;
			} else {
			  $quarter =  $current_quarter - 1;
			}
			break;

		case 'first':
			$quarter = 1;
			break;

		case 'last':
			$quarter = 4;
			break;

		default:
			$quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $current_quarter : $quarter;
			break;
		}
		if ( $quarter === 'this' ) {
			$quarter = ceil((new DateTime)->format('n') / 3);
		}
		$start = new DateTime($year.'-'.(3*$quarter-2).'-1 00:00:00');
		$end = new DateTime($year.'-'.(3*$quarter).'-'.($quarter == 1 || $quarter == 4 ? 31 : 30) .' 23:59:59');

		return array(
			'start' => $format ? $start->format($format) : $start,
			'end' => $format ? $end->format($format) : $end,
		);
	}

}