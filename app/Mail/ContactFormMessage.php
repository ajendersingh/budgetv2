<?php

namespace App\Mail;
 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;
 
class ContactFormMessage extends Mailable{
    use Queueable, SerializesModels;
 
 
    public function __construct(){
        //
    }
 
 
    public function build(Request $request){
         
        return $this->from([
                'address' => $request->email, 
                'name' => $request->name 
            ])
            ->to( env('MAIL_FROM_ADDRESS') )
            ->subject( __('contact_us.success_msg') . $request->subject )
            ->view('emails.contactform')
            ->with([    
                'contactName' => $request->name,
                'contactSubject' => $request->subject,
                'contactEmail' => $request->email,
                'contactMessage' => $request->message
            ]);
 
    }
}