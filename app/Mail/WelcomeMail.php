<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class WelcomeMail extends Mailable
{
    use Queueable, SerializesModels;


    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		
        $subject =  __('welcome_email.registration_subject');
		$address=env('MAIL_FROM_ADDRESS');
 		$name=env('MAIL_FROM_NAME');
		
		return $this->view('emails.welcome')
                    ->from($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject);
         
 					
     }
}
