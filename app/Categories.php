<?php
 
  

namespace App;
  
use DB;
use Illuminate\Database\Eloquent\Model;

   

class Categories extends Model

{ 

	public $table = 'category';
    public $fillable = [

        'name_en', 'name_fr','name_nl','name_dk','name_at','name_de','name_se','name_no','name_it',
		'description_en','description_fr','description_nl','description_dk','description_at','description_de',
		'description_se','description_no','description_it','created_on'

    ];
	public $timestamps = false;
	 
	 public function setUpdatedAt($value)

    {

      return NULL;

    }


    public function setCreatedAt($value)
 
    {

      return NULL;

    }
	
	public  static function getuserData(){
    $value=DB::table('category')->orderBy('id', 'asc')->get();
	$data_val=array(); 
	foreach ($value as $val)
	{
	
	$data_val[]=$val;
	
	}
	 
    return $data_val;
  }
  
  
  
  public  static function getBookCat($book_id){
 	$dataCms = DB::select("select category_id from ebook_category where book_id='".$book_id."' order by category_id asc");
	$data_val=array(); 
	foreach ($dataCms as $val)
	{
	
	$data_val[]=$val;
	
	}
	 
    return $data_val;
  }
  
  
  public  static function getBooksOfCat($cat_id_all){
  	$dataCms = DB::select("select * from category where id in (".$cat_id_all.") ");
	$data_val=array(); 
	foreach ($dataCms as $val)
	{
	
	$data_val[]=$val->name_en;
	
	}
	 
    return $data_val;
  }
  
  
  
}