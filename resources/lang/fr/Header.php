<?php

return array (
  'home_link' => 'Home',
  'benefit_link' => 'Benefits',
  'pricing_link' => 'Pricing',
  'support_link' => 'Support',
  'cancel_membership_link' => 'Cancel Membership',
  'login_link' => 'Login',
  'signup_link' => 'Sign Up',
  'change_password' => 'Change Password',
  'logout_link' => 'Logout',
);
