<?php

return array (
  'action' => 'Action',
  'add_budget' => 'Add Budget',
  'budget_amount' => 'Budget Amount',
  'edit_budget' => 'Edit Budget',
  'name' => 'Name',
  'status' => 'Status',
  'title' => 'Budgets',
);
