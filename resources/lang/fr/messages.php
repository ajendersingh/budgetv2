<?php

return array (
  'welcome' => 'Welcome to Budget App',
  'login_link' => 'S\'identifier',
  'register_link' => 'S\'inscrire',
  'change_password_link' => 'changer le mot de passe',
  'about_link' => 'Sur',
  'benefit_link' => 'Avantages',
  'contact_link' => 'Nous contacter',
  'logout_link' => 'Se déconnecter',
  'mail_address_login' => 'Adresse électronique',
  'password_text' => 'Mot de passe',
  'remember_me_text' => 'Souviens-toi de moi',
  'forgot_pass_text' => 'Mot de passe oublié?',
);
