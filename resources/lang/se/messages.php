<?php

return array (
  'welcome' => 'Welcome to Budget App',
  'login_link' => 'Sign Up or Log In',
  'register_link' => 'Register',
  'change_password_link' => 'Change Password',
  'login_text' => 'Login',
  'about_link' => 'About',
  'benefit_link' => 'Benefits',
  'contact_link' => 'Contact Us',
  'logout_link' => 'Logout',
  'mail_address_login' => 'E-Mail Address',
  'password_text' => 'Password',
  'remember_me_text' => 'Remember Me',
  'forgot_pass_text' => 'Forgot Your Password?',
  'features_text' => 'Features',
  'about_ebook_text' => 'About Ebook',
  'the_software_text' => 'The Software',
  'cancel_account_text' => 'Cancel Account',
  'subscribe_link' => 'Subscribe',
);
