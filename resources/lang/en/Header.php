<?php

return array (
  'home_link' => 'Home',
  'benefit_link' => 'Benefits',
  'pricing_link' => 'Pricing',
  'support_link' => 'Support',
  'login_link' => 'Login',
  'signup_link' => 'Join Now',
  'change_password' => 'Change Password',
  'logout_link' => 'Logout',
  'update_profile' => 'Update Profile',
  'the_software' => 'The Software',
  'about_budgetpay' => 'Success stories',
  'features_link' => 'Features',
  'dashboard' => 'Dashboard',
  'features' => 'List of features',
  'cancel_account' => 'Cancel Account',
  'signup_login' => 'Sign Up or Log In',
);
