<?php

return array (
  'page_title' => 'Enter your Account Details',
  'first_name' => 'First Name',
  'last_name' => 'Last Name',
  'address' => 'Address',
  'zip_code' => 'Postal Code',
  'city' => 'City',
  'email' => 'E-mail',
  'phone' => 'Phone Number',
  'term_text' => 'I am 18 years old and accept the <a href="http://stage3.famcominc.com/budgetv2/terms" style="color: #545454;" target="_blank">terms and conditions</a>.<br/>
  When you sign up for the campaign. You will be charged 49.00 EUR every 30 days, until you cancel your membership.',
  'submit_btn' => 'Continue',
  'country' => 'Country',
  'term_text2' => 'I am 18 years old and accept the <a href="http://stage3.famcominc.com/budgetv2/terms" style="color: #545454;" target="_blank">terms and conditions</a>.<br/>
  When you sign up for the campaign, you receive a 3 days trial, which you pay 1.95 EUR. If you choose not to cancel the trial within the trial period, you will be charged 67.00 EUR every 30 days, until you cancel your membership.',
  'please_select_country' => 'Please select your country',
);
