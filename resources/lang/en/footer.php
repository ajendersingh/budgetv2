<?php

return array (
  'help_link' => 'Customer Support',
  'refund_link' => 'Becoming debt free',
  'terms_link' => 'Terms of Use',
  'privacy_link' => 'Becoming debt free',
  'contact_support_team' => 'Contact our support team!',
  'tab3_title' => 'Do you need help or more information?',
  'tab2_title' => 'Company',
  'tab1_title' => 'Browse Books',
  'epic_digital' => 'Epic!`s digital library includes many of the best  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using `Content here, content here`, makintg it look like readable English. <a href="http://stage3.famcominc.com/ebook/about">Read More</a>',
  'about_link' => 'About us',
  'contact_us' => 'Contact Us',
  'rights_reserved' => 'All rights reserved',
  'copyright' => 'Copyright',
  'faq' => 'Faq',
  'testimonials' => 'Testimonials',
  'news_room' => 'Talk to us',
  'footer_address' => 'East Terms Limited, Rear office Suites A, 48a High Street, Hadleigh, lpswich, United Kingdom, IP7 5AL',
  'footer_text' => 'Allow expert accountants and business managers to help you get control of your finances. Our team has a range of skills and they’re all passionate about empowering modern consumers to enjoy long-term financial freedom.',
);
