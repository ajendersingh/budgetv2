<?php

return array (
  'Accounts' => 'Accounts',
  'Name' => 'Name',
  'Is Active' => 'Is Active',
  'Action' => 'Action',
  'Description' => 'Description',
  'account_name_required' => 'Account name is required',
  'account_type_required' => 'Account Type is required',
  'currency' => '€',
  'opening_balance' => 'Opening balance',
  'account_type' => 'Account Type',
  'account_role' => 'Account Role',
  'current_balance' => 'Current Balance',
  'status' => 'status',
  'edit' => 'Edit',
  'delete' => 'Delete',
  'add_account' => 'Add Account',
  'edit_account' => 'Edit Account',
  'submit' => 'Submit',
  'back' => 'Back',
  'total_balance' => 'Total available amount',
  'amount_spent' => 'Total amount spent',
  'amount_left' => 'Total Amount Left',
  'page_title' => 'Accounts',
  'add_heading_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
  'edit_heading_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
  'listing_heading_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
  'account_added' => 'Account added successfully!',
  'account_updated' => 'Account updated successfully!',
  'delete_confirm' => 'Are you sure you want to delete this account?',
);
