<?php

return array (
  'lost_password' => 'Lost Password',
  'lost_password_text' => 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.',
  'username_email' => 'Username or email',
  'reset_password' => 'Reset Password',
);
