<?php

return array (
  'page_title' => 'Change Password',
  'cur_pass' => 'Current Password',
  'new_pass' => 'New Password',
  'confirm_pass' => 'Confirm New Password',
  'submit_btn' => 'Change Password',
);
