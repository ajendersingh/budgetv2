<?php

return array (
  'your_name' => 'Your name',
  'your_email' => 'Your email address',
  'subject' => 'Subject',
  'message' => 'Message',
  'send_btn' => 'SEND',
  'page_title' => 'Contact Us',
  'success_msg' => 'Message is sent! We will get back to you soon!',
  'new_message_email_template' => 'A new message from contact form!',
  'from' => 'From:',
  'subject_template' => 'Subject:',
  'message_template' => 'Message:',
  'cheers' => 'That`s it! Cheers!',
  'name_validation' => 'The name field is required.',
  'email_validation' => 'The email field is required.',
  'subject_validation' => 'The subject field is required.',
  'message_validation' => 'The message field is required.',
  'captcha_validation' => 'Please confirm you are not a robot',
);
