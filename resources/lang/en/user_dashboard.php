<?php

return array (
  'page_title' => 'Dashboard',
  'welcome_text' => 'Welcome to Budgets! On this page you get a quick overview of your finances. For more information, check out Accounts → Asset Accounts and of course the Budgets pages. Or just take a look around and see where you end up.',
  'remaining_to_spent' => 'Remaining to spend',
  'total_expenditure' => 'Total Expenditure',
  'provisional_balance' => 'Provisional Balance',
  'total_budgeted' => 'Total Budgeted',
  'net_income' => 'Net Disposable Income',
  'withdrwn_amount' => 'Amount Withdraw Account wise',
  'account_balance_title' => 'Available balance in accounts',
  'add_account_msg' => 'It seems you don\'t have any account. Please click bellow button to create account.',
  'add_account' => 'Add Account',
  'add_budget_msg' => 'It seems you don\'t have any budget. Please click bellow button to create budget.',
  'add_budget' => 'Add Budget',
  'add_cat_msg' => 'It seems you don\'t have any category. Please click bellow button to create category.',
  'add_category' => 'Add Category',
  'add_libility_msg' => 'It seems you don\'t have any liability. Please click bellow button to create Liability.',
  'add_liability' => 'Add Liability',
  'recent_transactions' => 'Recent Transactions',
);
