<?php

return array (
  'title' => 'Welcome Email',
  'welcome_site' => 'Welcome to the site',
  'registered_email' => 'Your registered email-id is',
  'password' => 'Your Password is',
  'registration_subject' => 'Thanks for registering with Budget Pay',
  'budget_team' => 'Budget Team',
  'footer_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged',
  'header_text' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged',
  'login_credentials' => 'Please find your login details bellow:',
  'regards_text' => 'Regards',
);
