<?php

return array (
  'your_email' => 'Your Email',
  'order_id' => 'Order ID / Last 4 Digits of Card *',
  'order_number' => 'Order ID or Last 4 digits of CC',
  'finish_cancellation' => 'Cancel Membership',
  'email_validation' => 'The email field is required.',
  'order_id_validation' => 'The order id or last 4 digit of cc is required.',
  'email' => 'Email *',
  'faq_heading' => 'Frequently Asked Questions (FAQs)',
  'account_title' => 'Account',
  'tech_problem_title' => 'Technical Problems',
);
