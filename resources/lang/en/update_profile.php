<?php

return array (
  'profile_tab' => 'Update profile',
  'card_tab' => 'Card details',
  'membership_tab' => 'Membership',
  'profile_section_title' => 'View/Update Profile Details',
  'card_section_title' => 'View/Update Card Details',
  'first_name' => 'First Name',
  'last_name' => 'Last Name',
  'address' => 'Address',
  'city' => 'City',
  'country' => 'Country',
  'please_select_country' => 'Please select your country',
  'zip_code' => 'Postal Code',
  'phone' => 'Phone',
  'profile_btn_name' => 'Update',
  'existing_card_number' => 'Existing Card Number',
  'card_number' => 'Card Number:',
  'expiration_date' => 'Expiration date',
  'cvv' => 'CVV No.',
  'cvv_details' => '3 digits on the back of your card',
  'process_button' => 'Update',
  'current_plan' => 'Current Plan:',
  'standard_label' => 'STANDARD',
  'premium_label' => 'PREMIUM',
  'upgrade_btn' => 'Upgrade',
  'downgrade_btn' => 'DownGrade',
  'cancel_membership' => 'Cancel Membership',
  'no_plan' => 'No Plan Selected',
  'profile_updated' => 'Profile updated successfully.',
);
