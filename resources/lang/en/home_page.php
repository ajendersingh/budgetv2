<?php

return array (
  'meet_next_description' => '<p>Subscribe to emotion and knowledge, to balance and critcal thinking, to <br />the pleasure that only reading can provide.</p>
<a href="pricing" class="btn-default">Get Started Today</a>
<span>Joining is free and easy.</span>',
  'email' => 'Email',
  'password' => 'Password',
  'remember_me' => 'Remember Me',
  'forgot_password' => 'Forgot Your Password?',
  'login_btn' => 'Login',
  'see_more_benefits' => 'See More Benefits',
  'bdg_make_fun' => 'Ways To Make Budgeting Fun
<span>Simplest way to manage personal finances.<br/>
Because money matters.</span>',
  'bdg_start_now' => 'Start Now - It\'s Free',
  'bdg_cash_flow' => 'Calculate profitability and cash flow',
  'bdg_help_you' => 'Your personal accountant on hand 24/7',
  'bdg_help_you_desc' => 'Thanks to our extensive range of features, consumers and businesses make better decisions.',
  'bdg_every_where' => 'BudgetPay is Everywhere.',
  'bdg_every_where_desc' => 'Budgetpay is a responsive web app. Use it anywhere with just your browser!<br/>
Don\'t like web apps? Don\'t worry, dedicated apps for Mac, iOS, and Android are coming soon!',
  'bdg_get_start' => 'Register for 3 Day Trial (Free)',
  'bdg_what_user_say' => 'What are users saying?',
  'bdg_minds' => 'The minds behind <span class="text-blue">BudgetPay</span>',
  'bdg_minds_desc' => 'We are a team of five designers from Vienna. Our mission is to make visual communication faster, easier and more satisfying for everyone.',
  'bdg_learn_more' => 'Lear more',
  'bdg_about_us' => 'About us',
  'bdg_about_us_desc' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed .',
  'bdg_lives_changed' => 'Financial freedom starts with ONE correct decision',
  'platform_tour' => 'Take the platform tour',
  'bdg_idea_plan' => 'Budget templates',
  'bdg_simply' => 'Reminders',
  'bdg_sheet' => 'Analysis',
  'bdg_pagemaker' => 'Banking',
  'bdg_variations' => 'Tutorials',
  'bdg_dictonary' => 'Reports',
  'closer_look' => 'Effortless, Modern, Accessible',
  'closer_look_desc' => 'You can have budget information on hand no matter where you are. Our software is accessible via desktop and mobile. So, when you’re about to make that purchase, check with your app first and make informed decisions that will serve you well in the long term.',
  'bdg_see_why' => '100s of users can’t be wrong - this works!',
  'join_now' => 'Start budgeting now',
  'bdg_faq' => 'Frequently Asked Questions',
  'bdg_security' => 'Use with Peace of Mind',
  'bdg_security_desc' => 'When it comes to money matters, security is of the highest priority. Our servers are secure, we use encryption and you can request proof of our certification to put your mind at rest.',
  'top_h1' => 'Learn to Budget and Enjoy Financial Freedom - All Through an App',
  'top_p1' => 'The Money Solution You’ve Been Waiting for',
  'bdg_get_start_middle' => 'Ready to smile when you think about your bank balance?',
);
