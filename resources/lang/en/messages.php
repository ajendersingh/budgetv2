<?php

return array (
  'login_link' => 'Sign Up or Log In',
  'register_link' => 'Register',
  'change_password_link' => 'Change Password',
  'logout_link' => 'Logout',
  'about_link' => 'About',
  'benefit_link' => 'Benefits',
  'contact_link' => 'Contact Us',
  'remember_me_text' => 'Remember Me',
  'forgot_pass_text' => 'Forgot Your Password?',
  'login_text' => 'Login',
  'mail_address_login' => 'E-Mail Address',
  'password_text' => 'Password',
  'welcome' => 'Welcome to Budget App',
  'features_text' => 'Features',
  'about_ebook_text' => 'About Ebook',
  'the_software_text' => 'The Software',
  'cancel_account_text' => 'Cancel Account',
  'subscribe_link' => 'Subscribe',
);
