<?php

return array (
  'fill_details' => 'Please fill in your details',
  'card_accepted' => 'Accepted Cards :',
  'choose_subscription' => 'Choose your subscription:',
  'plan_data' => '(Recurring every 30 days at 49 EUR) -  Unlimited access to all  test',
  'card_number' => 'Card Number:',
  'expiration_date' => 'Expiration date',
  'cvv' => 'CVV No.',
  'cvv_details' => '3 digits on the back of your card',
  'term_ext' => 'I have read and agree to the <a href="terms"  target="_blank" >Terms of Use</a> and the <a href="privacy-policy"  target="_blank" >Privacy Policy</a>, and certify that I am at least 18 years of age. <span id="plan" style="display:inline;">By clicking register, I agree to enrol in a 30 days membership, starting today. My credit card will be automatically billed 49 EUR every 30 days from the date of my subscription and further billing notifications will be sent from help@eBook.com.</span>The charge will appear on your card statement as eBook.com +441495367206',
  'special_offer_text' => 'This special offer comes with an  days trial to  for the price of  .
                                             This product rebills at   monthly unless cancelled.',
  'process_button' => 'Process payment',
  'plan_data2' => '3 day trial for 1.95 EUR (Recurring every 30 days at 67 EUR) -  Unlimited access to all  test',
  'term_ext2' => 'I have read and agree to the <a href="terms"  target="_blank" >Terms of Use</a> and the <a href="privacy-policy"  target="_blank" >Privacy Policy</a>, and certify that I am at least 18 years of age. <span id="plan" style="display:inline;">By clicking register, I agree to enrol in a 30 days membership, starting today. My credit card will be automatically billed 67 EUR every 30 days from the date of my subscription and further billing notifications will be sent from help@eBook.com.</span>The charge will appear on your card statement as eBook.com +441495367206',
  'complete_order' => 'Secure Checkout',
  'membership_price' => 'Membership Price',
  'secure_payment_description' => 'This transaction is protected by SSL technology and as an additional security layer for online transactions we also use 3-D secure.',
  'footer_text' => 'membershiSimply Division Limited, Unit 3b Marbridge House, Harlow, Essex, United Kingdom, CM19 5BJp footer text',
  'plan1_price' => '<div class="price text-right ">
						<p class="text-blue"><i>€</i> <span>49/</span>month</p>
					</div>
					<p>There is no limitations. You can try everything.</p>',
  'plan2_price' => '<div class="price text-right ">
						<p class="text-blue"><i>€</i> <span>67/</span>month</p>
					</div>
					<p>There is no limitations. You can try everything.</p>',
);
