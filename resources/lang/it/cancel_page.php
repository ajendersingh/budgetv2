<?php

return array (
  'full_name' => 'Full Name',
  'your_name' => 'Your Name',
  'email' => 'Email',
  'your_email' => 'Your Email',
  'phone_number' => 'Phone Number',
  'order_id' => 'Order Id',
  'order_number' => 'Order ID or Last 4 digits of CC:',
  'finish_cancellation' => 'Cancel Membership',
  'go_home' => 'Go to Home page',
  'name_validation' => 'The test name field is required.',
  'email_validation' => 'The email field is required.',
  'phone_number_validation' => 'The phone number field is required.',
  'order_id_validation' => 'The order id or last 4 digit of cc is required.',
);
