<?php

return array (
  'page_title' => 'Join Now & Start Reading Today',
  'pricing_and_duration' => '<div class="price"><sup>€</sup>29.99<span>/ month</span>
 <u>3-day trial period / 1.50 EUR</u>
 </div>',
  'first_name' => 'First Name',
  'last_name' => 'Last Name',
  'address' => 'Address',
  'zip_code' => 'Postal Code',
  'city' => 'City',
  'email' => 'E-mail',
  'phone' => 'Phone Number',
  'term_text' => 'I am 18 years old and accept the <a href="terms" style="color: #545454;" target="_blank">terms and conditions</a>.<br/>
  When you sign up for the campaign, you receive a 3 days trial, which you pay 1.50 EUR. If you choose not to cancel the trial within the trial period, you will be charged 29.99 EUR every 30 days, until you cancel your membership.',
  'submit_btn' => 'Continue',
  'country' => 'Country',
  'special_offer_text' => 'This special offer comes with an  days trial to  for the price of .
                                             This product re-bills at   monthly unless cancelled.',
);
