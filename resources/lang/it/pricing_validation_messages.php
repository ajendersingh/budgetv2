<?php

return array (
  'first_name' => 'Please fill your first name',
  'last_name' => 'Please fill your last name',
  'address' => 'Please fill your address',
  'zip_code' => 'Please fill your postal code',
  'country_code' => 'Please select country',
  'city' => 'Please fill your city',
  'email' => 'Please fill your email',
  'phone' => 'Please fill phone number',
  'terms_check' => 'Please accept terms and conditions',
);
