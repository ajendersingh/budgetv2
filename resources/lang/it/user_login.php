<?php

return array (
  'user_or_email' => 'Username or email address',
  'login_text' => 'Login',
  'register_text' => 'Register',
  'password' => 'Password',
  'login_btn' => 'LOGIN',
  'remember_me' => 'Remember Me',
  'lost_password' => 'Lost your password?',
);
