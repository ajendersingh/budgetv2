<?php

return array (
  'title' => 'Welcome Email',
  'welcome_site' => 'Welcome to the site',
  'registered_email' => 'Your registered email-id is',
  'password' => 'Your Password is',
  'registration_subject' => 'Thanks for registering with Ebook',
);
