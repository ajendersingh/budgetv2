<?php

return array (
  'logout_link' => 'Logout',
  'change_password' => 'Change Password',
  'signup_link' => 'Sign Up',
  'login_link' => 'Login',
  'home_link' => 'Home',
  'benefit_link' => 'Benefits',
  'pricing_link' => 'Pricing',
  'support_link' => 'Support',
  'cancel_membership_link' => 'Cancel Membership',
);
