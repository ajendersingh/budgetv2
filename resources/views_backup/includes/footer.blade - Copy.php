<div class="footersection">
<div class="footer-top">
<div class="body-container">
<div class="footer-toplogo">
<img src="{{asset('img/logo.png')}}" alt=""/>
</div>

<div class="footer-toppara">
<p>  
{!!html_entity_decode(__('footer.epic_digital'))!!}</p>
</div>
</div>
</div>
@auth
 <div class="footer-bottom loggedin">   
@endauth
@guest
  <div class="footer-bottom loggedin">
@endguest


<div class="body-container">

@auth

@endauth

<div class="footernav-two footer-minhight">
<!--<h1>{{ __('footer.tab2_title') }}</h1>-->
<ul>
<li>
<a href="{{ url('/about') }}">{{ __('footer.about_link') }}</a> 
<a href="{{ url('/terms') }}">{{ __('footer.terms_link') }}</a>
<a href="{{ url('/privacy-policy') }}">{{ __('footer.privacy_link') }}</a>
<a href="{{ url('/refund-policy') }}">{{ __('footer.refund_link') }} </a>
<a href="{{ url('/cancel-account') }}">{{ __('footer.help_link') }}</a>
<!--<a href="{{ url('/cancel-account') }}">{{ __('footer.cancel_link') }}</a>-->
</li>



</ul>
</div>

<div class="footernav-copy footer-minhight">
<h1>{{ __('footer.tab3_title') }}</h1>
<h2>{{ __('footer.contact_support_team') }}</h2>
<p><a href="mailto:support@ebookstore.com">support@eBookStore.com</a></p>
<div style="height:20px;"></div>

<img class="cards" src="{{asset('img/cards-secure.png')}}" style="width: 280px;margin-top: 20px;" />
<!--<ul>
<li><a href="#"><img src="{{asset('img/twitter-social.png')}}" alt=""/></a></li>
<li><a href="#"><img src="{{asset('img/facebook-social.png')}}" alt=""/></a></li>
<li><a href="#"><img src="{{asset('img/pinterest-social.png')}}" alt=""/></a></li>
<li><a href="#"><img src="{{asset('img/insta-social.png')}}" alt=""/></a></li>
</ul>-->
<div style="height:20px;"></div>
<span>© Copyright ebook. All rights reserved.</span>
</div>
<div class="centreclass"><img class="cards desktopimg" src="{{asset('img/company-textwhite.png')}}" alt="" />
<img class="cards mobileimg" src="{{asset('img/company-textwhite-mobile.png')}}" alt="" />
</div>


</div>
</div>


</div>
