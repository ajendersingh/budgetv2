<header>
		<nav>
			<div class="logosec">
				<img src="{{asset('img/safe-checkout.png')}}"/><h4>{!!html_entity_decode(__('buy_membership.safe_checkout'))!!}</h4>
			</div>
			<div class="pull-right">
				<p>{!!html_entity_decode(__('buy_membership.question_call'))!!}</p>
			</div>
			<div class="main-heading"><h1>{!!html_entity_decode(__('buy_membership.complete_order'))!!}</h1></div>
        </nav>
	</header>
    
 