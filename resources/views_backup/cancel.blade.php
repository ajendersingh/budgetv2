@extends('layouts.app')

@section('content')
<div class="inner-container">
<div class="titlemain"><h1>Support </h1></div>
    <div class="row justify-content-center cancelaccounermain">
        <div class="halfdiv-section halfblue">
            <div class="subscription-box">
                 {!!html_entity_decode($dataCms2->{'description_'.config('app.locale')})!!}
                
                
                
                
                <div class="form" style="width:80%">
                    @if (count($errors))
                    <div class="alert alert-danger">
                    	<ul>
                    	@foreach ($errors->all() as $error)
                    	<li>{{ $error }}</li>
                    	@endforeach
                    	</ul>
                    </div>    
                    @endif
                    <form method="POST" action="{{ route('cancel-account') }}">
                    @csrf 
                            
                        
                         <div class="form-group">
                            <label for="email">{{ __('cancel_page.email') }}</label>
                            <input type="phone_number" name="email"  class="form-control {{ $errors->has('email') ? 'invalid' : '' }}" value="{{ old('email') }}"  placeholder="{{ __('cancel_page.your_email') }}" >
                            
                        </div>      
                        
                         <div class="form-group">
                            <label for="email">{{ __('cancel_page.order_id') }}</label>
                            <input type="order_id" name="order_id"  class="form-control {{ $errors->has('order_id') ? 'invalid' : '' }}" value="{{ old('order_id') }}"  placeholder="{{ __('cancel_page.order_number') }}" >
                            
                        </div>         
                        
                         	

                         
                        
                         
        
                        <div style="float: left" ng-show="successMessage" class="help-block-success ng-binding ng-hide"></div>

						<div class="form-group">
	                        
                            <div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}"></div>	           
                            
                           
						</div>    
                <div class="clear h25"></div> 
                        <button type="submit" class="finishbtn" style="border:0px;" >
                            <span>{{ __('cancel_page.finish_cancellation') }}</span>
                        </button>
                    </form>
                </div>
                
                
                
                
                 
                 
                 
                
                
                 
            </div>
            
        </div>
        
         <div class="halfdiv-section"><div class="membership-contact subscription-box">
         
<div class="acordian-section">
<div class="container">
<div class="acordian-main">
<div class="acordian-main-heading"><h1>{{ __('cancel_page.faq_heading') }}</h1></div> 
        <div class="acordian-main-data">

        <div class="accordion">
        	<h5>{{ __('cancel_page.account_title') }}</h5>
            <ul class="list-group">
            @foreach ($faqData as $node)
            <li class="list-group-item" data-toggle="collapse" data-target="#faq-{{$node->id}}" >
            <h3>{!!html_entity_decode($node->{'name_'.config('app.locale')})!!}</h3>
            <div id="faq-{{$node->id}}" class="collapse innercontent">
            <p>{!!html_entity_decode($node->{'description_'.config('app.locale')})!!}</p>
            </div>
            </li>
            @endforeach 
            
            
            </ul>
            <div class="clear h25"></div>
        	<h5>{{ __('cancel_page.tech_problem_title') }}</h5>
            <ul class="list-group">
            @foreach ($faqDataTech as $node)
            <li class="list-group-item" data-toggle="collapse" data-target="#technical-{{$node->id}}" >
            <h3>{!!html_entity_decode($node->{'name_'.config('app.locale')})!!}</h3>
            <div id="technical-{{$node->id}}" class="collapse innercontent">
            <p>{!!html_entity_decode($node->{'description_'.config('app.locale')})!!}</p>
            </div>
            </li>
            @endforeach 
            
            
            </ul>
        </div>
      
		</div>

</div>
</div>
</div>
{!!html_entity_decode($dataCms->{'description_'.config('app.locale')})!!}
 
 
            
            

           </div></div> 
            
            
    </div>
    
   <!-- <div class="featured-books-section">
    <div class="title">Featured Books <a href="">View All</a></div>
    
    
    
    </div>-->
    
    
    
    
</div>
@endsection
