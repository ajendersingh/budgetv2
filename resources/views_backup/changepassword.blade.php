@extends('layouts.app')

@section('content')
<div class="inner-container">
    <div class="logincontainer">
                    <div class="loginsection">
                <div class="login-signup-title"><h3 style="color: rgb(0, 0, 0);">{{ __('change_password.page_title') }}</h3></div>
                <div class="loginform">
                    @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                    <form class="form-horizontal" method="POST" action="{{ route('changePassword') }}">
                        {{ csrf_field() }}

                        <div class=" form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                            <label class="col-form-label">{{ __('change_password.cur_pass') }}</label>
                                <input id="current-password" type="password" placeholder="" class="form-control" name="current-password" required>

                                @if ($errors->has('current-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class=" form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                            <label class="col-form-label">{{ __('change_password.new_pass') }}</label>
                                <input id="new-password" type="password" class="form-control" placeholder="" name="new-password" required>

                                @if ($errors->has('new-password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group"> 
                            <label class="col-form-label">{{ __('change_password.confirm_pass') }}</label>
                                <input id="new-password-confirm" type="password" placeholder="" class="form-control" name="new-password_confirmation" required>
                            </div>

                        <div class="loginform-buttons">
                                <button type="submit" class="btn btn-primary"><span class="arrow-icon">{{ __('change_password.submit_btn') }}</span> </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>

 
@endsection