@extends('layouts.payment')

@section('content')


<div class="container ">
<script  type="application/javascript">

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
};

$(document).ready(function(){
    $('#Card').keypress(validateNumber);
});

</script>
<form  method="post" action="{{ route('subscribe-membership') }}">
@csrf 
 <div class="container">
		<div class="membership-sec">
			<ul>
				<li>
					<h3 class="pull-left membertitle">{!!html_entity_decode(__('buy_membership.membership_price'))!!}</h3>
                    
                    @if ($plan_id==1)
                    {!!html_entity_decode(__('buy_membership.plan1_price'))!!} 
                    @endif
                    @if ($plan_id==2)
                    {!!html_entity_decode(__('buy_membership.plan2_price'))!!} 
                    @endif
                    
 				</li><hr/>
				<li class="btm-detail">
					{!!html_entity_decode(__('buy_membership.secure_payment_description'))!!} 
				</li>
			</ul>			
		</div>
		<div class="form-sec">
			<div class="card-sec"><h4>{{ __('buy_membership.card_accepted') }} <img src="{{asset('img/payment.png')}}"/></h4></div>
			<h3 class="f20">{{ __('buy_membership.fill_details') }}</h3>
			<div class="subscription">
				<label>{{ __('buy_membership.choose_subscription') }}</label>
			    <select  id="product_id_choosed"  name="product_id">
			      <option  value="1" {{ $plan_id == "1" ? "selected" : "" }} >{{ __('buy_membership.plan_data') }}</option>
                  <option  value="2" {{ $plan_id == "2" ? "selected" : "" }} >{{ __('buy_membership.plan_data2') }}</option>
			    </select>
			</div>
			<div class="card-number">
				<label>{{ __('buy_membership.card_number') }}</label>
                <input type="text" name="cc-cardnumber"  maxlength="16"  placeholder="0000 0000 0000 0000" id="Card" value="{{ old('cc-cardnumber') }}" >
			</div>
			<div class="exp-date">
				<label>{{ __('buy_membership.expiration_date') }}</label>
                    <select  name="cc-expires-month">
                        <option value="01" {{ old("cc-expires-month") == "01" ? "selected" : "" }}>01</option>
                        <option value="02" {{ old("cc-expires-month") == "02" ? "selected" : "" }}>02</option>
                        <option value="03" {{ old("cc-expires-month") == "03" ? "selected" : "" }}>03</option>
                        <option value="04" {{ old("cc-expires-month") == "04" ? "selected" : "" }}>04</option>
                        <option value="05" {{ old("cc-expires-month") == "05" ? "selected" : "" }}>05</option>
                        <option value="06" {{ old("cc-expires-month") == "06" ? "selected" : "" }}>06</option>
                        <option value="07" {{ old("cc-expires-month") == "07" ? "selected" : "" }}>07</option>
                        <option value="08" {{ old("cc-expires-month") == "08" ? "selected" : "" }}>08</option>
                        <option value="09" {{ old("cc-expires-month") == "09" ? "selected" : "" }}>09</option>
                        <option value="10" {{ old("cc-expires-month") == "10" ? "selected" : "" }}>10</option>
                        <option value="11" {{ old("cc-expires-month") == "11" ? "selected" : "" }}>11</option>
                        <option value="12" {{ old("cc-expires-month") == "12" ? "selected" : "" }}>12</option>
                    </select>
			</div>
		    <div class="exp-year">
		    	<label>&nbsp;</label>
                    <select  name="cc-expires-year" >
                    
                        @for($i=date('Y');$i<=date('Y')+10;$i++ )
                        
                        <option value="{{$i}}" {{ old("cc-expires-year") == "$i" ? "selected" : "" }}>{{$i}}</option>
                        
                        @endfor
                    
                    </select> 
			</div>
			<div class="cvv-number">
				<label>{{ __('buy_membership.cvv') }}</label>
                <input type="text"  name="cc-cvv2" id="CVV"  maxlength="4"  size="3" value="" placeholder="XXX" >
			</div>
			<div class="cvv-img">
				<label>&nbsp;</label>
			    <img src="{{asset('img/cvv.png')}}"/><p>< {{ __('buy_membership.cvv_details') }}</p>
			</div>
			<div class="terms-sec">
				<label> 
                <input type="checkbox" id="terms" name="terms" value="I accept the terms" data-parsley-required="true" data-parsley-trigger="click" data-parsley-multiple="terms">
                
                 <p>
                    @if ($plan_id==1)
                    {!!html_entity_decode(__('buy_membership.term_ext'))!!} 
                    @endif
                    @if ($plan_id==2)
                    {!!html_entity_decode(__('buy_membership.term_ext2'))!!} 
                    @endif
                 </p>
                 
                 </label>
			</div>
			<div class="submit-btn">
				<input type="submit" name="Process Payment" value="{{ __('buy_membership.process_button') }}" >
			</div>
		</div>
	</div>
  </form>    
</div>
 
@endsection
