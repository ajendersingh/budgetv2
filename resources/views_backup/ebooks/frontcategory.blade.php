@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 
 
 
				 
                <div class="category-sidebar">
                <h1 style="font-size:26px;" id="mobile_cat_div" ><a  href="javascript:void(0);" id="cat_title_ebook">{!!html_entity_decode(__('category_books_listing.title'))!!}</a></h1>
                <h1 style="font-size:26px;" id="desktop_cat_div" >{!!html_entity_decode(__('category_books_listing.title'))!!}</h1>
                	
                     
                    <ul class="show_hide_cat_div hideCatClass">
                      
						@foreach ($categorydata_all as $node)                          
                         <li class='{{$Category_id == $node->id ? 'active': ''}}' ><a href='{{ env('APP_URL') }}bookcategory/{{$node->id}}'> {{ $node->{'name_'.config('app.locale')} }}</a> </li>
        			    @endforeach
                    </ul> 
                    
                </div>
                <div class="category innerright-side">
                 
                
                           <h1> 
                           		@if($categorydata!="")                 
                           	    {{ $categorydata->{'name_'.config('app.locale')} }}
                                 @endif  
                                <span>
                               @if($totalBooks>0)
                               ({{$totalBooks}} Result)
                               @endif
                               </span> </h1>  
                                <!--<div class="page-heading">
                                  
                                <p class="total-result"></p>
                            </div>-->
                <ul>
                	@if($ebooks>0)
                    @foreach ($ebooks as $node)
                    <li class="category-list"> 
                        <div class="category-box">
                            <a href="{{ env('APP_URL') }}ebookview/{{ $node->id }}">
                                <div class="category-img">
                                    <img src="{{ env('APP_URL') }}/public/pdfimages/{{ $node->cover_image }}"/>
                                </div>
                                <div class="category-text">
                                    <div class="title"> 
                                     {{ $node->{'book_name_'.config('app.locale')} }}  
                                     </div>
                                    <!--<p><span>Author:</span> Amy Alward</p> -->   				
                                </div>
                                <div class="rating">
                                    <span class="stats">&nbsp;</span>
                                </div>
                            </a>
            
                        </div>
                    </li>
                    @endforeach
                    @endif 
                   
            
                </ul>
                 @if($book_exist==0)
    			<div style="text-align:center ">{!!html_entity_decode(__('category_books_listing.no_books_found'))!!}</div>
    		    @endif
                  
                </div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 