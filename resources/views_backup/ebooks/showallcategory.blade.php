@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>
<div class="page">
<div class="inner-container">
<h1>Books</h1>

<div class="brief-text"><p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p></div>

	<div class="breadcrumbs" style="display:none">
<a href="{{ env('APP_URL') }}">Home</a> <span>\</span> <u>Books</u>
</div>			
                 
    <div class="category  allbookslisting">
    		      
                    <!--<div class="page-heading">
                      
                    <p class="total-result"></p>
                </div>-->
    <ul>
    	 
    	@foreach ($showallcategory as $node)
    	<li class="category-list"> 
    		<div class="category-box">
    			<a href="{{ env('APP_URL') }}bookcategory/{{$node->id}}">
	    			<div class="category-img">
	    				<img src="{{ env('APP_URL') }}/public/catimages/{{ $node->image }}"/>
	    			</div>
	    			<div class="category-text">
	    				<div class="title">@if(app()->getLocale() =='en')
                        {!!html_entity_decode($node->name_en)!!} 
                        @endif
                         
                        @if(app()->getLocale() =='fr')
                        {!!html_entity_decode($node->name_fr)!!}
                        @endif
                        
                        @if(app()->getLocale() =='sp')
                        {!!html_entity_decode($node->name_sp)!!}
                        @endif</div>
	    				<!--<p><span>Author:</span> Amy Alward</p> -->   				
	    			</div>
	    			<div class="rating">
	    				<span class="stats"><img src="{{asset('img/bookimg/rate.jpg')}}"/> (4/5)</span>
	    			</div>
    			</a>

    		</div>
    	</li>
        @endforeach
    	 
    	 
    	 
    	 
    	 
    	 
    	 
    	 
    	 
    	 


    </ul>
    </div>
    <div class="clear"></div>
    </div>
</div>
@endsection
 