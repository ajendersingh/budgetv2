@extends('layouts.flex') 

@section('content')

      <div id="documentViewer" class="flexpaper_viewer" style="width:100%;height:100vh"></div>

<script type="application/javascript"  defer="defer">
    
	  
	$('#documentViewer').FlowPaperViewer(
                         { config : {

                           IMGFiles : ''+site_url_js_path+'/public/pdfhtml5images/{{$swf_file_name}}-{page}.jpg',
                           JSONFile : ''+site_url_js_path+'/public/pdfjson/{{$swf_file_name}}.js',
                           FontsToLoad : ["g_font_54","g_font_55","g_font_56","g_font_57","g_font_58","g_font_59","g_font_60","g_font_61","g_font_62","g_font_63","g_font_64"],
                           
                           Scale : 0.6,
                           ZoomTransition : 'easeOut',
						   PrintEnabled: false,
						   PrintToolsVisible: false, 
                           ZoomTime : 0.5, 
                           ZoomInterval : 0.1,
                           FitPageOnLoad : true,  
                           FitWidthOnLoad : false,
                           AutoAdjustPrintSize : true,
                           PrintPaperAsBitmap : false,
                           FullScreenAsMaxWindow : false, 
                           ProgressiveLoading : false,
                           MinZoomSize : 0.3,
                           MaxZoomSize : 5,
                           SearchMatchAll : true,
                           InitViewMode : 'Zine',
                           RenderingOrder : 'html5,flash',
                           StartAtPage : 1,

                           PreviewMode : true,
                           MixedMode : true,
                           EnableWebGL : true,
                           ViewModeToolsVisible : true,
                           ZoomToolsVisible : true,
                           NavToolsVisible : true,
                           CursorToolsVisible : true,
                           SearchToolsVisible : true,
                           jsDirectory : ''+site_url_js_path+'js/',
                           cssDirectory : ''+site_url_js_path+'css/',
						   localeDirectory : ''+site_url_js_path+'locale/',
                           WMode : 'transparent',
						   key : '$7b133916524c8c15b1f',
                           localeChain: 'en_US'
                         }}
                  );
				
				
				
   
</script>
    </div>
    </div>
</div>
@endsection
 