@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>

<template>
<component :is="'style'">
body{background:#fff}
</component>
</template>


<div class="page">
<div class="inner-container">


 
 



<div class="breadcrumbs">
<a href="{{ env('APP_URL') }}">{!!html_entity_decode(__('ebbokview.home_link'))!!}</a> <span>\</span>   <u> {{ $ebooks->{'book_name_'.config('app.locale')} }} </u>
</div>
				
                 
    <div class="category allbookslisting">
    		  
<div class="book-detail-left"><img src="{{ env('APP_URL') }}/public/pdfimages/{{ $ebooks->cover_image }}"  />
<br/>
 
<a href="{{ env('APP_URL') }}ebookpreview/{{ $ebooks->id }}"  target="_blank">{!!html_entity_decode(__('ebbokview.read_link'))!!}</a><br/>
 
</div>
<div class="book-detail-right">
                    
                     <h1> 
                     	{{ $ebooks->{'book_name_'.config('app.locale')} }}                 
                         
                    <div class="clear"></div>
                    <!--<div class="rating"><span class="stats"><img src="http://stage3.famcominc.com/budget/img/bookimg/rate.jpg"> (4/5)</span></div>--> 
                    
                    </h1>
                    
                    
                    
                    	{!!html_entity_decode($ebooks->{'book_description_'.config('app.locale')})!!}
                        
                    	 
                    <!--<div class="page-heading">
                      
                    <p class="total-result"></p>
                </div>-->
                
                </div>
                <div class="clear" style="margin-bottom:100px;"></div>
       

 
    </div>
    <div class="clear"></div>
    </div>
</div>
@endsection
 