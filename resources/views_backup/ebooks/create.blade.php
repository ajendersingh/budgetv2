@extends('layouts.admin')

  

@section('content')

<div class="row">


    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Ebook</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary-edit" href="{{ route('ebooks.index') }}"> Back</a>

        </div>

    </div>

</div>


   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

 <div>
 <form method="post" action="{{ route('upload') }}" id="frm_ebbok" enctype="multipart/form-data">
 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

    @csrf

  

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (English):</strong>

              <input type="text" name="book_name_en" id="book_name_en" class="form-control" placeholder="Name (English)" value="{{ old('book_name_en') }}">

            </div>

       </div> 
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (France):</strong>

                <input type="text" name="book_name_fr" id="book_name_fr" class="form-control" placeholder="Name (France)" value="{{ old('book_name_fr') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Netherlands):</strong>

                <input type="text" name="book_name_nl" id="book_name_nl" class="form-control" placeholder="Name (Netherlands)" value="{{ old('book_name_nl') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Denmark):</strong>

                <input type="text" name="book_name_dk" id="book_name_dk" class="form-control" placeholder="Name (Denmark)" value="{{ old('book_name_dk') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Austria):</strong>

                <input type="text" name="book_name_at" id="book_name_at" class="form-control" placeholder="Name (Austria" value="{{ old('book_name_at') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Germany):</strong>

                <input type="text" name="book_name_de" id="book_name_de" class="form-control" placeholder="Name (Germany)" value="{{ old('book_name_de') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Sweden):</strong>

                <input type="text" name="book_name_se" id="book_name_se" class="form-control" placeholder="Name (Sweden)" value="{{ old('book_name_se') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Norway):</strong>

                <input type="text" name="book_name_no" id="book_name_no" class="form-control" placeholder="Name (Norway)" value="{{ old('book_name_no') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Italian):</strong>

                <input type="text" name="book_name_it" id="book_name_it" class="form-control" placeholder="Name (Italian)" value="{{ old('book_name_it') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (English):</strong>
				
                <textarea name="book_description_en" id="book_description_en" style="width:100%">{{ old('book_description_en') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (France):</strong>
				
                <textarea name="book_description_fr" id="book_description_fr" style="width:100%">{{ old('book_description_fr') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Netherlands):</strong>
				
                <textarea name="book_description_nl" id="book_description_nl" style="width:100%">{{ old('book_description_nl') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Denmark):</strong>
				
                <textarea name="book_description_dk" id="book_description_dk" style="width:100%">{{ old('book_description_dk') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Austria):</strong>
				
                <textarea name="book_description_at" id="book_description_at" style="width:100%">{{ old('book_description_at') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Germany):</strong>
				
                <textarea name="book_description_de" id="book_description_de" style="width:100%">{{ old('book_description_de') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Sweden):</strong>
				
                <textarea name="book_description_se" id="book_description_se" style="width:100%">{{ old('book_description_se') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Norway):</strong>
				
                <textarea name="book_description_no" id="book_description_no" style="width:100%">{{ old('book_description_no') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Italian):</strong>
				
                <textarea name="book_description_it" id="book_description_it" style="width:100%">{{ old('book_description_it') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Category:</strong>

                 @foreach ($dataCms as $node)
                 <input type="checkbox" name="cat_id" value="{{ $node->id }}" />&nbsp;{{ $node->name_en }}&nbsp;&nbsp;
                @endforeach
 
            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Featured:</strong>

                <input type="checkbox" name="featured" id="featured" value="1" />

            </div>

        </div>
        
        <!--<div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Tags:</strong>

                 @foreach ($dataCms2 as $node)
                <input type="checkbox" name="tag_id" value="{{ $node->id }}" />&nbsp;{{ $node->name_en }}&nbsp;&nbsp;
                 @endforeach
 
            </div>

        </div>-->
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Select Pdf</strong>

                <div class="col-md-12" style="padding-left:0px;">
                              <input type="file" name="file" id="file" />
                            </div>
                            <div class="col-md-12" style="padding-left:0px;">
                              <input type="submit" name="upload" value="Upload" class="btn btn-success" style="margin-top: 10px;"/>
                            </div> 

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Select Image</strong>

                <div class="col-md-12" style="padding-left:0px;">
                               <input type="file" name="image_file" id="image_file" />
                            </div>
                            <div class="col-md-12" style="padding-left:0px;">
                              <input type="button" name="btn1_upload_img" id="btn1_upload_img" value="Upload" class="btn btn-success" style="margin-top: 10px;"/>
                            </div> 

            </div>

        </div>
                        
                    
                    
                     
                         
                         
                    <div style="clear:both"></div>
                        <div class="progress" style="width: 90%;margin-left: 16px;margin-top: 20px;margin-bottom: 20px;height: 30px;font-size: 15px;">
                      <div class="progress-bar" role="progressbar" aria-valuenow=""
                      aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        0%
                      </div>
                    </div>
                    <br />
                    <div id="success" style="margin-left: 15px;">

                    </div>
        
         
        
         
        
       
        
 				<input type="hidden" name="pdf_file_name_hidden" id="pdf_file_name_hidden"  />
                <input type="hidden" name="image_file_name_hidden" id="image_file_name_hidden"  />
         
         
        
         
        
         
 
          
        
         
		<div id="process_image" style="position: fixed; top: 0px; left: 0px; right: 0; bottom: 0; background: #00000080;z-index: 999999; display:none;"><img src="{{ env('APP_URL') }}img/loader.gif" style="margin: 150px auto;position: fixed;left: 0;right: 0;"/></div>
        
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <input type="button" name="btn1_upload" id="btn1_upload" class="btn btn-primary-edit" value="Submit" style="width: 100px; height: 40px;">

        </div>

    </div>

   

</form>


<div>

@endsection
