@extends('layouts.admin')

 

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Subscribed Users </h2>

            </div>

             

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p> 

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>

            <th width="5%">No</th>

            <th>Name</th>
            
            <th>Email</th>
                        
            <th>Register Date</th>
            
            <th>Status</th>
            
            <th>Membership Purchase Date</th>
            
            
 
            <th>Action</th>

        </tr>

        @foreach ($subscribers as $subscriber)

        <tr>

            <td>{{ ++$i }}</td>

            <td>{{ $subscriber->name }}</td> 

            <td>{{ $subscriber->email }}</td>
            
             
            <td>{{date('m-d-Y', strtotime($subscriber->created_at))}}</td>
            
            
            <td>{{$subscriber->status}}</td>
            
            
            <td>{{date('m-d-Y', strtotime($subscriber->membership_purchase_date))}}</td>
            
            
            
             

            <td>

                 

   

                    

    

                    <a class="btn btn-success" href="{{ env('APP_URL') }}admin/subscriber_delete/{{ $subscriber->id }}">Unsubscribe</a>

   

                     

            </td>
 
        </tr>

        @endforeach

    </table>

  

    {!! $subscribers->links() !!}

      

@endsection
