<form action="" id="search_form" method="get" autosugget="on" class="search_form">
{{ csrf_field() }}
 					
		<div class="formitem">
		
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.title'))!!}<span class="red">*</span></strong>
				<input class="date form-control" autosugget="off"  type="text" name="search_text" value="{{isset($s_text) ?$s_text:''}}">
            </div>
        </div>	
		
					
		<div class="formitem">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.start_date'))!!}<span class="red">*</span></strong>
				<input class="date form-control startDate" autosugget="off" type="text" id="datepicker" value="{{isset($s_start_date) ?$s_start_date:''}}" name="start_date">
            </div>
        </div>	 

		<div class="formitem">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.end_date'))!!}<span class="red">*</span></strong>
				<input class="date form-control endDate" autosugget="off" type="text" value="{{isset($s_end_date) ?$s_end_date:''}}" id="datepicker1" name="end_date">
            </div>
        </div>


		
			<div class="formitem">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.trans_type'))!!}<span class="red">*</span></strong>

                <select class="form-control" name="transaction_type" >
					 
					<option value="">{!!html_entity_decode(__('transactions.select_option'))!!} </option>
					@foreach ($trans_types as $key => $value)
						<option value="{{ $value->title }}" {{isset($s_trans_type) && $value->title== $s_trans_type?'selected':''}}>
						{{ $value->title}}</option>
					@endforeach
				</select>

            </div>

        </div>
		<div class="formitem" id="source_account_div"> 
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.source_account'))!!} <span class="red">*</span>:</strong>
               <select class="form-control" id="account" name="account" >
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($accounts as $key => $value)
						<option value="{{ $value->id }}" {{isset($s_account) && $value->id== $s_account?'selected':''}} >
						{{ $value->title}}</option>
					@endforeach
					</select>
            </div>

        </div>
				<div class="formitem">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.category'))!!}:</strong>
               <select class="form-control" id="category" name="category">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($categories as $key => $value)
						<option value="{{ $value->cat_id }}" {{isset($s_category) && $value->cat_id== $s_category?'selected':''}}>
						{{ $value->cat_name}}</option>
					@endforeach
					</select>
            </div>

        </div>
		
				<div class="formitem">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.budget'))!!}:</strong>
               <select class="form-control" id="budget" name="budget">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($budgets as $key => $value)
						<option value="{{ $value->id }}" {{isset($s_budget) && $value->id== $s_budget?'selected':''}}>
						{{ $value->name}}</option>
					@endforeach
					</select>
            </div>

        </div>
					<div class="submit-search">
                    <span><input type="submit" value="{!!html_entity_decode(__('transactions.search'))!!}" /></span>
					<span><a href="{{ env('APP_URL') }}transactions" ><i class="fa fa-trash" aria-hidden="true"></i> {!!html_entity_decode(__('transactions.clear_search'))!!}</a></span>
                    </div>
					</form>
                    
                     