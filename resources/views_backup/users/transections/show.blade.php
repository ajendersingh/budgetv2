@extends('categories.layout')


@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2> Show Transaction</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('transactions.index') }}"> Back</a>

            </div>

        </div>

    </div>

   

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.title'))!!}:</strong>

                {{ $trans->title }}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.trans_type'))!!}:</strong>

                {{ $trans->transaction_type }}

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.amount'))!!} :</strong>

                {!!html_entity_decode(__('common.currency_symbol'))!!}{{$trans->trans_amount}}

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.trans_date'))!!}:</strong>

                {{$trans->trans_date}}

            </div>

        </div>

    </div>

@endsection
