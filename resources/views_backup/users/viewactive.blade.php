@extends('layouts.admin')

   

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>View Active Subscriber</h2>

            </div>

            <div class="pull-right"> 

                <a class="btn btn-primary-edit" href="{{ env('APP_URL') }}admin/active/subscribers" > Back</a>

            </div>

        </div>

    </div>
 
   

    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

  @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

    

   

         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>First Name:</strong>

                    <input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" placeholder="First Name" readonly="readonly">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Last Name:</strong>

                    <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" placeholder="Last Name" readonly="readonly">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Address:</strong>

                    <input type="text" name="address" value="{{ $user->address }}" class="form-control" placeholder="Address" readonly="readonly"> 

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Postal Code:</strong>

                    <input type="text" name="zip_code" value="{{ $user->zip_code }}" class="form-control" placeholder="Postal Code" readonly="readonly">

                </div>

            </div>

			<div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Country:</strong>

                    
                <select name="country_code" id="country_code" class="form-control " readonly="readonly" > 
                @foreach ($countrydata as $node)
                <option value="{{$node->id}}" {{$node->id == $user->country_id ? "selected": ""}} >{{$node->country_name}}</option>
                @endforeach 
                </select>
                    

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12"> 

                <div class="form-group">

                    <strong>City:</strong>

                    <input type="text" name="city" value="{{ $user->city }}" class="form-control" placeholder="City" readonly="readonly">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Email:</strong>

                    <input type="text" name="email" value="{{ $user->email }}" class="form-control" placeholder="Email" readonly="readonly">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Phone:</strong>

                    <input type="text" name="phone" value="{{ $user->phone }}" class="form-control" placeholder="Phone" readonly="readonly">

                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Unsubscribe Date:</strong>

                    <input type="text" name="phone" value="{{ $user->unsubscribe_date }}" class="form-control" placeholder="" readonly="readonly">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12" style="text-align:left;">
            <a class="btn btn-success" href="{{ env('APP_URL') }}admin/subscriber_remove/{{ $user->id }}">Unsubscribe</a>
            </div>	
            
       <form  method="POST"  action="{{ action('UserController@change_password') }}">

        @csrf

        @method('PUT')
        <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Password:</strong>

                    <input type="text" name="password" value="" class="form-control" placeholder="" >
                    <input type="hidden" name="subscriber_id" value="{{ $subscriber_id }}" class="form-control" placeholder="">

                </div>

            </div>
             
             
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

        </div>
            </form>
            
            <!--<div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Password:</strong>

                <input type="password" name="password" class="form-control" >

            </div>

        </div>-->
            
             
            
             

            <!--<div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

            </div>-->

        </div>

   

 

@endsection
