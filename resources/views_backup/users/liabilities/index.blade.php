@extends('layouts.app')
@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">


<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
     
     $(document).on('click', '#cat_title_ebook', function(event){
   			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 		
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		</div>
		<div class="category innerright-side">
		<div class="page-title">		
				<h1>{!!html_entity_decode(__('liabilities.page_title'))!!} </h1>
				 <a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}liabilities/add">{!!html_entity_decode(__('liabilities.add_liability'))!!}</a>
				   </div>
                 <div class="dashborad-about"> 
				<p>{!!html_entity_decode(__('liabilities.listing_heading_text'))!!}</p> </div>

				  <div class="flashmessage">
                  @include('flash-message')
                  </div>
                  
				  <div class="dashboard-recenttable">
				 <div id="no-more-tables">
                 <table class="table table-bordered">

 			<thead>
        <tr>
           <th>{!!html_entity_decode(__('liabilities.title'))!!} </th>
			<th>{!!html_entity_decode(__('liabilities.amount'))!!}</th>
			<th>{!!html_entity_decode(__('liabilities.debt_amount'))!!}</th>
			<th>{!!html_entity_decode(__('liabilities.account'))!!}</th>
			<th>{!!html_entity_decode(__('liabilities.start_date'))!!}</th>
			<th>{!!html_entity_decode(__('liabilities.end_date'))!!}</th>
			<th>{!!html_entity_decode(__('liabilities.type'))!!}</th>
			 
			<th>{!!html_entity_decode(__('liabilities.action'))!!}</th>

        </tr>
		 </thead>
        @foreach ($liabilities as $lib)

        <tr>


            <td data-title="{!!html_entity_decode(__('liabilities.title'))!!}">
			<a href="{{ env('APP_URL') }}liabilities/show/{{$lib->id }}">{{ $lib->title }}</a>
			</td>
			<td data-title="{!!html_entity_decode(__('liabilities.amount'))!!}">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $lib->liability_amount }} </td>
			<td data-title="{!!html_entity_decode(__('liabilities.debt_amount'))!!}">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $lib->amount_of_debt }} </td>
			 
			<td data-title="{!!html_entity_decode(__('liabilities.account'))!!}">{{ $lib->account_name}}</td>
			<td data-title="{!!html_entity_decode(__('liabilities.start_date'))!!}">{{ $lib->start_date}}</td>
			<td data-title="{!!html_entity_decode(__('liabilities.end_date'))!!}">{{ $lib->end_date }}</td>
			<td data-title="{!!html_entity_decode(__('liabilities.type'))!!}">{{ $lib->type_name }}</td>
 			 
            <td data-title="{!!html_entity_decode(__('liabilities.action'))!!}"> 
			<a  class="btn btn-success editicon" href="{{ route('liabilities.edit',$lib->id) }}">{!!html_entity_decode(__('liabilities.edit'))!!}</a>
			<a  class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('liabilities.delete_confirm'))!!}')"  href="{{ env('APP_URL') }}remove_liability/{{$lib->id }}">{!!html_entity_decode(__('liabilities.delete'))!!}</a>
			 

            </td>

        </tr>

        @endforeach

    </table>
		</div></div>
        
        <div class="pagination">
			{{ $liabilities->links() }}
		</div>						 
				 
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 