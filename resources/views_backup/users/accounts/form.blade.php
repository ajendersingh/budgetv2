    <form action="{{ route('accounts.update',$account->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
				<input type="hidden" name="id" value="{{$account->id}}" />
                <input type="text" name="title" class="form-control"  value="{{ $account->title }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Account Type:</strong>
                <select name="account_type">
					@foreach ($account_types as $key => $val)
						<option value="{{ $val->name }}" {{ ( $val->name == $account->account_type) ? 'selected' : '' }} >
						{{ $val->name}}</option>
					@endforeach
					</select>
            </div>
        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">
           <div class="form-group">
                <strong>Opening balance</strong>
               <input type="text" name="opening_balance" class="form-control"  value="{{ $account->opening_balance }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Account Role:</strong>

               <select name="account_role">
					@foreach ($account_roles as $key => $value)
						<option value="{{ $value->name }}"
						@if ( $value->name == $account->account_role)
							selected="selected"
						@endif
						>
						{{ $value->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
         <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description:</strong>

                <textarea name="description" id="description" style="width:100%">{{ $account->description }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

            </div>

        </div>

    </form>

