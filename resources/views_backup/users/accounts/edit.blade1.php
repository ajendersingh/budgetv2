@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
		<div class="category innerright-side">

				<div class="page-title">
                <h1>{!!html_entity_decode(__('accounts.edit_account'))!!}</h1>
  
               <a class="btn btn-success purple-btn" href="{{ route('accounts.index') }}"> {!!html_entity_decode(__('accounts.back'))!!}</a>
     </div>
			<div class="dashborad-about">
			<p>{!!html_entity_decode(__('accounts.edit_heading_text'))!!}</p>
  </div>
 
 <div class="dashboard-form">	
    <form action="{{ route('accounts.update',$account->id) }}" method="POST" enctype="multipart/form-data">

        @csrf

        @method('PUT')

   

         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.Name'))!!}:</strong>
				<input type="hidden" name="id" value="{{$account->id}}" />
                <input type="text" name="title" class="form-control"  value="{{ $account->title }}">

            </div>

        </div>
        
         
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.account_type'))!!}:</strong>

                <select name="account_type" class="form-control">
					@foreach ($account_types as $key => $val)
						<option value="{{ $val->name }}" {{ ( $val->name == $account->account_type) ? 'selected' : '' }} >
						{{ $val->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.opening_balance'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!}</strong>

                <input type="text" readonly name="opening_balance" class="form-control"  value="{{ $account->opening_balance }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Account Role:</strong>

               <select name="account_role" class="form-control">
					@foreach ($account_roles as $key => $value)
						<option value="{{ $value->name }}"
						@if ( $value->name == $account->account_role)
							selected="selected"
						@endif
						>
						{{ $value->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
 
 
        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.Description'))!!}:</strong>

                <textarea name="description" id="description" style="width:100%">{{ $account->description }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit purple-btn" style="width: 100px; height: 40px;">{!!html_entity_decode(__('accounts.submit'))!!}</button>

            </div>

        </div>

   

    </form>
</div>
</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 
