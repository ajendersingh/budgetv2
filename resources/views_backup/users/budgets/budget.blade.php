@extends('layouts.app')
@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
     
     $(document).on('click', '#cat_title_ebook', function(event){
   			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 
  
</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		</div>
		<div class="category innerright-side">
				
                <div class="page-title">
				<h1> {!!html_entity_decode(__('budgets.title'))!!}</h1> 
                <a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}budgets/add">{!!html_entity_decode(__('budgets.add_budget'))!!}</a>
                 </div>
                  <div class="dashborad-about">
				 <p>{!!html_entity_decode(__('budgets.listing_heading_text'))!!}</p>
                 </div>
                 
                 <div class="flashmessage">
				  @include('flash-message')
				  </div>
                  
                  <div class="dashbaordchart">
				  <h2>{!!html_entity_decode(__('budgets.total_available_budget'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} {{$total_budget}}
					<p style="margin:0;">{!!html_entity_decode(__('budgets.between'))!!} {{$month_date}}</p>
				  </h2>
				  
				  <div class="progress">
				  <div class="progress-bar" style="width:{{$pct_spent}}%"></div>
				</div>
				<p> {!!html_entity_decode(__('budgets.spent'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} {{$total_spent}} </p>
				</div>				 
                 
                 
                 
                 <div class="dashboard-recenttable">
                 
                  <div id="no-more-tables"> 
                 <table class="table table-bordered" style="margin-top:10px;">

		<thead>
        <tr>
            <th>{!!html_entity_decode(__('budgets.name'))!!} </th>
			<th>{!!html_entity_decode(__('budgets.budget_amount'))!!}</th>
			<th>{!!html_entity_decode(__('budgets.budget_type'))!!}</th>
			<th>{!!html_entity_decode(__('budgets.spent'))!!}</th>
			<th>{!!html_entity_decode(__('budgets.left'))!!}</th>
			<th>{!!html_entity_decode(__('budgets.status'))!!}</th>
			<th>{!!html_entity_decode(__('budgets.action'))!!}</th>

        </tr>
		 </thead>
        @foreach ($budgets as $budget)

        <tr>


            <td data-title="{!!html_entity_decode(__('budgets.name'))!!}"><a href="{{ env('APP_URL') }}budgets/show/{{$budget->id}}">{{ $budget->name }}</a></td>
			<td data-title="{!!html_entity_decode(__('budgets.budget_amount'))!!}">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $budget->budget_amount }} </td>
			<td data-title="{!!html_entity_decode(__('budgets.budget_type'))!!}"> {{ $budget->budget_period_name }} </td>
			
			<td data-title="{!!html_entity_decode(__('budgets.spent'))!!}">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $budget->amount_spent }} </td>
			<td data-title="{!!html_entity_decode(__('budgets.left'))!!}">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $budget->amount_left }} </td>
			 
			<td data-title="{!!html_entity_decode(__('budgets.status'))!!}">
			@if($budget->is_active ==1)
				{!!html_entity_decode(__('budgets.active'))!!}
			@else
				{!!html_entity_decode(__('budgets.inactive'))!!}
			@endif
			 
            <td data-title="{!!html_entity_decode(__('budgets.action'))!!}"> 
			<form action="{{ route('budgets.destroy',$budget->id) }}" method="POST"></form>
			<a  class="btn btn-success editicon" href="{{ route('budgets.edit',$budget->id) }}">{!!html_entity_decode(__('budgets.edit'))!!}</a>
			@csrf
			@method('DELETE')
			 
			<a  class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('budgets.delete_confirm'))!!}')"  href="{{ env('APP_URL') }}delete_budget/{{$budget->id }}">{!!html_entity_decode(__('budgets.delete'))!!}</a>

            </td>

        </tr>

        @endforeach

    </table>
								 
</div>
		</div> 
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 