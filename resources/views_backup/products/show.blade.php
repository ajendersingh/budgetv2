@extends('layouts.admin')

@section('content')
<div class="home-section">

<!-----Home Banner--------->
<div class="home-banner">
<div class="body-container">
<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>View Products Tracking</h2>

        </div>
        <div class="pull-right">

            <a class="btn btn-success" href="{{ route('products.create') }}/{{$product_id}}" style="width:200px;"> Create new Tracking</a>

        </div>
        <div>&nbsp;</div>
		
        <div class="pull-right">

            <a class="btn btn-success" href="{{ route('products.index') }}"> Back</a>

        </div>

    </div>

</div>
@foreach (['danger', 'warning', 'success', 'info'] as $key)
 @if(Session::has($key))
     <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
 @endif
@endforeach


<table class="table table-bordered">

        <tr>

 
            <th>Product Name</th>

             
            <th>Tracking Id</th>

            <th width="280px">Action</th>

        </tr>
		@if(count($trackings)>0)
        @foreach ($trackings as $tracking)
		<tr>
         <td>{{$tracking->post_title}}</td> 

             
         <td>{{$tracking->tracking_id}}</td>

         <td width="280px"><a href="{{ env('APP_URL') }}admin/products/tracking/view/{{$tracking->id}}" class="btn btn-success">Update Tracking Status</a></td>
		</tr>
        @endforeach
        @else
        <tr><td colspan="3"  style="text-align:center">No Tracking record found</td></tr>
        @endif

    </table>
    {!! $trackings->links() !!}
</div>
</div>
<!-----Home Banner--------->






<!-----Home Collection--------->

<div class="home-collection">
<div class="body-container">

</div>






</div>




 

 
 
 

</div>

 

@endsection










