@extends('layouts.admin')

  

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Tracking</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-success" href="{{ route('products.index') }}/tracking/{{$product_id}}"> Back</a>

        </div>

    </div>

</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('products.store') }}" method="POST">

    @csrf

  

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>No of trackings you want to add:</strong>

                <input type="text" name="total_tracking" class="form-control" placeholder="Number of Trackings">
                <input type="hidden" name="product_id" class="form-control" value="{{$product_id}}" placeholder="">

            </div>

        </div>

         

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-success" style="height: 40px; float:left">Submit</button>

        </div>

    </div>

   

</form>

@endsection
