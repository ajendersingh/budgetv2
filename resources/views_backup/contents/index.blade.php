@extends('layouts.admin')

 
 
@section('content')
 
    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Contents</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('contents.create') }}"> Create New Content</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Name (English)</th>
            
            <th>Name (France)</th>
            
            <th>Name (Netherlands)</th>


            
            <th>Created Date</th>
            
            <th>Mofified Date</th>

            <th width="15%">Action</th>

        </tr>

        @foreach ($contents as $content)

        <tr>

            <td>{{ ++$i }}</td>

            <td>{{ $content->title_en }}</td>

            <td>{{ $content->title_fr }}</td>
            
            <td>{{ $content->title_nl }}</td>
            
            
            
            <td>{{date('m-d-Y', strtotime($content->created_on))}}</td>
            
            <td>
             @if(isset($tag->modified_on)  && !empty($tag->modified_on))
            	
             @else
                  &nbsp;
             @endif
            </td>

            <td>

                <form action="{{ route('contents.destroy',$content->id) }}" method="POST">

   


    
					@if($content->is_language==1)
                    
                    <a  class="btn btn-success" href="{{ env('APP_URL') }}admin/{{$content->language_url}}" target="_blank">Manage Language</a><br /><br />
                    
                    @endif
                    <a  class="btn btn-success" href="{{ route('contents.edit',$content->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                   

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $contents->links() !!}

      

@endsection




























