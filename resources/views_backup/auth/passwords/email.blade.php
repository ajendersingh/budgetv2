@extends('layouts.app')

@section('content')
<div class="inner-container">
<<<<<<< HEAD
    <div class="logincontainer">
               <div class="loginsection">
             <div class="login-signup-title"><h3 style="color:#000">{{ __('user_lost_password.lost_password') }}</h3></div>
            <div class="loginform paddingtop">
=======
    <div class="login-sec  row justify-content-center">
        <div class="col-md-12">
         <div class="login-signup-title"><h3 style="color:#000">{{ __('user_lost_password.lost_password') }}</h3></div>
            <div class="card loginform">
>>>>>>> ce5dcdba6d29ae9c3a2c13a2d7f0b67c87ca9db9
                
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group">
                       <div class="lostdetails">{{ __('user_lost_password.lost_password_text') }}</div>
                            <label for="email" class="col-form-label">{{ __('user_lost_password.username_email') }}</label>

                            <div class="nopadding">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

									<div class="loginform-buttons">
                           <div class="nopadding">
                                <button type="submit" class="btn btn-primary">
                                     {{ __('user_lost_password.reset_password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div></div>
</div>
@endsection
