@extends('layouts.app')

@section('content')

<div class="inner-container">
<<<<<<< HEAD
    <div class="logincontainer">
        <div class="loginsection">
        <div class="login-signup-title"><a href="{{ route('login') }}" class="active loginform">{{ __('user_login.login_text') }}</a><a href="{{ env('APP_URL') }}pricing">{{ __('user_login.register_text') }}</a></div>
            <div class="loginform">
=======
    
    <div class="login-sec">
        <div class="row">
            <div class="login-left">
                <div class="login-signup-title"><h3><a href="{{ route('login') }}" class="active">{{ __('user_login.login_text') }}</a>&nbsp; / &nbsp;<a href="{{ env('APP_URL') }}pricing">{{ __('user_login.register_text') }}</a></h3></div>
            <div class="card loginform">
                <div class="card-header"></div>
>>>>>>> ce5dcdba6d29ae9c3a2c13a2d7f0b67c87ca9db9
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="col-md-12 col-form-label">{{ __('user_login.user_or_email') }}*</label>

                            <div class="nopadding">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-12 col-form-label">{{ __('user_login.password') }} *</label>

                            <div class="nopadding">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

<<<<<<< HEAD
                        <div class="loginform-buttons">
                            <div class=" nopadding">
                                <button type="submit" class="btn btn-primary">
                                    <span class="arrow-icon">{{ __('user_login.login_btn') }}</span>
                                     
                                </button>
<div class="form-group">
                            <div class="rememberleft" style="float:left">
=======
                        <div class="form-group mb-0">
                            <div class="col-md-12 nopadding">
                            <button type="submit" class="btn btn-primary">{{ __('user_login.login_btn') }}</button>
                        <div class="form-group">
                            <div class="col-md-6 nopadding" style="float:left">
>>>>>>> ce5dcdba6d29ae9c3a2c13a2d7f0b67c87ca9db9
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                         {{ __('user_login.remember_me') }}
                                    </label>
                                </div>
                            </div>
                           <div class="lostpass" style="float:right">  @if (Route::has('password.request'))
                                    <a class="btn btn-links" href="{{ route('password.request') }}">{{ __('user_login.lost_password') }}</a>
                                @endif</div>
                        </div>
                               
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
            </div>
<<<<<<< HEAD
    </div>
=======

            <div class="login-right">
                
            </div>
        </div>
        
        </div>
>>>>>>> ce5dcdba6d29ae9c3a2c13a2d7f0b67c87ca9db9
</div>
@endsection
