@extends('layouts.app')

@section('content')
<div class="inner-container">
<div class="acontdetail-container">
   <div class="loginsection">
 <div class="pricingmain">
  
 <form  method="post" action="{{ route('pricing_step2') }}">
 @csrf 
 <div class="pricing-form">
<div class="login-signup-title"><h3>{!!html_entity_decode(__('pricing.page_title'))!!} </h3></div>
<div class="clear"></div> 
 @if ($errors->any())

    <div class="alert "  > 


        <ul>

            @foreach ($errors->all() as $error)

                <li style="text-align:left; color:#FF0000;">{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

	
  <!--@if ($plan_id==1)  
  {!!html_entity_decode(__('pricing.pricing_and_duration'))!!} 
  @endif
  @if ($plan_id==2)  
  {!!html_entity_decode(__('pricing.pricing_and_duration_plan2'))!!} 
  @endif-->
 <div class="loginform">
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.first_name'))!!}</label>
 <input type="text" name="first_name" value="{{ old('first_name') }}"  class="form-control @error('first_name') is-invalid @enderror" required></div>
 
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.last_name'))!!}</label>
 <input type="text" name="last_name" value="{{ old('last_name') }}"  class="form-control @error('last_name') is-invalid @enderror" required></div>
 
  <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.address'))!!}</label>
 <input type="text" name="address" value="{{ old('address') }}" class="form-control @error('address') is-invalid @enderror" required></div>
 
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.city'))!!}</label>
 <input type="text" name="city" value="{{ old('city') }}" class="form-control @error('city') is-invalid @enderror" required></div>
 
  <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.country'))!!}  </label>
 <select name="country_code" id="country_code"  class="form-control @error('country_code') is-invalid @enderror" required> 
 <option value="">{!!html_entity_decode(__('pricing.please_select_country'))!!}</option>
  @foreach ($countrydata as $node)
  <option value="{{$node->id}}">{{$node->country_name}}</option>
   @endforeach 
 </select>
 </div> 
  
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.zip_code'))!!}</label>
 <input type="text" name="zip_code" value="{{ old('zip_code') }}"  class="form-control @error('zip_code') is-invalid @enderror" required></div>
 <div class="form-group half"> 
 <label for="subject">{!!html_entity_decode(__('pricing.email'))!!}</label>
 <input type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  required></div>
 
  <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('pricing.phone'))!!} </label>
 <input type="text" name="phone" value="{{ old('phone') }}" class="form-control @error('phone') is-invalid @enderror" required></div>
 <div class="clear"></div>
  <div class="form-group conditionbox"> 
  <input name="terms_check" type="checkbox" value="1" />
  
  @if ($plan_id==1)  
  {!!html_entity_decode(__('pricing.term_text'))!!}  
  @endif
  @if ($plan_id==2)  
  {!!html_entity_decode(__('pricing.term_text2'))!!}  
  @endif

</div> 
<div class="loginform-buttons">
  <div class="form-group"> 
  <input name="button" id="submit1" type="submit" value="{!!html_entity_decode(__('pricing.submit_btn'))!!}" class="button" />
  </div> 
 </div>
 </div>
 
 
 
 <div class="clear"></div>
 
 </div>
 </form>
 
 
 
 
 <!--{!!html_entity_decode($dataCms->{'description_'.config('app.locale')})!!}-->


  

</div>    
    </div>
  </div>    
    
    
    
     
</div>
@endsection
