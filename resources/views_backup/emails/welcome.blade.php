<!DOCTYPE html>
<html>
<head>
    <title>{!!html_entity_decode(__('welcome_email.title'))!!}</title>
</head>

<body>
<h2>{!!html_entity_decode(__('welcome_email.welcome_site'))!!} {{$user['name']}}</h2>
<br/>
{!!html_entity_decode(__('welcome_email.registered_email'))!!} {{$user['email']}}
<br/>
{!!html_entity_decode(__('welcome_email.password'))!!}  {{$user['password']}} 
</body>

</html>