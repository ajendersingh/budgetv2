@extends('layouts.admin')

   

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Edit Seo</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('seos.index') }}"> Back</a>

            </div>

        </div>

    </div>

   

    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

  

    <form action="{{ route('seos.update',$seo->id) }}" method="POST">

        @csrf

        @method('PUT')

   

         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Name:</strong>

                <input type="text" name="page_name" class="form-control"  value="{{ $seo->page_name }}" readonly="readonly">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (English):</strong>

                <input type="text" name="page_title_en" class="form-control"  value="{{ $seo->page_title_en }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (English):</strong>

                <input type="text" name="meta_title_en" class="form-control"  value="{{ $seo->meta_title_en }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (English):</strong>

                <input type="text" name="meta_description_en" class="form-control"  value="{{ $seo->meta_description_en }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (France):</strong>

                <input type="text" name="page_title_fr" class="form-control"  value="{{ $seo->page_title_fr }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (France):</strong>

                <input type="text" name="meta_title_fr" class="form-control"  value="{{ $seo->meta_title_fr }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (France):</strong>

                <input type="text" name="meta_description_fr" class="form-control"  value="{{ $seo->meta_description_fr }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Netherlands):</strong>

                <input type="text" name="page_title_nl" class="form-control"  value="{{ $seo->page_title_nl }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Netherlands):</strong>

                <input type="text" name="meta_title_nl" class="form-control"  value="{{ $seo->meta_title_nl }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Netherlands):</strong>

                <input type="text" name="meta_description_nl" class="form-control"  value="{{ $seo->meta_description_nl }}">

          </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Denmark):</strong>

                <input type="text" name="page_title_dk" class="form-control"  value="{{ $seo->page_title_dk }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Denmark):</strong>

                <input type="text" name="meta_title_dk" class="form-control"  value="{{ $seo->meta_title_dk }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Denmark):</strong>

                <input type="text" name="meta_description_dk" class="form-control"  value="{{ $seo->meta_description_dk }}">

          </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Austria):</strong>

                <input type="text" name="page_title_at" class="form-control"  value="{{ $seo->page_title_at }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Austria):</strong>

                <input type="text" name="meta_title_at" class="form-control"  value="{{ $seo->meta_title_at }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Austria):</strong>

                <input type="text" name="meta_description_at" class="form-control"  value="{{ $seo->meta_description_at }}">

          </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Germany):</strong>

                <input type="text" name="page_title_de" class="form-control"  value="{{ $seo->page_title_de }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Germany):</strong>

                <input type="text" name="meta_title_de" class="form-control"  value="{{ $seo->meta_title_de }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Germany):</strong>

                <input type="text" name="meta_description_de" class="form-control"  value="{{ $seo->meta_description_de }}">

          </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Sweden):</strong>

                <input type="text" name="page_title_se" class="form-control"  value="{{ $seo->page_title_se }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Sweden):</strong>

                <input type="text" name="meta_title_se" class="form-control"  value="{{ $seo->meta_title_se }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Sweden):</strong>

                <input type="text" name="meta_description_se" class="form-control"  value="{{ $seo->meta_description_se }}">

          </div>

        </div>
         
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Norway):</strong>

                <input type="text" name="page_title_no" class="form-control"  value="{{ $seo->page_title_no }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Norway):</strong>

                <input type="text" name="meta_title_no" class="form-control"  value="{{ $seo->meta_title_no }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Norway):</strong>

                <input type="text" name="meta_description_no" class="form-control"  value="{{ $seo->meta_description_no }}">

          </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Page Title (Italian):</strong>

                <input type="text" name="page_title_it" class="form-control"  value="{{ $seo->page_title_it }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Meta Keyword (Italian):</strong>

                <input type="text" name="meta_title_it" class="form-control"  value="{{ $seo->meta_title_it }}">

          </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">
 
                <strong>Meta Description (Italian):</strong>

                <input type="text" name="meta_description_it" class="form-control"  value="{{ $seo->meta_description_it }}">

          </div>

        </div>
        
        
         
            
             
        
         
        
         

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

            </div>

        </div>

   

    </form>

@endsection
