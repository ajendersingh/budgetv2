<a href="javascript:void(0)" class="mobilelinks" id="show_nav">Show Dashboard Menu <span><i class="fa fa-bars" aria-hidden="true"></i></span></a>
<a href="javascript:void(0)" class="mobilelinks" id="hide_nav">Hide Dashboard Menu <span><i class="fa fa-times" aria-hidden="true"></i></span></a> 
<ul class="show_hide_cat_div hideCatClass" id="side_nav_ul">
  <li class='{{$active_class == 'dashboard' ? 'active': ''}}'><a href="{{ env('APP_URL') }}dashboard/"><span><i class="fa fa-tachometer" aria-hidden="true"></i> </span>Dashboard</a> </li>
  <li class='{{$active_class == 'accounts' ? 'active': ''}}'><a href="{{ env('APP_URL') }}accounts/"><span><i class="fa fa-book" aria-hidden="true"></i> </span>Accounts </a> </li>
  <li style="display:none;" class='{{$active_class == 'income' ? 'active': ''}}' ><a href="{{ env('APP_URL') }}income/"><span><i class="fa fa-money" aria-hidden="true"></i> </span>Income </a> </li>
  <li class='{{$active_class == 'liabilities' ? 'active': ''}}' ><a href="{{ env('APP_URL') }}liabilities/"><span><i class="fa fa-money" aria-hidden="true"></i> </span>Liabilities </a> </li>
  <li class='{{$active_class == 'budgets' ? 'active': ''}}'><a href="{{ env('APP_URL') }}budgets/"><span><i class="fa fa-usd" aria-hidden="true"></i></span>Budgets </a> </li>
  <li class='{{$active_class == 'categories' ? 'active': ''}}' ><a href="{{ env('APP_URL') }}categories/"><span><i class="fa fa-plus-square-o" aria-hidden="true"></i> </span>Categories </a> </li>
  <li class='{{$active_class == 'transactions' ? 'active': ''}}'><a href="{{ env('APP_URL') }}transactions/"><span><i class="fa fa-university" aria-hidden="true"></i> </span>Transactions </a> </li>
</ul>  
       <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">