<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(!empty($seoData->page_name))
	<meta name="keywords" content="{!!html_entity_decode($seoData->{'meta_title_'.config('app.locale')})!!}">
	@endif
    
    @if(!empty($seoData->page_name))
	<meta name="description" content="{!!html_entity_decode($seoData->{'meta_description_'.config('app.locale')})!!}">
	@endif
    
    @if(request()->route()->getName() == 'home')
     
    @else
    <meta name="robots" content="noindex, nofollow, noarchive"> 
    @endif 
    

    <title>{{ config('app.name', 'Laravel') }}@if(!empty($seoData->page_name))-{!!html_entity_decode($seoData->{'page_title_'.config('app.locale')})!!} @endif</title>

<script type="application/javascript" src="{{ asset('js/app.js') }}" defer></script>
<script type="application/javascript" src="https://www.google.com/recaptcha/api.js"></script>

<script type="application/javascript" src="{{ asset('js/aos.js') }}" defer></script>
<!-- Styles -->
 
<!--- 

<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/aos.css') }}" rel="stylesheet">
<link href="{{ asset('css/media.css') }}" rel="stylesheet">

 
-->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/hover.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}?123" rel="stylesheet">

   <link rel="stylesheet" href="{{ asset('css/v2dashboard.css') }}">
   <link rel="stylesheet" href="{{ asset('css/v2main.css') }}">  
   <link rel="stylesheet" href="{{ asset('css/v2media.css') }}">  
   <link rel="stylesheet" href="{{ asset('css/v2font-awesome.css') }}">  
   <link rel="stylesheet" href="{{ asset('css/v2owl.carousel.min.css') }}">
   <link rel="stylesheet" href="{{ asset('css/v2owl.theme.min.css') }}">
   <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
   <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
	<script type="application/javascript"src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!--<link href="{{ asset('css/aos.css') }}" rel="stylesheet">-->
 
<script type="text/javascript">
var site_url_js_path="{{ env('APP_URL') }}";

</script>
 
<script type="application/javascript" >
$(window).bind('orientationchange', function (event) {
    location.reload(true);
});

</script>
   
   
   
   <!--<link href="{{ asset('css/datepicker.css') }}" rel="stylesheet" />

  <script type="text/javascript"src="{{ asset('js/bootstrap-datepicker.js') }}"></script>-->
  <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
 
  <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
  
	 
<script>
    jQuery(document).ready(function($) {
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
				weekStart: 0,
				calendarWeeks: true,
				autoclose: true,
				todayHighlight: true,
				rtl: true,
				orientation: "auto"
        });
		
		
		$(function () {
    $(".startDate").datepicker({
        numberOfMonths: 1,
		format: "mm/dd/yyyy",
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $(".endDate").datepicker("option", "minDate", dt);
        }
    });
    $(".endDate").datepicker({
        numberOfMonths: 1,
		format: "mm/dd/yyyy",
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $(".startDate").datepicker("option", "maxDate", dt);
        }
    });
	$(".minDate").datepicker({
        numberOfMonths: 1,
		format: "mm/dd/yyyy",
         minDate : 0
    });
});


    });
</script>
  
</head>
<body id="top">
<input type="text" autofocus="autofocus" style="display:none" />
    <div id="app">
		 
             @include('includes.header')
		 

        
            @yield('content')
              
        
         	@include('includes.footer')
        
		 
	    <div class="clear"></div>
		<div id="preloader"> 
			<div id="loader"></div>
		</div> 
    </div>
 
 
<script>
$(document).ready(function() {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
	
	
	
	 var ssOffCanvas = function() {}

	       var menuTrigger = $('#header-menu-trigger'),
	       nav             = $('#menu-nav-wrap'),
	       closeButton     = nav.find('.close-button'),
	       siteBody        = $('body'),
	       mainContents    = $('section, footer');

		// open-close menu by clicking on the menu icon
		menuTrigger.on('click', function(e){
			 
			e.preventDefault();
			menuTrigger.toggleClass('is-clicked');
			siteBody.toggleClass('menu-is-open');
		});

		// close menu by clicking the close button
		closeButton.on('click', function(e){
			e.preventDefault();
			menuTrigger.trigger('click');	
		});

		// close menu clicking outside the menu itself
		siteBody.on('click', function(e){		
			if( !$(e.target).is('#menu-nav-wrap, #header-menu-trigger, #header-menu-trigger span') ) {
				menuTrigger.removeClass('is-clicked');
				siteBody.removeClass('menu-is-open');
			}
		});
  
});
</script>

 <script type="application/javascript" src="{{ asset('js/transactions.js') }}?v=099855"></script>
 
   <script type="application/javascript" src="{{ asset('js/v2plugins.js') }}?v=0989"></script>
  <script type="application/javascript" src="{{ asset('js/v2main.js') }}?v=0045"></script> 
  
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<!---<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">--->
  
  <script>
  AOS.init();
</script>

  
 
</body>
</html>>