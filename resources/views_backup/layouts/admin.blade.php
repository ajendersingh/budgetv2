<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script type="text/javascript">
	var site_url_js_path="{{ env('APP_URL') }}";

	</script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"   ></script>
   
    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.2.4.min.js" type="text/javascript"></script>
    <script src="{{ asset('public/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
     
    <script src="https://www.google.com/recaptcha/api.js"></script>
    
   
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" defer></script>
  <script src="http://malsup.github.com/jquery.form.js" defer></script>
 
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    
 
  
    
</head>
<body>
    <div id="app" class="dashboard-section">
    
    
    <div class="admin-left">
     <nav class="navbar navbar-expand-md navbar-light">
             @include('includes.headeradmin')
        </nav>
    </div>
    <div class="admin-right">
    <main class="py-4" style="width: 100%;margin-left: 0%; min-height:500px;">
            @yield('content')
        </main>
         <footer>
         	@include('includes.adminfooter') 
         </footer>
         
         </div>
    
       

        
    </div>
    <script src="{{ asset('js/admin.js') }}" defer></script>
</body>
</html>
