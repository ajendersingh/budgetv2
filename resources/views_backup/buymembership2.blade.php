@extends('layouts.payment')

@section('content')


<div class="container buymembershipsection">
 <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <div class="col-xl-12 col-lg-12 col-xs-12 col-sm-12 col-md-12 mainouter ">
                    
                         <div class="col-sm-7">
                             
                             <div class="visible-xs">
                                <div class="inner">
                                    <h6>Protected by SSL-encryption</h6>
                                    <h2>Secure Payment - 3 days trial</h2>
                                    <hr>
                                    <div class="clearfix">
                                        <h4 class="total f-left">Trial price</h4>
                                        <h4 class="total f-right">1.50 USD</h4>
                                    </div>
                                    <hr>
                                 </div>
                             </div>

                            <h1 class="hidden-xs">Enter your information</h1>
                           
                             
                               <form  method=" " action=" "> 
                                
                              
                                     
                                     
                                     

                                   

                                     <label for="cc">Credit cards accepted:</label>
                                     <img class="cards" src="{{asset('img/cards-secure.png')}}" >
                                     
                                   
                                     <hr class="hidden-xs">
 
                                     
                                      
                                     
                                     <label for="cc">Card Number:</label>
                                     <div class="input-icon">
                                        <span class="fa fa-lock"></span>
                                        <input type="text" name="cc-cardnumber"  maxlength="16" class="text-uppercase" placeholder="0000 0000 0000 0000" id="Card" value="{{ old('cc-cardnumber') }}" >
                                     </div>

                                    <label for="exp">Expiration date:</label> 
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                             <select class="half custom-select" name="cc-expires-month">
                                              <option value="01" {{ old("cc-expires-month") == "01" ? "selected" : "" }}>01</option>
                                              <option value="02" {{ old("cc-expires-month") == "02" ? "selected" : "" }}>02</option>
                                              <option value="03" {{ old("cc-expires-month") == "03" ? "selected" : "" }}>03</option>
                                              <option value="04" {{ old("cc-expires-month") == "04" ? "selected" : "" }}>04</option>
                                              <option value="05" {{ old("cc-expires-month") == "05" ? "selected" : "" }}>05</option>
                                              <option value="06" {{ old("cc-expires-month") == "06" ? "selected" : "" }}>06</option>
                                              <option value="07" {{ old("cc-expires-month") == "07" ? "selected" : "" }}>07</option>
                                              <option value="08" {{ old("cc-expires-month") == "08" ? "selected" : "" }}>08</option>
                                              <option value="09" {{ old("cc-expires-month") == "09" ? "selected" : "" }}>09</option>
                                              <option value="10" {{ old("cc-expires-month") == "10" ? "selected" : "" }}>10</option>
                                              <option value="11" {{ old("cc-expires-month") == "11" ? "selected" : "" }}>11</option>
                                              <option value="12" {{ old("cc-expires-month") == "12" ? "selected" : "" }}>12</option>
                                            </select>
                                        </div>
                                        
                                         <div class="col-sm-6 col-xs-6">
                                            <select class="custom-select" name="cc-expires-year" >
                                             
                                            @for($i=date('Y');$i<=date('Y')+10;$i++ )
                                             
                                             <option value="{{$i}}" {{ old("cc-expires-year") == "$i" ? "selected" : "" }}>{{$i}}</option>
                                            
                                             @endfor
                                             
                                            </select> 
                                        </div>
                                     </div>
                                     <label for="ccv">Verification code (CVV / CVC):</label>
                                     <div class="row">
                                         <div class="col-sm-6 col-xs-6">
                                            <div class="input-icon">
                                                <span class="fa fa-lock"></span>
                                                <input type="text" class="text-uppercase half" name="cc-cvv2" id="CVV"  maxlength="3"  size="3" value="" placeholder="XXX" >
                                             </div>
                                         </div>
                                         <div class="col-sm-6 col-xs-6 clearfix">
                                            <img class="help f-left" src="{{asset('img/cvv.png')}}" >
                                             <h6 class="f-left help-txt hidden-sm hidden-xs"><i class="arrow left"></i> 3 digits on the back of your card</h6>
                                         </div>
                                     </div> 

                                    

                                    

									 <button type=" " class="btn btn-primary" style="margin-top: 20px;"><span>Process payment</span></button>
                                     

                                </form> 
                        </div>
                         
                        <div class="col-sm-5">
                             
                            <div class="box hidden-xs">
                            <img src="{{asset('img/logo-img.jpg')}}" alt="logo" >
                                 <div class="inner">
                                    <h6>Secured via SSL encryption</h6>
                                    <h2>Secure payment</h2>
                                    <hr>
                                    eBook.com
                                     <hr>
                                    <div class="clearfix">
                                        <h4 class="total f-left">Current costs</h4>
                                        <h4 class="total f-right">1.50 USD</h4>
                                    </div>
                                    <hr>
                                   
                                    
                                    <h2>Questions?</h2>
                                    <div class="clearfix">
                                        <p class="call f-left">Call us</p>
                                        <p class="call f-right">+3234592686</p>
                                    </div>
                                    <hr>
                                    <p class="terms">
                                        Refunds for purchases or recurring charges may be requested by contacting customer support. Refunds or credits will not be issued for partially used Memberships. Cancellation for all future recurring billing may be requested in accordance with Section 8 - Cancellation. eBook.com reserves the right to grant a refund or a credit applicable to purchases to the Site at its discretion. The decision to refund a charge does not imply the obligation to issue additional future refunds. Should a refund be issued by eBook.com for any reason, it will be credited solely to the payment method used in the original transaction. eBook.com will not issue refunds by cash. 
                                     </p>
                                </div>
                            </div>
                            
                             <div class="visible-xs">
                                <img class="img-mobile" src="{{asset('img/logo-img.jpg')}}" alt="logo" >
                                 <h2>Questions?</h2>
                                 <div class="clearfix">
                                     <p class="total f-left">Call us</p>
                                     <p class="total f-right">+441495367206</p>
                                 </div>
                                 <hr>
                                 <p class="terms">
                                        Refunds for purchases or recurring charges may be requested by contacting customer support. Refunds or credits will not be issued for partially used Memberships. Cancellation for all future recurring billing may be requested in accordance with Section 8 - Cancellation. Azursnd247.com reserves the right to grant a refund or a credit applicable to purchases to the Site at its discretion. The decision to refund a charge does not imply the obligation to issue additional future refunds. Should a refund be issued by Azursnd247.com for any reason, it will be credited solely to the payment method used in the original transaction. Azursnd247.com will not issue refunds by cash.
                                     </p>
                            </div>
                            
                            
                        </div>

                    </div>
                  
    <div class="clear"></div>
</div>
    <div class="clear"></div>
 <div class="centreclass"><img class="cards desktopimg" src="{{asset('img/company-text.png')}}" alt="" />
<img class="cards mobileimg" src="{{asset('img/company-text-mobile.png')}}" alt="" />
</div>

@endsection
