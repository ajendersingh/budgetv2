@extends('layouts.app')

@section('content')
<div class="inner-container">
 <div class="editprofile-tab">
 <div class="tab-section">
 <div class="tab" id="sec1">{!!html_entity_decode(__('update_profile.profile_tab'))!!}</div>
 <div class="tab" id="sec2">{!!html_entity_decode(__('update_profile.card_tab'))!!}</div>
 <div class="tab" id="sec3">{!!html_entity_decode(__('update_profile.membership_tab'))!!}</div>
 </div>
 @foreach (['danger', 'warning', 'success', 'info'] as $key)
 @if(Session::has($key))
     <p class="alert alert-{{ $key }}">{{ Session::get($key) }}</p>
 @endif
@endforeach
 
 <script type="text/javascript">
 $(document).ready(function(){
 $("#sec1").addClass("active");
 $("#tab2_div").css("display", "none");
 $("#tab3_div").css("display", "none");
 
 	$("#sec1").click(function(){
	
		$("#tab2_div").css("display", "none");
		$("#tab3_div").css("display", "none");
		$("#tab1_div").css("display", "block");
		$("#sec1").addClass("active");
		$("#sec2").removeClass("active");
		$("#sec3").removeClass("active");
 	}); 
	$("#sec2").click(function(){
		$("#tab1_div").css("display", "none");
		$("#tab3_div").css("display", "none");
		$("#tab2_div").css("display", "block");
		$("#sec2").addClass("active");
		$("#sec1").removeClass("active");
		$("#sec3").removeClass("active");
 	}); 
	$("#sec3").click(function(){
		$("#tab1_div").css("display", "none");
		$("#tab2_div").css("display", "none");
		$("#tab3_div").css("display", "block");
		$("#sec3").addClass("active");
		$("#sec1").removeClass("active");
		$("#sec2").removeClass("active");
 	}); 
 }); 
 
 </script> 
 
 
 
 <div id="tab1_div">
 
 <div class="acontdetail-container">
<div class="loginsection">
 <div class="pricingmain">
<div class="login-signup-title"><h3>{!!html_entity_decode(__('update_profile.profile_section_title'))!!} </h3></div>
<div class="loginform">
 <form  method="post" action="{{ action('MainController@update_profile') }}">
 @csrf 
 <div class="pricing-form">
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.first_name'))!!}</label>
 <input type="text" name="first_name" value="{{ $userData->first_name }}"  class="form-control @error('first_name') is-invalid @enderror" required></div>
 
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.last_name'))!!}</label>
 <input type="text" name="last_name" value="{{ $userData->last_name }}"  class="form-control @error('last_name') is-invalid @enderror" required></div>
 
  <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.address'))!!}</label>
 <input type="text" name="address" value="{{ $userData->address }}" class="form-control @error('address') is-invalid @enderror" required></div>
 
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.city'))!!}</label>
 <input type="text" name="city" value="{{ $userData->city }}" class="form-control @error('city') is-invalid @enderror" required></div>
 
  <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.country'))!!}  </label>
 <select name="country_code" id="country_code"  class="form-control @error('country_code') is-invalid @enderror" required> 
 <option value="">{!!html_entity_decode(__('update_profile.please_select_country'))!!}</option>
  @foreach ($countrydata as $node)
  <option value="{{$node->id}}" {{ $node->id == $userData->country_id ? "selected" : "" }}>{{$node->country_name}}</option>
   @endforeach 
 </select>
 </div> 
  
 <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.zip_code'))!!}</label>
 <input type="text" name="zip_code" value="{{ $userData->zip_code }}"  class="form-control @error('zip_code') is-invalid @enderror" required></div>
 
 
  <div class="form-group half">
 <label for="subject">{!!html_entity_decode(__('update_profile.phone'))!!} </label>
 <input type="text" name="phone" value="{{ $userData->phone }}" class="form-control @error('phone') is-invalid @enderror" required></div>
 <div class="clear"></div>
    
  <div class="form-group"> 
  <div class="loginform-buttons">
  <input name="button" id="submit1" type="submit" value="{!!html_entity_decode(__('update_profile.profile_btn_name'))!!}" class="button" />
</div>
  </div> 
 
 
 <div class="clear"></div>
 
 </div>
 </form>
 </div>
 </div>
 </div>
 </div>
 
 
 </div>
 
 <div class="buymembershipsection" id="tab2_div">
 <div class="logincontainer">
 <div class="loginsection">
<div class="login-signup-title"><h3>{!!html_entity_decode(__('update_profile.card_section_title'))!!} </h3></div>
<div class="loginform">
 <form  method="post" action="{{ route('subscribe-membership') }}"> 
                                
                               	@csrf 
									  @if(!empty($orderDataUser))
  									<div class="cardnumbertext">
                                     <label for="cc">{!!html_entity_decode(__('update_profile.existing_card_number'))!!} </label>
                                     <div class="input-icon">
                                        <span class="fa fa-lock"></span>
                                         XXXXXXXXXXXX{{$orderDataUser->last_four}}
                                     </div>
                                     </div>
                                      
                                     @endif
                                    
                                      
                                      
                                     <div class="cardnumber">
                                     <label for="cc">{{ __('update_profile.card_number') }}</label>
                                     <div class="input-icon">
                                        <span class="fa fa-lock"></span>
                                        <input type="text" name="cc-cardnumber"  maxlength="16" class="text-uppercase @error('cc-cardnumber') is-invalid @enderror" required  placeholder="0000 0000 0000 0000" id="Card" value="{{ old('cc-cardnumber') }}" >
                                     </div></div>
									
									<div class="card-expmain">
                                    
                                    <div class="card-exp-input">
                                    <label for="exp">{{ __('update_profile.expiration_date') }}</label> 
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                             <select name="cc-expires-month" class="half custom-select @error('cc-expires-month') is-invalid @enderror" required>
                                              <option value="01" {{ old("cc-expires-month") == "01" ? "selected" : "" }}>01</option>
                                              <option value="02" {{ old("cc-expires-month") == "02" ? "selected" : "" }}>02</option>
                                              <option value="03" {{ old("cc-expires-month") == "03" ? "selected" : "" }}>03</option>
                                              <option value="04" {{ old("cc-expires-month") == "04" ? "selected" : "" }}>04</option>
                                              <option value="05" {{ old("cc-expires-month") == "05" ? "selected" : "" }}>05</option>
                                              <option value="06" {{ old("cc-expires-month") == "06" ? "selected" : "" }}>06</option>
                                              <option value="07" {{ old("cc-expires-month") == "07" ? "selected" : "" }}>07</option>
                                              <option value="08" {{ old("cc-expires-month") == "08" ? "selected" : "" }}>08</option>
                                              <option value="09" {{ old("cc-expires-month") == "09" ? "selected" : "" }}>09</option>
                                              <option value="10" {{ old("cc-expires-month") == "10" ? "selected" : "" }}>10</option>
                                              <option value="11" {{ old("cc-expires-month") == "11" ? "selected" : "" }}>11</option>
                                              <option value="12" {{ old("cc-expires-month") == "12" ? "selected" : "" }}>12</option>
                                            </select>
                                        </div>
                                        
                                         <div class="col-sm-6 col-xs-6">
                                            <select class="custom-select @error('cc-expires-year') is-invalid @enderror" required  name="cc-expires-year" >
                                             
                                            @for($i=date('Y');$i<=date('Y')+10;$i++ )
                                             
                                             <option value="{{$i}}" {{ old("cc-expires-year") == "$i" ? "selected" : "" }}>{{$i}}</option>
                                            
                                             @endfor
                                             
                                            </select> 
                                        </div>
                                     </div>
                                     </div>
                                     
                                 	<div class="card-exp-cvv">	
                                            <label for="ccv">{{ __('update_profile.cvv') }}</label>
                                     
                                            <div class="input-icon">
                                                <span class="fa fa-lock"></span>
                                                <input type="text" class="text-uppercase half @error('cc-cvv2') is-invalid @enderror" required  name="cc-cvv2" id="CVV"  maxlength="3"  size="3" value="" placeholder="XXX" >
                                             </div>
                                             
                                             
                                         <div class="cvvimg">
                                            <img class="help f-left cvv" src="{{asset('img/cvv.png')}}" />
                                             <h6 class="f-left help-txt hidden-sm hidden-xs"><i class="arrow left"></i> {{ __('update_profile.cvv_details') }}</h6>
                                         </div>
                                         </div>
                                         </div>
                          
 									<div class="loginform-buttons">
									 <button type="submit" class="btn btn-primary" style="margin-top: 50px;"><span>{{ __('update_profile.process_button') }}</span></button>
                                     </div>

                                </form>

</div>
</div>                                
</div>          
                                
 </div>
 
 <div id="tab3_div" class="change-plan">
 @if(!empty($membershipDataUser))
 <div class="title">{{ __('update_profile.current_plan') }} <span>
  	@if($membershipDataUser->membership_type==1)
 	{{ __('update_profile.standard_label') }}
    @endif 
    @if($membershipDataUser->membership_type==2)
 	{{ __('update_profile.premium_label') }}  
    @endif 
    </span></div>
	 @if($membershipDataUser->membership_type==1)
	<button type="submit" ><span>{{ __('update_profile.upgrade_btn') }}</span></button>
   	@endif 
    @if($membershipDataUser->membership_type==2)
	<button type="submit" ><span>{{ __('update_profile.downgrade_btn') }}</span></button>
    @endif 
	<a href="{{ url('') }}/cancel-account" style="background: #e44132;color: #fff;border-radius: 500px;border: 0px;padding: 13px 16px;text-align: center;margin: 0 auto;text-decoration:none;font-family: Roboto;font-weight: 500;text-transform: uppercase;font-size: 14px;letter-spacing: .5px;">{{ __('update_profile.cancel_membership') }}</a>
  </div>
 @endif
  @if(empty($membershipDataUser))
 <div class="title">{{ __('update_profile.current_plan') }} <span>{{ __('update_profile.no_plan') }}</span></div>
   </div>
 @endif
 
 
 
 

  

</div>    
    
      
    
    
    
     
</div>
@endsection
