@extends('layouts.app')

@section('content')
<div class="inner-container">
    <div class="row justify-content-center">
        <div class="col-md-12" style="min-height:500px;">
                <h1>
                                  
                  {{ $dataCms->{'title_'.config('app.locale')} }}
 
                </h1> 
               <p>
               
                 
                 {!!html_entity_decode($dataCms->{'description_'.config('app.locale')})!!}
                
                 </p>           
        </div>
    </div>
</div>
@endsection
