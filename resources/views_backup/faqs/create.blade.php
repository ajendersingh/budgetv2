@extends('layouts.admin')

  

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Faq</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary-edit" href="{{ route('faqs.index') }}"> Back</a>

        </div>

    </div>

</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('faqs.store') }}" method="POST" enctype="multipart/form-data">

    @csrf

  

     <div class="row">
     <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Type :</strong>
				
                <select class="form-control" name="faq_type" id="faq_type">
                <option value="faq">Faq</option>
                <option value="technical_problems">Technical Problems</option>
                </select>

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (English):</strong>

                <input type="text" name="name_en" class="form-control" placeholder="Title (English)" value="{{ old('name_en') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (France):</strong>

                <input type="text" name="name_fr" class="form-control" placeholder="Title (France)" value="{{ old('name_fr') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Netherlands):</strong>

                <input type="text" name="name_nl" class="form-control" placeholder="Title (Netherlands)" value="{{ old('name_nl') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Denmark):</strong>

                <input type="text" name="name_dk" class="form-control" placeholder="Title (Denmark)" value="{{ old('name_dk') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Austria):</strong>

                <input type="text" name="name_at" class="form-control" placeholder="Title (Austria)" value="{{ old('name_at') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Germany):</strong>

                <input type="text" name="name_de" class="form-control" placeholder="Title (Germany)" value="{{ old('name_de') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Sweden):</strong>

                <input type="text" name="name_se" class="form-control" placeholder="Title (Sweden)" value="{{ old('name_se') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Norway):</strong>

                <input type="text" name="name_no" class="form-control" placeholder="Title (Norway)" value="{{ old('name_no') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Italian):</strong>

                <input type="text" name="name_it" class="form-control" placeholder="Title (Italian)" value="{{ old('name_it') }}">

            </div>

        </div>
        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (English):</strong>

                <textarea name="description_en" id="description_en" style="width:100%">{{ old('description_en') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (France):</strong>

                <textarea name="description_fr" id="description_fr" style="width:100%">{{ old('description_fr') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Netherlands):</strong>

                <textarea name="description_nl" id="description_nl" style="width:100%">{{ old('description_nl') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Denmark):</strong>

                <textarea name="description_dk" id="description_dk" style="width:100%">{{ old('description_dk') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Austria):</strong>

                <textarea name="description_at" id="description_at" style="width:100%">{{ old('description_at') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Germany):</strong>

                <textarea name="description_de" id="description_de" style="width:100%">{{ old('description_de') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Sweden):</strong>

                <textarea name="description_se" id="description_se" style="width:100%">{{ old('description_se') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Norway):</strong>

                <textarea name="description_no" id="description_no" style="width:100%">{{ old('description_no') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Italian):</strong>

                <textarea name="description_it" id="description_it" style="width:100%">{{ old('description_it') }}</textarea>

            </div>

        </div>
        
        
        
        
         
        
         
        
         
        
         

         
        
         

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

        </div>

    </div>

   

</form>

@endsection
