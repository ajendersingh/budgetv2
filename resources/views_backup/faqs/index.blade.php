@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Faq's</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('faqs.create') }}"> Create New Faq</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>
			
            <th>Type</th>

            <th>Name (English)</th>
            
            <th>Name (France)</th>
            
            <th>Name (Netherlands)</th>

            
            <th>Created Date</th>
            

            <th>Action</th>

        </tr>

        @foreach ($faqs as $faq)

        <tr>

			<td>{{$faq->faq_type == 'faq' ? 'Faq': 'Technical Problems'}}</td>
            
            <td>{{ $faq->name_en }}</td>

            <td>{{ $faq->name_fr }}</td>
            
            <td>{{ $faq->name_nl }}</td>
            
            
            
            <td>{{date('m-d-Y', strtotime($faq->created_on))}}</td>
            
             

            <td> 

                <form action="{{ route('faqs.destroy',$faq->id) }}" method="POST">
 
   


    

                    <a  class="btn btn-success" href="{{ route('faqs.edit',$faq->id) }}">Edit</a>

   	

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $faqs->links() !!}

      

@endsection




























