@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Category</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('categories.create') }}"> Create New Category</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>


            <th>Name (English)</th>
            
            <th>Name (France)</th>
            
            <th>Name (Netherlands)</th>

            
            <th>Created Date</th>
            
            <th>Cover Image</th>

            <th>Action</th>

        </tr>

        @foreach ($categories as $category)

        <tr>


            <td>{{ $category->name_en }}</td>

            <td>{{ $category->name_fr }}</td>
            
            <td>{{ $category->name_nl }}</td>
            
            
            
            <td>{{date('m-d-Y', strtotime($category->created_on))}}</td>
            
            <td>
             <img src="{{ env('APP_URL') }}/public/catimages/{{ $category->image }}" width="100" height="80" />
            </td>

            <td> 

                <form action="{{ route('categories.destroy',$category->id) }}" method="POST">
 
   


    

                    <a  class="btn btn-success" href="{{ route('categories.edit',$category->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $categories->links() !!}

      

@endsection




























