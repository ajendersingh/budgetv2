@extends('layouts.admin')

  

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Content</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary-edit" href="{{ route('contents.index') }}"> Back</a>

        </div>

    </div>

</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('contents.store') }}" method="POST">
 

    @csrf

  

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (English):</strong>

                <input type="text" name="title_en" class="form-control" placeholder="Title (English)" value="{{ old('title_en') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (France):</strong>

                <input type="text" name="title_fr" class="form-control" placeholder="Title (France)" value="{{ old('title_fr') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Netherlands):</strong>

                <input type="text" name="title_nl" class="form-control" placeholder="Title (Netherlands)" value="{{ old('title_nl') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Denmark):</strong>

                <input type="text" name="title_dk" class="form-control" placeholder="Title (Denmark)" value="{{ old('title_dk') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Austria):</strong>

                <input type="text" name="title_at" class="form-control" placeholder="Title (Austria)" value="{{ old('title_at') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Germany):</strong>

                <input type="text" name="title_de" class="form-control" placeholder="Title (Germany)" value="{{ old('title_de') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Sweden):</strong>

                <input type="text" name="title_se" class="form-control" placeholder="Title (Sweden)" value="{{ old('title_se') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Norway):</strong>

                <input type="text" name="title_no" class="form-control" placeholder="Title (Norway)" value="{{ old('title_no') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title (Italian):</strong>

                <input type="text" name="title_it" class="form-control" placeholder="Title (Italian)" value="{{ old('title_it') }}">

            </div>

        </div>
        
         <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (English):</strong>

                <textarea name="description_en" id="description_en" class="editor_class" style="width:100%">{{ old('description_en') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (France):</strong>

                <textarea name="description_fr" id="description_fr" class="editor_class" style="width:100%">{{ old('description_fr') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Netherlands):</strong>

                <textarea name="description_nl" id="description_nl" class="editor_class" style="width:100%">{{ old('description_nl') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Denmark):</strong>

                <textarea name="description_dk" id="description_dk" class="editor_class" style="width:100%">{{ old('description_dk') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Austria):</strong>

                <textarea name="description_at" id="description_at" class="editor_class" style="width:100%">{{ old('description_at') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Germany):</strong>

                <textarea name="description_de" id="description_de" class="editor_class" style="width:100%">{{ old('description_de') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Sweden):</strong>

                <textarea name="description_se" id="description_se" class="editor_class" style="width:100%">{{ old('description_se') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Norway):</strong>

                <textarea name="description_no" id="description_no" class="editor_class" style="width:100%">{{ old('description_no') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Italian):</strong>

                <textarea name="description_it" id="description_it" class="editor_class" style="width:100%">{{ old('description_it') }}</textarea>

            </div>

        </div>
        
        
        
        
         
        
        
        
         
        
         

         
 <script type="application/javascript">
 
 	
    var editor1=CKEDITOR.replace( 'description_en' );
	editor1.config.allowedContent = true;
	var editor2=CKEDITOR.replace( 'description_fr' );
	editor2.config.allowedContent = true;
	var editor3=CKEDITOR.replace( 'description_nl' );
	editor3.config.allowedContent = true;
	var editor4=CKEDITOR.replace( 'description_dk' );
	editor4.config.allowedContent = true;
	var editor5=CKEDITOR.replace( 'description_at' );
	editor5.config.allowedContent = true;
	var editor6=CKEDITOR.replace( 'description_de' );
	editor6.config.allowedContent = true;
	var editor7=CKEDITOR.replace( 'description_se' );
	editor7.config.allowedContent = true;
	var editor8=CKEDITOR.replace( 'description_no' ); 
	editor8.config.allowedContent = true;
	var editor9=CKEDITOR.replace( 'description_it' ); 
	editor9.config.allowedContent = true;
 </script>       
         

<div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

        </div>

    </div>

   

</form>


@endsection
