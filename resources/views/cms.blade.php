@extends('layouts.app')

@section('content')
<div class="inner-container">
<div class="titlemain"><h1> {{ $dataCms->{'title_'.config('app.locale')} }}</h1></div>
<div class="basicmain">
{!!html_entity_decode($dataCms->{'description_'.config('app.locale')})!!}
</div>

</div>
@endsection
