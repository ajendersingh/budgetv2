@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Seo</h2>

            </div>

             

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>

             
            <th width="15%">Page Name</th>

            <th width="15%">Page Title </th>
            
            <th width="13%">Created Date</th>
  
            <th width="15%">Action</th>

        </tr>

        @foreach ($seos as $seo)

        <tr>

  
            <td>{{ $seo->page_name }}</td>

            <td>{{ $seo->page_title_en }}</td>
            
            
            
            
            <td>{{date('m-d-Y', strtotime($seo->created_on))}}</td>
            
             

            <td>

                <form action="{{ route('seos.destroy',$seo->id) }}" method="POST">

   


    

                    <a  class="btn btn-success" href="{{ route('seos.edit',$seo->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                     

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $seos->links() !!}

      

@endsection




























