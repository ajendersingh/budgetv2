@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
		<div class="category innerright-side">
				<div class="page-title">
                <h1> {!!html_entity_decode(__('budg_detail.page_title'))!!} </h1> 
               <a class="btn btn-success purple-btn" href="{{route('budgets.index') }}"> {!!html_entity_decode(__('common.back'))!!}</a>
		</div>

				 <div class="dashborad-about">
				 <p>{!!html_entity_decode(__('budg_detail.heading_text'))!!}  {{$budget->name}} {!!html_entity_decode(__('common.between'))!!} {{$month_date}}</p>
				 </div>
				 
			<!--<div class="chart-container">
				<div class="pie-chart-container">
				 <canvas id="line-chart" width="800" height="450"></canvas>
				</div>
			  </div>  -->
				 
                 <div class="flashmessage">
				  @include('flash-message')
			</div>

				 <div class="dashboard-recenttable">
				<table class="table table-bordered">

			<tr>


				<th>{!!html_entity_decode(__('transactions.title'))!!} </th>
				<th>{!!html_entity_decode(__('transactions.amount'))!!}</th>
				<th>{!!html_entity_decode(__('transactions.account'))!!}</th>
				<th>{!!html_entity_decode(__('transactions.trans_type'))!!}</th>
				<th>{!!html_entity_decode(__('transactions.category'))!!}</th> 
				<!--<th>{!!html_entity_decode(__('transactions.budget'))!!}</th>-->
				<th>{!!html_entity_decode(__('transactions.date'))!!}</th>
				<th>{!!html_entity_decode(__('transactions.action'))!!}</th>

			</tr>
		 
        @foreach ($transactions as $trans)

        <tr>


            <td data-title="{!!html_entity_decode(__('transactions.title'))!!}" >{{ $trans->title }}</td>
			<td data-title="{!!html_entity_decode(__('transactions.amount'))!!}" >{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $trans->trans_amount }} </td>
			<td data-title="{!!html_entity_decode(__('transactions.account'))!!}"> 
				@if($trans->transaction_type == 'EXPENCES')
				{{$trans->trans_account}} 
				@elseif($trans->transaction_type == 'REVENUE')
				{{$trans->trans_destini}}
				@elseif($trans->transaction_type == 'TRANSFER')
				{{$trans->trans_account}}->{{$trans->trans_destini}}
				@endif
			</td>
			<td data-title="{!!html_entity_decode(__('transactions.trans_type'))!!}">{{ $trans->transaction_type }}</td>
			<!--<td>{{ $trans->trans_cat }}</td>-->
			<td data-title="{!!html_entity_decode(__('transactions.category'))!!}"> {{$trans->budget_name}}</td>
			<td data-title="{!!html_entity_decode(__('transactions.date'))!!}"> {{ $trans->trans_date }}</td>
			
		 
			 
			 
			 
            <td data-title="{!!html_entity_decode(__('transactions.action'))!!}"> 
			 
			<a  class="btn btn-success editicon" href="{{ route('transactions.edit',$trans->id) }}">{!!html_entity_decode(__('transactions.edit'))!!}</a>
			<a  class="btn btn-success cloneicon" onclick="return confirm('{!!html_entity_decode(__('transactions.copy_confirm'))!!}')" href="{{ env('APP_URL') }}clone_transaction/{{ $trans->id}}">{!!html_entity_decode(__('transactions.clone'))!!}</a>
			<a  class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('transactions.delete_confirm'))!!}')" href="{{ env('APP_URL') }}remove_transaction/{{$trans->id }}">{!!html_entity_decode(__('transactions.delete'))!!}</a>
			 

            </td>

        </tr>

        @endforeach

    </table>
    				</div>
    
    
				@if($all == '')				 
		<a href="{{ env('APP_URL') }}budgets/show/{{$budget->id }}/all">{!!html_entity_decode(__('common.show_all'))!!} </a>
				@else
					<a href="{{ env('APP_URL') }}budgets/show/{{$budget->id }}">{!!html_entity_decode(__('common.show_current_month'))!!}</a>
				@endif
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
	 
</div>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
 
<script type="application/javascript" >

$(function(){
	
	 
 
 new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{ 
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }  
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});

  });
</script>

@endsection
 
 
 

 