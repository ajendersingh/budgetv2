@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
         
		<div class="category innerright-side">
    <div class="page-title">
                <h1>{!!html_entity_decode(__('budgets.add_budget'))!!}</h1>
               <a class="btn btn-success purple-btn" href="{{route('budgets.index') }}"> {!!html_entity_decode(__('budgets.back'))!!}</a>
   				</div>	
		<div class="dashborad-about">
    		<p>{!!html_entity_decode(__('budgets.add_heading_text'))!!}</p>
   
   </div>
   
   <div class="flashmessage">
    @include('flash-message')
 </div>
 
	
    <div class="dashboard-form">
    <form action="{{ env('APP_URL') }}budgets/save" method="POST" enctype="multipart/form-data" autocomplete="off">
         
		{{ csrf_field() }}
        @method('POST')
         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('budgets.name'))!!} : <span class="red">*</span></strong>
				<input type="hidden" name="id" value="" />
                <input type="text" name="name" class="form-control"  value="" required pattern=".*\S+.*" />
				 

            </div>

        </div>
        
         
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('budgets.budget_type'))!!} : <span class="red">*</span></strong>

                <select class="form-control" name="budget_type" required>
				<option value=""></option>
					@foreach ($budget_types as $key => $val)
						<option value="{{ $val->id }}"   >
						{{ $val->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('budgets.budget_amount'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} <span class="red">*</span></strong>
                <input type="number"  name="budget_amount" class="form-control" placeholder="1000" title="Only numbers are allowd here!" value="" required>
            </div>
        </div>
		
        <div class="col-xs-12 col-sm-12 col-md-f">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('budgets.budget_period'))!!} : <span class="red">*</span></strong>
               <select class="form-control" id="budget_period" name="budget_period" required>
			   <option value=""></option>
					@foreach ($periods as $key => $value)
						<option value="{{ $value->id }}">
						{{ $value->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
 
 
        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('budgets.description'))!!} :</strong>

                <textarea name="budget_description" id="description" style="width:100%"></textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit purple-btn">Submit</button>

            </div>

        </div>

   

    </form>
</div>
</div> 
    			 
    
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 