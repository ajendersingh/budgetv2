@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
		<div class="category innerright-side">
                <div class="page-title">
                <h1>{!!html_entity_decode(__('transactions.edit_transaction'))!!}</h1>
               <a class="btn btn-success purple-btn" href="{{route('transactions.index') }}"> {!!html_entity_decode(__('transactions.back'))!!}</a>
				</div>
                
              <div class="dashborad-about">    
			<p>{!!html_entity_decode(__('transactions.edit_heading_text'))!!}</p>
			</div>

		<div class="flashmessage">
	 @include('flash-message')
	 </div>
	
    <div class="dashboard-form">
    <form action="{{ env('APP_URL') }}transactions/save" autosugget="on" id="trans_form" method="POST" enctype="multipart/form-data">
         
		{{ csrf_field() }}
        @method('POST')
         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.title'))!!}<span class="red">*</span></strong>
				<input type="hidden" name="id" value="{{$trans->id}}" />
                <input type="text" name="title" class="form-control" autosugget="off" value="{{$trans->title}}" required>
				 

            </div>

        </div>
        
         
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.trans_type'))!!}<span class="red">*</span></strong>

                <select class="form-control" name="transaction_type" id="trans_type" required>
					 
					<option value="">{!!html_entity_decode(__('transactions.select_option'))!!} </option>
					@foreach ($types as $key => $value)
						<option value="{{ $value->title }}" {{$trans->transaction_type == $value->title?'selected':''}}>
						{{ $value->title}}</option>
					@endforeach
				</select>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('transactions.amount'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} <span class="red">*</span></strong>
                <input type="number"  name="trans_amount" autosugget="off" class="form-control" placeholder="1000" title="Only numbers are allowd here!" value="{{$trans->trans_amount}}" required>
            </div>
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('transactions.trans_date'))!!} <span class="red">*</span></strong>
                <input type="text"  name="trans_date" autosugget="off" class="form-control datepicker" placeholder="mm/dd/yyy" title="Only numbers are allowd here!" value="{{$trans->trans_date}}" required>
            </div>
        </div>
		
        <div class="col-xs-12 col-sm-12 col-md-f" id="source_account_div" style="{{$trans->transaction_type == 'EXPENCES' || $trans->transaction_type == 'TRANSFER'?'':'display:none;'}}"> 
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.source_account'))!!} <span class="red">*</span>:</strong>
               <select class="form-control" id="source_account" name="source_account" >
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($accounts as $key => $value)
						<option value="{{ $value->id }}" {{$trans->source_account == $value->id?'selected':''}}>
						{{ $value->title}}</option>
					@endforeach
					</select>
            </div>

        </div>       

		<div class="col-xs-12 col-sm-12 col-md-f" id="destination_account_div" style="{{$trans->transaction_type == 'REVENUE' || $trans->transaction_type == 'TRANSFER'?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.destination_account'))!!}<span class="red">*</span>:</strong>
               <select class="form-control" id="destination_account" name="destination_account">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($accounts as $key => $value)
						<option value="{{ $value->id }}" {{$trans->destination_account == $value->id?'selected':''}}>
						{{ $value->title}}</option>
					@endforeach
					</select>
            </div>

        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-f">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.category'))!!}:</strong>
               <select class="form-control" id="category" name="category">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($categories as $key => $value)
						<option value="{{ $value->cat_id }}" {{$trans->category == $value->cat_id?'selected':''}}>
						{{ $value->cat_name}}</option>
					@endforeach
					</select>
            </div>

        </div>		
		
		<div class="col-xs-12 col-sm-12 col-md-f">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.budget'))!!}:</strong>
               <select class="form-control" id="budget" name="budget">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($budgets as $key => $value)
						<option value="{{ $value->id }}" {{$trans->budget == $value->id?'selected':''}}>
						{{ $value->name}}</option>
					@endforeach
					</select>
            </div>

        </div>
 
 
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.is_recurring_transaction'))!!} </strong>

                <input type="checkbox" name="is_recurring_transaction" id="is_recurring_transaction"  value="1" {{ $trans->is_recurring_transaction == 1 ? 'checked' : '' }}/> 

            </div>

        </div>
		
        <div class="col-xs-12 col-sm-12 col-md-f" id="reccuring_parent" style="{{$trans->is_recurring_transaction == 1 ?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.recurring_period'))!!} <span class="red">*</span></strong>
               <select class="form-control" id="recurring_period" name="recurring_period" data_select="{!!html_entity_decode(__('transactions.select_option'))!!}">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@if(!empty($periods))
					@foreach ($periods as $key => $value)
						<option value="{{ $value->id }}" {{$trans->recurring_period == $value->id?'selected':''}}>
						{!!html_entity_decode(__('common.'.$value->name))!!}</option>
					@endforeach
					@endif
					</select>
            </div>

        </div>
		
		 
		<div class="col-xs-12 col-sm-12 col-md-f" id="recurring_option" style="{{$trans->recurring_period > 1 ?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.recurring_option'))!!}<span class="red">*</span></strong>
               <select class="form-control" id="recurring_select" name="repeat_on_every" data_select="{!!html_entity_decode(__('transactions.select_option'))!!}">
				<option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
	 
				 
				@foreach ($p_options as $key => $value)
						<option value="{{ $value->id }}" {{$trans->repeat_on_every == $value->id?'selected':''}}>
						{!!html_entity_decode(__('common.'.$value->name))!!}</option>
					@endforeach
				 
				
				</select>
            </div>
        </div>
	 
		<div class="col-xs-12 col-sm-12 col-md-f" id="repeat_month_day_div" style="{{$trans->repeat_month_day >=1 ?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.recurring_day'))!!}<span class="red">*</span></strong>
               <select class="form-control" id="repeat_month_day" name="repeat_month_day" data_select="{!!html_entity_decode(__('transactions.select_option'))!!}">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
			  
			    @foreach ($month_days as $key => $value)
						<option value="{{ $value->id }}" {{$trans->repeat_month_day == $value->id?'selected':''}}>
						{{ $value->name}}
						
						</option>
				@endforeach
				 
			   </select>
            </div>
        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('transactions.description'))!!}:</strong>

                <textarea name="description" id="description" style="width:100%">{{$trans->description}}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary-edit purple-btn">Submit</button>
            </div>

        </div>

   

    </form>
</div>
</div> 
    
  
 
    
    <style>
	.error{color:border-color:#ff0000; background-color:#ff0000;}
	</style>
    <div class="clear"></div>
    </div>
</div>
@endsection
 