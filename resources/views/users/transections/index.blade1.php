@extends('layouts.app')
@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">


<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
     
     $(document).on('click', '#cat_title_ebook', function(event){
   			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 		
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		</div>
		<div class="category innerright-side">
        <div class="page-title">
			 <h1>{!!html_entity_decode(__('transactions.title'))!!}</h1>  
             <a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}transations/add">{!!html_entity_decode(__('transactions.add_transaction'))!!}</a>
             </div>
             
              <div class="dashborad-about"> 
				<p>{!!html_entity_decode(__('transactions.listing_heading_text'))!!}</p>	
				</div>
                
                <div class="dashboard-search dashboard-form">
                  @include('users.transections.search')
				</div>
                
                <div class="flashmessage">
                  @include('flash-message')
				  </div>
<div class="dashboard-recenttable">
<table class="table table-bordered">

        <tr>


            <th>{!!html_entity_decode(__('transactions.title'))!!} </th>
			<th>{!!html_entity_decode(__('transactions.amount'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.account'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.trans_type'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.category'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.budget'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.date'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.action'))!!}</th>

        </tr>
		 
        @foreach ($transactions as $trans)

        <tr>


            <td>{{ $trans->title }}</td>
			<td>{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $trans->trans_amount }} </td>
			<td> 
				@if($trans->transaction_type == 'EXPENCES')
				{{$trans->trans_account}} 
				@elseif($trans->transaction_type == 'REVENUE')
				{{$trans->trans_destini}}
				@endif
			</td>
			<td>{{ $trans->transaction_type }}</td>
			<td>{{ $trans->trans_cat }}</td>
			<td> {{$trans->budget_name}}</td>
			<td> {{ $trans->trans_date }}</td>
			
		 
			 
			 
			 
            <td> 
			 
			<a  class="btn btn-success editicon" href="{{ route('transactions.edit',$trans->id) }}">{!!html_entity_decode(__('transactions.edit'))!!}</a>
			<a  class="btn btn-success cloneicon" href="{{ env('APP_URL') }}clone_transaction/{{ $trans->id}}">{!!html_entity_decode(__('transactions.clone'))!!}</a>
			<a  class="btn btn-success deleteicon" href="{{ env('APP_URL') }}remove_transaction/{{$trans->id }}">{!!html_entity_decode(__('transactions.delete'))!!}</a>
			 

            </td>

        </tr>

        @endforeach

    </table>
</div>
    
		<div class="pagination">
			{{ $transactions->appends(request()->query())->links() }}
		</div>						 
				 
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 