@extends('layouts.app')
@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>
 
<div class="page dashboardpages"> 
<div class="inner-container">


<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
     
     $(document).on('click', '#cat_title_ebook', function(event){
   			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 		
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		</div>
		<div class="category innerright-side">
        <div class="page-title">
			 <h1>{!!html_entity_decode(__('transactions.title'))!!}</h1>  
			 @if($show_add_account == 0 || $show_add_budget == 0|| $show_add_cat == 0 )
             <a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}transations/add">{!!html_entity_decode(__('transactions.add_transaction'))!!}</a>
			 @endif
             </div>
             
              <div class="dashborad-about"> 
				<p>{!!html_entity_decode(__('transactions.listing_heading_text'))!!}</p>	
				</div>
                

				  
@if($show_add_account == 1 || $show_add_budget ==1|| $show_add_cat ==1 )
<div class="hidden-xs dashboard-section2 padding0">
	@if($show_add_account)
		<div class="div33 addaccount-btns">
       <div class="aa-text">{!!html_entity_decode(__('user_dashboard.add_account_msg'))!!}</div>
       	<div class="clear"></div>
	<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}accounts/add">{!!html_entity_decode(__('user_dashboard.add_account'))!!}</a>
   <div class="clear"></div>
    </div>
	
	@endif
	
    
	@if($show_add_budget)
    <div class="div33 addaccount-btns">
		<div class="aa-text">{!!html_entity_decode(__('user_dashboard.add_budget_msg'))!!}</div>
        	<div class="clear"></div>
	<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}budgets/add">{!!html_entity_decode(__('user_dashboard.add_budget'))!!}</a>
	<div class="clear"></div>
    </div>
	@endif
	@if($show_add_cat)
		<div class="div33 addaccount-btns">
		<div class="aa-text">{!!html_entity_decode(__('user_dashboard.add_cat_msg'))!!}</div>
	<div class="clear"></div>
	<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}categories/add">{!!html_entity_decode(__('user_dashboard.add_category'))!!}</a>	
		
	<div class="clear"></div>
    </div>
	@endif
	 
</div>
@endif				  

<div class="dashboard-search dashboard-form">
  @include('users.transections.search')
</div>

<div class="flashmessage">
  @include('flash-message')
  </div>
				  
<div class="dashboard-recenttable">
<div id="no-more-tables">
<table class="table table-bordered">

        <thead>
        <tr>


            <th>{!!html_entity_decode(__('transactions.title'))!!} </th>
			<th>{!!html_entity_decode(__('transactions.amount'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.account'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.trans_type'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.category'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.budget'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.date'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.is_recurring'))!!}</th>
			<th>{!!html_entity_decode(__('transactions.action'))!!}</th>

        </tr>
		 </thead>
		 
         
        @foreach ($transactions as $trans)

        <tr>

 
            <td data-title="{!!html_entity_decode(__('transactions.title'))!!}">{{ $trans->title }}</td>
			<td data-title="{!!html_entity_decode(__('transactions.amount'))!!}" >{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $trans->trans_amount }} </td>
			<td data-title="{!!html_entity_decode(__('transactions.account'))!!}"> 
				@if($trans->transaction_type == 'EXPENCES')
				{{$trans->trans_account}} 
				@elseif($trans->transaction_type == 'REVENUE')
				{{$trans->trans_destini}}
			 
				@elseif($trans->transaction_type == 'TRANSFER')
				{{$trans->trans_account}}->{{$trans->trans_destini}}
				@endif
			</td>
			<td data-title="{!!html_entity_decode(__('transactions.trans_type'))!!}">{{ $trans->transaction_type }}</td>
			<td data-title="{!!html_entity_decode(__('transactions.category'))!!}" >{{ $trans->trans_cat }}</td>
			<td data-title="{!!html_entity_decode(__('transactions.budget'))!!}"> {{$trans->budget_name}}</td>
			<td data-title="{!!html_entity_decode(__('transactions.date'))!!}"> {{ $trans->trans_date }}</td>
			
			<td data-title="{!!html_entity_decode(__('transactions.is_recurring'))!!}"> 
			 @if($trans->is_recurring_transaction==1 && $trans->stop_recurring==0 )
				 {!!html_entity_decode(__('transactions.yes'))!!}
			 <a class="recurring" href="{{ env('APP_URL') }}stop_repeated_transections/{{ $trans->id}}">{!!html_entity_decode(__('transactions.stop_recurring'))!!}</a>
			 @else
				 {!!html_entity_decode(__('transactions.no'))!!}
			 @endif
			 
			</td>
			
		 

            <td data-title="{!!html_entity_decode(__('transactions.action'))!!}"> 
				<a  class="btn btn-success editicon" href="{{ route('transactions.edit',$trans->id) }}">{!!html_entity_decode(__('transactions.edit'))!!}</a>
				<a  class="btn btn-success cloneicon" onclick="return confirm('{!!html_entity_decode(__('transactions.copy_confirm'))!!}')" href="{{ env('APP_URL') }}clone_transaction/{{ $trans->id}}">{!!html_entity_decode(__('transactions.clone'))!!}</a>
				<a  class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('transactions.delete_confirm'))!!}')"  href="{{ env('APP_URL') }}remove_transaction/{{$trans->id }}">{!!html_entity_decode(__('transactions.delete'))!!}</a>
            </td>

        </tr>

        @endforeach

    </table>
   </div> 
</div>
    
		<div class="pagination">
			{{ $transactions->appends(request()->query())->links() }}
		</div>						 
				 
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 