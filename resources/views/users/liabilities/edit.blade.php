@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
		<div class="category innerright-side">
		<div class="page-title">
                <h1>{!!html_entity_decode(__('liabilities.edit_liability'))!!}</h1>
               <a class="btn btn-success purple-btn" href="{{route('liabilities.index') }}"> {!!html_entity_decode(__('common.back'))!!}</a>
</div>

 <div class="dashborad-about">
			<p>{!!html_entity_decode(__('liabilities.edit_heading_text'))!!}</p>
</div>


	 @include('flash-message')
 
	 <div class="dashboard-form">
    <form action="{{ env('APP_URL') }}liabilities/save"  id="trans_form" method="POST" enctype="multipart/form-data" autocomplete="off">
         
		{{ csrf_field() }}
        @method('POST')
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('liabilities.title'))!!} <span class="red">*</span></strong>
				<input type="hidden" name="id" value="{{$liability->id}}" />
                <input type="text" name="title" class="form-control"  value="{{$liability->title}}" required pattern=".*\S+.*" />
				 

            </div>

        </div>
        
         
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('liabilities.type'))!!} <span class="red">*</span></strong>

                <select class="form-control" name="type" id="liability_type" required>
					 
					<option value="">{!!html_entity_decode(__('liabilities.select_option'))!!} </option>
					@foreach ($types as $key => $value)
						<option value="{{ $value->id }}" {{$liability->type == $value->id?'selected':''}} >
						{{ $value->name}}</option>
					@endforeach
				</select>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('liabilities.amount'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} <span class="red">*</span></strong>
                <input type="number"  name="liability_amount" class="form-control" placeholder="1000" value="{{$liability->liability_amount}}" required>
            </div>
        </div>        
		
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('liabilities.debt_amount'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} <span class="red">*</span></strong>
                <input type="number"  name="amount_of_debt" class="form-control" placeholder="1000" value="{{$liability->amount_of_debt}}" required>
            </div>
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">   
                <strong>{!!html_entity_decode(__('liabilities.start_date'))!!} <span class="red">*</span></strong>
                <input type="text"  name="start_date" class="form-control startDate" placeholder="mm/dd/yyyy" value="{{$liability->start_date}}" required>
            </div>
        </div>		
		
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('liabilities.end_date'))!!} <span class="red">*</span></strong>
                <input type="text"  name="end_date" class="form-control endDate" placeholder="mm/dd/yyyy" value="{{$liability->end_date}}" required>
            </div>
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">  
                <strong>{!!html_entity_decode(__('liabilities.first_transaction_go_on'))!!} <span class="red">*</span></strong>
                <input type="text"  name="first_go_on" class="form-control minDate" placeholder="mm/dd/yyyy" value="{{$liability->first_go_on}}" required>
            </div>
        </div>
		
		
        <div class="col-xs-12 col-sm-12 col-md-f" > 
            <div class="form-group">
                <strong>{!!html_entity_decode(__('liabilities.account'))!!} : <span class="red">*</span></strong>
               <select class="form-control" id="source_account" name="linked_account" required >
			   <option value="">{!!html_entity_decode(__('liabilities.select_option'))!!}</option>
					@foreach ($accounts as $key => $value)
						<option value="{{ $value->id }}" {{$liability->linked_account == $value->id?'selected':''}}>
						{{ $value->title}}</option>
					@endforeach
					</select>
            </div>

        </div>       

 
 
 
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group"> 

                <strong>{!!html_entity_decode(__('liabilities.is_recurring_transaction'))!!} </strong>

                <input type="checkbox" name="is_recurring_transaction"  id="is_recurring_transaction"  value="1" {{ $liability->is_recurring_transaction == 1 ? 'checked' : '' }}> 

            </div>

        </div>
		
        <div class="col-xs-12 col-sm-12 col-md-f" id="reccuring_parent" style="{{$liability->is_recurring_transaction == 1 ?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('liabilities.recurring_period'))!!} <span class="red">*</span></strong>
               <select class="form-control" id="recurring_period" name="recurring_period" data_select="{!!html_entity_decode(__('liabilities.select_option'))!!}">
			   <option value="">{!!html_entity_decode(__('liabilities.select_option'))!!}</option>
					@foreach ($periods as $key => $value)
						<option value="{{ $value->id }}" {{$liability->recurring_period == $value->id?'selected':''}}>
						{!!html_entity_decode(__('common.'.$value->name))!!}</option>
					@endforeach
					</select>
            </div>

        </div>
		 
			<div class="col-xs-12 col-sm-12 col-md-f bimonth_div" style="{{$liability->recurring_period == 7 ?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.first_payment_date'))!!} <span class="red">*</span></strong>
               <select class="form-control" id="first_payment_date" name="first_payment_date" data_select="{!!html_entity_decode(__('transactions.select_option'))!!}">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($first_pay_days as $key => $value)
						<option value="{{ $value->id }}" {{$liability->first_payment_date == $value->id?'selected':''}} > {{$value->name}}</option>
					@endforeach
					</select>
            </div>

        </div>
		<div class="col-xs-12 col-sm-12 col-md-f bimonth_div" style="{{$liability->recurring_period == 7 ?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('transactions.second_payment_date'))!!} <span class="red">*</span></strong>
               <select class="form-control" id="second_payment_date" name="second_payment_date" data_select="{!!html_entity_decode(__('transactions.select_option'))!!}">
			   <option value="">{!!html_entity_decode(__('transactions.select_option'))!!}</option>
					@foreach ($sec_pay_days as $key => $value)
						<option value="{{ $value->id }}" {{$liability->second_payment_date == $value->id?'selected':''}} > {{$value->name}}</option>
					@endforeach
					</select>
            </div>
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-f" id="recurring_option" style="{{$liability->recurring_period > 1 && $liability->recurring_period !=7 ?'':'display:none;'}}">
            <div class="form-group">
                <!--<strong>{!!html_entity_decode(__('liabilities.recurring_option'))!!}<span class="red">*</span></strong>-->
				
				<strong id="normal_text">{!!html_entity_decode(__('transactions.recurring_option'))!!} <span class="red">*</span></strong>
                
				
               <select class="form-control" id="recurring_select" name="repeat_on_every" data_select="{!!html_entity_decode(__('liabilities.select_option'))!!}">
				<option value="">{!!html_entity_decode(__('liabilities.select_option'))!!}</option>
				@foreach ($p_options as $key => $value )
						<option value="{{ $value->id }}" {{$liability->repeat_on_every == $value->id?'selected':''}} >
						{!!html_entity_decode(__('common.'.$value->name))!!}</option>
					@endforeach
				
				</select>
            </div>
        </div>
		
		<div class="col-xs-12 col-sm-12 col-md-f" id="repeat_month_day_div" style="{{$liability->repeat_month_day >=1?'':'display:none;'}}">
            <div class="form-group">
                <strong>{!!html_entity_decode(__('liabilities.recurring_day'))!!} <span class="red">*</span></strong>
               <select class="form-control" id="repeat_month_day" name="repeat_month_day" data_select="{!!html_entity_decode(__('liabilities.select_option'))!!}">
			   <option value="">{!!html_entity_decode(__('liabilities.select_option'))!!}</option>
			   @foreach ($month_days as $key => $value)
						<option value="{{ $value->id }}" {{$liability->repeat_month_day == $value->id?'selected':''}} >
						{{ $value->name}}</option>
					@endforeach
			   </select>
            </div>
        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('liabilities.description'))!!} :</strong>

                <textarea name="budget_description" id="description" style="width:100%"></textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit purple-btn">Submit</button>

            </div>

        </div>

   

    </form>
    </div>

</div> 
    
  
 
    
    <style>
	.error{color:border-color:#ff0000; background-color:#ff0000;}
	</style>
    <div class="clear"></div>
    </div>
</div>
@endsection
 