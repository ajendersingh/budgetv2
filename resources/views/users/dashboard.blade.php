@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
$(function(){
$("#cancelSearch").click(function(){
	$("#startDate").val("");
	$("#endDate").val("");
				
	 $("#dsForm"). submit(); 
})
})
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			  
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
         
		<div class="category innerright-side">
				<h1> {!!html_entity_decode(__('user_dashboard.page_title'))!!}</h1>
                <div class="dashborad-about">  
				<p>{!!html_entity_decode(__('user_dashboard.welcome_text'))!!}</p>
				<p class="date">{{$month_date}}</p>
                </div>
		<!--		 
	 <form action="" id="dsForm" class="dsForm" autocomplete="on">
		 <fieldset>
			<label for="startDate">Start Date</label>
			<input type="text" autocomplete="off" style="width: 88px;" class="startDate" id="startDate" size="10" name="start_date" />
		</fieldset>
		<fieldset>
			<label for="dataEnd">End Date</label>
		   <input type="text" style="width: 88px;" class="end_date endDate" id="endDate" size="10" name="end_date" value="" />
		</fieldset>
		<input type="submit" name="submit" value="Submit" /> 
		<input type="submit" id="cancelSearch" name="cancel" value="Clear Search" /> 
	 </form> 
	 -->
	 		 
@if($show_add_account == 1 || $show_add_budget ==1|| $show_add_cat ==1 )

<div class="hidden-xs dashboard-section2 padding0">
	@if($show_add_account)
		<div class="div25 addaccount-btns">
      <div class="aa-text"> {!!html_entity_decode(__('user_dashboard.add_account_msg'))!!}</div>
       	<div class="clear"></div>
	<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}accounts/add">{!!html_entity_decode(__('user_dashboard.add_account'))!!}</a>
   <div class="clear"></div>
    </div>
	
	@endif
	
    
	@if($show_add_budget)
    <div class="div25 addaccount-btns">
		<div class="aa-text">{!!html_entity_decode(__('user_dashboard.add_budget_msg'))!!}</div>
        	<div class="clear"></div>
	<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}budgets/add">{!!html_entity_decode(__('user_dashboard.add_budget'))!!}</a>
	<div class="clear"></div>
    </div>
	@endif
	@if($show_add_cat)
		<div class="div25 addaccount-btns">
		<div class="aa-text">{!!html_entity_decode(__('user_dashboard.add_cat_msg'))!!}</div>
	<div class="clear"></div>
	<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}categories/add">{!!html_entity_decode(__('user_dashboard.add_category'))!!}</a>	
		
	<div class="clear"></div>
    </div>
	@endif
    
    @if($show_add_budget)
    <div class="div25 addaccount-btns">
		<div class="aa-text">{!!html_entity_decode(__('user_dashboard.add_libility_msg'))!!}</div>
        	<div class="clear"></div>
<a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}liabilities/add">{!!html_entity_decode(__('user_dashboard.add_liability'))!!}</a>	<div class="clear"></div>
    </div>
	@endif
	 
</div>



@else
<div class="hidden-xs dashboard-section2"> 
    <div class="dashboard-section2-row green">
        <div class="info-box bg-aqua-gradient" id="box_out_holder">
            <span class="info-box-icon  success-icon"><i class="fa fa-balance-scale"></i></span>

            <div class="info-box-content" ng-init="getBalance()">
                <span class="info-box-text">
                    <a href="javascript:void(0)" class="text-success">{!!html_entity_decode(__('user_dashboard.net_income'))!!}</a></span>
                <span class="info-box-number" id="box-balance-sums">{!!html_entity_decode(__('common.currency_symbol'))!!}{{$income}}</span>

            </div>
        </div>
    </div>

    <div class="dashboard-section2-row blue">
        <div class="info-box bg-teal-gradient">
            <span class="info-box-icon info-icon"><i class="fa fa-calendar-o"></i></span>

            <div class="info-box-content" ng-init="regular_transections()">
                <span class="info-box-text"><a href="javascript:void(0)" class="text-info">{!!html_entity_decode(__('user_dashboard.total_budgeted'))!!}</a></span>
                <span class="info-box-number text-success" id="box-bills-unpaid">{!!html_entity_decode(__('common.currency_symbol'))!!}{{$budgets}}</span>

               
            </div>
        </div>
    </div>

        <div class="dashboard-section2-row purple">
        <div class="info-box bg-green-gradient" id="box-left-to-spend-box">
            <span class="info-box-icon fancy-icon"><i class="fa fa-money"></i></span>
            <div class="info-box-content" ng-init="left_budgets()">
                <span class="info-box-text"><a id="box-left-to-spend-text" class="text-fancy" href="javascript:void(0)">{!!html_entity_decode(__('user_dashboard.provisional_balance'))!!}</a></span>
                <span class="info-box-number" id="box-left-to-spend">{!!html_entity_decode(__('common.currency_symbol'))!!}{{$provisonal_bal}}</span>

               
            </div>
        </div> 
    </div>

        <div class="dashboard-section2-row orange">
        <div class="info-box bg-blue-gradient" ng-init="get_expences();">
            <span class="info-box-icon warning-icon"><i class="fa fa-line-chart"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">
                    <a href="javascript:void(0)" class="text-success">{!!html_entity_decode(__('user_dashboard.total_expenditure'))!!} </a></span>
				 
                <span class="info-box-number" id="box-net-worth">{!!html_entity_decode(__('common.currency_symbol'))!!}{{$expences}}</span>
                 
				  
            </div>
        </div>
    </div>
	
	
	<div class="dashboard-section2-row orange">
        <div class="info-box bg-blue-gradient" ng-init="get_expences();">
            <span class="info-box-icon warning-icon"><i class="fa fa-line-chart"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">
                    <a href="javascript:void(0)" class="text-success">Remaining to spend</a></span>
				 
                <span class="info-box-number" id="box-net-worth">{!!html_entity_decode(__('common.currency_symbol'))!!}{{$left_to_spent}}</span>
                
				  
            </div>
        </div>
    </div>
	
	
</div>
@endif
<div class="hidden-xs dashboard-widget"> 
<h2>{!!html_entity_decode(__('user_dashboard.account_balance_title'))!!}: {!!html_entity_decode(__('common.currency_symbol'))!!}{{$available_balance}} </h2>
<div class="clear"></div>
 @foreach ($listaccounts as $account)
    <div class="dashboard-widget-row">
        <div class="info-box bg-aqua-gradient" id="box_out_holder">
            <span class="info-box-icon  success-icon"><i aria-hidden="true" class="fa fa-eur"></i></span>

            <div class="info-box-content" >
                <span class="info-box-text">
                    <a href="{{ env('APP_URL') }}show_account/{{$account->id }}" class="text-success">{{$account->title}}</a></span>
                <span class="info-box-number" id="box-balance-sums">{!!html_entity_decode(__('common.currency_symbol'))!!}{{$account->balance}}</span>

            </div>
        </div>
    </div>
	@endforeach
</div>	
	



   <div class="dashbaordchart">
     <div class="chart-container" style="display:none;">
				<div class="pie-chart-container">
				  <canvas id="pie-chart"></canvas>
				</div>
			  </div>           
			  
			  <div class="chart-container" style="display:none;">
				<div class="pie-chart-container">
				  <canvas id="doughnut-chart" width="500" height="450"></canvas>
				</div>
			  </div> 
			  
			  
			  <div class="chart-container">
				<div class="pie-chart-container">
				 <canvas id="bar-chart" width="800" height="450"></canvas>
				</div>
			  </div>                 
                 
                  			  
			 <div class="chart-container" style="display:none;">
				<div class="pie-chart-container">
				 <canvas id="line-chart" width="800" height="450"></canvas>
				</div>
			  </div>                 
                 
                  
                </div> 
				
        <div class="dashboard-recenttable">
        <h2>{!!html_entity_decode(__('user_dashboard.recent_transactions'))!!} </h2>
        <div id="no-more-tables">
        <table class="table table-bordered">
    
            <thead>
            <tr>
    
    
                <th>{!!html_entity_decode(__('transactions.title'))!!} </th>
                <th>{!!html_entity_decode(__('transactions.amount'))!!}</th>
                <th>{!!html_entity_decode(__('transactions.account'))!!}</th>
                <th>{!!html_entity_decode(__('transactions.trans_type'))!!}</th>
                <th>{!!html_entity_decode(__('transactions.category'))!!}</th>
                <th>{!!html_entity_decode(__('transactions.budget'))!!}</th>
                <th>{!!html_entity_decode(__('transactions.date'))!!}</th>
                <th>{!!html_entity_decode(__('transactions.action'))!!}</th>
    
            </tr> 
             </thead>
             @foreach ($transactions as $trans)
    
            <tr>
    
    
                <td data-title="Title">{{ $trans->title }}</td>
                <td data-title="Amount">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $trans->trans_amount }} </td>
                <td data-title="Account"> 
                    @if($trans->transaction_type == 'EXPENCES')
                    {{$trans->trans_account}} 
                    @elseif($trans->transaction_type == 'REVENUE')
                    {{$trans->trans_destini}}
                    @elseif($trans->transaction_type == 'TRANSFER')
					{{$trans->trans_account}}->{{$trans->trans_destini}}
					@endif
                </td>
                <td data-title="Type">{{ $trans->transaction_type }}</td>
                <td data-title="Category">{{ $trans->trans_cat }}</td>
                <td data-title="Budget">{{$trans->budget_name}}</td>
                <td data-title="Date">{{ $trans->trans_date }}</td>
                
             
                 
                 
                 
                <td data-title="Action"> 
					<a class="btn btn-success editicon" href="{{ route('transactions.edit',$trans->id) }}">{!!html_entity_decode(__('transactions.edit'))!!}</a>
					<a class="btn btn-success cloneicon" onclick="return confirm('{!!html_entity_decode(__('transactions.copy_confirm'))!!}')" href="{{ env('APP_URL') }}clone_transaction/{{ $trans->id}}">{!!html_entity_decode(__('transactions.clone'))!!}</a>
					<a class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('transactions.delete_confirm'))!!}')" href="{{ env('APP_URL') }}remove_transaction/{{$trans->id }}">{!!html_entity_decode(__('transactions.delete'))!!}</a>
                </td>
    
            </tr>
    
            @endforeach
    
        </table>
        </div>
        </div>	
				
 


    </div>
    
    <div class="clear"></div>
    </div>
</div>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <script type="application/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
 <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script type="application/javascript" >

$(function(){
	
	
	
	
		  new Chart(document.getElementById("doughnut-chart"), {
		type: 'doughnut',
		data: {
		  labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
		  datasets: [
			{
			  label: "Population (millions)",
			  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
			  data: [2478,5267,734,784,433]
			}
		  ]
		},
		options: {
		  title: {
			display: true,
			text: 'Predicted world population (millions) in 2050'
		  }
		}
	});

      //get the pie chart canvas
      var cData = JSON.parse(`<?php echo $chart_data; ?>`);
      var ctx = $("#pie-chart");
 
      //pie chart data
      var data = {
        labels: cData.label,
        datasets: [
          {
            label: "Users Count",
            data: cData.data,
            backgroundColor: [
              "#DEB887",
              "#A9A9A9",
              "#DC143C",
              "#F4A460",
              "#2E8B57",
              "#1D7A46",
              "#CDA776",
            ],
            borderColor: [
              "#CDA776",
              "#989898",
              "#CB252B",
              "#E39371",
              "#1D7A46",
              "#F4A460",
              "#CDA776",
            ],
            borderWidth: [1, 1, 1, 1, 1,1,1]
          }
        ]
      };
 
      //options
      var options = {
        responsive: true,
        title: {
          display: true,
          position: "top",
          text: "Last Week Registered Users -  Day Wise Count",
          fontSize: 18,
          fontColor: "#111"
        },
        legend: {
          display: true,
          position: "bottom",
          labels: {
            fontColor: "#333",
            fontSize: 16
          }
        }
      };
 
      //create Pie Chart class object
      var chart1 = new Chart(ctx, {
        type: "pie",
        data: data,
        options: options
      });
 
 
	 /***************************/
	 new Chart(document.getElementById("bar-chart"), {
		type: 'bar',
		data: {
		  labels: cData.label,
		  datasets: [
			{
				label: "<?php echo $month_date;?>",
			  //backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"], cData.color
			  backgroundColor: cData.color, 
			  data: cData.data
			}
		  ]
		},
		options: {
		  legend: { display: false },
		  tooltips: {
					callbacks: {
						label: function (tooltipItem, data) {
							//var datasetLabel = data.account_name;
							var datasetLabel = data.datasets[tooltipItem.datasetIndex].label || '';
 
							var datasetValueINR = new Intl.NumberFormat('en-IN', {
								maximumSignificantDigits: 3,
								//style: 'currency',
								//currency: "<?php echo $currency;?>"
							}).format(tooltipItem.yLabel);
  
								return datasetLabel + ' : ' +"<?php echo $currency;?>"+ datasetValueINR;
							
						}
					}
				},
		  title: {
			display: true,
			text: "<?php echo $bar_chart_title ?>"
		  }
		}
	});
 /*********************/
 
 new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
    datasets: [{ 
        data: [86,114,106,106,107,111,133,221,783,2478],
        label: "Africa",
        borderColor: "#3e95cd",
        fill: false
      }, { 
        data: [282,350,411,502,635,809,947,1402,3700,5267],
        label: "Asia",
        borderColor: "#8e5ea2",
        fill: false
      }, { 
        data: [168,170,178,190,203,276,408,547,675,734],
        label: "Europe",
        borderColor: "#3cba9f",
        fill: false
      }, { 
        data: [40,20,10,16,24,38,74,167,508,784],
        label: "Latin America",
        borderColor: "#e8c3b9",
        fill: false
      }, { 
        data: [6,3,2,2,7,26,82,172,312,433],
        label: "North America",
        borderColor: "#c45850",
        fill: false
      }
    ]
  },
  options: {
    title: {
      display: true,
      text: 'World population per region (in millions)'
    }
  }
});

  });
</script>
 
@endsection
