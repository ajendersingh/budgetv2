@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
         
		<div class="category innerright-side">
		<div class="page-title"><h1>{!!html_entity_decode(__('accounts.add_account'))!!}</h1><a class="btn btn-success purple-btn" href="{{route('accounts.index') }}"> {!!html_entity_decode(__('accounts.back'))!!}</a></div>

			
			<div class="dashborad-about">
			<p>{!!html_entity_decode(__('accounts.add_heading_text'))!!}</p>
			</div>


	<div class="flashmessage">
	@if(count($errors))
	 
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.
			<br/>
			<ul>
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
    </div>
	
 

	<div class="dashboard-form">	
    <form action="{{ env('APP_URL') }}accounts/save" method="POST" enctype="multipart/form-data" autocomplete="off">
         
		{{ csrf_field() }}
        @method('POST')
         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.Name'))!!} : <span class="red">*</span> </strong>
				<input type="hidden" name="id" value="" />
                <input type="text" name="title" class="form-control"  value="" required pattern=".*\S+.*" />
				 

            </div>

        </div>
        
         
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.account_type'))!!} : <span class="red">*</span></strong>

                <select class="form-control" name="account_type" required>
				<option value=""></option>
					@foreach ($account_types as $key => $val)
						<option value="{{ $val->name }}"   >
						{{ $val->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">  
			

                <strong>{!!html_entity_decode(__('accounts.opening_balance'))!!} {!!html_entity_decode(__('common.currency_symbol'))!!} : </strong>

                <input type="number"  name="opening_balance" class="form-control" placeholder="1000" min="0" title="Only numbers are allowd here!" value="">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.account_role'))!!} : </strong>

               <select class="form-control" name="account_role">
			   <option value=""></option>
					@foreach ($account_roles as $key => $value)
						<option value="{{ $value->name }}">
						{{ $value->name}}</option>
					@endforeach
					</select>

            </div>

        </div>
 
 
        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('accounts.Description'))!!} : </strong>

                <textarea name="description" id="description" style="width:100%"></textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit purple-btn">Submit</button>

            </div>

        </div>

   

    </form>
</div>
</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

</div>
@endsection
 