@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
		<div class="category innerright-side">
				<div class="page-title">
				<h1> 
				@if($title!="")                 
				{{ $title }}
				 @endif  
				 </h1><a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}accounts/add">{!!html_entity_decode(__('accounts.add_account'))!!}</a></div>
                 
                 <div class="dashborad-about"> 
					<p>{!!html_entity_decode(__('accounts.listing_heading_text'))!!}</p>			 
				  @include('flash-message')
				 </div>
                 <div class="dashboard-recenttable">
                 <div id="no-more-tables">
                 <table class="table table-bordered">
		<thead>
        <tr>


            <th>{!!html_entity_decode(__('accounts.Name'))!!} </th>
			<th>{!!html_entity_decode(__('accounts.current_balance'))!!}</th>
			<th>{!!html_entity_decode(__('accounts.status'))!!}</th>
			<th>{!!html_entity_decode(__('accounts.Action'))!!}</th>

        </tr>
			</thead>
        @foreach ($accounts as $account)

        <tr>


            <td data-title="{!!html_entity_decode(__('accounts.Name'))!!}"><a href="{{ env('APP_URL') }}show_account/{{$account->id }}">{{ $account->title }}</a></td>
			<td data-title="{!!html_entity_decode(__('accounts.current_balance'))!!}">{!!html_entity_decode(__('common.currency_symbol'))!!}{{ $account->balance }} </td>
			<td data-title="{!!html_entity_decode(__('accounts.status'))!!}">{{ $account->is_active==1?'Active':'Inactive' }}</td>
            
            <td data-title="{!!html_entity_decode(__('accounts.Action'))!!}"> 
			
			
			<a  class="btn btn-success editicon" href="{{ route('accounts.edit',$account->id) }}">{!!html_entity_decode(__('accounts.edit'))!!}</a>
			<a class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('accounts.delete_confirm'))!!}')" href="{{ env('APP_URL') }}delete_account/{{$account->id }}">{!!html_entity_decode(__('accounts.delete'))!!}</a>
			 

            </td>

        </tr>

        @endforeach

    </table>
    </div>
								 </div>
				 
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
     <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</div>
@endsection
 