@extends('layouts.app')
@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {
     
     $(document).on('click', '#cat_title_ebook', function(event){
   			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		</div>
		<div class="category innerright-side">
				<div class="page-title">					
				<h1> {!!html_entity_decode(__('categories.title'))!!}  </h1>
                <a class="btn btn-success purple-btn" href="{{ env('APP_URL') }}categories/add">{!!html_entity_decode(__('categories.add_category'))!!}</a>
                  </div>
                  
                   <div class="dashborad-about"> 
				 <p>{!!html_entity_decode(__('categories.listing_heading_text'))!!}</p>
				</div>
                
					<div class="flashmessage">
				  @include('flash-message')
				  </div>
                  
                  
                 <div class="dashboard-recenttable"> 
				 
                 <div id="no-more-tables">
                 <table class="table table-bordered">

        <thead>
        <tr>


            <th>{!!html_entity_decode(__('categories.cat_name'))!!} </th>
			 
			 
			<th>{!!html_entity_decode(__('categories.status'))!!}</th>
			<th>{!!html_entity_decode(__('categories.action'))!!}</th>

        </tr>
		 </thead>
         
        @foreach ($categories as $cat)

        <tr>


            <td data-title="{!!html_entity_decode(__('categories.cat_name'))!!}" ><a href="{{ env('APP_URL') }}categories/show/{{$cat->cat_id}}">{{ $cat->cat_name }}</a></td>
			 
			 
		 
			 
			<td data-title="{!!html_entity_decode(__('categories.status'))!!}">
			@if($cat->cat_is_active ==1)
				{!!html_entity_decode(__('categories.active'))!!}
			@else
				{!!html_entity_decode(__('categories.inactive'))!!}
			@endif
			 
            <td data-title="{!!html_entity_decode(__('categories.action'))!!}"> 
			<form action="{{ route('categories.destroy',$cat->cat_id) }}" method="POST">
			</form>
			<a  class="btn btn-success editicon" href="{{ route('categories.edit',$cat->cat_id) }}">{!!html_entity_decode(__('categories.edit'))!!}</a>
			 
			@csrf
			@method('DELETE')
			<a  class="btn btn-success deleteicon" onclick="return confirm('{!!html_entity_decode(__('categories.delete_confirm'))!!}')"  href="{{ env('APP_URL') }}delete_category/{{$cat->cat_id }}">{!!html_entity_decode(__('categories.delete'))!!}</a>
			

            </td>

        </tr>

        @endforeach

    </table>
								 
                                 </div>
				 	</div> 
		</div> 
    			 
    
   
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 