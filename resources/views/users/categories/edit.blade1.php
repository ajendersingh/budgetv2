@extends('layouts.app')

@section('content')

<div class="container 11">
     
</div>
<template>
<component :is="'style'">
body{background:#f6f6f6}
</component>
</template>

<div class="page dashboardpages"> 
<div class="inner-container">
<script  type="application/javascript">
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

if(isMobile.any()) {

	
   //some code...
   //$(".show_hide_cat_div").css("display", "none");
   
 
    
     $(document).on('click', '#cat_title_ebook', function(event){
 			
 			 
  			$('.show_hide_cat_div').toggleClass("hideCatClass"); 
 			
      		event.stopImmediatePropagation();
	});
     
}
 

</script>
 			 
		<div class="category-sidebar">
		 @include('layouts.sidebar')
		 </div>
		<div class="category innerright-side">
                <div class="page-title"><h1>{!!html_entity_decode(__('categories.edit_category'))!!}</h1>
               <a class="btn btn-success purple-btn" href="{{route('categories.index') }}"> {!!html_entity_decode(__('budgets.back'))!!}</a>
		
        </div>
        
        <div class="dashborad-about"> 
        	<p>{!!html_entity_decode(__('categories.edit_heading_text'))!!}</p>
			</div>
            
		<div class="flashmessage">
	 @include('flash-message')
		 </div>
	
    <div  class="dashboard-form">
    <form action="{{ env('APP_URL') }}categories/save" method="POST" enctype="multipart/form-data" autocomplete="off">
         
		{{ csrf_field() }}
        @method('POST')
         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('categories.title'))!!} : <span class="red">*</span></strong>
				<input type="hidden" name="id" value="{{$cat->cat_id}}" />
                <input type="text" name="cat_name" class="form-control"  value="{{$cat->cat_name}}" required />
				 

            </div>

        </div>
         
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>{!!html_entity_decode(__('categories.description'))!!} : </strong>

                <textarea name="cat_description" id="description" style="width:100%">{{$cat->cat_description}}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit purple-btn">{!!html_entity_decode(__('categories.update'))!!}</button>

            </div>

        </div>

   

    </form>
		</div>
</div> 
    			 
    
    
    <div class="clear"></div>
    </div>
</div>
@endsection
 