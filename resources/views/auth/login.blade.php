@extends('layouts.app')

@section('content')

<div class="inner-container">
    <div class="logincontainer">
        <div class="loginsection">
        <div class="login-signup-title"><a href="{{ route('login') }}" class="active loginform">{{ __('user_login.login_text') }}</a><a href="{{ env('APP_URL') }}pricing">{{ __('user_login.register_text') }}</a></div>
            <div class="loginform">
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="col-md-12 col-form-label">{{ __('user_login.user_or_email') }} <span class="red">*</span></label>

                            <div class="nopadding">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-12 col-form-label">{{ __('user_login.password') }} <span class="red">*</span></label>

                            <div class="nopadding">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        

                        <div class="loginform-buttons">
                            <div class=" nopadding">
                                <button type="submit" class="btn btn-primary">
                                    <span class="arrow-icon">{{ __('user_login.login_btn') }}</span>
                                     
                                </button>
							
                            <div class="form-group">
                            <div class="rememberleft" style="float:left">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                         {{ __('user_login.remember_me') }}
                                    </label>
                                </div>
                            </div>
                           <div class="lostpass" style="float:right">  @if (Route::has('password.request'))
                                    <a class="btn btn-links" href="{{ route('password.request') }}">{{ __('user_login.lost_password') }}</a>
                                @endif</div>
                        </div>
                               
                            </div>
                        </div>
                    </form>
                </div>
                
            </div>
            </div>
    </div>
</div>
@endsection
