@extends('layouts.app')

@section('content')

<div class="inner-container">
    <div class="logincontainer">
        <div class="loginsection">
                <div class="login-signup-title"><h3>{{ __('Admin Login') }}</h3></div>
                 <div class="loginform">
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.login.submit') }}">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="col-md-12 col-form-label"> {{ __('messages.mail_address_login') }}</label>

                            <div class="nopadding">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-form-label">{{ __('messages.password_text') }}</label>

                            <div class="nopadding">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                         

                        <div class="loginform-buttons">
                                <button type="submit" class="btn btn-primary">
                                    <span class="arrow-icon">{{ __('messages.login_text') }}</span>
                                     
                                </button>


						<div class="form-group">
                        <div class="rememberleft" style="float:left">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('messages.remember_me_text') }}
                                    </label>
                                </div>
                      		</div>
                      
                        </div>
                        
                        
                        </div>
                         
                        
                    </form>
                    </div>
                </div>
              
        </div>
    </div>
</div>
@endsection
