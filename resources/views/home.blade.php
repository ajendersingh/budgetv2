@extends('layouts.app')

@section('content')
 
  <!-- end home-content-tablecell --> 		   
		
					
    
   <!-- end home-content-table -->
 <div id="beaware">
<div class="beaware-row">
@foreach ($features as $feature)
 
	{!!html_entity_decode($feature->{'description_'.config('app.locale')})!!}
	 
  @endforeach
<!--
	<div class="beaware-col orange">
		<div class="beaware-icons">
		<img src="{{asset('images/beawareicon1.png')}}" alt="icon">
		</div>

		<div class="beaware-hading">
		<h3>Be aware of your spending</h3>
		</div>


		<div class="beaware-content" >
		<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
		</div>

		<div class="beaware-arrow">
		<a href="#"><img src="{{asset('images/arrow-orange.png')}}"></a>
		</div>
	</div>



	<div class="beaware-col purple margin">
		<div class="beaware-icons">
		<img src="{{asset('images/beawareicon2.png')}}" alt="icon">
		</div>

		<div class="beaware-hading">
		<h3>Know what's safe to spend</h3>
		</div>


		<div class="beaware-content">
		<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
		</div>

		<div class="beaware-arrow">
		<a href="#"><img src="{{asset('images/arrow-purple.png')}}"></a>
		</div>
	</div>

 

	<div class="beaware-col green">
		<div class="beaware-icons">
		<img src="{{asset('images/beawareicon3.png')}}" alt="icon">
		</div>

		<div class="beaware-hading">
		<h3>Put your budget on autopilot</h3>
		</div>


		<div class="beaware-content">
		<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
		</div>

		<div class="beaware-arrow">
		<a href="#"><img src="{{asset('images/arrow-green.png')}}"></a>
		</div>
	</div>

-->
<div class="beaware-bottomheading" data-aos="fade-right">
<h2>{!!html_entity_decode(__('home_page.bdg_lives_changed'))!!}</h2>
</div>

</div>
</div>



<div id="budgetpay">
<div class="container">
<div class="budgetpay-main">
<div class="budgetpay-heading" data-aos="fade-in">
<h2>{!!html_entity_decode(__('home_page.bdg_help_you'))!!} </h2>
<p>{!!html_entity_decode(__('home_page.bdg_help_you_desc'))!!}</p>
<a href="{{ url('/pricing') }}">{!!html_entity_decode(__('home_page.platform_tour'))!!} </a>


</div>

<div class="budgetpay-tabs bhoechie-tab-container">
<div class="bhoechie-tab-menu budgetpay-tabs-left" data-aos="fade-right">
<div class="list-group">
                <a href="#" class="list-group-item active text-center"><img src="{{asset('images/ideaicon.png')}}"><span>{!!html_entity_decode(__('home_page.bdg_idea_plan'))!!} </span></a>
                <a href="#" class="list-group-item text-center"><img src="{{asset('images/simplyicon.png')}}"><span>{!!html_entity_decode(__('home_page.bdg_simply'))!!}</span></a>
                <a href="#" class="list-group-item text-center"><img src="{{asset('images/sheetsicon.png')}}"><span>{!!html_entity_decode(__('home_page.bdg_sheet'))!!}</span></a>
                <a href="#" class="list-group-item text-center"><img src="{{asset('images/pagemakericon.png')}}"><span>{!!html_entity_decode(__('home_page.bdg_pagemaker'))!!}</span></a>
                <a href="#" class="list-group-item text-center"><img src="{{asset('images/variationsicon.png')}}"><span>{!!html_entity_decode(__('home_page.bdg_variations'))!!}</span></a>
                <a href="#" class="list-group-item text-center"><img src="{{asset('images/dictionaryicon.png')}}"><span>{!!html_entity_decode(__('home_page.bdg_dictonary'))!!}</span></a>
              </div>
              </div>
              
              
<div class="budgetpay-tabs-content bhoechie-tab" data-aos="fade-left">
<div class="bhoechie-tab-content active">
<img src="{{asset('images/tabimg.png')}}" alt="tab">
</div>
      
<div class="bhoechie-tab-content">
<img src="{{asset('images/tabimg2.png')}}" alt="tab">
</div>

<div class="bhoechie-tab-content">
<img src="{{asset('images/tabimg3.png')}}" alt="tab">
</div>

<div class="bhoechie-tab-content">
<img src="{{asset('images/tabimg4.png')}}" alt="tab">
</div>


<div class="bhoechie-tab-content">
<img src="{{asset('images/tabimg5.png')}}" alt="tab">
</div>


<div class="bhoechie-tab-content">
<img src="{{asset('images/tabimg2.png')}}" alt="tab">
</div>

            
</div>
</div>
</div>
</div>
</div>




<div class="blueslider">
<div class="blueslider-heading">
 
<h2>{!!html_entity_decode(__('home_page.closer_look'))!!}</h2>
<p>{!!html_entity_decode(__('home_page.closer_look_desc'))!!} </p>
</div>

<div class="blueslider-main">
<div class="blueopacity"></div>
<div class="owl-slider">
<div class="mobilefram"> <img src="{{asset('images/mobileframe.png')}}"></div>
                    <div id="carousel" class="owl-carousel">
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg1.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg2.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg3.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg4.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg5.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg6.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg7.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        <div class="item">
                         <div class="testi-slider">
                         <div class="testi-sliderimg">
                         <img src="{{asset('images/mobileimg8.png')}}">
                         </div>
                          </div> 
                            
                        </div>
                        
                        
                  
                    </div>
                </div>
</div>

</div>





<div id="getstarted">
<div class="container">
<div class="getstarted" data-aos="fade-right">
<h2>{!!html_entity_decode(__('home_page.bdg_get_start_middle'))!!}</h2>
<a href="{{ url('/pricing') }}">{!!html_entity_decode(__('home_page.join_now'))!!}</a>
</div>


<div class="getstarted-slider">
<div class="getstarted-sliderheading" data-aos="fade-right">
<h2>{!!html_entity_decode(__('home_page.bdg_see_why'))!!} </h2>
</div>

<div class="getstarted-slidermain">
<div class="gradientleft"></div>
<div class="gradientright"></div>

<div id="carousel-test" class="owl-carousel">

<!--<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>


<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>

<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>

<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>-->


 @foreach ($testimonials as $node)
 
 
<div class="item">
<div class="sliderow">
	<p>{!!html_entity_decode(substr($node->{'description_'.config('app.locale')}, 0, 85) )!!}</p>
	<div class="slidename">
		<div class="img">
		@if($node->thumbnail !='')
				<img src="{{asset('public/images/')}}/{{$node->thumbnail}}" style="max-height:50px;" />
		@else
			<img src="{{asset('images/slideimg.png')}}">			
		@endif
		
		</div>
		<div class="name"><span><strong>{!!html_entity_decode($node->testimonial_client_en)!!}</strong>{!!html_entity_decode($node->testimonial_location_en)!!}</span></div>
	</div>
</div> 
 </div>
 
  @endforeach
			  
			  
<!--<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>

<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>

<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>

<div class="item">
<div class="sliderow">
<p>These gives me the confidence to send and track emails while staying on top of my inbox</p>
<div class="slidename">
<div class="img"><img src="{{asset('images/slideimg.png')}}"></div>
<div class="name"><span><strong>Mat Vogels</strong> CEO, Zestful</span></div>
</div>

</div> 
</div>-->
</div>

</div>


</div>
</div>	
</div>


<!-------Accourdian----------->


<div class="acordian-section">
<div class="container">
<div class="acordian-main">
<div class="acordian-main-heading"><h1>{!!html_entity_decode(__('home_page.bdg_faq'))!!}</h1></div>

<div class="acordian-main-data">

<div class="accordion">
        <ul class="list-group">
		
		@foreach ($faqData as $node)
            <li class="list-group-item" data-target="#faq-{{$node->id}}" >
            <h3>{!!html_entity_decode($node->{'name_'.config('app.locale')})!!}</h3>
            <div id="faq-{{$node->id}}" class="collapse innercontent">
            <p>{!!html_entity_decode($node->{'description_'.config('app.locale')})!!}</p>
            </div>
            </li>
            @endforeach 
			
			
         <!-- <li class="list-group-item" data-toggle="collapse" data-target="#one" data-aos="fade-left">
            <h3>What is Budget Buddy?</h3>
            <div id="one" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          <li class="list-group-item" data-toggle="collapse" data-target="#two" data-aos="fade-left">
            <h3>Is Budget Buddy private?</h3>
            <div id="two" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          <li class="list-group-item" data-toggle="collapse" data-target="#three" data-aos="fade-left">
            <h3>What currencies are available?</h3>
            <div id="three" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          <li class="list-group-item" data-toggle="collapse" data-target="#four" data-aos="fade-left">
            <h3>Can I set a password?</h3>
            <div id="four" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          <li class="list-group-item" data-toggle="collapse" data-target="#five" data-aos="fade-left">
            <h3>How much does it cost?</h3>
            <div id="five" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>-->
        </ul>
      </div>
      
      
      <div class="accordion right">
        <ul class="list-group">
          
           @foreach ($faqDataTech as $node)
            <li class="list-group-item" data-target="#technical-{{$node->id}}" >
            <h3>{!!html_entity_decode($node->{'name_'.config('app.locale')})!!}</h3>
            <div id="technical-{{$node->id}}" class="collapse innercontent">
            <p>{!!html_entity_decode($node->{'description_'.config('app.locale')})!!}</p>
            </div>
            </li>
            @endforeach 
          <!--<li class="list-group-item" data-toggle="collapse" data-target="#six" data-aos="fade-left">
            <h3>Are there any ads?</h3>
            <div id="six" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          <li class="list-group-item" data-toggle="collapse" data-target="#seven" data-aos="fade-left">
            <h3>How do you make money?</h3>
            <div id="seven" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          <li class="list-group-item" data-toggle="collapse" data-target="#eight" data-aos="fade-left">
            <h3>Can I save pictures of receipts?</h3>
            <div id="eight" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          
              <li class="list-group-item" data-toggle="collapse" data-target="#eight" data-aos="fade-left">
            <h3>Can I login from my computer?</h3>
            <div id="eight" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>
          
          
           <li class="list-group-item" data-toggle="collapse" data-target="#eight" data-aos="fade-left">
            <h3>Are you available on Android?</h3>
            <div id="eight" class="collapse innercontent">
              <p>simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
            </div>
          </li>-->
          
          
        </ul>
      </div>
      
      
</div>
</div>
</div>
</div>





<div id="ssl-section">
<div class="container">
<div class="ssl-main">
<div class="ssl-left" data-aos="fade-right"><img src="{{asset('images/sslimg.png')}}"></div>
<div class="ssl-right" data-aos="fade-left">
<h2>{!!html_entity_decode(__('home_page.bdg_security'))!!} </h2>
<p>{!!html_entity_decode(__('home_page.bdg_security_desc'))!!} </p>
</div>
</div>
</div>
</div>
     <script type="application/javascript"  src="{{ asset('js/v2owl.carousel.min.js') }}"></script>
     
    
    
    
    <script type="application/javascript" >
	$(document).ready(function() {
		$('body').addClass('home');
		$("#carousel").owlCarousel({
          items:5,
            itemsDesktop:[1024,3],
            slideSpeed: 300,
            center: true,
            paginationSpeed: 400,
            itemsDesktopSmall:[980,3],
            itemsTablet:[768,3],
            itemsmobile:[767,1],
            pagination:true,
            mouseDrag: true,
            autoPlay:true,
			addClassActive:true,
			navigation : true
        });
        $("#carousel-test").owlCarousel({
          items:3,
            itemsDesktop:[1000,3],
            slideSpeed: 300,
            center: true,
            paginationSpeed: 400,
            itemsDesktopSmall:[980,3],
            itemsTablet:[768,3],
            itemsmobile:[767,1],
            pagination:false,
            mouseDrag: true,
            autoPlay:true,
        });
	 });	
    </script>
@endsection
