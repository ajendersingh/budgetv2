@extends('layouts.payment')

@section('content')


<div class="container buymembershipsection">
<script  type="application/javascript">

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
        return true;
    }
};

$(document).ready(function(){
    $('#Card').keypress(validateNumber);
});

</script>
 <link href="{{ asset('css/site.css') }}" rel="stylesheet">
    <div class="col-xl-12 col-lg-12 col-xs-12 col-sm-12 col-md-12 mainouter">
                    
                         <div class="col-sm-7">
                             
                             
                             <div class="visible-xs">
                                {!!html_entity_decode($dataCms2->{'description_'.config('app.locale')})!!}
                             </div>
                             
                             
                             

                            <h1 class="hidden-xs">{{ __('buy_membership.fill_details') }}</h1>
                            @if ($errors->any())

    <div class="alert alert-danger">
 

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif
                             
                               <form  method="post" action="{{ route('subscribe-membership') }}"> 
                                
                               @csrf 
                                     
                                     
                                     

                                     <!--<label for="cc">Name:</label>
                                     <div class="input-icon">
                                        <span class="fa fa-lock"></span>
                                        <input type="text" name="name"  class="text" placeholder="Name" id="name" value="{{ old('name') }}" >
                                     </div>-->
                                   
                                      
                                   
 
                                     <!--<label for="cc">Email Address:</label>
                                     <div class="input-icon">
                                        <span class="fa fa-lock"></span>
                                        <input type="email" name="email"  class="text" placeholder="Email" id="email"  value="{{ old('email') }}">
                                     </div>-->

                                     <label for="cc">{{ __('buy_membership.card_accepted') }}</label>
                                     <img class="cards" src="{{asset('img/cards-secure.png')}}" >
                                     
                                   
                                     <hr class="hidden-xs">
 <label for="cc">{{ __('buy_membership.choose_subscription') }}</label>
                                     <div class="input-icon">
                                        
                                       <select class="custom-select custom-select2 single_media_select" id="product_id_choosed" single_media_selecttype="text" name="product_id">
                                                    <option  value="1" {{ $plan_id == "1" ? "selected" : "" }} >{{ __('buy_membership.plan_data') }}</option>
                                                    <option  value="2" {{ $plan_id == "2" ? "selected" : "" }} >{{ __('buy_membership.plan_data2') }}</option>
                                                     
                                                </select>
                                     </div>
                                      
                                     
                                     <label for="cc">{{ __('buy_membership.card_number') }}</label>
                                     <div class="input-icon">
                                        <span class="fa fa-lock"></span>
                                        <input type="text" name="cc-cardnumber"  maxlength="16" class="text-uppercase" placeholder="0000 0000 0000 0000" id="Card" value="{{ old('cc-cardnumber') }}" >
                                     </div>

                                    <label for="exp">{{ __('buy_membership.expiration_date') }}</label> 
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                             <select class="half custom-select" name="cc-expires-month">
                                              <option value="01" {{ old("cc-expires-month") == "01" ? "selected" : "" }}>01</option>
                                              <option value="02" {{ old("cc-expires-month") == "02" ? "selected" : "" }}>02</option>
                                              <option value="03" {{ old("cc-expires-month") == "03" ? "selected" : "" }}>03</option>
                                              <option value="04" {{ old("cc-expires-month") == "04" ? "selected" : "" }}>04</option>
                                              <option value="05" {{ old("cc-expires-month") == "05" ? "selected" : "" }}>05</option>
                                              <option value="06" {{ old("cc-expires-month") == "06" ? "selected" : "" }}>06</option>
                                              <option value="07" {{ old("cc-expires-month") == "07" ? "selected" : "" }}>07</option>
                                              <option value="08" {{ old("cc-expires-month") == "08" ? "selected" : "" }}>08</option>
                                              <option value="09" {{ old("cc-expires-month") == "09" ? "selected" : "" }}>09</option>
                                              <option value="10" {{ old("cc-expires-month") == "10" ? "selected" : "" }}>10</option>
                                              <option value="11" {{ old("cc-expires-month") == "11" ? "selected" : "" }}>11</option>
                                              <option value="12" {{ old("cc-expires-month") == "12" ? "selected" : "" }}>12</option>
                                            </select>
                                        </div>
                                        
                                         <div class="col-sm-6 col-xs-6">
                                            <select class="custom-select" name="cc-expires-year" >
                                             
                                            @for($i=date('Y');$i<=date('Y')+10;$i++ )
                                             
                                             <option value="{{$i}}" {{ old("cc-expires-year") == "$i" ? "selected" : "" }}>{{$i}}</option>
                                            
                                             @endfor
                                             
                                            </select> 
                                        </div>
                                     </div>
                                     <label for="ccv">{{ __('buy_membership.cvv') }}</label>
                                     <div class="row">
                                         <div class="col-sm-6 col-xs-6">
                                            <div class="input-icon">
                                                <span class="fa fa-lock"></span>
                                                <input type="text" class="text-uppercase half" name="cc-cvv2" id="CVV"  maxlength="3"  size="3" value="" placeholder="XXX" >
                                             </div>
                                         </div>
                                         <div class="col-sm-6 col-xs-6 clearfix">
                                            <img class="help f-left" src="{{asset('img/cvv.png')}}" >
                                             <h6 class="f-left help-txt hidden-sm hidden-xs"><i class="arrow left"></i> {{ __('buy_membership.cvv_details') }}</h6>
                                         </div>
                                     </div> 

                                     <div class="row">
                                        <div class="col-md-12 checkbox">
                                            <input type="checkbox" id="terms" name="terms" value="I accept the terms" data-parsley-required="true" data-parsley-trigger="click" data-parsley-multiple="terms">
                                            <label for="terms" style="font-size: 13px;">
                                             
                                            @if ($plan_id==1)
                                            {!!html_entity_decode(__('buy_membership.term_ext'))!!} 
                                            @endif
                                            @if ($plan_id==2)
                                            {!!html_entity_decode(__('buy_membership.term_ext2'))!!} 
                                            @endif
                                             </label>
                                        </div> 
                                     </div> 

                                     <div class="row">
                                       <div class="col-md-12 checkbox">
                                         <div class="cross-sale-show ">
                                           <label>{{ __('buy_membership.special_offer_text') }}</label>
                                         </div>
                                       </div>
                                     </div>

									 <button type="submit" class="btn btn-primary" style="margin-top: 50px;"><span>{{ __('buy_membership.process_button') }}</span></button>
                                     

                                </form> 
                        </div>
                         
                        <div class="col-sm-5">
                             
                            <div class="box hidden-xs">
                            <img src="{{asset('img/logo-img.jpg')}}" alt="logo" >
                                  @if ($plan_id==2)
                                  {!!html_entity_decode($dataCmsPlan1->{'description_'.config('app.locale')})!!}
                                  @endif
                                  @if ($plan_id==1)
                                  {!!html_entity_decode($dataCmsPlan2->{'description_'.config('app.locale')})!!}
                                  @endif
                            </div>
                            
                            
                            <div class="visible-xs">
                                <img class="img-mobile" src="{{asset('img/logo-img.jpg')}}" alt="logo" >
                                {!!html_entity_decode($dataCms3->{'description_'.config('app.locale')})!!}
                            </div>
                             
                            
                        </div>

                    </div>
    <div class="clear"></div>
</div>
                    <div class="clear"></div>

  <div class="centreclass"><img class="cards desktopimg" src="{{asset('img/company-text.png')}}" alt="" />
<img class="cards mobileimg" src="{{asset('img/company-text-mobile.png')}}" alt="" />
</div>
 
@endsection
