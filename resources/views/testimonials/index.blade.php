@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Testimonials</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('testimonials.create') }}"> Create New Testimonial</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>


            <th>Testimonial (English)</th>
            
            <th>Testimonial (France)</th>
            
            <th>Designation</th>
            <th>Thumbnail</th>

            
            <th>Created Date</th>
            

            <th>Action</th>

        </tr>

        @foreach ($testimonials as $testimonial)

        <tr>


            <td>{{ $testimonial->description_en }}</td>

            <td>{{ $testimonial->description_fr }}</td>
            
            <td>{{ $testimonial->testimonial_location_en }}</td>
            <td>
			 
			@if($testimonial->thumbnail !='')
				<img src="{{asset('public/images/')}}/{{$testimonial->thumbnail}}" style="max-height:50px;" />
			@endif
			</td>
            
            
            
            <td>{{date('m-d-Y', strtotime($testimonial->created_on))}}</td>
            
             

            <td> 

                <form action="{{ route('testimonials.destroy',$testimonial->id) }}" method="POST">
 
   


    

                    <a  class="btn btn-success" href="{{ route('testimonials.edit',$testimonial->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $testimonials->links() !!}

      

@endsection




























