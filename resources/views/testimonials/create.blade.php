@extends('layouts.admin')

  

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Testimonial</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary-edit" href="{{ route('testimonials.index') }}"> Back</a>

        </div>

    </div>

</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('testimonials.store') }}" method="POST" enctype="multipart/form-data">

    @csrf

  

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Client Name:</strong>

                <input type="text" name="testimonial_client_en" class="form-control" placeholder="Client Name" value="{{ old('testimonial_client_en') }}">

            </div>

        </div>
        
         <div class="form-group">
			<label style="color: black;" >Thumbnail</label>
			<br>
			{!! Form::file('image', array('class' => 'image')) !!}
		</div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Client Designation:</strong>

                <input type="text" name="testimonial_location_en" class="form-control" placeholder="Client Designation" value="{{ old('testimonial_location_en') }}">

            </div>

        </div>
        
         
        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (English):</strong>

                <textarea name="description_en" id="description_en" style="width:100%">{{ old('description_en') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (France):</strong>

                <textarea name="description_fr" id="description_fr" style="width:100%">{{ old('description_fr') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Netherlands):</strong>

                <textarea name="description_nl" id="description_nl" style="width:100%">{{ old('description_nl') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Denmark):</strong>

                <textarea name="description_dk" id="description_dk" style="width:100%">{{ old('description_dk') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Austria):</strong>

                <textarea name="description_at" id="description_at" style="width:100%">{{ old('description_at') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Germany):</strong>

                <textarea name="description_de" id="description_de" style="width:100%">{{ old('description_de') }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Sweden):</strong>

                <textarea name="description_se" id="description_se" style="width:100%">{{ old('description_se') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Norway):</strong>

                <textarea name="description_no" id="description_no" style="width:100%">{{ old('description_no') }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Italian):</strong>

                <textarea name="description_it" id="description_it" style="width:100%">{{ old('description_it') }}</textarea>

            </div>

        </div>
        
        
        
        
         
        
         
        
         
        
         

         
        
         

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

        </div>

    </div>

   

</form>

@endsection
