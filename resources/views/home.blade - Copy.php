@extends('layouts.app')

@section('content')
<div class="home-section">

<!-----Home Banner--------->
<div class="home-banner">
<div class="body-container">
<div class="section" data-aos="fade-in">
<h1>{!!html_entity_decode(__('home_page.meet_next'))!!}</h1>
{!!html_entity_decode(__('home_page.meet_next_description'))!!}
</div>
</div>
</div>
<!-----Home Banner--------->


<div class="home-login">
<div class="login-container">
                            <form method="POST" action="{{ route('login') }}">
                            @csrf
                            
                            <div class="homelogin-left">
                            <div class="home-form-group">
                            
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror                            <input id="email" type="email" placeholder="{!!html_entity_decode(__('home_page.email'))!!}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" >
                            
                            </div>
                            
                            <div class="home-form-group">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <input id="password" type="password" placeholder="{!!html_entity_decode(__('home_page.password'))!!}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                            
                            
                            </div>
                            
                            
                            <div class="home-form-group">
                            <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember">
                            {!!html_entity_decode(__('home_page.remember_me'))!!}
                            </label>
                            </div>
                            </div>
                            
                            <div class="home-form-group">
                            @if (Route::has('password.request'))
                            <a class="btn btn-links" href="{{ route('password.request') }}">
                            {!!html_entity_decode(__('home_page.forgot_password'))!!}
                            </a>
                            @endif
                            </div>
                            </div>
                            
                            <div class="homelogin-button">
                            <button type="submit" class="btn btn-primary">
                            <span class="">{!!html_entity_decode(__('home_page.login_btn'))!!}</span>
                            
                            </button>
                            </div>
                            
                            </form>
</div>
</div>




<!-----Home Collection--------->

<div class="home-collection">
<div class="body-container">
<div class="home-collection-text" data-aos="fade-up">
<h2>{!!html_entity_decode(__('home_page.your_public_title'))!!}</h2>
<p>{!!html_entity_decode(__('home_page.your_public_description'))!!}</p>
</div>
</div>


<div class="home-collection-slider" data-aos="fade-up">
<div class="body-container">
<div class="home-collection-slider-img">
 <div id="carousel">
      <a href="#"><img src="{{asset('img/proslide.png')}}" id="item-1" /></a>
      <a href="#"><img src="{{asset('img/proslide2.png')}}" id="item-2" /></a>
      <a href="#"><img src="{{asset	('img/proslide3.png')}}" id="item-3" /></a>
      <a href="#"><img src="{{asset('img/proslide4.png')}}" id="item-4" /></a>
      <a href="#"><img src="{{asset('img/proslide5.png')}}" id="item-5" /></a>
      <a href="#"><img src="{{asset('img/proslide6.png')}}" id="item-6" /></a>
	 <a href="#"><img src="{{asset('img/proslide3.png')}}" id="item-7" /></a>
      <a href="#"><img src="{{asset('img/proslide2.png')}}" id="item-8" /></a>
      <a href="#"><img src="{{asset('img/proslide5.png')}}" id="item-9" /></a>
    </div>
</div>
<a href="{{ env('APP_URL') }}bookcategory/1" class="btn-default" data-aos="fade-up">{!!html_entity_decode(__('home_page.browse_collection'))!!}</a>
</div>
</div>



</div>

<!-------------->
 
<div class="home-readers">
<div class="home-readers-section">
<div class="body-container">
<ul id="counter">
<li><h3 class="counter-value" data-count="26315">0</h3><span>{!!html_entity_decode(__('home_page.book_available'))!!}</span></li>
<li><h3 class="counter-value" data-count="1068">0</h3><span>{!!html_entity_decode(__('home_page.happy_readers'))!!}</span></li>
<li><h3 class="counter-value" data-count="153">0</h3><span>{!!html_entity_decode(__('home_page.book_categories'))!!}</span></li>
<li><h3 class="counter-value" data-count="10">0</h3><span>{!!html_entity_decode(__('home_page.add_per_week'))!!}</span></li>
</ul>
</div>
</div>
</div>

<!-------------->

<div class="home-discover">
<div class="body-container">
<h1>{!!html_entity_decode(__('home_page.discover'))!!}</h1>
<div class="home-discover-row"  data-aos="fade-left"><span></span>
<div class="home-discover-row-icon"><img src="{{asset('img/convienant-icon.png')}}" alt=""/></div>
<div class="home-discover-row-title">{!!html_entity_decode(__('home_page.discover_slide1_title'))!!}</div>
<div class="home-discover-row-desc">{!!html_entity_decode(__('home_page.discover_slide1_description'))!!}</div>

</div>

<div class="home-discover-row margincenter" data-aos="fade-up"><span></span>
<div class="home-discover-row-icon"><img src="{{asset('img/savings-icon.png')}}" alt=""/></div>
<div class="home-discover-row-title">{!!html_entity_decode(__('home_page.discover_slide2_title'))!!}</div>
<div class="home-discover-row-desc">{!!html_entity_decode(__('home_page.discover_slide2_description'))!!}</div>

</div>

<div class="home-discover-row" data-aos="fade-right"><span></span>
<div class="home-discover-row-icon"><img src="{{asset('img/anywhere-icon.png')}}" alt=""/></div>
<div class="home-discover-row-title">{!!html_entity_decode(__('home_page.discover_slide3_title'))!!}</div>
<div class="home-discover-row-desc">{!!html_entity_decode(__('home_page.discover_slide3_description'))!!}</div>

</div> 
</div>
 
</div>

<!------------->

<div class="home-benefits">
<div class="body-container">
<div class="home-benefits-section">
<h1>{!!html_entity_decode(__('home_page.great_benefits'))!!}</h1>

<div class="home-benefits-row">
<div class="home-benefits-col mental"   data-aos="fade-left">
<h3>{!!html_entity_decode(__('home_page.mental_simulation'))!!} </h3>
<p>{!!html_entity_decode(__('home_page.mental_simulation_description'))!!}</p>
</div>


<div class="home-benefits-col stress"  data-aos="fade-left">
<h3>{!!html_entity_decode(__('home_page.reduce_stress'))!!}</h3>
<p>{!!html_entity_decode(__('home_page.reduce_stress_description'))!!}</p>
</div>


<div class="home-benefits-col expansion" data-aos="fade-left">
<h3>{!!html_entity_decode(__('home_page.vocabulary_expansion'))!!}</h3>
<p>{!!html_entity_decode(__('home_page.vocabulary_expansion_description'))!!}</p>
</div>

<a href="{{ url('/benefits') }}" class="seemore" data-aos="fade-up">{!!html_entity_decode(__('home_page.see_more_benefits'))!!}</a>
</div>


</div>
</div>

<div class="home-discover-mobiimage">
<img src="{{asset('img/benefits-bg-mobi.png')}}" alt=""/>
</div>

</div>

<!------------->

 	
<div class="home-testimonial">
    <div class="body-container">
    <div class="section">
      
     <div class="testimonialslider owl-carousel owl-theme" data-aos="fade-out">
              
              
              @foreach ($testimonials as $node)
              <div class="item" data-dot="<button>01</button>">
                 {!!html_entity_decode($node->{'description_'.config('app.locale')})!!}
                <h5>{!!html_entity_decode($node->testimonial_client_en)!!}<br />{!!html_entity_decode($node->testimonial_location_en)!!}</h5>
              </div>
              @endforeach
              
                
              
            </div> 
            </div>
    
            
    </div>
</div>



<!-------->

<!--<div class="home-subcribe" data-aos="fade-up">
<div class="body-container">
<div class="home-subcribe-left">
<h1>Stay Always In Touch!</h1>
<p>Subscribe to our newsletter and get exclusive deals you wont find
anywhere else straight to your inbox!</p>
</div>


<div class="home-subcribe-right">
<div class="home-subcribe-right-input">
<input type="text" name="" value="" placeholder="Your E-mail Address" />
<button type="submit">Subscribe</button>
</div>
</div>
</div>
</div>-->
 
<script type="application/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" defer="defer" ></script>
<script type="application/javascript"  src="{{ asset('js/custom.js') }}" defer="defer"></script>

</div>

  

@endsection
