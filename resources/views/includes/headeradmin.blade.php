<div class="container">
    <div class="logo">
        <!--<a href="{{ url('') }}"><img src="{{asset('img/budget-pay-logo.png')}}" alt="logo"/></a>-->
        
        	BudgetPay
    </div>
                
                 
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar --> 
                    <ul class="navbar-nav ml-auto">
                    	<li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/home') }}">Dashboard</a>
                        </li>
                    	<li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/users') }}">Prospects</a>
                        </li> 
                        
                       <!--<li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/categories') }}">Category</a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/testimonials') }}">Testimonials</a>
                        </li>
                        <!--<li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/translations') }}">Language Manager</a>
                        </li>-->
                        
                        <!--<li class="nav-item"> 
                            <a class="nav-link" href="{{ url('/admin/tags') }}">Tags</a>
                        </li>-->
                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/active/subscribers') }}">Subscribers</a>
                        </li>
                        
                        <!--<li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/ebooks') }}">Ebooks</a>
                        </li>-->
                        
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/contents') }}">Cms</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/seos') }}">Seo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/faqs') }}">Faq</a>
                        </li>
						
						<li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/features') }}">Features</a>
                        </li>
                         
                                
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item highlight">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('messages.login_link') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <!---<a class="nav-link" href="{{ route('register') }}">{{ __('messages.register_link') }}</a>--->
                                </li>
                            @endif
                        @else
                        <li class="nav-item">
                            <a  class="nav-link  highlight" href="javascript:void(0);" role="button" >
                                    {{ Auth::user()->email }} <span class="caret"></span>
                                </a>
                        </li>
                        <li class="nav-item">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('messages.logout_link') }}
                                    </a>

                                   <form id="logout-form" action="{{ 'App\Admin' == Auth::getProvider()->getModel() ? route('admin.logout') : route('logout') }}" method="POST" style="display: none;">
                                         	{{ csrf_field() }}
                                    </form>
                        </li>
                             
                        @endguest
                    </ul>
                </div>
            </div>

<!--<div class="navbar">
    <div class="navbar-inner">
        
        <ul class="nav">
            <li><a href="/">Home</a></li>
            @if(isset(Auth::user()->email))
            <li><a href="/login">Welcome {{Auth::user()->email}}</a> &nbsp; <a href="{{url('/main/logout')}}">Logout</a></li>
            <li><a href="budgets">Budget</a></li>
            <li><a href="incomes">Income</a></li>
            <li><a href="expenses">Expenses</a></li>
            @else
 			Welcome Guest
            <li><a href="register">Register</a></li>
            @endif
         </ul>
    </div>
</div>-->