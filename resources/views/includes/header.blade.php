<!--<div class="container">
<div class="logo">
  <a href="{{ url('') }}"><img src="{{asset('img/logo.png')}}" alt="logo"/></a>
</div>
<div class="right-nav">
@guest
    <div class="sm-highlight"><a href="{{ route('login') }}">{{ __('signup_login') }}</a></div>
	@endguest
    <div class='menu-button'><span></span><span></span><span></span></div>
    <ul class="menu">
      <li><a href="{{ url('/benefits') }}">{{ __('Header.features_link') }}</a></li>
      <li><a href="{{ url('/about') }}">{{ __('Header.about_budgetpay') }} </a></li>
      <li><a href="{{ url('/help') }}">{{ __('Header.the_software') }}</a></li>
      <li><a href="{{ url('/cancel-account') }}">{{ __('Header.support_link') }}</a></li>
      
	  
	  @guest
                             <li class="nav-item logout" class="highlight">
                                <a class="nav-link hvr-sweep-to-left" href="{{ url('/pricing') }}">{{ __('Header.signup_link') }}</a>
                            </li>
                            
                            <li class="nav-item login" class="highlight">
                                <a class="nav-link hvr-sweep-to-right" href="{{ route('login') }}">{{ __('Header.login_link') }}</a>
                            </li>
                            
                            
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    
                                </li>
								
                            @endif
                        @else
                            <li class="nav-item dropdown ">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle highlight" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('') }}/dashboard" >
                                        
                                        {{ __('Header.dashboard') }}
                                        
                                    </a> 
                                	<a class="dropdown-item" href="{{ route('changePassword') }}" >
                                        
                                        {{ __('Header.change_password') }}
                                        
                                    </a> 
                                    
                                    <a class="dropdown-item" href="{{ url('') }}/view-profile" >
                                        
                                        {{ __('Header.update_profile') }}
                                        
                                    </a> 
                                     
                                    <a class="dropdown-item" href="{{URL('logout')}}">
                                        {{ __('Header.logout_link') }}
                                    </a>

                                     
                                </div>
                            </li>
                        @endguest
						
						
    </ul>
</div>
<div class="clear"></div>
</div>
-->


 
   <section class="headersection">

   	<div class="overlay"></div>


   	<div class="header-logo">
	      <a href="{{ url('') }}"><img src="{{asset('images/logo.png')}}" alt="logo"></a></div> 

<a id="header-menu-trigger" href="javascript:void(0)">
	<span class="header-menu-icon"></span>
</a>
 

@guest
<a href="{{ route('login') }}" class="loginmainbtn">{{ __('Header.login_link') }}</a>
<a href="{{ url('/pricing') }}" class="loginbuton">{{ __('Header.signup_link') }}</a> 
@endguest
@auth
 <div class="logindrop">
 <ul class="nav-list">
<li class="nav-item dropdown ">
<a id="navbarDropdown" class="nav-link dropdown-toggle highlight" href="javascript:void(0);" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
	<span class="user-name">{{ Auth::user()->name }}</span> <span class="caret"></span>
</a>

<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
<a class="dropdown-item" href="{{ url('') }}/dashboard" >
		
		{{ __('Header.dashboard') }}
		
	</a> 
	<a class="dropdown-item" href="{{ route('changePassword') }}" >
		
		{{ __('Header.change_password') }}
		
	</a> 
	
	<a class="dropdown-item" href="{{ url('') }}/view-profile" >
		
		{{ __('Header.update_profile') }}
		
	</a> 
	 
	<a class="dropdown-item" href="{{URL('logout')}}">
		{{ __('Header.logout_link') }}
	</a>

	 
</div>
</li>
</ul> 
</div>
@endauth

		<nav id="menu-nav-wrap">
			<a href="javascript:void(0)" class="close-button" title="close"><span>X</span></a>	

			<ul class="nav-list">
		 
	  
	  
				<li class="current">
				<a class="smoothscroll" href="{{ url('') }}" title="">{{ __('Header.home_link') }}</a>
				</li>
				<li><a class="smoothscroll" href="{{ url('/benefits') }}" title="">{{ __('Header.features_link') }}</a></li>
				<li><a class="smoothscroll" href="{{ url('/about') }}" title="">{{ __('Header.about_budgetpay') }}</a></li>
				<li><a class="smoothscroll" href="{{ url('/pricing') }}" title="">{{ __('Header.pricing_link') }}</a></li>
				<li><a class="smoothscroll" href="{{ url('/cancel-account') }}" title="">{{ __('Header.support_link') }}</a></li>
				 					
				@guest
				<li style="display:none;">
                      <a class="smoothscroll" href="{{ url('/pricing') }}">{{ __('Header.signup_link') }}</a>
				</li>
				
				<li style="display:none;">
					<a class="smoothscroll" href="{{ route('login') }}">{{ __('Header.login_link') }}</a>
				</li>
				 @endguest		
			</ul>		
 
					
		</nav>  <!-- end #menu-nav-wrap -->
 <!-- end header --> 
 <div class="bannerdiv">
 
 <div class="home-content-table">	
		   <div class="home-content-tablecell" data-aos="fade-in">
            <p>{{ __('home_page.top_p1') }} </p>
		    <h1>{{ __('home_page.top_h1') }}</h1>
           <a href="{{ url('/pricing') }}" class="getstred">{{ __('home_page.bdg_get_start') }}</a>

		   	</div> <!-- end row --> 
		   </div>
 
           </div>
           
 </section>
 		   
           

<div class="clear" style="clear:both;"></div>

 
 