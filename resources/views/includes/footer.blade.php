
<div id="footersection">
<div class="container">
<div class="footermian">
<div class="footer-left">
<a href="{{ url('') }}"><img src="{{asset('images/footerlogo.png')}}"></a>
<p>{{ __('footer.footer_text') }}</p>
</div>
<div class="footer-right">
<div class="footer-right-col">
<ul>
<li><a href="{{ url('/benefits') }}">{{ __('Header.features') }}</a></li>
<li><a href="{{ url('/about') }}">{{ __('Header.about_budgetpay') }}</a></li>
<!---<li><a href="{{ url('/help') }}">{{ __('Header.the_software') }}</a></li>
<li><a href="{{ url('/cancel-account') }}">{{ __('Header.support_link') }}</a></li>---->
</ul>
</div>


<div class="footer-right-col">
<ul>
<li><a href="{{ url('/privacy-policy') }}">{{ __('footer.privacy_link') }}</a></li>
<li><a href="{{ url('/cancel-account') }}">{{ __('footer.news_room') }} </a></li>
<!---<li><a href="{{ url('/testimonials') }}">{{ __('footer.testimonials') }}</a></li>
<li><a href="{{ url('/cancel-account') }}">{{ __('footer.faq') }}</a></li>-->
</ul>
</div>

<div class="footer-right-col">
<p>{{__('footer.copyright')}} © BudgetBuddy {{ date("Y")}}. <br />{{__('footer.rights_reserved')}}</p>
</div>


</div>
</div>


<div class="footermian-copy">
<img src="{{asset('images/footercopyrightv2.png')}}" class="deskimg">
<img src="{{asset('images/footercopyrightv2-mobi.png')}}" class="mobimg">
<p style="display:none">{{ __('footer.footer_address') }} </p>
</div>

</div>
</div>



<!--



    <div class="container">
        <div class="footer-left">
            <a href="#" class="footer-logo"><img src="{{asset('img/budget-pay-logo.png')}}"></a>
            <ul>
                <li><a href="{{ url('/benefits') }}">{{ __('Header.features') }}</a></li>
                <li><a href="{{ url('/about') }}">{{ __('Header.about_budgetpay') }}</a></li>
                <li><a href="{{ url('/help') }}">{{ __('Header.the_software') }}</a></li>
               
                <li><a href="{{ url('/contact') }}">{{ __('Header.cancel_account') }}</a></li>
            </ul>
        </div>
            <div class="footer-right">
                <p>{{__('footer.copyright')}} © BudgetBuddy {{ date("Y")}}. {{__('footer.rights_reserved')}}.</p> 
            </div>
            <div class="clear"></div>
            <div class="centreclass"><img src="{{asset('img/company-text.png')}}" alt="" class="cards desktopimg"> <img src="{{asset('img/company-text-mobile.png')}}" alt="" class="cards mobileimg"></div>
    </div>-->
  
