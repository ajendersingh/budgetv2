@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Ebook</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('ebooks.create') }}"> Create New Ebook</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    
     @inject('category', 'App\Categories')

   

    <table class="table table-bordered">

        <tr>


            <th>Book Name (English)</th>
            
            <th>Book Name (France)</th>
            
            <th>Book Name (Netherlands)</th>
             
            <th>Category </th>
            
            <th>Featured </th>
             
            <th>File Name</th>
            
            <th>Cover Image</th>
            
            <th>Created Date</th>
            

            <th width="15%">Action</th>

        </tr>

        @foreach ($ebooks as $ebook)

        <tr>


            <td>{{ $ebook->book_name_en }}</td>
            
            <td>{{ $ebook->book_name_fr }}</td>
            
            <td>{{ $ebook->book_name_nl }}</td>
             
            <td>
            
             
             @php
             $url2 = url('/');
 			 $data=$category->getBookCat($ebook->id);
             $data_val=array();
             foreach ($data as $val)
             
             {
             $data_val[]=$val->category_id;
             }
             $total_books_cat='';
             if(count($data_val)>0)
			 {
             $total_books_cat=implode(",",$data_val);
             $data_cat=$category->getBooksOfCat($total_books_cat);
             $book_name="";
                 if(count($data_cat)>0)
                 {
                 $book_name=implode(",",$data_cat);
                 echo $book_name;
                 }
             
             }
              
           	   
			 @endphp
            
            
            </td>
            
            <td>
             @if ($ebook->featured == "1")
             Featured  
     		 @endif 
            </td>

            <td><a href="{{ env('APP_URL') }}/public/pdffiles/{{ $ebook->pdf_file }}" target="_blank"><img src="{{ env('APP_URL') }}/img/pdf.png" width="50" /></a></td>
            
            <td><img src="{{ env('APP_URL') }}/public/pdfimages/{{ $ebook->cover_image }}" width="50"  /></td>
            
             
            
            
            <td>{{date('m-d-Y', strtotime($ebook->created_on))}}</td>
            
            

            <td>

                <form action="{{ route('ebooks.destroy',$ebook->id) }}" method="POST">

   


    

                    <a  class="btn btn-success" href="{{ route('ebooks.edit',$ebook->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $ebooks->links() !!}

      

@endsection




























