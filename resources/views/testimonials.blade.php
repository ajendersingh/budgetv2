@extends('layouts.app')

@section('content')
<div class="inner-container">
<div class="titlemain"><h1> {!!html_entity_decode(__('common.testimonials'))!!}</h1></div>
 


<div class="basicmain">
 
<ul class="testimonialspage">

 @foreach ($testimonials as $node)
 
 
<li>
	<p>{!!html_entity_decode(substr($node->{'description_'.config('app.locale')}, 0, 85) )!!}</p>
	<div class="slidename">
		<div class="img">
		@if($node->thumbnail !='')
				<img src="{{asset('public/images/')}}/{{$node->thumbnail}}" style="max-height:50px;" />
		@else
			<img src="{{asset('images/slideimg.png')}}">			
		@endif
		
		</div>
		<div class="name"><span><strong>{!!html_entity_decode($node->testimonial_client_en)!!}</strong>{!!html_entity_decode($node->testimonial_location_en)!!}</span></div>
	</div>
</li> 
 
 
  @endforeach
  
 </ul> 
</div>

</div>
@endsection
