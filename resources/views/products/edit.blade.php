@extends('layouts.admin')

   

@section('content')

    <div class="row">
   	 	

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Update  Product Tracking</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ env('APP_URL') }}admin/products/tracking/{{$product_id}}"> Back</a>

            </div>

        </div>

    </div>

   

    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    <form action="{{ action('ProductController@update') }}"   method="POST" >

        @csrf

        @method('PUT')

   

         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Product Name:</strong>

 					{{$trackings->post_title}}
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Colour:</strong>

                    <input type="text" name="colour" id="colour" value="" class="form-control" placeholder="Colour">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Size:</strong>

                    <input type="text" name="size" id="size" value="" class="form-control" placeholder="Size">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Purchaser:</strong>

                    <input type="text" name="purchaser" id="purchaser" value="" class="form-control" placeholder="Purchaser">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12" style="width:200px;">

                <div class="form-group" style="width:200px;">

                    <strong>Date Purchased:</strong>

                    <input type="date" name="date_purchased" id="date_purchased" value="" class="form-control" placeholder="Date Purchased">

                </div>

            </div>
            
		
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Buyers Location:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Buyers Location">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Buyers Name:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Buyers Name">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Country of Origin:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Country of Origin">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Date Product was Produced:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Date Product was Produced">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Date Product arrived in UK:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Date Product arrived in UK">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Shelf-Life / Expected Lifespan:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Shelf-Life / Expected Lifespan">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Materials Used + link to certificates:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Materials Used + link to certificates">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Staff Involved in Production:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Staff Involved in Production">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Factory Details:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Factory Details">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Time to Produce 1 piece:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Time to Produce 1 piece">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Raw Material Suppliers:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Raw Material Suppliers">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Country it was Designed:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Country it was Designed">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Machins Involved in Production:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Machins Involved in Production">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Manufacturing Process:</strong>

                    <input type="text" name="name" value="" class="form-control" placeholder="Manufacturing Process">

                </div>

            </div>

             

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-success" style="height: 40px; float:left">Submit</button>

            </div>

        </div>
	  
   

    </form>

  

     

@endsection
