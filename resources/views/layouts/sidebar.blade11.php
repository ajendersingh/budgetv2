<a href="javascript:void(0)" id="show_nav">show</a>
<a href="javascript:void(0)" id="hide_nav">hide</a>
<ul class="show_hide_cat_div hideCatClass" id="side_nav_ul">
  <li class='{{$active_class == 'dashboard' ? 'active': ''}}'><a href="{{ env('APP_URL') }}dashboard/">Dashboard</a> </li>
  <li class='{{$active_class == 'accounts' ? 'active': ''}}'><a href="{{ env('APP_URL') }}accounts/">Accounts </a> </li>
  <li style="display:none;" class='{{$active_class == 'income' ? 'active': ''}}' ><a href="{{ env('APP_URL') }}income/">Income </a> </li>
  <li class='{{$active_class == 'liabilities' ? 'active': ''}}' ><a href="{{ env('APP_URL') }}liabilities/">Liabilities </a> </li>
  <li class='{{$active_class == 'budgets' ? 'active': ''}}'><a href="{{ env('APP_URL') }}budgets/">Budgets </a> </li>
  <li class='{{$active_class == 'categories' ? 'active': ''}}' ><a href="{{ env('APP_URL') }}categories/">Categories </a> </li>
  <li class='{{$active_class == 'transactions' ? 'active': ''}}'><a href="{{ env('APP_URL') }}transactions/">Transactions </a> </li>
</ul> 
 
