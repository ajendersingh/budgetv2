<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(!empty($seoData->page_name))
	<meta name="keywords" content="{!!html_entity_decode($seoData->{'meta_title_'.config('app.locale')})!!}">
	@endif
    
    @if(!empty($seoData->page_name))
	<meta name="description" content="{!!html_entity_decode($seoData->{'meta_description_'.config('app.locale')})!!}">
	@endif
    
    @if(request()->route()->getName() == 'home')
     
    @else
    <meta name="robots" content="noindex, nofollow, noarchive"> 
    @endif 
    

    <title>{{ config('app.name', 'Laravel') }}@if(!empty($seoData->page_name))-{!!html_entity_decode($seoData->{'page_title_'.config('app.locale')})!!} @endif</title>

    <!-- Scripts -->
		<script type="application/javascript" src="https://www.google.com/recaptcha/api.js"></script>
        
        <script type="application/javascript" src="{{ asset('js/aos.js') }}" defer></script>
        <style type="text/css" media="screen">
          html, body	{ height:100%; }
          body { margin:0; padding:0; overflow:auto; }
          #flashContent { display:none; }
          a { color:#aaaaaa}
        </style>
            <link rel="dns-prefetch" href="//fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
            
            <!-- Styles -->
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
            <link href="{{ asset('css/media.css') }}" rel="stylesheet">
            
            <!-- Hover Style -->
            <link href="{{ asset('css/hover.css') }}" rel="stylesheet">
            <link href="{{ asset('css/aos.css') }}" rel="stylesheet">

    		<link rel="stylesheet" type="text/css" href="{{ asset('css/flowpaper.css') }}" />
			<script type="text/javascript">
            var site_url_js_path="{{ env('APP_URL') }}";
            
            </script>
            
             <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    		<script type="text/javascript" src="{{ asset('js/jquery.extensions.min.js') }}"></script>
    			<!--[if gte IE 10 | !IE ]><!-->
    			<script type="text/javascript" src="{{ asset('js/three.min.js') }}"></script>
    			<!--<![endif]-->
    		<script type="text/javascript" src="{{ asset('js/flowpaper.js') }}"></script>
    		<script type="text/javascript" src="{{ asset('js/flowpaper_handlers.js') }}"></script>
      </head>
      <body>
		<div id="app">
        <nav class="navbar navbar-expand-md navbar-light">
             @include('includes.header')
        </nav>
        <div id="documentViewer" class="flowpaper_viewer" style="position:relative;width:100%;height:100%;"></div>
         <script type="text/javascript">

          $('#documentViewer').FlowPaperViewer(
                         { config : {

                           IMGFiles : ''+site_url_js_path+'/public/pdfhtml5images/{{$swf_file_name}}-{page}.jpg',
                           JSONFile : ''+site_url_js_path+'/public/pdfjson/{{$swf_file_name}}.js',
                           FontsToLoad : ["g_font_54","g_font_55","g_font_56","g_font_57","g_font_58","g_font_59","g_font_60","g_font_61","g_font_62","g_font_63","g_font_64"],
                           
                           Scale : 0.8,
                           ZoomTransition : 'easeOut',
                           ZoomTime : 0.5,
                           ZoomInterval : 0.1,
                           FitPageOnLoad : true,
                           FitWidthOnLoad : false,
                           AutoAdjustPrintSize : true,
                           PrintPaperAsBitmap : false,
                           FullScreenAsMaxWindow : false,
                           ProgressiveLoading : false,
                           MinZoomSize : 0.3,
                           MaxZoomSize : 5,
                           SearchMatchAll : true,
                           InitViewMode : 'Zine',
                           RenderingOrder : 'html5,flash',
                           StartAtPage : 1,
						   PrintEnabled: false,
						   PrintToolsVisible: false,	
                           PreviewMode : true,
                           MixedMode : true,
                           EnableWebGL : true,
                           ViewModeToolsVisible : true,
                           ZoomToolsVisible : true,
                           NavToolsVisible : true,
                           CursorToolsVisible : true,
                           SearchToolsVisible : true,
                           jsDirectory : ''+site_url_js_path+'js/',
                           cssDirectory : ''+site_url_js_path+'css/',
						   localeDirectory : ''+site_url_js_path+'locale/',
                           WMode : 'transparent',
						   key : '$7b133916524c8c15b1f',
                           localeChain: 'en_US'
                         }}
                  );
          </script>
          <footer>
         	@include('includes.footer')
         </footer>
          </div>
          <script type="text/javascript">
				$("#navbarDropdown").click(function(){
				
				
					var x = document.getElementById("navbarDropdown").getAttribute("aria-expanded"); 
					if (x == "true") 
					{
					x = "false";
					$('.dropdown-menu-right').removeClass('show');
					} 
					else 
					{
					x = "true";
					$('.dropdown-menu-right').addClass('show');
					}
				 	document.getElementById("navbarDropdown").setAttribute("aria-expanded", x);
				 
				}); 
				$('.navbar-toggler').on('click',function() {
 				var fields = document.getElementsByClassName('navbar-toggler');
				var result = '';
				for (var i = 0; i < fields.length; i ++) {
				var val = fields[i].getAttribute('aria-expanded');
				result= val;
				}
					if (result == "true") 
					{
					result = "false";
					$('#navbarSupportedContent').removeClass('show');
					} 
					else 
					{
					result = "true";
					$('#navbarSupportedContent').addClass('show'); 
					}
					var fields1 = document.getElementsByClassName('navbar-toggler');
					for (var j = 0; j < fields1.length; j ++) {
					 fields1[j].setAttribute('aria-expanded',result);
 					}
					document.getElementsByClassName("navbar-toggler").setAttribute("aria-expanded", result);	
				}); 
			</script>
</body>
  
 
</html>
