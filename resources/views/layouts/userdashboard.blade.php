<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(!empty($seoData->page_name))
	<meta name="keywords" content="{!!html_entity_decode($seoData->{'meta_title_'.config('app.locale')})!!}">
	@endif
    
    @if(!empty($seoData->page_name))
	<meta name="description" content="{!!html_entity_decode($seoData->{'meta_description_'.config('app.locale')})!!}">
	@endif
    
    @if(request()->route()->getName() == 'home')
     
    @else
    <meta name="robots" content="noindex, nofollow, noarchive"> 
    @endif 
    

    <title>{{ config('app.name', 'Laravel') }}@if(!empty($seoData->page_name))-{!!html_entity_decode($seoData->{'page_title_'.config('app.locale')})!!} @endif</title>

    <!-- Scripts -->
<script type="application/javascript" src="{{ asset('js/app.js') }}" defer></script>
<script type="application/javascript" src="https://www.google.com/recaptcha/api.js"></script>

<script type="application/javascript" src="{{ asset('js/aos.js') }}" defer></script>
 
 
    

    <!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css?019') }}" rel="stylesheet">
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('css/media.css') }}" rel="stylesheet">

<!-- Hover Style -->
<link href="{{ asset('css/hover.css') }}" rel="stylesheet">
<link href="{{ asset('css/aos.css') }}" rel="stylesheet">
     
     
    

<script type="text/javascript">
var site_url_js_path="{{ env('APP_URL') }}";

</script>
<link rel="stylesheet" type="text/css" href="{{ asset('css/flowpaper.css') }}" />
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="application/javascript" src="{{ asset('js/flowpaper.js') }}"></script>
<script type="application/javascript" src="{{ asset('js/flowpaper_handlers.js') }}"></script>
<script type="application/javascript" src="{{ asset('js/transactions.js?912129') }}"></script>
<script type="application/javascript" >
$(window).bind('orientationchange', function (event) {
    location.reload(true);
});

</script>

<link rel="stylesheet" media="all" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css"/>
<link rel="stylesheet" media="all" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css"/>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
  
  
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.js"></script>
 
 
 
 
 	 
   
</head>
<body>
<input type="text" autofocus="autofocus" style="display:none" />
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light">
             @include('includes.header')
        </nav>

        
            @yield('content')
              
         <footer>
         	@include('includes.footer')
         </footer>
    </div>
	
	<script>
 
</script>


</body>
</html>
