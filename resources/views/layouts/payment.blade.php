<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"/>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(!empty($seoData->page_name))
	<meta name="keywords" content="{!!html_entity_decode($seoData->{'meta_title_'.config('app.locale')})!!}">
	@endif
    
    @if(!empty($seoData->page_name))
	<meta name="description" content="{!!html_entity_decode($seoData->{'meta_description_'.config('app.locale')})!!}">
	@endif
    
    @if(request()->route()->getName() == 'home' || request()->route()->getName() == 'cancel-account')
     
    @else
     <meta name="robots" content="noindex, nofollow, noarchive, noodp, nosnippet, noimageindex, noydir" />
    @endif 
    

    <title>{{ config('app.name', 'Laravel') }}@if(!empty($seoData->page_name))-{!!html_entity_decode($seoData->{'page_title_'.config('app.locale')})!!} @endif</title>

    <!-- Scripts -->
<script type="application/javascript" src="{{ asset('js/app.js') }}" defer></script>
<script type="application/javascript" src="https://www.google.com/recaptcha/api.js"></script>

<script type="application/javascript" src="{{ asset('js/aos.js') }}" defer></script>
 
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Cabin:wght@400;500;700&display=swap" rel="stylesheet"> 
    

    <!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
<link href="{{ asset('css/payment.css') }}" rel="stylesheet">

<!-- Hover Style -->
     
     
    

<script type="text/javascript">
var site_url_js_path="{{ env('APP_URL') }}";

</script>
<script type="application/javascript" src="{{ asset('js/jquery.min.js') }}"></script>


   
</head>
<body>
<input type="text" autofocus style="display:none" />
    <div id="app">
         
             @include('includes.payheader')
         

        
            @yield('content')
              
<footer>
            <div class="centreclass">
            <img src="{{asset('images/company-text-b.png')}}" class="cards desktopimg">
            <img src="{{asset('images/company-text-mobile-b.png')}}" class="cards mobileimg">
            </div>
			<!---<p>{!!html_entity_decode(__('buy_membership.footer_text'))!!} </p>--->
		  </footer>


    </div>
</body>
</html>
