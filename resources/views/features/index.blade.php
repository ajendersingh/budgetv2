@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Feature's</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('features.create') }}"> Create New Feature</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>
			
            <!--<th>Type</th>-->

            <th>Name (English)</th>
            
            <th>Name (France)</th>
            
            <th>Name (Netherlands)</th>

            
            <th>Created Date</th>
            

            <th>Action</th>

        </tr>

        @foreach ($features as $feature)

        <tr>

			<!--<td>{{$feature->feature_type == 'feature' ? 'Faq': 'Technical Problems'}}</td>-->
            
            <td>{{ $feature->name_en }}</td>

            <td>{{ $feature->name_fr }}</td>
            
            <td>{{ $feature->name_nl }}</td>
            
            
            
            <td>{{date('m-d-Y', strtotime($feature->created_on))}}</td>
            
             

            <td> 

                <form action="{{ route('features.destroy',$feature->id) }}" method="POST">
 
   


    

                    <a  class="btn btn-success" href="{{ route('features.edit',$feature->id) }}">Edit</a>

   	

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $features->links() !!}

      

@endsection




























