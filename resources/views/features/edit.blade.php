@extends('layouts.admin')

   

@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Edit feature</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('features.index') }}"> Back</a>

            </div>

        </div>

    </div>

   

    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

  

    <form action="{{ route('features.update',$feature->id) }}" method="POST" enctype="multipart/form-data">

        @csrf

        @method('PUT')

   

         <div class="row">
         

            <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (English):</strong>

                <input type="text" name="name_en" class="form-control"  value="{{ $feature->name_en }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (France):</strong>

                <input type="text" name="name_fr" class="form-control"  value="{{ $feature->name_fr }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Netherlands):</strong>

                <input type="text" name="name_nl" class="form-control"  value="{{ $feature->name_nl }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Denmark):</strong>

                <input type="text" name="name_dk" class="form-control"  value="{{ $feature->name_dk }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Austria):</strong>

                <input type="text" name="name_at" class="form-control"  value="{{ $feature->name_at }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Germany):</strong>

                <input type="text" name="name_de" class="form-control"  value="{{ $feature->name_de }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Sweden):</strong>

                <input type="text" name="name_se" class="form-control"  value="{{ $feature->name_se }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Norway):</strong>

                <input type="text" name="name_no" class="form-control"  value="{{ $feature->name_no }}">

            </div>

        </div> 
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Italian):</strong>

                <input type="text" name="name_it" class="form-control"  value="{{ $feature->name_it }}">

            </div>

        </div>
        
        
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (English):</strong>

                <textarea name="description_en" id="description_en" style="width:100%">{{ $feature->description_en }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (France):</strong>

                <textarea name="description_fr" id="description_fr" style="width:100%">{{ $feature->description_fr }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Netherlands):</strong>

                <textarea name="description_nl" id="description_nl" style="width:100%">{{ $feature->description_nl }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Denmark):</strong>

                <textarea name="description_dk" id="description_dk" style="width:100%">{{ $feature->description_dk }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Austria):</strong>

                <textarea name="description_at" id="description_at" style="width:100%">{{ $feature->description_at }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Germany):</strong>

                <textarea name="description_de" id="description_de" style="width:100%">{{ $feature->description_de }}</textarea>

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Sweden):</strong>

                <textarea name="description_se" id="description_se" style="width:100%">{{ $feature->description_se }}</textarea>

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Norway):</strong>

                <textarea name="description_no" id="description_no" style="width:100%">{{ $feature->description_no }}</textarea>

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Description (Italian):</strong>

                <textarea name="description_it" id="description_it" style="width:100%">{{ $feature->description_it }}</textarea>

            </div>

        </div>
            
   <script type="application/javascript">
 
 	
    var editor1=CKEDITOR.replace( 'description_en' );
	editor1.config.allowedContent = true;
	var editor2=CKEDITOR.replace( 'description_fr' );
	editor2.config.allowedContent = true;
	var editor3=CKEDITOR.replace( 'description_nl' );
	editor3.config.allowedContent = true;
	var editor4=CKEDITOR.replace( 'description_dk' );
	editor4.config.allowedContent = true;
	var editor5=CKEDITOR.replace( 'description_at' );
	editor5.config.allowedContent = true;
	var editor6=CKEDITOR.replace( 'description_de' );
	editor6.config.allowedContent = true;
	var editor7=CKEDITOR.replace( 'description_se' );
	editor7.config.allowedContent = true;
	var editor8=CKEDITOR.replace( 'description_no' ); 
	editor8.config.allowedContent = true;
	var editor9=CKEDITOR.replace( 'description_it' ); 
	editor9.config.allowedContent = true; 
 </script>         
                    
         

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">

              <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

            </div>

        </div>

   

    </form>

@endsection
