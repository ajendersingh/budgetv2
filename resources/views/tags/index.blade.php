@extends('layouts.admin')

 
 
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Tags</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('tags.create') }}"> Create New Tag</a>

            </div>

        </div>

    </div>

   

    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif

   

    <table class="table table-bordered">

        <tr>

            <th width="5%">No</th>

            <th width="15%">Name (English)</th>
            
            <th width="15%">Name (France)</th>
            
            <th width="15%">Name (Netherlands)</th>


            
            <th width="13%">Created Date</th>
            
            <th width="13%">Mofified Date</th>

            <th width="15%">Action</th>

        </tr>

        @foreach ($tags as $tag)

        <tr>

            <td>{{ ++$i }}</td>

            <td>{{ $tag->name_en }}</td>

            <td>{{ $tag->name_fr }}</td>
            
            <td>{{ $tag->name_nt }}</td>
            
            
            
            <td>{{date('m-d-Y', strtotime($tag->created_on))}}</td>
            
            <td>
             @if(isset($tag->modified_on)  && !empty($tag->modified_on))
            	
             @else
                  &nbsp;
             @endif
            </td>

            <td>

                <form action="{{ route('tags.destroy',$tag->id) }}" method="POST">

   


    

                    <a  class="btn btn-success" href="{{ route('tags.edit',$tag->id) }}">Edit</a>

   

                    @csrf

                    @method('DELETE')

      

                    <button type="submit" class="btn btn-danger">Delete</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>

  

 {!! $tags->links() !!}

      

@endsection




























