@extends('layouts.admin')

  

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Tag</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary-edit" href="{{ route('tags.index') }}"> Back</a>

        </div>

    </div>

</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('tags.store') }}" method="POST">

    @csrf

  

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (English):</strong>

                <input type="text" name="name_en" class="form-control" placeholder="Name (English)" value="{{ old('name_en') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (France):</strong>

                <input type="text" name="name_fr" class="form-control" placeholder="Name (France)" value="{{ old('name_fr') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Netherlands):</strong>

                <input type="text" name="name_nt" class="form-control" placeholder="Name (Netherlands)" value="{{ old('name_nt') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Denmark):</strong>

                <input type="text" name="name_dm" class="form-control" placeholder="Name (Denmark)" value="{{ old('name_dm') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Austria):</strong>

                <input type="text" name="name_at" class="form-control" placeholder="Name (Austria)" value="{{ old('name_at') }}">

            </div>

        </div>
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Germany):</strong>

                <input type="text" name="name_ge" class="form-control" placeholder="Name (Germany)" value="{{ old('name_ge') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Sweden):</strong>

                <input type="text" name="name_sw" class="form-control" placeholder="Name (Sweden)" value="{{ old('name_sw') }}">

            </div>

        </div>
        
        
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name (Norway):</strong>

                <input type="text" name="name_no" class="form-control" placeholder="Name (Norway)" value="{{ old('name_no') }}">

            </div>

        </div>
        
        
        
        
         
        
        
        
         
        
         

         
        
         

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary-edit" style="width: 100px; height: 40px;">Submit</button>

        </div>

    </div>

   

</form>

@endsection
