<!DOCTYPE html>
<html>
<head>
    <title>{!!html_entity_decode(__('welcome_email.title'))!!}</title>
</head>

<body>
	<div>
		<div class="logo">
		  <a href="{{ url('') }}"><img src="{{asset('img/logo.png')}}" alt="logo"/></a>
		</div>
		<h2>{!!html_entity_decode(__('welcome_email.welcome_site'))!!} {{$user['name']}}</h2>
		<div class="container">
			<p>{!!html_entity_decode(__('welcome_email.header_text'))!!}</p>
			<p>{!!html_entity_decode(__('welcome_email.login_credentials'))!!}</p>
			<table>
				<tr>
					<td>{!!html_entity_decode(__('welcome_email.registered_email'))!!}</td>
					<td>{{$user['email']}}</td>
				</tr>
				
				<tr>
					<td>{!!html_entity_decode(__('welcome_email.password'))!!}</td>
					<td>{{$user['password']}}</td>
				</tr>
				
			</table>
			<p>{!!html_entity_decode(__('welcome_email.footer_text'))!!}</p>	 
				   
			 
		</div>
		<p>&nbsp; </p>
		<p>{!!html_entity_decode(__('welcome_email.regards_text'))!!}<br />
		{!!html_entity_decode(__('welcome_email.budget_team'))!!} </p>
	</div>
</body>

</html>