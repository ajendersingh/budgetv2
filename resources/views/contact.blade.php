@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h3>{!!html_entity_decode(__('contact_us.page_title'))!!}</h3></div>
                <div class="card-body">
                @if ($success = session('success'))
            	<div class="flash-success">{{ $success }}</div>
            @endif
            
            
            @if (count($errors))
            	<ul class="errors">
            	@foreach ($errors->all() as $error)
            		<li>{{ $error }}</li>
            	@endforeach
            	</ul>    
            @endif
                    <form method="POST" action="{{ action('ContactController@index') }}">
            
            @csrf
            
            <p>
            <label for="name">{!!html_entity_decode(__('contact_us.your_name'))!!}</label>
            <input   type="text" id="name" name="name" class="form-control {{ $errors->has('name') ? 'invalid' : '' }}" value="{{ old('name') }}" />
            </p>
            
            <p>
            <label for="email">{!!html_entity_decode(__('contact_us.your_email'))!!}</label>
            <input  type="email" id="email" name="email" class="form-control {{ $errors->has('email') ? 'invalid' : '' }}" value="{{ old('email') }}" />
            </p>
            
            <p>
            <label for="subject">{!!html_entity_decode(__('contact_us.subject'))!!}</label>
            <input  type="text" id="subject" name="subject"  class=" form-control {{ $errors->has('subject') ? 'invalid' : '' }}"  value="{{ old('subject') }}" />
            </p>
            
            <p>
            <label for="message">{!!html_entity_decode(__('contact_us.message'))!!}</label>
            <textarea  id="message" name="message" class=" form-control {{ $errors->has('message') ? 'invalid' : '' }}" >{{old('message')}}</textarea>
            </p> 
             <p><div class="g-recaptcha" data-sitekey="{{env('RECAPTCHA_SITE_KEY')}}"> </div></p>
            
            <p><button type="submit" class="btn btn-primary">{!!html_entity_decode(__('contact_us.send_btn'))!!}</button></p>     
            
            </form>
                </div>
                 
            </div>
        </div>
    </div>
    
</div>


 
@endsection
